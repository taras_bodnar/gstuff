<div id="tree"></div>
{{--{{dd($ajax_url)}}--}}
<script>
    $(document).ready(function(){

//        $.jstree._themes = "styles/vendor/jstree-theme/";

            $("#tree").jstree({
                "json_data" : {
                    "ajax" : {
                        "url" : "{{$ajax_url}}",
                        "data" : function (n) {
                            return { id : n.data ? n.attr("id") : 0 };
                        },
                        'method' : 'POST'
                    }
                },
                // the `plugins` array allows you to configure the active plugins on this instance
                "plugins" : [ "themes","json_data","ui","crrm","cookies","dnd","search","types","contextmenu" ],
                // each plugin you have included can have its own config object
                "core" : {
                    "animation" : 100,
                    'themes': {
                        'name': 'proton',
                        'responsive': true
                    }
                },
                <?php if($search) :  ?>
                "search" : {
                    "case_insensitive" : true,
                    "ajax" : {
                        "url" : "<?=$ajax_url?>",
                        "data" : function (n) {
                            return { id : n.data ? n.attr("id") : 0 };
                        },
                        'method' : 'POST'
                    }
                },
                <?php endif; ?>
                    <?php if(isset($contextmenu) && !empty($contextmenu)) : ?>
                    "contextmenu" : {
                    "items" : function ($node) {
                        return <?=str_replace(
                                    array('\/', '"function','}"}','\"'),
                                    array('/', 'function','}}','"'),
                                    json_encode($contextmenu)
                                )?>;
                    },
                    hide_on_mouseleave: true
                },
                <?php else :  ?>
                "contextmenu" : {},
                <?php endif; ?>
                    // set a theme
                "themes" : {
                    "theme" : "admin"
                }
            })
                    .on('click', 'a', function() {
                        var treeLink = $(this).attr("href");
                        if (treeLink !== "#")
                            document.location.href = treeLink;

                        return false;
                    })
                    .bind("move_node.jstree", function (e, data) {
                        data.rslt.o.each(function (i) {
                            var id    = $(this).attr("id"),
                                    from  = data.rslt.op.attr("id"),
                                    to    = data.rslt.cr === -1 ? 0 : data.rslt.np.attr("id"),
                                    table = '<?=$table?>';
                            if(from == 'tree') from=0;
                            $.ajax({
                                async : false,
                                type: 'GET',
                                url: "tree/move/" + table + '/' + id + '/' + from + '/' + to,
                                success : function (r) {
                                    if(r > 0){
                                        $("#tree").jstree("refresh");
                                        if($("table").length){
                                            var oTable = $('table').dataTable();
                                            oTable.fnDraw(oTable.fnSettings());
                                        }
                                    }

                                }
                            });
                        });
                    });

    });
</script>