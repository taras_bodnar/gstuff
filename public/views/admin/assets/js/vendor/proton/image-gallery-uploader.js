$(document).ready(function () {
    //!verboseBuild || console.log('-- starting proton.imageGallery build');
    imageGallery.build();

});
imageGallery = {
    build: function () {
        if($('#imageGalleryDropzone').length == 0) return;
        // Initiate imageGallery events
        imageGallery.events();

        Dropzone.options.imageGalleryDropzone = false; // Prevent Dropzone from auto discovering this element
        var content_id = $('#contnet_id').val();
        var dropZoneTemplate = $.get('/admin/Admin/Content/getImageTemplate/'+content_id, function(template) {
            imageGallery.makeDropzone(template); // Make Dropzone after loading template html
        })
            .fail(function() {
                alert( "Image Gallery Error: could not load gallery html template" );
            });
    },
    events: function () {

        $('.gallery-uploader').on('click', '.remove-item', function(event) {
            event.preventDefault();
            $(this).parents('.dz-preview').fadeOut(250, function () {
                $(this).remove();
            });
        });

        $('.gallery-uploader').on('click', '.trash-item, .remove-cancel', function(event) {
            event.preventDefault();
            $(this).parents('.dz-preview').find('.controls').fadeToggle('150');
        });
        $('.gallery-uploader').on('click', '.add', function(event) {
            event.preventDefault();
            $(this).fadeOut(75, function () {
                $(this).parents('.gallery-uploader').toggleClass('active');
                $(this).siblings('.add').fadeIn(150);
            });
        });



        $('#edit-image-modal').on('click', '.btn-success', function(event) {
            $('.editing-item .dz-filename span').text($('#edit-image-modal #image-caption').val());
        });
    },
    makeDropzone: function (template) {
        var imagesContainer = $('#imageGalleryDropzone');
        var url = imagesContainer.data('target');
        var token = "{{ Session::getToken() }}";
        var id = $("#contnet_id").val();
        imagesContainer.dropzone({
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            acceptedFiles: '.jpg,.jpeg,.png,.gif',
            uploadMultiple: true,
            previewsContainer: '.gallery-container',
            previewTemplate: template,
            parallelUploads:1,
            url: url,
            params: {
                        _token: token,
                        id: id
                    },
            thumbnailWidth: 50,
            thumbnailHeight: 50,
            init: function() {
                this.on("addedfile", function(file) {
//                    console.log(file);
                });
//                this.on("success", function(file) {
//                    console.log(file);
//                });
            },
            success: function(file, data) {
                data = $.parseJSON(data);

                var div = $('.gallery-container > div:last');
                div.attr('id', data.id);
                var str = div.html()
                    .toString()
                    .replace (/{imageId}/g, data.id)
                    .replace (/{imageName}/g, data.name);
                div.html(str);
                return true;
            }
        });
    }
}