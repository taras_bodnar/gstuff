<?php



//
$language=''; $bodyId=''; $bodyClass=''; $contentsCss='';

$language =  \App\Http\Controllers\app\Settings::instance()->get('editor_language');
$bodyId =  \App\Http\Controllers\app\Settings::instance()->get('editor_bodyId');
$bodyClass =  \App\Http\Controllers\app\Settings::instance()->get('editor_bodyClass');
$contentsCss = \App\Http\Controllers\app\Settings::instance()->get('editor_contentsCss');

header('Content-type: text/javascript');
echo "CKEDITOR.editorConfig = function( config ) {\n";
echo "
            config.language = '$language';
            config.bodyId = '$bodyId';
            config.bodyClass = '$bodyClass';
            config.contentsCss = '$contentsCss';
            config.baseHref = '".$_SERVER['DOCUMENT_ROOT']."/';
            ";
echo "
            config.height = '550px';
            config.filebrowserImageBrowseUrl = '/public/views/admin/assets/js/vendor/ckeditor/plugins/ckfsys/browser/default/browser.html?Type=Image&Connector=/public/assets/admin/js/vendor/ckeditor/plugins/ckfsys/connectors/php/connector.php';
            config.filebrowserFileBrowseUrl  = '/public/views/admin/assets/js/vendor/ckeditor/plugins/ckfsys/browser/default/browser.html?Type=File&Connector=/public/assets/admin/js/vendor/ckeditor/plugins/ckfsys/connectors/php/connector.php';
            config.filebrowserFlashBrowseUrl = '/public/views/admin/assets/js/vendor/ckeditor/plugins/ckfsys/browser/default/browser.html?Type=Flash&Connector=/public/assets/admin/js/vendor/ckeditor/plugins/ckfsys/connectors/php/connector.php';
            config.filebrowserBrowseUrl      = '/public/views/admin/assets/js/vendor/ckeditor/plugins/ckfsys/browser/default/browser.html?Type=File&Connector=/public/assets/admin/js/vendor/ckeditor/plugins/ckfsys/connectors/php/connector.php';
            config.specialChars = [
                '&quot;', '&','&lt;', '=', '&gt;','`', '~',
                '&euro;', '&lsquo;', '&rsquo;', '&ldquo;', '&rdquo;', '&ndash;', '&mdash;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;',
                '&copy;', '&ordf;', '&laquo;', '&not;', '&reg;', '&macr;', '&deg;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;',
                '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;',
                '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;',
                '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;',
                '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;',
                '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&OElig;', '&oelig;',
                '&#372;', '&#374', '&#373', '&#375;', '&sbquo;', '&#8219;', '&bdquo;', '&hellip;', '&trade;', '&#9658;', '&bull;', '&rarr;', '&rArr;', '&hArr;', '&diams;', '&asymp;',

            '&#913;', '&#914;', '&#915;', '&#916;', '&#917;', '&#918;', '&#919;', '&#920;', '&#921;', '&#922;', '&#923;', '&#924;', '&#925;', '&#926;', '&#927;', '&#928;',
                '&#929;', '&#931;', '&#932;', '&#933;', '&#934;', '&#935;', '&#936;', '&#937;', '&#945;', '&#946;', '&#947;', '&#948;', '&#949;', '&#950;', '&#951;', '&#952;',
                '&#953;', '&#954;', '&#955;', '&#956;', '&#957;', '&#958;', '&#959;', '&#960;', '&#961;', '&#962;', '&#963;', '&#964;', '&#965;', '&#966;', '&#967;', '&#968;', '&#969;'
             ];

            config.protectedSource.push( /<script[\s\S]*?script>/g );
                config.allowedContent = true;

                config.toolbar = [
                { name: 'document', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo'  ] },

                { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
                { name: 'insert', items : [ 'Image','Table','SpecialChar','PageBreak' ] },

                { name: 'tools', items : [  'Templates','-','ShowBlocks' ] },
                { name: 'file', items : [ 'Maximize','-', 'Source'] },
                '/',
                { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','-','Blockquote','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
                { name: 'styles', items : [ Styles','Format','Font','FontSize' ]},
                { name: 'colors', items : [ 'TextColor','BGColor' ] },
            ];
            config.extraPlugins = 'codemirror,insertpre';
        ";
echo "};";
die();