/**
 * Created by taras on 31.10.15.
 */

var Languages = {
  create : function (){
      $.ajax({
        url : '/admin/ajax/Admin/Languages/create',
          method : 'POST',
          success : function(d) {
              modal.dialog({
                  title: d.t,
                  content: d.view,
                  buttons: [
                      {
                          text  : 'Зберегти',
                          class : "btn btn-primary",
                          click : function(){
                              $('#form').submit();
                          }
                      }
                  ]
              });
          }
      });
  },
    edit : function(id){

        $.ajax({
            url : '/admin/ajax/Admin/Languages/edit/'+id,
            method : 'POST',
            success : function(d) {
                modal.dialog({
                    title: d.t,
                    content: d.view,
                    buttons: [
                        {
                            text  : 'Зберегти',
                            class : "btn btn-primary",
                            click : function(){
                                $('#form').submit();
                            }
                        }
                    ]
                });
            }
        });
    },
    pub : function(id,s)
    {
        $.get('/admin/ajax/Admin/Languages/publish/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: 'Інформація',
                    type: 'success',
                    history: false,
                    text: 'Статус змінено'
                });
                var oTable = $('#languages').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    delete : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити мову?',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/ajax/Admin/Languages/destroy/'+id,
                    success : function(d){
                        $(".modal").modal('hide');
                        var oTable = $('#languages').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    },
    autoGenerateAlias: function()
    {
        $.get('/admin/Admin/Languages/generateAlias', function(d){
            $('#notification').html(d);
        });
    },
    translation : {
        delete : function(id) {
            return modal.confirm(
                'Ви дійно хочете видалити перклад?',
                function () {
                    $.ajax({
                        method: 'POST',
                        data:({'id':id}),
                        url:'/admin/ajax/Admin/Translation/destroy/'+id,
                        success : function(d){
                            $(".modal").modal('hide');
                            var oTable = $('#translations').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                        }
                    });
                });
        }
    }
};