/**
 * Created by taras on 29.11.15.
 */
features = {
    content_id: 0,
    content_type: 0,
    init: function()
    {
        var self = this;
        var cf = $('#content_features');
        this.content_id = cf.data('id');
        this.content_type = cf.data('type');
        //console.log('Features Init');

        $(document).on('change', '#data_type', function(){
            var type = $('#data_type').find('option:selected').val();

            var features_id = $('#features_id').val();
            self.getValues(features_id, type);
        });

        $(document).on(
            'change',
            '#categories',
            function(e)
            {
                if(self.content_type != 'product') return;
                var cid = e.val[0];
                if(typeof cid == 'undefined') {
                    cf.html('');
                    return ;
                }
                $.get(
                    'content/getFeatures/' + cid+'/'+self.content_id,
                    function(d)
                    {
                        cf.html(d);
                    }
                );
            }
        );

        this.initPageSorting();

    },
    delete : function(id)
    {
        return modal.confirm(
            'Видалити динамічний параметр',
            function(){
                $.get('/admin/ajax/Admin/Features/delete/'+id,function(d){
                    if(d > 0){
                        var oTable = $('#features').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                        $(".modal").modal('hide');
                    }
                });
            });
    },
    pub : function(id,s)
    {
        $.get('features/pub/'+id+'/'+s,function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#features').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    show : function(id,col,s)
    {
        $.get('features/show/'+id+'/'+col+'/'+s,function(d){
            if(d > 0){
                $.pnotify({
                    title: engine.lang.core.info,
                    type: 'success',
                    history: false,
                    text: engine.lang.core.status_changed
                });
                var oTable = $('#features').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },
    createValue: function(options_id)
    {
        $.get('/admin/ajax/Admin/Features/createValue/'+options_id,function(d){
            $('#typeValuesCnt').prepend(d);
        });
    },
    removeValue: function(id)
    {
        $.get('/admin/ajax/Admin/Features/removeValue/'+id,function(d){
            $('#v-'+id).remove();
        });
    },
    getValues: function(id, type)
    {
        $.get('/admin/ajax/Admin/Features/getValues/'+id+'/'+type, function(d)
        {
            $('#features_values').html(d);
        });
    },
    initPageSorting : function(){
        $("#content_features").sortable({
            placeholder: "sort-drop-here",
            update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').toString();
                var content_id = $('#content_id').val();
                $.ajax({
                    type: "POST",
                    url: 'content/sortFeatures',
                    data: {
                        content_id : content_id,
                        sort : newOrder
                    },
                    success: function(d){
                        $.pnotify({
                            title: engine.lang.core.info,
                            type: 'success',
                            history: false,
                            text: engine.lang.options.sorting_success
                        });
                    },
                    dataType: 'html'
                });

            }
        });
    },
    removeContentFeatures : function(content_id, features_id)
    {
        return engine.modal.confirm(
            'Ви дійсно хочете видалити динамічний параметр',
            function(){
                $.ajax({
                    type: "POST",
                    url: 'content/removeFeatures',
                    data: {
                        content_id : content_id,
                        features_id : features_id
                    },
                    success: function(d){
                        if(d > 0){
                            $('#co-' + features_id).fadeOut(300,function(){$(this).remove();});
                            $(".modal").modal('hide');
                        }
                        $.pnotify({
                            title: engine.lang.core.info,
                            type: 'success',
                            history: false,
                            text: engine.lang.features.remove_success
                        });
                    },
                    dataType: 'html'
                });
            });

    },
    addContentFeaturesValue: function(content_id, features_id)
    {
        $.ajax({
            type: "POST",
            url: 'features/quickAddValue',
            data: {
                content_id  : content_id,
                features_id : features_id
            },
            success: function(d){
                modal.dialog({
                    title: d.t,
                    content: d.c,
                    buttons: [
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#quickAddValueForm').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'json'
        });
    },
    addModal:  function(content_id)
    {
        $('.modal').remove();
        var $this = this, v = [];
        //if($('#categories').length){
        //    $('#categories :selected').each(function(i, selected){
        //        v[i] = $(selected).val();
        //    });
        //}
        $.ajax({
            type: "POST",
            url: 'content/addModalFeatures/',
            data: {
                cid:v,
                content_id: content_id
            },
            success: function(d){
                engine.modal.dialog({
                    title: engine.lang.features.picklist_title,
                    content: d,
                    buttons: [
                        {
                            text  : engine.lang.features.create_features,
                            class : "btn btn-success",
                            click : function(){
                                $this.createModal(content_id);
                                $('.modal').modal('hide');
                            }
                        },
                        {
                            text  : engine.lang.core.save,
                            class : "btn btn-primary",
                            click : function(){
                                $('#pickContentFeatures').submit();
                                $('.modal').modal('hide');
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    createModal: function(content_id)
    {
        $.getJSON('content/createModalFeatures/'+content_id,function(d){
            engine.modal.dialog({
                title: d.t,
                content: d.c,
                buttons: [
                    {
                        text  : engine.lang.core.save,
                        class : "btn btn-primary",
                        click : function(){
                            $('#modalFeaturesForm').submit();
                            $('.modal').modal('hide');
                        }
                    }
                ]
            });
        });
    },
    translateModalFeaturesName: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $(el)
            .parents('form#modalFeaturesForm')
            .find('[id^=info_]')
            .serialize();
        data += '&'+ $('.ms-languages').serialize();

        $.post('./translator/translateModalFeaturesName/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
//                    console.log(e.n, e.v);
                    $('#modalFeaturesForm').find('#' + e.n).val(e.v);//.trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: engine.lang.core.info,
                    type: '',
                    history: false,
                    text: engine.lang.core.translation_complete
                });
            },'json');
    }
};

$(document).ready(
    function()
    {
        features.init();
    }
);