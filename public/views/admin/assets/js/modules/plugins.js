/**
 * Created by taras on 24.12.15.
 */
Plugins = {
    install : function(plugin)
    {
        $.ajax({
            type: "POST",
            url:'/admin/ajax/Admin/Plugins/install',
            data: {
                plugin:plugin
            },
            success: function(d){
                modal.dialog({
                    content: d,
                    title  : 'Інсталювати плагін',
                    buttons: [
                        {
                            text  : 'Інсталювати',
                            class : "btn btn-primary",
                            click : function(){
                                $('#pluginsForm').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    edit : function(id)
    {
        $.ajax({
            type: "GET",
            url:'/admin/Admin/Plugins/edit/'+id,
            success: function(d){
                modal.dialog({
                    content: d,
                    title  : 'Редагування',
                    buttons: [
                        {
                            text  : 'Зберегти',
                            class : "btn btn-primary",
                            click : function(){
                                $('#pluginsForm').submit();
                            }
                        }
                    ]
                });
            },
            dataType: 'html'
        });
    },
    destroy : function(id)
    {
        return modal.confirm(
            'Ви хочете відключити плагін',
            function(){
                $.ajax({
                    type: "POST",
                    url:'/admin/ajax/Admin/Plugins/destroy',
                    data: {
                        'id' : id
                    },
                    success: function(d){
                        if(d > 0){
                            var oTable = $('#plugins').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                            $(".modal").modal('hide');
                        }
                    },
                    dataType: 'html'
                });
            });
    }
};