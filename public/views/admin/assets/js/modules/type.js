/**
 * Created by taras on 05.12.15.
 */
var Type = {
    delete : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити тип?',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/ajax/Admin/Type/destroy/'+id,
                    success : function(d){
                        $(".modal").modal('hide');
                        var oTable = $('#type').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    },
    deleteTemplates : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити шаблон?',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/ajax/Admin/Templates/destroy/'+id,
                    success : function(d){
                        $(".modal").modal('hide');
                        var oTable = $('#templates').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    }
};
