/**
 * Created by taras on 28.11.15.
 */
nav = {
    delete : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити Меню',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/ajax/Admin/Nav/delete/'+id,
                    success : function(d){
                        var oTable = $('#nav_menu').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    }
};