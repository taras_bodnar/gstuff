/**
 * Created by taras on 28.11.15.
 */
content = {
    delete : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити сторінку?',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/ajax/Admin/Content/delete',
                    success : function(d){
                        $(".modal").modal('hide');
                        var oTable = $('#content').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    },
    pub : function(id,s)
    {
        $.get('/admin/ajax/Admin/Content/publish/'+id+'/'+s+'/',function(d){
            if(d > 0){
                $.pnotify({
                    title: 'Інформація',
                    type: 'success',
                    history: false,
                    text: 'Статус змінено'
                });
                var oTable = $('#content').dataTable();
                oTable.fnDraw(oTable.fnSettings());
            }
        });
    },

    images : {
        delete : function(id) {
            $.get('/admin/Admin/Content/deleteImages/'+id,function(d){
                if(d > 0){
                    $('.dz-file-preview#' + id).fadeOut(300, function(){$(this).remove();});
                }
            });
        },
        editInfo : function(id){
            $.get('/admin/Admin/Content/editImagesInfo/'+id,function(d){
                modal.dialog({
                    content: d,
                    buttons: [
                        {
                            text  : 'Зберегти',
                            class : "btn btn-primary",
                            click : function(){
                                $('#imagesInfo').ajaxForm({
                                    success: function (response) {
                                        if(response) {
                                            $(".modal").modal('hide');
                                        } else {
                                            modal.alert("Error");
                                        }
                                    }
                                });
                                $('#imagesInfo').submit();
                            }
                        }
                    ]
                });
            });
        }
    },

    imagesSize : {
        delete : function(id) {
            return modal.confirm(
                'Ви дійно хочете видалити розмір?',
                function () {
                    $.ajax({
                        method: 'POST',
                        url:'/admin/ajax/Admin/Size/destroy/'+id,
                        success : function(d){
                            $(".modal").modal('hide');
                            var oTable = $('#images_size').dataTable();
                            oTable.fnDraw(oTable.fnSettings());
                        }
                    });
                });
        },
        crop : function(id)
        {
            return modal.confirm(
                'Ви бажаєте перенарізати всі зображення для даного розміру?',
                function(){
                    $.get('/admin/request/Size/crop/'+id,function(d){
                        $('#notification').html(d);
                        $(".modal").modal('hide');
                    });
                });
        }
    },

    products : {
        updatePrice : function(id) {
            alert(id);
        }
    },
    file : {
        delete : function(id) {
            $.get('/admin/Admin/Content/deleteFiles/'+id,function(d){
                if(d > 0){
                    $('.dz-file-preview#' + id).fadeOut(300, function(){$(this).remove();});
                }
            });
        },
        edit : function(id) {
            $.ajax({
                url : '/admin/ajax/Admin/Content/fileInfo',
                type : 'POST',
                data : {'id':id},
                success : function(d) {
                    modal.dialog({
                        title: d.t,
                        content: d.view,
                        buttons: [
                            {
                                text  : 'Зберегти',
                                class : "btn btn-primary",
                                click : function(){
                                    $('#FileInfo').submit();
                                }
                            }
                        ]
                    });
                }
            });
        }
    },
    features : {
        show : function(id,col,s){
            $.get('/admin/request/Features/publish/'+id+'/'+col+'/'+s,function(d){
                if(d > 0){
                    $.pnotify({
                        title: 'Статус змінено',
                        type: 'success',
                        history: false,
                        text: 'Статус змінено'
                    });
                    var oTable = $('#features').dataTable();
                    oTable.fnDraw(oTable.fnSettings());
                }
            });
        }
    }
};