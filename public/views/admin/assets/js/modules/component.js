/**
 * Created by taras on 31.10.15.
 */
Component = {
    init : function(){
     // this.delete();
    },
    delete : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити компоненту',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/ajax/Admin/Components/delete',
                    success : function(d){
                        var oTable = $('#components').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    }
};