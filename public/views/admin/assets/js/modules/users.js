/**
 * Created by taras on 05.12.15.
 */
var Users = {
    delete : function(id) {
        return modal.confirm(
            'Ви дійно хочете видалити користувача?',
            function () {
                $.ajax({
                    method: 'POST',
                    data:({'id':id}),
                    url:'/admin/request/Users/destroy/'+id,
                    success : function(d){
                        $(".modal").modal('hide');
                        var oTable = $('#users').dataTable();
                        oTable.fnDraw(oTable.fnSettings());
                    }
                });
            });
    }
};
