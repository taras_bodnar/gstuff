/**
 * Created by taras on 07.10.15.
 */
$(document).ready(function(){
    myroslav.init('#tree');
   //$("#form").ajaxForm({
   //     method: 'POST',
   //    type: 'json',
   //    success : function(d){
   //
   //        var obj = $.parseJSON(d);
   //        var alert_success  = "<div class='alert alert-success'>" +
   //            "<strong>"+obj.m+"</strong>. " +
   //            "</div>";
   //        var alert_danger = "<div class='alert alert-danger'> " +
   //            "<strong>"+obj.m+"</strong>" +
   //            "</div>";
   //       if(obj.s == true) {
   //           $('.form-submit').removeClass('disabled');
   //           $(alert_success).insertAfter('.breadcrumb').stop().slideDown(200);
   //           setTimeout(function(){$('.alert-success').stop().slideUp(200);},4000);
   //           var path = window.location.pathname.substring(1);
   //           path = path.split('/');
   //           var redirect = '/'+path[0]+'/'+path[1]+'/'+path[2];
   //           if(obj.process=="create" || obj.created==1) {
   //               setTimeout(function(){
   //                   location.href = redirect;
   //               },2000);
   //           }
   //       } else {
   //           $(alert_danger).appendTo('.breadcrumb').stop().slideDown(200);
   //           setTimeout(function(){ $('.alert-danger').stop().slideUp(200);},4000);
   //       }
   //    },
   //    beforeSubmit : function() {
   //     $('.form-submit').addClass('disabled');
   //    },
   //    beforeSerialize : function() {
   //        if($('.ckeditor').length){
   //            for ( var instance in CKEDITOR.instances ) {
   //                CKEDITOR.instances[instance].updateElement();
   //            }
   //        }
   //    }
   //});

    //$('.form-submit').click(function(){ $('#form:visible').submit(); });
    $('#form').submit(function(e) {

        var form = $(this);
        e.preventDefault();
        var alert_success,alert_danger;
        //if ( form.parsley('validate') ) {
            form.ajaxSubmit({
                target   : '#responce',
                method: 'POST',
                dataType: 'json',
                success : function(obj){

                    //var obj = $.parseJSON(d);
                    //console.log(obj.m);
                    var alert_success  = "<div class='alert alert-success'>" +
                        "<strong>"+obj.m+"</strong>. " +
                        "</div>";
                    var alert_danger = "<div class='alert alert-danger'> " +
                        "<strong>"+obj.m+"</strong>" +
                        "</div>";
                    if(obj.s == true) {
                        $('.form-submit').removeClass('disabled');
                        $(alert_success).insertAfter('.breadcrumb').stop().slideDown(200);
                        setTimeout(function(){$('.alert-success').stop().slideUp(200);},4000);
                        var path = window.location.pathname.substring(1);
                        path = path.split('/');
                        var redirect = '/'+path[0]+'/'+path[1]+'/'+path[2];
                        if(obj.process=="create" || obj.created==1) {
                            setTimeout(function(){
                                location.href = redirect;
                            },2000);
                        }
                    } else {
                        $(alert_danger).appendTo('.breadcrumb').stop().slideDown(200);
                        setTimeout(function(){ $('.alert-danger').stop().slideUp(200);},4000);
                    }
                },
                beforeSubmit : function() {
                    $('.form-submit').addClass('disabled');
                },
                beforeSerialize : function() {
                    if($('.ckeditor').length){
                        for ( var instance in CKEDITOR.instances ) {
                            CKEDITOR.instances[instance].updateElement();
                        }
                    }
                    if( $('.features_textarea').length) {
                        $('.features_textarea').each(function (i,e) {
                            var element = $(e).attr('id');

                            
                            var summernoteValue = $("#" + element).summernote('code');

                            $("#form input[name='"+$(e).attr('name')+"']").val(summernoteValue);
                        })
                    }
                }
            });
        //} else {
        //    $(alert_danger).appendTo('.breadcrumb').stop().slideDown(200);
        //    setTimeout(function(){ $('.alert-danger').stop().slideUp(200);},4000);
        //}
    });

    //$('input[type="checkbox"]').each(function(i,e){
    //   var value = $(e).val();
    //    if(value==1) {
    //        $(e).attr('checked','checked');
    //    }
    //});
    //
    //$('input[type="checkbox"]').iCheck({
    //    checkboxClass: 'icheckbox_minimal-blue',
    //    radioClass: 'iradio_minimal-blue'
    //});

    //$('input[type="checkbox"]').bootstrapSwitch();
    Component.init();
});

var translator = {
    translateAll: function(id, code, el)
    {

        var btn = $(el);
        btn.button('loading');
        //var form = btn.next().find('form');
        var form = $("#form");
        var data = form.find('[id*=info_]').serialize();
        data += '&'+ $('.s-languages').serialize();
        this.translate(id, code, data, btn, form);

    },
    translateSelected: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $('[id^=info_'+id+'],[id^=info_]:visible').serialize();
        data += '&'+ $('.s-languages').serialize();

        this.translate(id, code, data, btn);
    },
    translateValue: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $(el).parent('div.col-xs-3').next('.col-xs-7').find('[id^=options_value_info_],[id^=new_options_value_info_]').serialize();
        data += '&'+ $('.s-languages').serialize();

        $.post('./translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: 'Інформація',
                    type: '',
                    history: false,
                    text: lang.core.translation_complete
                });
            },'json');
    },
    translateNewValue: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');
        var data = $('#newValue').find('[id^=value_]').serialize();
        data += '&'+ $('.s-languages').serialize();

        $.post('./translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: 'Інформація',
                    type: '',
                    history: false,
                    text: lang.core.translation_complete
                });
            },'json');
    },
    translateNewOptionName: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');
        var data = $('#newValue').find('[id^=value_]').serialize();
        data += '&'+ $('.s-languages').serialize();

        $.post('./translator/translateOptionsValue/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
                    $('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: 'Інформація',
                    type: '',
                    history: false,
                    text: lang.core.translation_complete
                });
            },'json');
    },
    translate: function(id, code, data, btn, form)
    {
        $.ajax({
            type: "POST",
            url: '/admin/ajax/Admin/Translator/translateAll/'+id+'/'+code,
            data: data,
            success: function(d){
                $(d).each(function(i,e){
                    form.find('#' + e.n).val(e.v).trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: 'Автопереклад здійснено',
                    type: '',
                    history: false,
                    text: 'Автопереклад здійснено',
                    before_open: function(pnotify){
                        pnotify.css({
                            "top":"50px",
                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                        });
                    }
                });
            },
            dataType: 'json'
        });
    },
    translateModalOptionsName: function (id, code, el)
    {
        var btn = $(el);
        btn.button('loading');

        var data =  $(el)
            .parents('form#modalOptionsForm')
            .find('[id^=info_]')
            .serialize();
        data += '&'+ $('.ms-languages').serialize();

        $.post('./translator/translateModalOptionsName/'+code+'/', data ,
            function(d){
                $(d).each(function(i,e){
//                    console.log(e.n, e.v);
                    $('#modalOptionsForm').find('#' + e.n).val(e.v);//.trigger('change');
                });
                btn.button('complete');
                $.pnotify({
                    title: 'Інформація',
                    type: '',
                    history: false,
                    text: lang.core.translation_complete
                });
            },'json');
    }
};
var pages = {
    mkAlias: function(target,id,languages_id,languages_code,text)
    {
        $.post('/admin/ajax/Admin/Content/mkAlias/'+id+'/'+languages_id+'/'+languages_code+'/'+text+'/',{alias: text}, function(t){
            target.val(t).trigger('change');
        })
    }

};

var themes = {
    activate: function (theme) {
        return modal.confirm(
           'Активувати',
            function () {
                $.get('/admin/ajax/Admin/Themes/activate/' + theme, function (d) {
                    if (d > 0) {
                        window.location.reload(true);
                    }
                });
            });
    }
};


function myAlert(msg)
{
    var d = $('<div class="m-pop-up-message">\
                <div class="text">\
                    <p>'+msg+'</p>\
                </div>\
            </div>');
    $.fancybox({
        content:d,
        fitToView	: false,
        padding     : 0,
        margin: [0, 0, 0, 0],
        wrapCSS     : "pop-up-style",
        closeBtn    : true,
        openEffect	: 'fadeIn',
        closeEffect	: 'fadOut',
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0,0,0,0.7)',
                    'transition' : 'all 0.35s ease;'
                }
            }
        },
    });
}

var modal = {
    time   : new Date().getTime(),
    tpl :   '<div class="modal fade" id="modal_%t%" tabindex="-1" role="dialog"  aria-hidden="true">' +
    '    <div class="modal-dialog">' +
    '        <div class="modal-content"> ' +
    '           <div class="modal-header"> ' +
    '               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> ' +
    '               <h4 class="modal-title" id="myModalLabel">%title%</h4>  ' +
    '          </div>' +
    '           <div class="modal-body">%body%</div>' +
    '          %footer%' +
    '        </div>' +
    '    </div>' +
    '</div>',
    footer: '<div class="modal-footer"></div>',
    alert: function(msg)
    {
        $('.modal,.modal-backdrop').remove();
        var modal = this.tpl
            .replace('%t%', this.time)
            .replace('%title%', 'Інфо')
            .replace('%body%', msg)
            .replace('%footer%', '');

        $(modal).appendTo('body').modal('show');
    },
    confirm: function(msg, action)
    {
        $('.modal,.modal-backdrop').remove();
        var options  = {
            content : msg,
            buttons: [
                {
                    text  : 'Так',
                    class : "btn btn-danger",
                    click : action
                },
                {
                    text  : 'ні',
                    click : function () {
                        $(".modal").modal('hide');
                    }
                }
            ]
        };

        return this.dialog(options);
    },
    dialog: function(options)
    {
        $('.modal,.modal-backdrop').remove();

        options  = jQuery.extend({
            title : 'Заголовок', // modal title
            effect : 'hide fade',
            autoOpen : true,
            content : '', // modal body
            buttons : {}
        }, options);


        var $modal = $(
            this.tpl
                .replace('%t%',      this.time)
                .replace('%title%',  options.title)
                .replace('%body%',   options.content)
                .replace('%footer%', this.footer)
        );

        if($modal.find('.modal-footer').length == 0){
            $modal.find('.modal-content').append('<div class="modal-footer"></div>')
        }

        jQuery.each( options.buttons, function( name, props ) {

            props = jQuery.isFunction( props ) ? { click: props, html: name } : props;

            var click = props.click;
            delete props.click;

            var button = jQuery( '<button class="btn"></button>' )
                .attr( props, true )
                .text( props.text )
                .unbind( "click" )
                .click(function() {
                    click.apply( button, arguments );
                    return false;
                });

            $modal.find('.modal-footer').append(button);
        });

        $modal.appendTo('body').modal('show');
    }
};


var myroslav = {

    init: function(tree){
        myroslav.select();
        $(tree).on('click', function(e){
            e.target.nodeName == 'DIV' ? $('#page-wrapper').toggleClass('open') : ''
        });
        setTimeout(function(){
            if(!$('#wrapper').find('#tree').length){
                $('#page-wrapper').css('margin', '0 0 0 250px');
                $('.content-wrap').css('width', '100%');
            }
        },300);
    },

    select: function(){
        if($('select').length){
            $('select').select2({});
        }
    }

};
