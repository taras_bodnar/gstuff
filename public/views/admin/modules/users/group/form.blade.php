<div class="alert alert-success" style="display: none;">
    <strong>Well done!</strong> You successfully read this important alert message.
</div>
<div class="alert alert-danger" style="display: none;">
    <strong>Oh snap!</strong> Change a few things up and try submitting again.
</div>

<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    @include('admin.chunk.lang')
                    <div class="tab-content">
                        @foreach($languages as $lang)
                            <input class="s-languages" type="hidden" value="{{$lang->code}}"
                                   name="s_languages[{{$lang->id}}]">
                            <div id="{{$lang->code}}"
                                 class="tab-pane fade in @if($lang->front_default==1) active @endif">
                                <div class="form-group">
                                    <label>Назва</label>
                                    <input id="info_{{$lang->id}}_name" required class="form-control"
                                           name="info[{{$lang->id}}][name]"
                                           value="{{isset($info[$lang->id]['name'])?$info[$lang->id]['name']:''}}">
                                </div>
                                <div class="form-group">
                                    <label>Опис</label>
                            <textarea id="info_{{$lang->id}}_description" required class="form-control" rows="3"
                                      name="info[{{$lang->id}}][description]">
                                {{isset($info[$lang->id]['description'])?$info[$lang->id]['description']:''}}
                            </textarea>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Додаткові параметри</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Наслідує</label>
                        <select required name="data[parent_id]" class="form-control">
                            <option value="0">Батьківська</option>
                            @foreach($parent as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Ранг</label>
                        <input required class="form-control" required type="number"
                               value="{{isset($data->rang)?$data->rang:''}}"
                               name="data[rang]">
                    </div>
                    <div class="form-group">
                        <label>Сортвання</label>
                        <input required class="form-control" required type="number"
                               value="{{isset($data->sort)?$data->sort:''}}"
                               name="data[sort]">
                    </div>
                </div>
            </div>


        </div>
        <input type="hidden" name="data[process]" value="{{$process}}">
    </form>
</div>
<!-- /.row -->


