{{--{{dd($data)}}--}}
<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>П.І.Б.</label>
                        <input id="name" required class="form-control"
                               name="data[name]"
                               value="{{isset($data->name)?$data->name:''}}">
                    </div>
                    <div class="form-group">
                        <label>Телефон</label>
                        <input type="text" class="form-control" required
                               value="{{isset($data->phone)?$data->phone:''}}"
                               id="phone" name="data[phone]">
                    </div>

                    <div class="form-group">
                        <label>E-mail</label>
                        <input class="form-control" required id="email"
                               value="{{isset($data->email)?$data->email:''}}"
                               type="email" name="data[email]">
                    </div>

                    <div class="form-group">
                        <label>Пароль</label>
                        <input class="form-control" @if($process!='edit') require @endif; id="password" type="password" name="data[password]">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Додаткові параметри</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Група</label>
                        <select required name="data[users_group_id]" class="form-control">
                            @foreach($parent as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Мова спілкування</label>
                        <select required name="data[languages_id]" class="form-control">
                            @foreach($languages as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div style="width: 100%;" class="dz-preview dz-file-preview">
                    <div style="width: 100%; display: block; height: auto;" class="dz-details">
                        <img id="avatar_img" style="width: 100%;" src="{{isset($data->avatar)?$data->avatar:'/themes/default/assets/img/no-avatar.jpg'}}" data-dz-thumbnail="">
                        <div style="width: 100%;" class="overlay">
                            <div style="text-align: center; width: 100%;" class="controls clearfix">
                                <a id="avatar_trash" onclick="avatarClickTrash();" class="trash-item"><i class="icon-trash"></i></a>
                            </div>
                            <div id="avatar_trash_confirm" style="display:none; width: 100%;" class="controls confirm-removal clearfix">
                                <a onclick="avatarClickYes();" class="remove-item"><i class="icon-ok"></i></a>
                                <a onclick="avatarClickNo();" class="remove-cancel"><i class="icon-remove"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="position: relative; cursor: pointer;">
                    <a style="width: 100%;" class="btn btn-info"><i class="icon-upload icon-black"></i> Change</a>
                    <input onchange="getPreviewAvatarImage(this);" style="position: absolute; width: 100%; top: 0; height: 100%; opacity: 0; cursor: pointer;" type="file" name="avatar">
                </div>
            </div>
        </div>


        <input type="hidden" name="data[process]" value="{{$process}}">
    </form>
</div>

<script>
    function getPreviewAvatarImage(input) {
        $('#avatar_trash_confirm').css('display', 'none');
        $('#avatar_trash').css('display', 'inline-block');
        var fr = new FileReader();
        fr.onloadend = function () {
            $('#avatar_img').attr('src', fr.result);
        };
        if (input) {
            fr.readAsDataURL(input.files[0]);
        } else {
            $('#avatar_img').attr('src', '/themes/default/assets/img/no-avatar.jpg');
        }
    }
    function avatarClickTrash()
    {
        $('#avatar_trash').css('display', 'none');
        $('#avatar_trash_confirm').css('display', 'block');
    }
    function avatarClickYes()
    {
        $.get('customers/deleteAvatar/{{isset($id)?$id:0}}', function(){
            $('#avatar_img').attr('src', '/themes/default/assets/img/no-avatar.jpg');
            $('#avatar_trash_confirm').css('display', 'none');
            $('#avatar_trash').css('display', 'inline-block');
        });
    }
    function avatarClickNo()
    {
        $('#avatar_trash_confirm').css('display', 'none');
        $('#avatar_trash').css('display', 'inline-block');
    }

</script>