<div id="v-{{$value_id}}" class="row form-group features-value">
    <div class="col-xs-3">
        <button class="btn btn-info" onclick="translateValue(1, 'uk', this); return false;"
                data-complete-text="<i class='icon-sort-by-alphabet icon-white'></i>"
                data-loading-text="<i class='icon-spinner icon-white'></i>"
                title="Перекласти"
                >
            <i class="icon-sort-by-alphabet icon-white"></i>
        </button>
    </div>
    <div class="col-xs-7">
        @foreach($values as $row)
        <div class="form-group">
            <input type="text" name="features_value[{{$value_id}}][{{$row['languages_id']}}]" value="{{$row['value']}}"
                   required="" class="form-control" placeholder="{{$row['placeholder']}}" id="features_value_{{$value_id}}_{{$row['languages_id']}}">
        </div>
        @endforeach
    </div>
    <div class="col-xs-2">
        <a href="javascript:void(0);" onclick="features.removeValue('{{$value_id}}')" class="btn btn-danger">
            <i class="icon-remove"></i>
        </a>
    </div>
</div>
{{--<script>--}}
    {{--function translateValues(id, code, el)--}}
    {{--{--}}
        {{--var btn = $(el);--}}
        {{--btn.button('loading');--}}

        {{--var data =  $(el).parent('div.col-xs-3')--}}
                {{--.next('.col-xs-7')--}}
                {{--.find('[id^=features_value_info_],[id^=new_features_value_info_]')--}}
                {{--.serialize();--}}
        {{--//features_value_info[2]--}}
        {{--data += '&amp;'+ $('.s-languages').serialize();--}}

        {{--$.post('translator/translateOptionsValue/'+code+'/', data ,--}}
                {{--function(d){--}}
                    {{--$(d).each(function(i,e){--}}
                        {{--$('#' + e.n).val(e.v).trigger('change');--}}
                    {{--});--}}
                    {{--btn.button('complete');--}}
                    {{--$.pnotify({--}}
                        {{--title: engine.lang.core.info,--}}
                        {{--type: '',--}}
                        {{--history: false,--}}
                        {{--text: engine.lang.core.translation_complete--}}
                    {{--});--}}
                {{--},'json');--}}
    {{--}--}}
{{--</script>--}}