<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    @foreach($languages as $k=>$lang)

                        @if($lang->front_default == 1)
                            <?php $lang_id = $lang->id;
                            $lang_code = $lang->code;
                            ?>
                        @endif

                    @if($k==0)
                                {!! MyForm::button(
             'Translate',
                            '',
                            array(
                                'class'   =>'btn-translate-all btn-info',
                                'onclick' => 'translator.translateAll('.$lang_id.', \''.$lang_code.'\', this); return false;',

                            )
            ) !!}
                        @endif
                        <input class="s-languages" type="hidden" value="{{$lang->code}}"
                               name="s_languages[{{$lang->id}}]">
                        <div class="form-group">
                            <label>Текст ({{$lang->code}})</label>
                            <input id="info_{{$lang->id}}_name" required class="form-control" name="info[{{$lang->id}}][name]"
                                   value="{{isset($info[$lang->id]['name'])?$info[$lang->id]['name']:''}}">
                        </div>
                    @endforeach

                        <div class="form-group">
                            <label>Тип</label>
                            <select id="data_type" required name="data[type]"  class="form-control">
                                @foreach($type as $item)
                                    <option {{$item['selected']}} value="{{$item['id']}}">{{$item['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id='features_values'>{!!$features_values!!}</div>
                </div>
                <input type="hidden" name="data[process]" value="{{$process}}">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Додаткові</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Розширює сторінку</label>
                    <select name="content_features[]" id="content_features" multiple  class="form-control">
                        @foreach($categories as $item)
                            <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Розширює тип контенту</label>
                    <select  name="content_type_features[]" multiple id="content_type_features" class="form-control">
                        @foreach($content_type as $item)
                            <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        </div>
        <input  type="hidden" id="features_id" value="{{$features_id}}">
    </form>
</div>

<script>
    $(document).ready(function () {
        if($("#content_features,#content_type_features").length) {
            $("#content_features,#content_type_features").select2();
        }
    });
</script>



