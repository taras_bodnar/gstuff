<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 28.11.15
 * Time: 17:47
 */
?>
<div class=" form-group">
    <label>Варіанти</label>
    <div class="row">
        <div class="col-xs-3">
            <a class="btn btn-primary" href="javascript:;" onclick="features.createValue({{$features_id}})">
                <i class="icon-plus"></i>
                Створити
            </a>
        </div>
        <div id="typeValuesCnt" class="col-xs-9 ui-sortable">
            {{$values}}
        </div>
    </div>
</div>
