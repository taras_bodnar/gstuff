<div class="col-lg-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i>{{$title}}</h3>
        </div>
        <div class="panel-body">
            <div class="list-group">
                @foreach($pages as $page)
                    <a href="/admin/Admin/{{$type}}/edit/{{$page->id}}" class="list-group-item">
                        <span class="badge">{{$page->editedon}}</span>
                        <i class="fa fa-fw fa-calendar"></i>{{$page->name}}
                    </a>
                @endforeach
            </div>
            {{--<div class="text-right">--}}
                {{--<a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>--}}
            {{--</div>--}}
        </div>
    </div>
</div>