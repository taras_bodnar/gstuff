<div class="themes row">
     @foreach($themes as $theme)
    <div class="col-md-4 theme {{$theme['current']}}">
        <section class="panel panel-default panel-block">
            <div class="panel-heading text-overflow-hidden">
                <div class="screenshot">
                    <img width="270px" src="{{ $theme['urlpath'] . $theme['screenshot']}}" alt="screenchot of {{$theme['name']}}"/>
                    <div class="description">
                        <div class="author">Автор: {{$theme['author']}}</div>
                        <div class="version">Версія: {{$theme['version']}}</div>
                        <div class="info">{{$theme['description']}}</div>
                    </div>
                </div>
                <h3 class="name">{{$theme['name']}}</h3>
                @if($theme['current'] == '') :
                <div class="actions">
                    <a onclick="themes.activate('{{$theme['theme']}}'); return false;"
                       class="btn btn-success"
                       href="javascript:void(0);"
                            >Активувати</a>
                </div>
                 @endif
            </div>
        </section>
    </div>
    @endforeach
</div>