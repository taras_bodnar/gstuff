<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-12" xmlns="http://www.w3.org/1999/html">
            <div class="box box-primary">
                <div class="box-heading">
                    <h3 class="box-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label>Назва</label>
                        <input id="data_name" required class="form-control" name="data[name]"
                               value="{{isset($data->name)?$data->name:''}}">
                    </div>
                    <div class="form-group">
                        <select sort_url="/admin/ajax/Admin/Nav/sort/{{$nav_id}}/" id="items" name=items[]" multiple>
                            @foreach($items as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="data[process]" value="{{$process}}">
    </form>
</div>
<script>
    $(function () {
        $('#items').pickList({
            sourceListLabel: 'не вибрані',
            targetListLabel: 'Вибрані',
            sortItems: true
        })
    });
</script>