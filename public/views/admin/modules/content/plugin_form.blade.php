{{--{{dd($position)}}--}}
{!! HTML::script('assets/admin/js/modules/plugins.js') !!}
{{--{!! HTML::script('assets/admin/js/core.js') !!}--}}
<form action="/admin/ajax/Admin/Plugins/{{$action}}" method="POST" id="pluginsForm">
    <div class="form-group">
        <select id="structure" name="structure[]" multiple required>
            @foreach($structure as $item)
                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <select id="position" name="data[position]" required>
            @foreach($position as $item)
                <option {{$item['selected']}} value="{{$item['id']}}">{{$item['name']}}</option>
            @endforeach
        </select>
    </div>
    @if(isset($plugin))
        <input type="hidden" value="{{$plugin}}" name="plugin">
    @endif
    <input type="hidden" value="{{$action}}" name="action">
    <input type="hidden" value="{{$id}}" name="id">
</form>

<script>
    $("#pluginsForm").ajaxForm({
        success: function(d) {
            var obj = $.parseJSON(d);
//            console.log();
            if(obj.s==true) {
                var oTable = $('#plugins').dataTable();
                oTable.fnDraw(oTable.fnSettings());
                $(".modal").modal('hide');
            }
        }
    });
</script>