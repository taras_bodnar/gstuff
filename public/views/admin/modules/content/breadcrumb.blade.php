{{--{{dd($items)}}--}}
<ol class="breadcrumb">
    <li>
        <i class="fa fa-dashboard"></i> <a href="/admin/Admin/DashBoard">LarAdmin</a>
    </li>
    @foreach($items as $item)
        <li class="active">
            <a class="bread-page-title"
               @if(!empty($item['children']))
               href="{{$item['controller']}}" data-toggle="dropdown"
                    @endif
                    >{{$item['name']}}</a>
            @if(!empty($item['children']))
                <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
                    @foreach($item['children'] as $c)
                        <li><a href="{{$c->controller}}">{{$c->name}}</a></li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach

    @if(isset($buttons) && is_array($buttons) )
        {!! implode(' ',$buttons) !!}
    @endif
</ol>