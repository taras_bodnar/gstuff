<script>
    $(document).ready(function(){
        resize({{$sizes_id}},{{$total}}, 0);
    });
    function resize(sizes_id, total, start)
    {
        if(start >= total) {
            $("#notification").html('<div class="alert alert-success">Ресайз здійснено</div>');
            return false;
        }

        var percent =  100 / total, done = Math.round( start * percent ) ;
        $("#progress").find('div').css('width', done + '%');
        /*
         setTimeout(function(){
         start++;
         resize(sizes_id, total, start);
         }, 1500);
         */

        $.ajax({
            type: "POST",
            url: '/admin/request/Size/cropProcess',
            data: {
                start: start,
                sizes_id: sizes_id
            },
            success: function(d){
                if(d > 0){
                    start++;
                    resize(sizes_id, total, start);
                }
            },
            dataType: 'html'
        });

        return false;
    }
</script>
<div class="box box-default box-block" id="alias-box">
    <div class="list-group">
        <div class="list-group-item button-demo" >
            <h4 class="section-title">Автоперенарізка</h4>
            <p>Не перезавантажуйте сторінку</p>
            <div id="progress" class="progress progress-thin  progress-striped active">
                <div style="width: 0%;" class="progress-bar progress-bar-success" data-original-title="" title=""></div>
            </div>
        </div>
    </div>
</div>
