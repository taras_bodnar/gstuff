<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6" xmlns="http://www.w3.org/1999/html">
            <div class="box box-primary">
                <div class="box-heading">
                    <h3 class="box-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label>Назва</label>
                        <input type="text" required class="form-control" name="data[name]"
                               value="{{isset($data->name)?$data->name:''}}">
                    </div>
                    <div class="form-group">
                        <label>Ширина</label>
                        <input type="number" required class="form-control" name="data[width]"
                               value="{{isset($data->width)?$data->width:''}}">
                    </div>
                    <div class="form-group">
                        <label>Висота</label>
                        <input type="number" required class="form-control" name="data[height]"
                               value="{{isset($data->height)?$data->height:''}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6" xmlns="http://www.w3.org/1999/html">
            <div class="box box-primary">
                <div class="box-heading">
                    <h3 class="box-title"><i class="fa fa-edit fa-fw"></i>Додаткові</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <select id="items" name=content_type[]" multiple>
                            @foreach($content_type as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="data[process]" value="{{$process}}">
    </form>
</div>
<script>
    $(function () {
        $('#items').pickList({
            sourceListLabel: 'Не вибрані',
            targetListLabel: 'Вибрані',
            sortItems: false
        })
    });
</script>