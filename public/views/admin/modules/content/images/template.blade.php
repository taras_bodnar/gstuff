<div class="dz-preview dz-file-preview"
       {!!(isset($image)?'id="'. $image->id .'"' : 'id={imageId}')!!}
        >
    <div class="dz-details">
        <img data-dz-thumbnail src="<?php if(isset($image)) echo "/".$path . $image->name; else echo base_path() . 'images/blank.png'; ?>"/>
        <div class="overlay">
            <div class="dz-filename text-overflow-hidden"><span data-dz-name></span></div>
            <!-- <div class="dz-size" data-dz-size></div> -->
            <div class="status">
                <a class="dz-error-mark remove-item" href="javascript:;"><i class="fa fa-remove-sign"></i></a>
                <div class="dz-error-message">Помилка <span data-dz-errormessage></span></div>
            </div>
            <div class="controls clearfix">
                <a
                        onclick="modal.alert('<div style=\'text-align:center\'><img style=\'max-width: 480px;max-height:320px;\' src=\'/<?=$path_view .((isset($image)) ? $image->name : '{imageName}')?>\'></div>');"
                        href="javascript:;"
                        ><i class="fa fa-search"></i></a>
                <a onclick="content.images.editInfo(<?=((isset($image)) ?  $image->id  : '{imageId}')?>);"  class="edit-item" href="javascript:;"><i class="fa fa-pencil"></i></a>
                <a class="trash-item" href="javascript:;"><i class="fa fa-trash-o"></i></a>
            </div>
            <div class="controls confirm-removal clearfix">
                <a onclick="content.images.delete(<?=((isset($image)) ?  $image->id  : '{imageId}')?>);" class="remove-item" href="javascript:;">Так</a>
                <a class="remove-cancel" href="javascript:;">Ні</a>
            </div>
        </div>
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
</div>