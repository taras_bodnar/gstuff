{{--{{dd($info)}}--}}
<form data-parsley-namespace="data-parsley-" data-parsley-validate="" action="/admin/ajax/Admin/Content/editImagesInfo/{{$id}}"
      id="imagesInfo" method="post" accept-charset="utf-8">
    <div class="row">
        @foreach($languages as $row)
            <div class="col-lg-12"><input type="hidden" name="s_languages[1]" value="uk" class="s-languages">
                <div class="form-group ">
                    <label for="info_{{$row->id}}_alt">Alt {{$row->name}}
                        <i class="icon-info-sign uses-tooltip" data-toggle="tooltip"
                                               data-placement="top" data-original-title="Alt"
                           title="Alt"></i></label>
                    <input class="form-control" type="text" name="info[{{$row->id}}][alt]"
                           required="required" placeholder="Alt"
                           data-parsley-required="true"
                           value="@if(isset($info[$row->id]['alt'])){{$info[$row->id]['alt']}}@endif"
                           id="info_{{$row->id}}_alt">
                </div>
            </div>
        @endforeach
    </div>
</form>