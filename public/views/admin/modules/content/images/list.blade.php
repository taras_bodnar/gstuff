<section class="box gallery-uploader collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-picture-o fa-fw"></i>Зображення</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-plus"></i></button>
        </div>
    </div>

    <div class="list-group box-body" @if (isset($images)) style="display: block;" @endif>
        <div class="list-group-item dropzone-container">
            <div class="form-group">
                <div class="dropzone" id="imageGalleryDropzone" data-target="/admin/ajax/Admin/Content/uploadImages">
                    <div class="dz-message clearfix">
                        <i class="icon-picture"></i>
                        <span>Зображення</span>
                        <div class="hover">
                            <i class="icon-download"></i>
                            <span>Перетягуйте файли сюди</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-group-item preview-container">
            <div class="form-group">
                <div class="gallery-container">
                    @if (isset($images))
                        {!!implode('', $images) !!}
                    @endif
                </div>
            </div>
        </div>
        <div>
                <div class="form-group">
                    <label>Url</label>
                    <input id="urlImage" class="form-control" type="text" name="url">
                </div>
        </div>
    </div>
    </div>
</section>

<script>
    $(".gallery-container").sortable({
//        handle: ".dz-file-preview",
        update: function(event, ui) {
            var newOrder = $(this).sortable("toArray").toString();
            console.log(newOrder);
            $.get("/admin/request/Content/sort/content_images/id/sort/"+newOrder);
        }
    });

    $("#urlImage").on("change",function(){
        var url = $(this).val();
        var content_id = $("#contnet_id").val();

        $.ajax({
            type: "POST",
            url: "/admin/request/Size/urlUpload",
            data: ({url:url,content_id:content_id}),
            success: function(d){
                $("#urlImage").val("");
                var html = "<div class=\"dz-preview dz-file-preview ui-sortable-handle\" id=\"3746\">"+
                        "<div class=\"dz-details\">"+
                        "<img data-dz-thumbnail=\"\" src=\"/uploads/content/"+content_id+"/thumb/"+d.name+"\">"+
                        "<div class=\"overlay\">"+
                       " <div class=\"dz-filename text-overflow-hidden\"><span data-dz-name=\"\"></span></div>"+
                "<div class=\"status\">"+
                        "<a class=\"dz-error-mark remove-item\" href=\"javascript:;\"><i class=\"fa fa-remove-sign\"></i></a>"+
                "<div class=\"dz-error-message\">Помилка <span data-dz-errormessage=\"\"></span></div>"+
                "</div>"+
                "<div class=\"controls clearfix\">"+
                        "<a onclick=\"modal.alert('<div style=\"text-align:center\">" +
                "<img style=\"max-width: 480px;max-height:320px;\" src=\"/uploads/content/"+content_id+"/source/"+ d.name+"\"></div>');\" href=\"javascript:;\"><i class=\"fa fa-search\"></i></a>"+
                "<a onclick=\"content.images.editInfo("+d.id+");\" class=\"edit-item\" href=\"javascript:;\"><i class=\"fa fa-pencil\"></i></a>"+
                "<a class=\"trash-item\" href=\"javascript:;\"><i class=\"fa fa-trash-o\"></i></a>"+
                "</div>"+
                "<div class=\"controls confirm-removal clearfix\">"+
                        "<a onclick=\"content.images.delete("+d.id+");\" class=\"remove-item\" href=\"javascript:;\">Так</a>"+
                        "<a class=\"remove-cancel\" href=\"javascript:;\">Ні</a>"
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "<div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress=\"\"></span></div>"+
                "</div>";
                $( ".gallery-container" ).append(html);
            },
            dataType: "json"
        });
    });
</script>




