{{--{{dd($items)}}--}}
@foreach ($items as $item)
    <?php
    $remove_link = $use_remove_link ?  "
    <a title='Видалити' href='javascript:void(0);'
    onclick='content.features.removeContentFeatures({$content_id},{$item->id});'>
    <i style='color: red' class='icon-remove-circle'></i>
    </a>
    " : '';

    $btn_add_values = $enable_values ?  "
    <a
            title=''
    href='javascript:void(0);'
    onclick='content.features.addContentFeaturesValue({$content_id},{$item->id});'
    >
    <i style='color: blue' class='icon-plus-sign'></i>
    </a>
    " : '';
    ?>

    <div class="content-features form-group" id="co-{{$item->id}}">
        @if($item->type == 'text' || $item->type=='number')

            <label for="features_{{$item->id}}">
                {{$item->name}}
                {{$remove_link}}
            </label>
            @if($enable_values)
                @foreach($item->values as $v)
                    <input placeholder="{{$v->placeholder}}"
                           {{$item->required}}
                           value="{{$v->value}}"
                           type="text"
                           id="features_{{$item->id}}"
                           name="features[text][{{$item->id}}][{{$v->languages_id}}]"
                           class="form-control form-group"
                            >
                @endforeach
            @endif
        @elseif($item->type == 'colour')
            <label for="features_{{$item->id}}">
                {{$item->name}}
                {{$remove_link}}
            </label>
            @if($enable_values)
                    <input {{$item->required}}
                           value="@if(isset($item->value->value)){{$item->value->value}}@endif"
                           type="color"
                           id="features_{{$item->id}}"
                           name="features[colours][{{$item->id}}]"
                           class="form-control form-group"
                    >
            @endif
        @elseif($item->type == 'radiogroup')
            <label for="features_{{$item->id}}">
                {{$item->name}}
                {{$remove_link}}
                {{$btn_add_values}}
            </label>
            @if($enable_values)
                @if(!empty($item->values))
                        @foreach ($item->values as $v)
                <div class="radio">
                    <label for="features_{{$v->id}}">{{$v->value}}
                        <input type="radio"
                               id="features_{{$v->id}}"
                               {{$v->checked}}
                               name="features[radiogroup][{{$item->id}}]"
                               value="{{$v->id}}"
                                >
                    </label>
                </div>
                @endforeach
                @endif
                @endif


            @elseif($item->type == 'checkboxgroup')
                <label for="features_{{$item->id}}">
                    {{$item->name}}
                    {{$remove_link}}
                    {{$btn_add_values}}
                </label>

                @if($enable_values)
                    @if(!empty($item->values))
                        @foreach ($item->values as $v)
                    <div class="checkbox">
                        <label for="features_{{$item->id}}_{{$v->id}}">{{$v->value}}
                            <input
                                    type="hidden"
                                    name="features[checkboxgroup][{{$item->id}}][{{$v->id}}]"
                                    value="0"
                                    >
                            <input
                                    type="checkbox"
                                    id="features_{{$item->id}}_{{$v->id}}"
                                    {{$v->checked}}
                                    name="features[checkboxgroup][{{$item->id}}][{{$v->id}}]"
                                    value="1"
                                    >
                        </label>
                    </div>
                    @endforeach
                    @endif
                    @endif

                @elseif($item->type == 'select')
                    <label for="features_{{$item->id}}">
                        {{$item->name}}
                        {{$remove_link}}
                        {{$btn_add_values}}
                    </label>
                    @if($enable_values)
                        <br>
                        <select class="features-sel" name="features[select][{{$item->id}}]" id="features_{{$item->id}}"
                                style="width: 100%">
                            @if(!empty($item->values))
                                @foreach ($item->values as $v)
                            <option {{($v->checked == 'checked') ? 'selected' : ''}}  value="{{$v->id}}">{{$v->value}}</option>
                            @endforeach
                            @endif
                        </select>
                     @endif

                @elseif( $item->type == 'textarea' )
                    <label for="features_{{$item->id}}">
                        {{$item->name}}
                        {{$remove_link}}
                    </label>
                    @if($enable_values)
                        @foreach($item->values as $v)
                            <textarea
                                    placeholder="{{$v->placeholder}}"
                                    id="features_{{$item->id}}_{{$v->languages_id}}"
                                    name="features[text][{{$item->id}}][{{$v->languages_id}}]"
                                    {{$item->required}}
                                    class="form-control form-group features_textarea ckeditor"
                                    style="height: 300px"
                                    >{{$v->value}}</textarea>
                        @endforeach
                    @endif
                @elseif( $item->type == 'code' )
                    <label for="features_{{$item->id}}">
                        {{$item->name}}
                        {{$remove_link}}
                    </label>
                    @if($enable_values)
                        @foreach($item->values as $v)
                            <textarea
                                    placeholder="{{$v->placeholder}}"
                                    id="features_{{$item->id}}_{{$v->languages_id}}"
                                    name="features[text][{{$item->id}}][{{$v->languages_id}}]"
                                    {{$item->required}}
                                    class="form-control form-group features_code_textarea"
                                    style="height: 300px"
                                    >{{$v->value}}</textarea>
                        @endforeach
                    @endif

                @elseif($item->type == 'checkbox')
                    <div class="checkbox">
                        <label for="features_{{$item->id}}">{{$item->name}}
                            {{$remove_link}}
                            @if($enable_values)
                                <input type="hidden" name="features[checkbox][{{$item->id}}]" value="0">
                                <input type="checkbox" {{$item->required}} name="features[checkbox][{{$item->id}}]"
                                       value="1" {{$item->checked}}>
                            @endif
                        </label>
                    </div>

                @elseif($item->type == 'sm')
                    <label for="features_{{$item->id}}">
                        {{$item->name}}
                        {{$remove_link}}
                        {{$btn_add_values}}
                    </label>
                    @if($enable_values)
                        <br>
                        <select class="features-sel" multiple name="features[sm][{{$item->id}}][]"
                                id="features_{{$item->id}}" style="width: 100%">
                            @if(!empty($item->values))
                                @foreach ($item->values as $v)
                            <option {{($v->checked == 'checked') ? 'selected' : ''}} value="{{$v->id}}">{{$v->value}}</option>
                            @endforeach
                        </select>
                    @endif

                @endif
    </div>
    @endif
    @endforeach

<script>
$(document).ready(function(){
    try{
        $('.features-sel').select2();
    } catch (r){}
});
//$('.features_code_textarea').each(function (i,e) {
//    var cm = CodeMirror.fromTextArea(document.getElementById($(e).attr('id')), {
//        theme: 'neo',
//        mode: 'xml',
//        htmlMode: true,
//        styleActiveLine: true,
//        lineNumbers: true,
//        lineWrapping: true,
//        autoCloseTags: true,
//        matchBrackets: true
//    });
//
//    cm.on("change", function () {
//        cm.save();
//        var c = cm.getValue();
//        $("textarea"+$(e).attr('id')).html(c);
//    });
//})

//$('.features_textarea').each(function (i,e) {
//    var element = $(e).attr('id');
//    $("#"+element).summernote();
//    $("#form").append("<input type='hidden' name='"+$(e).attr('name')+"'>");
//})

</script>