
<div class="row">
    <form data-parsley-validate="" data-parsley-validate role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Основні</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <div class="nav-tabs-custom ">
{{--                        @include('admin.chunk.lang')--}}
                        {{--{{dd($languages)}}--}}
                        {{$languagesTab}}
                        <div class="tab-content">
                            @foreach($languages as $lang)
                                <input class="s-languages" type="hidden" value="{{$lang->code}}"
                                       name="s_languages[{{$lang->id}}]">
                                <div id="{{$lang->code}}"
                                     class="tab-pane fade in @if($lang->front_default==1) active @endif">
                                    <div class="form-group">
                                        <label>Назва</label>
                                        <input id="info_{{$lang->id}}_name" required class="form-control"
                                               name="info[{{$lang->id}}][name]"
                                               value="{{isset($info[$lang->id]['name'])?$info[$lang->id]['name']:''}}"
                                               onkeyUp="{!!$autofil_title ?'$(\'#info_'.$lang->id.'_title\').val(this.value).trigger(\'change\')' : ''!!}"
                                               onChange="{!!$autofil_title ?'pages.mkAlias($(\'#info_'.$lang->id.'_alias\'),'.$data->parent_id.','. $lang->id .',\''. $lang->code .'\', this.value);' : ''!!}"
                                                >
                                    </div>
                                    <div class="form-group">
                                        <label>Alias</label>
                                        <input id="info_{{$lang->id}}_alias" class="form-control"
                                               name="info[{{$lang->id}}][alias]"
                                               value="{{isset($info[$lang->id]['alias'])?$info[$lang->id]['alias']:''}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input id="info_{{$lang->id}}_title" required class="form-control"
                                               name="info[{{$lang->id}}][title]"
                                               value="{{isset($info[$lang->id]['title'])?$info[$lang->id]['title']:''}}">
                                    </div>
                                    <div class="form-group">
                                        <label>KeyWords</label>
                                        <input id="info_{{$lang->id}}_keywords" class="form-control"
                                               name="info[{{$lang->id}}][keywords]"
                                               value="{{isset($info[$lang->id]['keywords'])?$info[$lang->id]['keywords']:''}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta H1</label>
                                        <input id="info_{{$lang->id}}_meta_h1" class="form-control"
                                               name="info[{{$lang->id}}][meta_h1]"
                                               value="{{isset($info[$lang->id]['meta_h1'])?$info[$lang->id]['meta_h1']:''}}">
                                    </div>
                                    <div class="form-group">
                                        <label>Опис</label>
                                <textarea id="info_{{$lang->id}}_description" class="form-control" rows="3" name="info[{{$lang->id}}][description]">{{isset($info[$lang->id]['description'])?$info[$lang->id]['description']:''}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Опис</label>
                                        <textarea id="info_{{$lang->id}}_meta_description" class="form-control" rows="3" name="info[{{$lang->id}}][meta_description]">{{isset($info[$lang->id]['meta_description'])?$info[$lang->id]['meta_description']:''}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Контент</label>
                                <textarea id="info_{{$lang->id}}_content" required class="form-control ckeditor form-group"
                                          rows="3" name="info[{{$lang->id}}][content]">
                                    @if(isset($info[$lang->id]['content']))
                                        {{$info[$lang->id]['content'] }}
                                    @endif
                                    {{--{{isset($info[$lang->id]['content'])?$info[$lang->id]['content']:''}}--}}
                                </textarea>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if(isset($plugins['left']))
                        {{implode('', $plugins['left'])}}
                    @endif
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <div class="col-lg-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Додаткові параметри</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <div class="form-group">
                        <label>Публікація</label>
                        {{--<input type="checkbox" name="data[published]" value="{{isset($data->published)?$data->published:1}}">--}}
                        {!! \App\Http\Controllers\Admin\Form::buttonSwitch('data[published]',isset($data->published) ? $data->published : 1) !!}
                    </div>
                    <div class="form-group">

                        <label>Шаблон</label>
                        <select required name="data[templates_id]" class="form-control">
                            @foreach($templates as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Sort</label>
                        <input class="form-control"
                               name="data[sort]"
                               value="{{isset($data->sort)?$data->sort:0}}">
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
           
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-wrench fa-fw"></i>Динамічні параметри</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <div id='content_features' data-type='{{$type}}' data-id='{{$id}}'>{!!$features!!}</div>
                    </div>
                </div>
            </div>
            @if (in_array('nav_menu', $form_fields))
                <?php $input = '';
                if (empty($nav_menu)) {
                    $input .= '<div class="alert alert-dismissable alert-warning fade in">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">
                                          <i class="icon-remove"></i>
                                        </button>
                                        <span class="title"><i class="icon-warning-sign"></i>Попередження</span>
                                        Не має жодного меню
                                    </div>';
                } else {

                    foreach ($nav_menu as $item) {
                        $input .= "<div class='checkbox'>
                        <label for='nav_menu_{$item->id}'>
                            {$item->name}
                            " . MyForm::checkbox('nav_menu[' . $item->id . ']', in_array($item->id, $selected_nav_menu)) . "
                        </label>
                   </div>";
                    }
                }
                ?>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-wrench fa-fw"></i>Меню</h3>
                    </div>
                    <div class="box-body">
                        {{$input}}
                    </div>
                </div>

            @endif
            @if (in_array('PostCat', $form_fields))
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-wrench fa-fw"></i>Категорії</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                <i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <label>Категорії</label>
                        <select name="categories[]" id="categories" required multiple class="form-control">
                            @foreach($categories as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif

            @if (in_array('all_pages', $form_fields))
                <div class="box">
                    <div class="box-body">
                        <label>Сторінки</label>
                        <select name="all_pages[]" id="all_pages" multiple class="form-control">
                            @foreach($all_pages as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif

            @if (in_array('product_options', $form_fields))
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-wrench fa-fw"></i>Властивості товару</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label>Ціна від</label>
                            <input required class="form-control" name="product_options[price]"
                                   value="{{isset($product_options->price) ? $product_options->price : 0}}">
                        </div>
                        <div class="form-group">
                            <label>Ціна до</label>
                            <input required class="form-control" name="product_options[old_price]"
                                   value="{{isset($product_options->old_price) ? $product_options->old_price : 0}}">
                        </div>

                        <div class="form-group">
                            <label>Код</label>
                            <input type="number"  class="form-control" name="product_options[code]"
                                   value="{{isset($product_options->code) ? $product_options->code : ''}}">
                        </div>

                        <div class="form-group">
                            <label>Кількість</label>
                            <input type="number"  class="form-control" name="product_options[quantity]"
                                   value="{{isset($product_options->quantity) ? $product_options->quantity : ''}}">
                        </div>

                        <div class="form-group">
                            {{MyForm::buttonSwitch(
                              'Показувати ціну',
                              'product_options[show_price]',
                              isset($product_options->show_price)?$product_options->show_price:1
                              )
                              }}
                        </div>

                        <div class="form-group">
                            {{MyForm::buttonSwitch(
                              'Доступність',
                              'product_options[availability]',
                              isset($product_options->availability)?$product_options->availability:1
                              )
                              }}
                        </div>
                        <div class="form-group">
                            {{MyForm::buttonSwitch(
                              'Розпродаж',
                              'product_options[sale]',
                              isset($product_options->sale)?$product_options->sale:1
                              )
                              }}
                        </div>

                        <div class="form-group">
                            {{MyForm::buttonSwitch(
                              'Хіт продаж',
                              'product_options[hit]',
                              isset($product_options->hit)?$product_options->hit:1
                              )
                              }}
                        </div>

                        <div class="form-group">
                            {{MyForm::buttonSwitch(
                              'Новинка',
                              'product_options[new]',
                              isset($product_options->new)?$product_options->new:1
                              )
                              }}
                        </div>

                    </div>

                </div>
            @endif

            {{$images}}

            @if(isset($plugins['right']))
                {!! implode('',$plugins['right'])!!}
            @endif


        </div>
        <input type="hidden" name="data[created]" value="{{$created}}">
        <input type="hidden" name="data[process]" value="{{$process}}">
        <input type="hidden" name="data[parent_id]" value="{{$parent_id}}">
        <input type="hidden" name="content_id" value="{{$id}}" id="contnet_id">
    </form>
</div>

{!! HTML::script('views/admin/assets/js/vendor/ckeditor/ckeditor.js') !!}
<script>
    $(document).ready(function () {
        if ($("#categories").length) {
            $("#categories").select2();
        }
        if($('.ckeditor').length){
            for ( var instance in CKEDITOR.instances ) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    });
</script>