<div class="row">
    <form id="form" method="POST" action="/admin/request/Settings/process">
        <div class="col-lg-6">
            <div class="box">
                <div class="box-header with-border">
                    <h4>Глобальні header&footer</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label>header</label>
                        <textarea name="data[et_header]" class="form-control"
                                  rows="3">{{isset($data['et_header'])?$data['et_header']:""}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>footer</label>
                        <textarea name="data[et_footer]" class="form-control"
                                  rows="3">{{isset($data['et_footer'])?$data['et_footer']:""}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-4 -->
        <div class="col-lg-6">
            <div class="box">
                <div class="box-header with-border">
                    <h4>Налаштування автогенерації</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <div class="form-group">
                        <label for="">Автозаповнення title</label>
                        {{MyForm::buttonSwitch(
                                   'data[autofil_title]',
                                   isset($data["autofil_title"])?$data["autofil_title"]:1
                                   )
                                   }}
                    </div>
                    <div class="form-group">
                        <label for="">Автозаповнення alias</label>
                        {{MyForm::buttonSwitch(
                                   'data[autotranslit_alias]',
                                   isset($data["autotranslit_alias"])?$data["autotranslit_alias"]:1
                                   )
                                   }}
                    </div>
                    <div class="form-group">
                        <label>Показувати фільтр по цінах</label>
                        <input class="form-control" type="number" name="data[price_filter]"
                               value="{{$data['price_filter']}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="box">
                <div class="box-header with-border">
                    <h4>Watermark</h4>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <img style="background-color:#000;" src="/uploads/watermark/watermark.png" alt="">
                        <input type="file" name="watermark">
                    </div>
                    <div class="form-group">
                        <label for="">Позиція</label>:

                        <select name="data[water_position]" id="">
                            <option @if($data['water_position']=="top-left") selected @endif value="top-left">top-left</option>
                            <option  @if($data['water_position']=="top") selected @endif value="top">top</option>
                            <option  @if($data['water_position']=="top-right") selected @endif value="top-right">top-right</option>
                            <option  @if($data['water_position']=="left") selected @endif value="left">left</option>
                            <option  @if($data['water_position']=="right") selected @endif value="right">right</option>
                            <option  @if($data['water_position']=="bottom") selected @endif value="bottom">bottom</option>
                            <option  @if($data['water_position']=="bottom-right") selected @endif value="bottom-right">bottom-right</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-lg-4 -->
    </form>

</div>