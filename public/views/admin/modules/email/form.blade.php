{!! HTML::style('views/admin/assets/js/vendor/codemirror/lib/codemirror.css') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/lib/codemirror.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/css.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/codemirror/mode/php.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/htmlmixed.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/sql.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/javascript.js') !!}


<div class="alert alert-success" style="display: none;">
    <strong>Well done!</strong> You successfully read this important alert message.
</div>
<div class="alert alert-danger" style="display: none;">
    <strong>Oh snap!</strong> Change a few things up and try submitting again.
</div>

<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    @include('admin.chunk.lang')
                    <div class="tab-content">

                        @foreach($languages as $lang)
                            <input class="s-languages" type="hidden" value="{{$lang->code}}"
                                   name="s_languages[{{$lang->id}}]">
                            <div id="{{$lang->code}}"
                                 class="tab-pane fade in @if($lang->front_default==1) active @endif">
                                <div class="form-group">
                                    <label>Від кого/Кому</label>
                                    <input id="info_{{$lang->id}}_reply_to" required class="form-control"
                                           name="info[{{$lang->id}}][reply_to]"
                                           value="{{isset($info[$lang->id]['reply_to'])?$info[$lang->id]['reply_to']:''}}">
                                </div>
                                <div class="form-group">
                                    <label>Тема повідомлення</label>
                                    <input id="info_{{$lang->id}}_subject" required class="form-control"
                                           name="info[{{$lang->id}}][subject]"
                                           value="{{isset($info[$lang->id]['subject'])?$info[$lang->id]['subject']:''}}">
                                </div>
                                <div class="form-group">
                                    <label>Текст повідомлення</label>
                            <textarea id="info_{{$lang->id}}_body" required class="form-control" rows="3"
                                      name="info[{{$lang->id}}][body]">{{isset($info[$lang->id]['body'])?$info[$lang->id]['body']:''}}</textarea>
                                </div>
                            </div>
                            <script>
                                var cm{{$lang->id}}  = CodeMirror.fromTextArea(document.getElementById('info_{{$lang->id}}_body'), {
                                    theme: 'neo',
                                    mode: 'html',
                                    styleActiveLine: true,
                                    lineNumbers: true,
                                    lineWrapping: true,
                                    autoCloseTags: true,
                                    matchBrackets: true
                                });

                                cm{{$lang->id}}.on("change", function () {
                                    cm{{$lang->id}}.save();
                                    var c{{$lang->id}}  = cm{{$lang->id}}.getValue();
                                    $('textarea#info_'+{{$lang->id}}+
                                    '_body'
                                    ).
                                    html(c{{$lang->id}});
                                });
                            </script>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{--{{dd($controllers)}}--}}
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Додаткові параметри</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label>Назва</label>
                        <input required class="form-control" required type="text"
                               value="{{isset($data->name)?$data->name:''}}"
                               name="data[name]">
                    </div>
                    <div class="form-group">
                        <label>Код</label>
                        <input required class="form-control" required type="text"
                               value="{{isset($data->code)?$data->code:''}}"
                               name="data[code]">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input required class="form-control" required type="email"
                               value="{{isset($data->email)?$data->email:''}}"
                               name="data[email]">
                    </div>
                </div>
                <input type="hidden" name="data[process]" value="{{$process}}">
            </div>
        </div>
    </form>
</div>



