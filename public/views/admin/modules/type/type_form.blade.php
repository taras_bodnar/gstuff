<div class="alert alert-success" style="display: none;">
    <strong>Well done!</strong> You successfully read this important alert message.
</div>
<div class="alert alert-danger" style="display: none;">
    <strong>Oh snap!</strong> Change a few things up and try submitting again.
</div>
<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-file fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Тип(Тільки латина)</label>
                        <input id="type" required class="form-control"
                               name="data[type]"
                               value="{{isset($data->type)?$data->type:''}}">
                    </div>
                    <div class="form-group">
                        <label>Назва</label>
                        <input type="text" class="form-control" required
                               value="{{isset($data->name)?$data->name:''}}"
                               id="name" name="data[name]">
                    </div>
                </div>
            </div>
        </div>


        <input type="hidden" name="data[process]" value="{{$process}}">
    </form>
</div>