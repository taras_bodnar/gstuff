<div class="alert alert-success" style="display: none;">
    <strong>Well done!</strong> You successfully read this important alert message.
</div>
<div class="alert alert-danger" style="display: none;">
    <strong>Oh snap!</strong> Change a few things up and try submitting again.
</div>
<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Назва</label>
                        <input id="type" required class="form-control"
                               name="data[name]"
                               value="{{isset($data->name)?$data->name:''}}">
                    </div>
                    <div class="form-group">
                        <label>Опис</label>
                <textarea class="form-control" required id="description" name="data[description]">
                {{isset($data->description)?$data->description:''}}
                    </textarea>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Додаткові параметри</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Контролер</label>
                        <select required name="data[type_id]" class="form-control">
                            @foreach($type as $item)
                                <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{MyForm::buttonSwitch(
                                            'Основний',
                                            'data[main]',
                                            isset($data->main)?$data->main:1
                                            )
                                            }}
                    </div>
                    <div class="form-group">
                        <label>Шлях</label>
                        <input type="text" id="path" required class="form-control"
                               name="data[path]"
                               value="{{isset($data->path)?$data->path:''}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Шаблон</h3>
                </div>
               <textarea class="form-control" required id="template" name="template">
                {{isset($template)?$template:''}}
                    </textarea>
            </div>
        </div>

{!! HTML::style('views/admin/assets/js/vendor/codemirror/lib/codemirror.css') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/lib/codemirror.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/css.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/codemirror/mode/php.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/htmlmixed.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/sql.js') !!}
{!! HTML::script('views/admin/assets/js/vendor/codemirror/mode/javascript.js') !!}
<script>
    var cm = CodeMirror.fromTextArea(document.getElementById('template'), {
        theme: 'neo',
//                        mode: 'htmlmixed',
        styleActiveLine: true,
        lineNumbers: true,
        lineWrapping: true,
        autoCloseTags: true,
        matchBrackets: true
    });

    cm.on("change", function () {
        cm.save();
        var c = cm.getValue();
        $("textarea#template").html(c);
    });
</script>


<input type="hidden" name="data[process]" value="{{$process}}">
</form>
</div>