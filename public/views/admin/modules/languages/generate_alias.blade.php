<script>
    $(window).load(function(){
        generateAlias({{$lng}},{{$total}},0);
    });
</script>
<div class="panel panel-default panel-block" id="alias-box">
    <div class="list-group">
        <div class="list-group-item button-demo" >
            <h4 class="section-title">Заголовок</h4>
            <p>Опис</p>
            <div id="progress-{{$lng}}" class="progress progress-thin  progress-striped active">
                <div style="width: 0%;" class="progress-bar progress-bar-success" data-original-title="" title=""></div>
            </div>
        </div>
    </div>
</div>
<script>
    function generateAlias(languages_id, total, start)
    {
        if(start >= total) {
            modal.alert(
                    'OK',
                    'OK',
                    'success',
                    '#notification',
                    'html'
            );
            return false;
        }

        var percent =  100 / total, done = Math.round( start * percent ) ;
        $("#progress-"+languages_id).find('div').css('width', done + '%');

        $.ajax({
            type: "POST",
            url: '/admin/ajax/Admin/Languages/processGenerateAlias',
            data: {
                start: start,
                total: total,
                languages_id: languages_id
            },
            success: function(d){
                generateAlias(languages_id, d.total, d.start)
            },
            dataType: 'json'
        });

        return false;
    }
</script>