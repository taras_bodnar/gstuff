<form action="{{$action}}" id="form">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Назва</label>
                    <input class="form-control" type="text" required name="data[name]"
                           value="{{isset($data->name)?$data->name:''}}">
                </div>
                <div class="form-group">
                    <label>Код</label>
                    <input class="form-control" type="text" required name="data[code]"
                           value="{{isset($data->code)?$data->code:''}}">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-wrench fa-fw"></i>Додаткові параметри</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="">Доступна для сайту</label>
                    {!!MyForm::buttonSwitch(
                          'data[front]',
                          isset($data->front)?$data->front:1
                          )
                          !!}
                </div>
                <div class="form-group">
                    <label for="">Доступна для адмінки</label>
                    {!!MyForm::buttonSwitch(
                          'data[back]',
                          isset($data->back)?$data->back:1
                          )
                          !!}
                </div>
                <div class="form-group">
                    <label for="">Мова по замовчуванню для сайту</label>
                    {!!MyForm::buttonSwitch(
                          'data[front_default]',
                          isset($data->front_default)?$data->front_default:1
                          )
                          !!}
                </div>
                <div class="form-group">
                    <label for="">Мова по замовчуванню для адмінки</label>
                    {!!MyForm::buttonSwitch(
                          'data[back_default]',
                          isset($data->back_default)?$data->back_default:1
                          )
                          !!}
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{$process}}" name="data[process]">
</form>