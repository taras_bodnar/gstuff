<div class="row">
    <form role="form" method="POST" id="form" action="{{$action}}">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Код</label>
                        <input required class="form-control" required type="text"
                               value="{{isset($data->code)?$data->code:''}}"
                               name="data[code]">
                    </div>
                    {!! MyForm::button(
     'Translate',
                    '',
                    array(
                        'class'   =>'btn-translate-all btn-info',
                        'onclick' => 'translator.translateAll(1, \'uk\', this); return false;',

                    )
    ) !!}
                    @foreach($languages as $lang)
                        <input class="s-languages" type="hidden" value="{{$lang->code}}"
                               name="s_languages[{{$lang->id}}]">
                        <div class="form-group">
                            <label>Текст ({{$lang->code}})</label>
                            <textarea id="info_{{$lang->id}}_value" required class="form-control" name="info[{{$lang->id}}][value]">{{isset($info[$lang->id]['value'])?$info[$lang->id]['value']:''}}</textarea>
                        </div>
                    @endforeach
                </div>
                <input type="hidden" name="data[process]" value="{{$process}}">
            </div>
        </div>
    </form>
</div>



