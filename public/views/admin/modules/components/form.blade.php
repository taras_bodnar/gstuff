<div class="row">
                <form role="form" method="POST" id="form" action="{{$action}}">
                    <div class="col-lg-6">
                     <div class="box box-primary">
                        <div class="box-heading">
                            <h3 class="box-title"><i class="fa fa-edit fa-fw"></i>Основні</h3>
                        </div>
                        <div class="box-body">
                            @include('admin.chunk.lang')
                    <div class="tab-content">
                        @foreach($languages as $lang)
<input class="s-languages" type="hidden" value="{{$lang->code}}" name="s_languages[{{$lang->id}}]">
                            <div id="{{$lang->code}}" class="tab-pane fade in @if($lang->front_default==1) active @endif">
                                <div class="form-group">
                                    <label>Назва</label>
                                    <input id="info_{{$lang->id}}_name" required class="form-control" name="info[{{$lang->id}}][name]"
                                           value="{{isset($info[$lang->id]['name'])?$info[$lang->id]['name']:''}}">
                                </div>
                                <div class="form-group">
                                    <label>Опис</label>
                            <textarea id="info_{{$lang->id}}_description" required class="form-control" rows="3" name="info[{{$lang->id}}][description]">
                                {{isset($info[$lang->id]['description'])?$info[$lang->id]['description']:''}}
                            </textarea>
                                </div>
                            </div>
                        @endforeach
                    </div>
                            </div>
                         </div>
                    </div>
                    {{--{{dd($controllers)}}--}}
                    <div class="col-lg-6">
                        <div class="box box-primary">
                            <div class="box-heading">
                                <h3 class="box-title"><i class="fa fa-wrench fa-fw"></i>Додаткові параметри</h3>
                            </div>
                            <div class="box-body">
                        <div class="form-group">
                            <label>Контролер</label>
                            <select required name="data[controller]"  class="form-control">
                                @foreach($controllers as $item)
                                    <option value="{{$item['value']}}">{{$item['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Наслідує</label>
                            <select required name="data[parent_id]"  class="form-control">
                                  <option value="0">Батьківська</option>
                                @foreach($parent as $item)
                                    <option {{$item->selected}} value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>icon</label>
                            <input required class="form-control" required type="text"
                                   value="{{isset($data->icon)?$data->icon:''}}"
                                   name="data[icon]">
                        </div>
                        <div class="form-group">
                            <label>Ранг</label>
                            <input required class="form-control" required type="number"
                                   value="{{isset($data->rang)?$data->rang:''}}"
                                   name="data[rang]">
                        </div>
                        <div class="form-group">
                            <label>Сортування</label>
                            <input required class="form-control" required type="number"
                                   value="{{isset($data->sort)?$data->sort:''}}"
                                   name="data[sort]">
                        </div>
                        <div class="form-group">
                            {{MyForm::buttonSwitch(
                                            'Публікація',
                                            'data[published]',
                                            isset($data->published)?$data->published:1
                                            )
                                            }}
                            </div>

                    </div>
                    <input type="hidden" name="data[process]" value="{{$process}}">
                            </div>
                        </div>
                </form>
            </div>
            <!-- /.row -->


