<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    <meta name="description" content="">
    <meta name="author" content="">
    <title>LarAdmin</title>
    <!-- Tell the browser to be responsive to screen width -->
    {{--<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">--}}
    {!!HTML::style('views/admin/assets/bootstrap/css/bootstrap.min.css')!!}
    {!!HTML::style('views/admin/assets/bootstrap/css/bootstrap-switch.css')!!}
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    {!!HTML::style('views/admin/assets/css/AdminLTE.min.css')!!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {!!HTML::style('views/admin/assets/css/skins/_all-skins.min.css')!!}
    <!-- iCheck -->
    {!!HTML::style('views/admin/assets/plugins/iCheck/flat/blue.css')!!}
    <!-- Morris chart -->
    {!!HTML::style('views/admin/assets/plugins/morris/morris.css')!!}
    <!-- jvectormap -->
    {!!HTML::style('views/admin/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')!!}
    <!-- Date Picker -->
    {!!HTML::style('views/admin/assets/plugins/datepicker/datepicker3.css')!!}
    <!-- Daterange picker -->
    {!!HTML::style('views/admin/assets/plugins/daterangepicker/daterangepicker-bs3.css')!!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!!HTML::style('views/admin/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}

    {!! HTML::style('views/admin/assets/css/vendor/jquery.pnotify.default.css') !!}
    {!! HTML::style('views/admin/assets/css/vendor/jquery.fancybox.css') !!}
    {!! HTML::style('views/admin/assets/css/vendor/dropzone/basic.css') !!}
    {!! HTML::style('views/admin/assets/css/vendor/dropzone/dropzone.css') !!}
    {!! HTML::style('views/admin/assets/plugins/select2/select2.min.css') !!}
    {!! HTML::style('views/admin/assets/plugins/iCheck/all.css') !!}
    {!! HTML::style('views/admin/assets/js/vendor/picklist/jquery-picklist.css') !!}
    {!! HTML::style('views/admin/assets/css/AdminLTE.css') !!}

    {!! HTML::style('views/admin/assets/plugins/summernote/summernote.css') !!}

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
    {!! HTML::style('views/admin/assets/css/style.css') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {!! HTML::script('views/admin/assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
            <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    {!! HTML::script('views/admin/assets/plugins/morris/morris.min.js') !!}

    {!! HTML::script('views/admin/assets/js/vendor/proton/modernizr.js') !!}
    {!! HTML::script('views/admin/assets/js/vendor/dropzone/dropzone.min.js') !!}
    {!! HTML::script('views/admin/assets/js/vendor/proton/proton.js') !!}
    {!! HTML::script('views/admin/assets/js/vendor/proton/image-gallery-uploader.js') !!}
    {!! HTML::script('views/admin/assets/js/vendor/proton/sidebar.js') !!}
            <!-- Bootstrap 3.3.5 -->
    {!! HTML::script('views/admin/assets/bootstrap/js/bootstrap.js') !!}
    {!! HTML::script('views/admin/assets/bootstrap/js/bootstrap-switch.js') !!}

    {!! HTML::script('views/admin/assets/plugins/summernote/summernote.js') !!}
    {{--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>--}}

</head>