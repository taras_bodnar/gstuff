<section class="panel panel-primary panel-block file-uploader">
    <div class="panel-heading text-overflow-hidden">
        <h3 class="panel-title"><i class="fa fa-picture-o fa-fw"></i>Файли
            <a href="javascript:;" class="add insert">
                <i class="icon-plus-sign"></i>
            <span>
                Завантажити файли
            </span>
            </a>
            <a href="javascript:;" class="add finished">
                <i class="icon-check-sign"></i>
            <span>
                Завершити завантаження
            </span>
            </a>
        </h3>
    </div>
    <div class="list-group">
        <div class="list-group-item dropzone file-container">
            <div class="form-group">
                <div class="dropzone" id="FileDropzone" data-target="/admin/ajax/Admin/Content/uploadFiles">
                    <div class="dz-message clearfix">
                        <i class="icon-picture"></i>
                        <span>Файли</span>
                        <div class="hover">
                            <i class="icon-download"></i>
                            <span>Перетягуйте файли сюди</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-group-item preview-container">
            <div class="form-group">
                <div class="file-container">
                    @if (isset($files))

                        {!!implode('', $files) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        FileDropzone.build();
    });

    $(".file-container").sortable({
//        handle: ".dz-file-preview",
        update: function(event, ui) {
            var newOrder = $(this).sortable("toArray").toString();
            console.log(newOrder);
            $.get("/admin/request/Content/sort/content_images/id/sort/"+newOrder);
        }
    });

    FileDropzone = {
        build: function () {
            if($('#FileDropzone').length == 0) return;
            // Initiate imageGallery events
            FileDropzone.events();

            Dropzone.options.FileDropzone = false; // Prevent Dropzone from auto discovering this element
            var content_id = $('#contnet_id').val();
            var dropZoneTemplate = $.get('/admin/Admin/Content/getFileTemplate/'+content_id, function(template) {
                FileDropzone.makeDropzone(template); // Make Dropzone after loading template html
            })
                    .fail(function() {
                        alert( "Image Gallery Error: could not load gallery html template" );
                    });
        },
        events: function () {

            $('.file-uploader').on('click', '.remove-item', function(event) {
                event.preventDefault();
                $(this).parents('.dz-preview').fadeOut(250, function () {
                    $(this).remove();
                });
            });

            $('.file-uploader').on('click', '.trash-item, .remove-cancel', function(event) {
                event.preventDefault();
                $(this).parents('.dz-preview').find('.controls').fadeToggle('150');
            });
            $('.file-uploader').on('click', '.add', function(event) {
                event.preventDefault();
                $(this).fadeOut(75, function () {
                    $(this).parents('.file-uploader').toggleClass('active');
                    $(this).siblings('.add').fadeIn(150);
                });
            });



            $('#edit-image-modal').on('click', '.btn-success', function(event) {
                $('.editing-item .dz-filename span').text($('#edit-image-modal #image-caption').val());
            });
        },
        makeDropzone: function (template) {
            var imagesContainer = $('#FileDropzone');
            var url = imagesContainer.data('target');
            var token = "{{ Session::getToken() }}";
            var id = $("#contnet_id").val();
            imagesContainer.dropzone({
                paramName: "upload", // The name that will be used to transfer the file
                maxFilesize: 10, // MB
                acceptedFiles: '.doc,.docx,.xls,.xlxs,.rar,.zip,.pdf',
                previewsContainer: '.file-container',
                previewTemplate: template,
                parallelUploads:1,
                url: url,
                params: {
                    //_token: token,
                    id: id
                },
                thumbnailWidth: 125,
                thumbnailHeight: 125,
                init: function() {
                    this.on("addedfile", function(file) {
//                    console.log(file);
                    });
//                this.on("success", function(file) {
//                    console.log(file);
//                });
                },
                success: function(file, data) {
                    data = $.parseJSON(data);

                    var div = $('.file-container > div:last');
                    div.attr('id', data.id);
                    var str = div.html()
                            .toString()
                            .replace (/{imageId}/g, data.id)
                            .replace (/{imageName}/g, data.name);
                    div.html(str);
                    return true;
                }
            });
        }
    }
</script>
