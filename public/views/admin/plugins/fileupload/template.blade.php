{{--{{dd($file)}}--}}
<div class="dz-preview dz-file-preview"
        {!!(isset($file)?'id="'. $file->id .'"' : 'id={imageId}')!!}
        >
    <div class="dz-details">
        <div class="overlay">
            <div class="dz-filename text-overflow-hidden">
                <span data-dz-name>
                    @if(isset($file->alt))
                        {!!$file->alt!!}
                    @endif
                </span>
            </div>
            <!-- <div class="dz-size" data-dz-size></div> -->
            <div class="status">
                <a class="dz-error-mark remove-item" href="javascript:;"><i class="fa fa-remove-sign"></i></a>
                <div class="dz-error-message">Помилка <span data-dz-errormessage></span></div>
            </div>
            <div class="controls clearfix">
                <a onclick="content.file.edit(<?=((isset($file)) ?  $file->id  : '{imageId}')?>);"  class="edit-item" href="javascript:;"><i class="fa fa-pencil"></i></a>
                <a class="trash-item" href="javascript:;"><i class="fa fa-trash-o"></i></a>
            </div>
            <div class="controls confirm-removal clearfix">
                <a onclick="content.file.delete(<?=((isset($file)) ?  $file->id  : '{imageId}')?>);" class="remove-item" href="javascript:;">Так</a>
                <a class="remove-cancel" href="javascript:;">Ні</a>
            </div>
        </div>
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
</div>