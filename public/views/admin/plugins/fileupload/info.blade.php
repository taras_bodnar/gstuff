{!! HTML::script('assets/admin/js/modules/content.js') !!}

<form id="FileInfo" action="/admin/ajax/Admin/Content/fileInfo" method="POST">
@foreach($languages as $language)
        <div class="form-group">
            <label>Назва {{$language->name}}</label>
            <input id="info_{{$language->id}}_name" required class="form-control"
                   name="info[{{$language->id}}][name]"
                   value="{{isset($info[$language->id])?$info[$language->id]:''}}">
        </div>
        <input type="hidden" value="{{$id}}" name="content_id">
        <input type="hidden" name="process">
@endforeach
</form>
<script>
    $("#FileInfo").ajaxForm({
        success: function(d) {
            var obj = $.parseJSON(d);
            if(obj.s>0) {
                $(".modal").modal('hide');
            }
        }
    });
</script>