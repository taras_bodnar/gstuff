<div class="panel panel-default panel-block ">
    <div class="list-group">
        <div class="list-group-item">
            <h4 class="section-title">Мітки </h4>
            @foreach($languages as $l)
            <div class="form-group ">
                <label>{{$l->name}}</label>
                <input id="project_tags" data-category="{{$id}}" data-lang="{{$l->id}}" type="text" name="tags[{{$l->id}}]"
                       placeholder="введіть мітки через кому"
                       value="@if(isset($tags[$l->id])) {{implode(',', $tags[$l->id])}}@endif" data-role="tagsinput" />
            </div>
            @endforeach
        </div>
    </div>
</div>

{{--<script>--}}
    {{--tags = {--}}
        {{--build : function()--}}
        {{--{--}}
            {{--var tags =$("#project_tags"), id=tags.data("category"), lang_id = tags.data("lang");--}}
            {{--console.log(lang_id,"here");--}}
            {{--tags.on("itemRemoved", function(event) {--}}
{{--//                console.log(event);--}}
{{--//            console.log("item removed : "+event.item, id,lang_id);--}}
                {{--$.ajax({--}}
                    {{--type: "POST",--}}
                    {{--url:"/plugins/tags/remove",--}}
                    {{--data: {--}}
                        {{--name: event.item,--}}
                        {{--id: id,--}}
                        {{--lang_id: lang_id--}}
                    {{--},--}}
                    {{--dataType: "html"--}}
                {{--});--}}

                {{--return true;--}}
            {{--});--}}
        {{--}--}}
    {{--};--}}

    {{--$(document).ready(function(){--}}
        {{--tags.build();--}}
    {{--});--}}
{{--</script>--}}