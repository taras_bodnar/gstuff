<html>
@include('admin.chunk.head')
<body class="skin-blue sidebar-mini">


<div id="wrapper" class="wrapper">


    @yield('content')
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


</body>
@include('admin.chunk.footer')
</html>
{{--{!!html_entity_decode($content)!!}--}}
