@extends('admin.index')

@section('content')
        <!-- Navigation -->
{!!$nav!!}
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    {!!$sidebar!!}
    <!-- Content Header (Page header) -->
    <section class="content-header">
        {!!$title_block!!}
    </section>

    <section class="content">
        <dov id="notification"></dov>

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                {!!html_entity_decode($content)!!}
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>
</div><!-- /.content-wrapper -->

    @endsection