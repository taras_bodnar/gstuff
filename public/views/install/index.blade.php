<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{$title}}</title>
    <link rel="stylesheet" href="public/views/install/assets/css/style.css"/>
</head>
<body>
<div class="install">

    <form method="post">
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="register">
                    {{--<div class="logo"><a href=""><img src="/themes/install/images/oyi.jpg" alt=""/></a></div>--}}
                    <h2 class="text-center">Вітаємо в системі </h2>
                    {!! $content !!}
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>