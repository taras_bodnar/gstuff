<div class="alert alert-dismissable alert-info fade in">
    Вітаємо! Систему встановлено. <br/>
</div>
<footer class="panel-footer text-right">
    <a class="btn btn-success" href="/laradmin">Панель адміністратора</a>
    <a class="btn btn-link" href="/">На сайт</a>
</footer>