<fieldset>
    <legend>Налаштування сайту</legend>
</fieldset>

<?php if(!empty($error)) : ?>
<div class="alert alert-dismissable alert-danger fade in">
    <?=implode('<br>', $error);?>
</div>
<?php endif; ?>

<div class="form-group">
    <label>Назва сайту <span class="text-danger">*</span></label>
    <input type="text" required="" name="data[name]" placeholder="введіть назву сайту" class="form-control ">
</div>

<div class="form-group">
    <label>Ім’я адміністратора <span class="text-danger">*</span></label>
    <input type="text" required="" name="data[user]" placeholder="Ведіть ваше ім'я" class="form-control ">
</div>

<div class="form-group">
    <label> Ваш email <span class="text-danger">*</span></label>
    <input type="text" required="" name="data[email]" placeholder="email@examle.com" class="form-control ">
</div>

<div class="form-group">
    <label>Пароль <span class="text-danger">*</span></label>
    <input type="password" required="" name="data[pass]" placeholder="pasword" class="form-control ">
</div>

<div class="form-group">
    <label> Мова сайту по замовчуванню <span class="text-danger">*</span></label>
    <select name="data[language]">
        <option value="uk">Українська</option>
        <option value="ru">Русский</option>
        <option value="en">English</option>
        <option value="pl">Polska</option>
        <option value="de">Deutch</option>
    </select>
</div>
<input type="hidden" name="step" value="2"/>

<footer class="panel-footer text-right">
    <button class="btn btn-success" type="submit">Наступний крок</button>
</footer>