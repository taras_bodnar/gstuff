<fieldset>
    <legend>It's some problem. Please check this</legend>
</fieldset>

<?php if(!empty($error)) : ?>
<div class="alert alert-dismissable alert-danger fade in">
    <ul>
        @foreach($error as $err)
            <li>{{$err}}</li>
        @endforeach
    </ul>
</div>
<?php endif; ?>
