<section class="lt-page-works">
    <div class="container cms">
        <div class="lt-cms-title">
            <div class="row">
                <div class="col-lg-8">
                    <div class="name-page">{{$page->name}}</div>
                    <h2>{{$t['our_portfolio']}}</h2>
                    <div class="page-text">
                        <p>
                            {!!$page->content!!}
                        </p>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="filters-button-group">
                        <a href="" class="active" data-filter="*">{{$t['all_projects']}}</a>
                        @foreach($categories as $item)
                            @if($item->t!=0)
                                <a href="{{$item->id}}" data-filter=".{{$item->id}}">{{$item->name}}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="grid-izotop fade-in">
                    <div class="grid-sizer"></div>
{{--                    {{dd($items)}}--}}
                    @foreach($items as $item)
                        <div class="grid-item @if(isset($item->class)){{$item->class}}@endif @if(isset($item->cover->size['value'][0]->value) && $item->cover->size['value'][0]->value=='Big') grid-item--width2 @endif apps shops websites"
                             @if(isset($item->categories))data-category="{{$item->categories}}"@endif style="background-image: url('@if(isset($item->cover->src)){{$item->cover->src}}@endif')">
                            <div class="grid-img">
                                {{--<img src="@if(isset($item->cover->src)){{$item->cover->src}}@endif" alt="@if(isset($item->cover->alt)){{$item->cover->alt}}@endif">--}}
                                <div class="tile-content">
                                    @if(!empty($item->cat))
                                        @foreach($item->cat as $category)
                                            <span class="category">{{$category->name}}</span>
                                        @endforeach
                                    @endif
                                    <h2>
                                        <a target="_blank" rel="nofollow" href="@if(isset($item->behance->value)){{$item->behance->value}}@endif">{{$item->name}}</a>
{{--                                        {{$item->name}}--}}
                                    </h2>
                                    <p class="description">
                                        {{$item->description}}
                                    </p>
                                </div>
                                <a target="_blank" rel="nofollow" href="@if(isset($item->behance->value)){{$item->behance->value}}@endif" class="cta">{{$t['viewm']}}</a>
                                <div class="filter-tags">
                                    @if(isset($item->tags) && !empty($item->tags))
                                        @foreach($item->tags as $tag)
                                            <span class="filter">{{$tag->name}}</span>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="gradient" data-color-start = "{{$item->colourF->value}}" data-color-end = "{{$item->colourT->value}}"></div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>