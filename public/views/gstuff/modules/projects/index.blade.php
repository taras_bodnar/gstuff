{{--{{dd($items)}}--}}
<div class="row">
    <div class="col-12">
        <div class="grid fade-in">
            <div class="grid-sizer"></div>
            @foreach($items as $item)
                <div class="grid-item @if(isset($item->cover->size['value'][0]->value) && $item->cover->size['value'][0]->value=='Big') grid-item--width2 @endif"
                style="background-image: url('@if(isset($item->cover->src)){{$item->cover->src}}@endif')">
                    <div class="grid-img">
                        {{--<img src="" alt="@if(isset($item->cover->alt)){{$item->cover->alt}}@endif">--}}
                        <div class="tile-content">
                            @if(!empty($item->categories))
                                @foreach($item->categories as $category)
                                    <span class="category">{{$category->name}}</span>
                                @endforeach
                            @endif
                            <h2>
                                <a target="_blank" rel="nofollow" href="@if(isset($item->behance->value)){{$item->behance->value}}@endif">{{$item->name}}</a>
                                {{--{{$item->name}}--}}
                            </h2>
                            <p class="description">
                                {{$item->description}}
                            </p>
                        </div>
                        <a target="_blank" rel="nofollow" href="@if(isset($item->behance->value)){{$item->behance->value}}@endif" class="cta">{{$t['viewm']}}</a>
                        @if(isset($item->tags) && !empty($item->tags))
                        <div class="filter-tags">
                            @foreach($item->tags as $tag)
                                <span class="filter">{{$tag->name}}</span>
                            @endforeach
                            </div>
                        @endif
                        <div class="gradient" data-color-start = "{{$item->colourF->value}}" data-color-end = "{{$item->colourT->value}}"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>