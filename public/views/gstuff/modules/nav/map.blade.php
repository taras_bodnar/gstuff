<div class="c-content-box">
    <div class="directory-inner"><i class="ico-shadow tp tp-road-sign"></i>

        <h2 class="directory-title"><i class="ico tp tp-road-sign"></i>
            {{$t->Choose_that_interest}}
        </h2>

        <div class="directory-content">
            <div class="menu-directory-container">
                <ul id="menu-directory" class="directory-menu">
                    @foreach($items as $item)
                        <li id="menu-item-{{$item->id}}"
                            class="menu-item menu-item-type-post_type menu-item-object-page
                            @if(isset($item->children))
                                    menu-item-has-children
                                    @endif
                                    ">
                            <a href="@if($item->isfolder!=1){{$item->id}}@endif">{{$item->name}}</a>
                            @if(isset($item->children))
                                <ul class="sub-menu">
                                    @foreach($item->children as $child)
                                        <li id="menu-item-244"
                                            class="menu-item menu-item-type-post_type menu-item-object-page">
                                            <a href="@if($child->isfolder!=1){{$child->id}}@endif" title="{{$child->title}}">{{$child->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>