﻿@if(count($items)>1)
    <div class="header-languages">
        @foreach($items as $item)
            <a @if($current_languages==$item->id)class="active" @endif href="{{$page->id}};l={{$item->id}};" title="{{$page->title}}"> {{$item->name}}</a>
        @endforeach
    </div>
@endif
