<ul class="header-nav">
    @foreach($items as $item)
        <li>
            <a @if($page->id==$item->id || $item->id==$page->parent_id)class="active" @endif href="{{$item->id}}">{{$item->name}}</a>
            @if($item->isfolder)
            <div class="header-widget">
                <div class="container">
                    <div class="row">
                        @foreach($item->children as $child)
                            @if(isset($child->image))
                                <div class="col-12 col-sm-4">
                                    <a href="{{$child->id}}" class="sub-nav">
                                        <img src="{{$child->image->src}}" alt="{{$child->image->alt}}">
                                        <span>{{$child->name}}</span>
                                    </a>
                                </div>
                                @else
                                <div class="col-md-3">
                                    <a href="{{$child->id}}" class="sub-nav">{{$child->name}}</a>
                                </div>
                            @endif

                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        </li>
    @endforeach
</ul>