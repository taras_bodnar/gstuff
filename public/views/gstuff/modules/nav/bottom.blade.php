<nav class="footer-menu">

    <ul id="menu-footer-items" class="menu-items">
        @foreach($items as $item)
            <li id="menu-item-242"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-242">
                <a href="{{$item->id}}"><span>{{$item->name}}</span></a></li>
        @endforeach
    </ul>
</nav>