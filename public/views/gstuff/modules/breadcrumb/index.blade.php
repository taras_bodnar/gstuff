{{--{{dd($items)}}--}}
<div class="breadcrumbs"><ul>
        <li class="home"><a href="1">{{$t->home}}</a></li>
        @foreach($items as $item)
            @if(!empty($item->id))
            @if($item->id!=1)
                <li>
                    @if($item->id!=$page->id)
                        <a href="{{$item->id}}" title="{{$item->title}}">
                            {{$item->name}}
                        </a>
                        @else
                        {{$item->name}}
                    @endif

                </li>
            @endif
            @endif
        @endforeach
    </ul>
</div>