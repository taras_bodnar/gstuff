{{--{{dd($items)}}--}}
<div id="lsvr_documents_widget-4" class="widget lsvr-documents">
    <div class="widget-inner">
        {{--<h3 class="widget-title m-has-ico"><i--}}
                    {{--class="widget-ico tp tp-papers"></i>{{$t->documents}}</h3>--}}

        <div class="widget-content">


            <ul class="document-list m-has-icons">
                @foreach($items as $item )
                    <li class="document post-131 lsvrdocument type-lsvrdocument status-publish hentry lsvrdocumentcat-council-agenda">
                        <div class="document-inner">


                            <div class="document-icon" title="{{$item->alt}}"><i
                                        class="fa fa-file-{{$item->ext}}-o"></i></div>

                            <h4 class="document-title">


                                <a style="font-size: 14px;" href="/{{$item->path}}"
                                   target="_blank" rel="nofollow">{{$item->alt}}</a>
                                <span class="document-filesize">({{$item->size}} kB)</span>


                            </h4>

                        </div>
                    </li>
                @endforeach
            </ul>
            @if(count($items)>5)
                @include($template_url.'modules.pagination', ['paginator' => $items])
            @endif
            {{--<p class="show-all-btn">--}}
                {{--<a href="documents">See All Documents</a>--}}
            {{--</p>--}}


        </div>
    </div>
</div>