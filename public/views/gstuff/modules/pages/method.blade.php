{{--{{dd($items)}}--}}
<ul class="nospace clear">
    @foreach($items as $k=>$item)
        <li class="one_half @if($k==0) first @endif ">
            <article><img class="borderedbox" src="{{$item->img->src}}" alt="">
                <h2>{{$item->name}}</h2>
                {!! $item->content !!}
                {{--<p class="right"><a href="#">Read More Here &raquo;</a></p>--}}
            </article>
        </li>
    @endforeach
</ul>