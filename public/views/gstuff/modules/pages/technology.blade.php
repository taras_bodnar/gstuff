<section class="lt-from-technology">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="lt-title-h2">
                    <h2>
                        {{$t['our_technology_1']}}
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container fade-in">
        <div class="row">
            @foreach($items as $item)
                <div class="col-lg-4 col-md-6 col-xs-12">
                    <a href="{{$item->id}}" class="title">
                        <img src="{{$item->cover->src}}" alt="{{$item->cover->alt}}">
                        <span>{{$item->name}}</span>
                    </a>
                    <ul class="lt-list-logos">
                        @foreach($item->images as $image)
                            <li>
                                <img src="{{$image->src}}" alt="{{$image->alt}}">
                                <span>{{$image->alt}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
</section>