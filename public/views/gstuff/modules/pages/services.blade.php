<section class="lt-advantages">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="lt-title-h2">
                    <h2>
                        {{$t['we_are_good_at']}}
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container fade-in">
        <div class="row">
            @foreach($items as $item)
                <div class="col-lg-3 col-md-6">
                    <div class="lt-item cms-content">
                        <h4>{{$item->name}}</h4>
                        <p>{{$item->description}}</p>
                        {{--<ul>
                            @foreach($item->options as $option)
                                <li>{{$option}}</li>
                            @endforeach
                        </ul>--}}
                        <a href="{{$item->id}}" class="btn btn-primary">
                            <span data-hover="{{$t['find_out_more']}}">{{$t['find_out_more']}}</span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>