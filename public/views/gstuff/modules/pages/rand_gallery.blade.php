{{--{{dd($item)}}--}}
<div id="lsvr_gallery_featured_widget-4" class="widget lsvr-gallery-featured">
    <div class="widget-inner"><h3 class="widget-title m-has-ico"><i
                    class="widget-ico tp tp-pictures"></i>{{$item->name}}</h3>

        <div class="widget-content">


            <div class="gallery-image" title="{{$item->title}}">
                <a href="{{$item->id}}"><img
                            src="@if(!empty($item->img->src)){{$item->img->src}}@endif" alt=""></a>
            </div>


            <p class="show-all-btn">
                <a href="24">{{$t->see_all_galleries}}</a>
            </p>

        </div>
    </div>
</div>