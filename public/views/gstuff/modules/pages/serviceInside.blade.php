@if(!empty($images))
    <div class="container cms">
        <div class="lt-cms-title">
            <div class="row">
                <div class="col-lg-8">
                    {{--<div class="name-page">{{$t['our_technology']}}</div>--}}
                    <h2>{{$info->name}}</h2>
                    <div class="page-text">
                        <p>
                            {{$info->description}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row row-sub-pages">
                <div class="col-md-12">
                    <ul class="lt-list-logos">
                        @foreach($images as $image)
                            <li>
                                <img src="{{$image->src}}" alt="{{$image->alt}}">
                                <span>{{$image->alt}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif