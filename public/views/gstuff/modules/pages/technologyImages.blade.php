<ul class="lt-list-logos">
    @foreach($images as $image)
        <li>
            <img src="{{$image->src}}" alt="{{$image->alt}}">
            <span>{{$image->alt}}</span>
        </li>
    @endforeach
</ul>