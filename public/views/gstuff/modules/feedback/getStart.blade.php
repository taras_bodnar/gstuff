<div id="lt-getstarted" class="lt-popup">
    <a href="" class="popup-close"><i class="icon-close"></i></a>
    <div class="lt-scroll">
        <div class="lt-getstarted">
            <h2>{{$t['build_products']}}</h2>
            <form id="get_start" method="post" action="/request/Feedback/getStart">
                <div class="row-checkbox">
                    <div class="checkbox">
                        <input id="website" type="checkbox" value="website" name="data[type][]">
                        <label for="website">{{$t['website']}}</label>
                    </div>
                    <div class="checkbox">
                        <input id="app" type="checkbox" value="app" name="data[type][]">
                        <label for="app">{{$t['app']}}</label>
                    </div>
                    <div class="checkbox">
                        <input id="other" type="checkbox" value="other" name="data[type][]">
                        <label for="other">{{$t['other']}}</label>
                    </div>
                    <div class="checkbox">
                        <input id="frontend" type="checkbox" value="frontend" name="data[type][]">
                        <label for="frontend">Front-End</label>
                    </div>
                    <div class="checkbox">
                        <input id="backend" type="checkbox" value="backend" name="data[type][]">
                        <label for="backend">Backend</label>
                    </div>
                    <div class="checkbox">
                        <input id="cms" type="checkbox" value="cms" name="data[type][]">
                        <label for="cms">Сms</label>
                    </div>
                </div>
                <p class="center">
                    {{$t['budget']}}
                </p>
                <div class="wrap-range">
                    <input type="text" id="slider-price" name="data[price]" value="" />
                </div>
                <div class="input-group">
                    <label for="l6">{{$t['name']}}</label>
                    <input id="l6" type="text" class="start-focus" name="data[name]">
                    <span></span>
                </div>
                <div class="input-group">
                    <label for="l7">{{$t['сompany']}}</label>
                    <input id="l7" type="text" name="data[company]">
                    <span></span>
                </div>
                <div class="input-group">
                    <label for="l8">Email</label>
                    <input id="l8" type="email" name="data[email]">
                    <span></span>
                </div>
                <div class="input-group">
                    <label for="l9">{{$t['location']}}</label>
                    <input id="l9" type="text" name="data[location]">
                    <span></span>
                </div>
                <div class="textarea-group">
                    <label for="l10">{{$t['short_info']}}</label>
                    <textarea id="l10" name="data[message]"></textarea>
                    <span></span>
                </div>
                <div class="lt-btn-file">
                    <div class="label-btn"><i class="icon-file"></i>{{$t['file_is_not_select']}}</div>
                    <input type="file" name="file">
                </div>
                <p class="center">
                   {{$t['hear_about_us']}}
                </p>
                <div class="select-group">
                    <select class="style-select" name="data[how_hear]">
                        <option value="">{{$t['socia_media']}}</option>
                        <option value="">{{$t['friends']}}</option>
                        <option value="">{{$t['news']}}</option>
                    </select>
                </div>
                <div class="wrap-btn">
                    <button class="btn-full btn btn-lg btn-hover-wh">
                        <span data-hover="{{$t['submit']}}">{{$t['submit']}}</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>