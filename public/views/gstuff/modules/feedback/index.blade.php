<div id="lt-contact-us" class="lt-popup">
    <a href="" class="popup-close"><i class="icon-close"></i></a>
    <div class="lt-scroll">
        <div class="lt-contact-us">
            <h2>{{$t['get_in_touch']}}</h2>
            <form id="get_in_touch" method="post" action="/request/Feedback/run">
                <div class="input-group border-top">
                    <label for="l1">{{$t['name']}}</label>
                    <input id="l1" type="text" class="start-focus" name="data[name]">
                    <span></span>
                </div>
                <div class="input-group">
                    <label for="l2">{{$t['phone']}}</label>
                    <input id="l2" type="text" name="data[phone]">
                    <span></span>
                </div>
                <div class="input-group">
                    <label for="l3">E-mail</label>
                    <input id="l3" type="email" name="data[email]">
                    <span></span>
                </div>
                <div class="textarea-group">
                    <label for="l4">{{$t['short_info']}}</label>
                    <textarea id="l4" name="data[message]"></textarea>
                    <span></span>
                </div>
                <div class="wrap-btn">
                    <button class="btn-full btn btn-lg btn-hover-wh">
                        <span data-hover="{{$t['submit']}}">{{$t['submit']}}</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>