<div class="article-comments" id="comments">
    <div class="c-content-box">


        <p class="c-alert-message m-info"><i class="ico fa fa-info-circle"></i>
            There are no comments yet. </p>


        <!-- COMMENT FORM : begin -->
        <div class="respond-form">

            <div id="respond" class="comment-respond">
                <h3 id="reply-title" class="comment-reply-title">Leave a Comment
                    <small><a rel="nofollow" id="cancel-comment-reply-link"
                              href="../index.html?p=103#respond"
                              style="display:none;">Cancel</a></small>
                </h3>
                <form action="http://demos.volovar.net/townpress.wp/demo/wp-comments-post.php"
                      method="post" id="commentform" class="comment-form">
                    <p class="comment-notes c-alert-message m-info"><i
                            class="ico fa fa-info-circle"></i>Your email address will not be
                        published. Required fields are marked (*).</p>

                    <p style="display: none;"
                       class="c-alert-message m-warning m-validation-error"><i
                            class="ico fa fa-exclamation-circle"></i>Fields with (*) are
                        required!</p>

                    <div class="form-fields"><p><label for="author">Name <span class="required">*</span></label><input
                                id="author" class="required" name="author" type="text"
                                value="" size="30" aria-required='true'></p>

                        <p><label for="email">Email <span
                                    class="required">*</span></label><input id="email"
                                                                            class="required email"
                                                                            name="email"
                                                                            type="text"
                                                                            value=""
                                                                            size="30"
                                                                            aria-required='true'>
                        </p>

                        <p><label for="url">Website</label><input id="url" name="url"
                                                                  type="text" value=""
                                                                  size="30"></p>

                        <p class="form-row comment-row"><label for="comment">Comment <span
                                    class="required">*</span></label><textarea id="comment"
                                                                               class="required"
                                                                               name="comment"
                                                                               cols="45"
                                                                               rows="8"></textarea>
                        </p>

                        <p class="form-allowed-tags">You may use these <abbr
                                title="HyperText Markup Language">HTML</abbr> tags and
                            attributes: <code>&lt;a href=&quot;&quot; title=&quot;&quot;&gt;
                                &lt;abbr title=&quot;&quot;&gt; &lt;acronym title=&quot;&quot;&gt;
                                &lt;b&gt; &lt;blockquote cite=&quot;&quot;&gt; &lt;cite&gt; &lt;code&gt;
                                &lt;del datetime=&quot;&quot;&gt; &lt;em&gt; &lt;i&gt; &lt;q
                                cite=&quot;&quot;&gt; &lt;s&gt; &lt;strike&gt;
                                &lt;strong&gt; </code></p></div>
                    <p class="form-submit"><input name="submit" type="submit" id="submit"
                                                  class="submit" value="Post Comment"/> <input
                            type='hidden' name='comment_post_ID' value='103'
                            id='comment_post_ID'/>
                        <input type='hidden' name='comment_parent' id='comment_parent'
                               value='0'/>
                    </p></form>
            </div>
            <!-- #respond -->
        </div>
        <!-- COMMENT FORM : end -->        </div>
</div>