<div class="l-page-blog__img">
    @if(!empty($image->src))
        <img src="{{$image->src}}" alt="{{$page->title}}">
    @endif
</div>

<div class="l-page-blog__content">
    {!!$page->content!!} 
</div>

<div class="l-page-blog__date">
    {{$page->createdon}}
</div>

<!-- ARTICLE TAGS : begin -->
{{--<div class="article-tags">--}}
{{--<i class="ico tp tp-tag"></i>--}}
{{--<a href="../tag/football/index.html" rel="tag">football</a>, <a--}}
{{--href="../tag/soccer/index.html" rel="tag">soccer</a></div>--}}
        <!-- ARTICLE TAGS : end -->