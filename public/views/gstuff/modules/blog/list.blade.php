s<section class="lt-grid-blog">
    <div class="container cms">
        <div class="row">
            <div class="col-lg-12">
                <div class="grid-blog">
                    <div class="grid-sizer"></div>
                    @foreach($items as $k=>$item)
                        <div class="grid-item-blog">
                            <div class="blog-img">
                                @if(isset($item->img->src))
                                    <a href="{{$item->id}}"><img src="{{$item->img->src}}" alt="{{$item->img->alt}}" /></a>
                                @endif
                            </div>
                            <div class="blog-cms">
                                <h4>{{$item->name}}</h4>
                                <p>{{$item->description}}</p>
                            </div>
                            <div class="blog-footer">
                                <div class="blog-date">
                                    {{date('d.m.Y',strtotime($item->createdon))}}
                                </div>
                                <a href="{{$item->id}}">{{$t['show_more']}}</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@include($template_url.'modules.pagination', ['paginator' => $items])