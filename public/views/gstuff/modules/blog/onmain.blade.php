<section class="lt-grid-blog light-bg">
    <div class="container cms">
        <div class="row">
            <div class="col-12">
                <div class="lt-blog-title">
                   {{$t['also_read']}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="grid-blog">
                    <div class="grid-sizer"></div>
                    @foreach($items as $item)
                    <div class="grid-item-blog">
                        <div class="blog-img">
                            @if(isset($item->img->src))
                                <img src="{{$item->img->src}}" alt="{{$item->img->alt}}" />
                            @endif
                        </div>
                        <div class="blog-cms">
                            <h4>{{$item->name}}</h4>
                            <p>{{$item->description}}</p>
                        </div>
                        <div class="blog-footer">
                            <div class="blog-date">
                                {{date('d.m.Y',strtotime($item->createdon))}}
                            </div>
                            <a href="{{$item->id}}">{{$t['show_more']}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>