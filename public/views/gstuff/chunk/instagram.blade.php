<section class="lt-ins-photos">
    <div class="center wrap-icon">
        <i class="icon-instagram"></i>
    </div>
    <div class="inst-slider">
        @foreach(\App\Http\Controllers\modules\Pages\Pages::instagram() as $item)
            <a href="{{$item['link']}}" target="_blank" rel="nofollow" title="{{$item['location']}}" class="photo-item">
                <img src="{{$item['standard']}}" alt="{{$item['location']}}">
            </a>
        @endforeach
    </div>
</section>