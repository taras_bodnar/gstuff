<section class="lt-about">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="lt-title">
                   {!! $t['speak_to_us_now'] !!}
                    <a href="lt-getstarted" class="btn btn-lg btn-primary lt-getstarted-popup">
                        <span data-hover="{{$t['get_started']}}">{{$t['get_started']}}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>