<head>
    @include($template_url."chunk/meta")

    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">--}}

    {!! HTML::style($assets_url.'assets/css/vendor.css') !!}
    {!! HTML::style($assets_url.'assets/css/style.css') !!}
</head>
