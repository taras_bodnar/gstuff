<!-- footer -->
<footer id="footer">
    <div class="lt-footer-bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <p>© {{$t['reserved']}}</p>
                </div>
                <div class="col-6">
                    <div class="lt-footer-social">
                        {{--<a href="">Dribbble</a>--}}
                        {{--<a href="">Behance</a>--}}
                        {{--<a href="">Facebook</a>--}}
                        {{--<a href="">LinkedIn</a>--}}
                        <a href="https://www.behance.net/gstuff" target="_blank" rel="nofollow">Behance</a>
                        <a href="https://www.instagram.com/gstuff_pro" target="_blank" rel="nofollow">Instagram</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /  -->
<!-- .footer -->

<!--other-elements-->
<div class="loader">
    <img src="{{$assets_url}}assets/img/site-logo.png" alt="">
    <div class="loader-animation">
        <div class="spinner">
            <div class="cube1"></div>
            <div class="cube2"></div>
        </div>
    </div>
</div>
<!--.other-elements-->

<!--page-end-->
{!! HTML::script($assets_url.'assets/js/vendor.js') !!}
{!! HTML::script($assets_url.'assets/js/vendor/jquery.form.min.js') !!}
        <!--{!! HTML::script($assets_url.'assets/js/vendor/select2.min.js') !!}-->
<!--{!! HTML::script($assets_url.'assets/js/vendor/pnotify.custom.min.js') !!}-->
{!! HTML::script($assets_url.'assets/js/main.js') !!}
{!! HTML::script($assets_url.'assets/js/dev.js') !!}