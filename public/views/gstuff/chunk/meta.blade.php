<title>{{$page->title}}  | GSTUFF</title>
<meta name="robots" content="/,follow"/>
<meta name="keywords" content="@if(!empty($page->keywords)){{$page->keywords}}@else{{$page->title}}@endif"/>
<meta name="description" content="@if(!empty($page->meta_description)){{$page->meta_description}}@else{{$page->description}}@endif"/>
<meta itemprop="name" content="{{$page->title}} | GSTUFF">
<meta property="og:description" content="@if(!empty($page->meta_description)){{$page->meta_description}}@else{{$page->description}}@endif">
<meta property="og:title" content="{{$page->title}}">
<meta property="og:url" content="{{$page->fullurl}}">
<meta property="og:description" content="@if(!empty($page->meta_description)){{$page->meta_description}}@else{{$page->description}}@endif">
<meta property="og:image" content="http://{{\Illuminate\Support\Facades\Request::server('HTTP_HOST')}}@if(!empty($page->img->src)){{$page->img->src}}@else{{$page->img}}@endif">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="{{$assets_url}}assets/img/favicon.ico" type="image/x-icon">
<meta name="document-state" content="Dynamic"/>
<meta name="document-rights" content="Public Domain"/>
<meta name="distribution" content="Global"/>
<meta name="theme-color" content="#5ace5f">
<link rel="canonical" href="{{strstr($page->fullurl, '?', true)}}"/>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108271694-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-108271694-1');
</script>
