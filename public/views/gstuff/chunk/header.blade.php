﻿<header id="header">
    <div class="container">
        <div class="row row-header">
            <a href="1" class="site-logo">
                <img src="{{$assets_url}}assets/img/site-logo.png" alt="">
            </a>
            <div id = "nav-btn" class = "nav-btn hidden-md-up">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="header-right col">
                <div class="row align-items-center">
                    <div class="col">
                        {mod:Nav::top}
                    </div>
                    <div class="col-12 col-md-auto">
                        <div class="row align-items-center">
                            <div class="col">
                                <a class="header-email" href="mailto:hello@gstuff.com">hello@gstuffpro.com</a>
                            </div>
                            <div class="col-md-auto header-contact">
                                <a href="lt-contact-us" class="btn btn-primary link-popup link-popup-contact-us">
                                    <span data-hover="{{$t['contact_us']}}">{{$t['contact_us']}}</span>
                                </a>
                            </div>
                            <div class="col-12 col-md-auto">
                                {mod:Nav::languages}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
