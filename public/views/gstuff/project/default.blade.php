{{--Created by Taras 2017-06-26 13:35--}}
        <!DOCTYPE html>
<html lang="en">
<!--head-->
@include($template_url.'chunk/head')
<!--.head-->
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
@include($template_url.'chunk/header')
<!--.header-->
    <!-- content -->
    <main class="content" id="content">
        <!-- wrap-screen -->

        <section class="lt-from-blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="lt-cms-title">
                            <div class="name-page">{{$page->name}}</div>
                            <div class="page-text">
                                {!! $page->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="lt-cms fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="lt-title">
                            <h2>{{$t['what_we_ve_done']}}</h2>
                        </div>
                    </div>
                </div>
                {mod:Projects::index}
            </div>
        </section>

        @include($template_url.'chunk/speak_to_us')

        @include($template_url.'chunk/instagram')
    </main>
    <!-- .content -->
</div>
<!--wrapper-->

@include($template_url.'chunk/footer')
</body>
</html>