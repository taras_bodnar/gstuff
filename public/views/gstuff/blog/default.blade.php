{{--Created by Taras 2017-06-26 13:35--}}
        <!DOCTYPE html>
<html lang="en">
<!--head-->
@include($template_url.'chunk/head')
<style>
    pre{
        background: #333;
        padding: 15px 15px 15px 15px;
        white-space: pre;
        display: block;
        color: #ccc;
        font-family: 'Courier New', Monospace;
        font-size: 16px;
        line-height: 20px;
        overflow: auto;
        tab-size: 4;
        clear: both;
    }
</style>
<!--.head-->
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
@include($template_url.'chunk/header')
<!--.header-->
    <!-- content -->
    <main class="content" id="content">
        <!-- wrap-screen -->

        <section class="blog-image" @if(isset($page->img) && !empty($page->img))style="background-image: url('{{$page->img}}')"@endif>
            <div class="table">
                <div class="table-cell">
                    <div class="container cms">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="lt-cms-title fade-in">
                                    <a href="" onclick="window.history.back();" class="name-page">back</a>
                                    <h2>{{$page->name}}</h2>
                                    <div class="page-text">
                                        <p>
                                            {{$page->description}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="blog-content">
            <div class="container cms">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="cms-content">
                            {!! $page->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="share">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="lt-share-title">
                            share
                        </div>
                        <div class="list-share">
                            <div class="item-share facebook">
                                <a target="_blank" rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?u={{$page->fullurl}}" class="table">
                                            <span class="table-cell">
                                                <i class="icon-facebook-logo"></i>
                                            </span>
                                </a>
                            </div>
                            <div class="item-share twitter">
                                <a target="_blank" rel="nofollow" href="https://twitter.com/home?status={{$page->fullurl}}" class="table">
                                            <span class="table-cell">
                                                <i class="icon-twitter-logo"></i>
                                            </span>
                                </a>
                            </div>
                            <div class="item-share google">
                                <a target="_blank" rel="nofollow" href="https://plus.google.com/share?url={{$page->fullurl}}" class="table">
                                            <span class="table-cell">
                                                <i class="icon-google-plus"></i>
                                            </span>
                                </a>
                            </div>
                            <div class="item-share linkedin">
                                <a target="_blank" rel="nofollow" href="https://www.linkedin.com/shareArticle?mini=true&url={{$page->fullurl}}&title={{$page->name}}&summary={{$page->description}}&source=" class="table">
                                            <span class="table-cell">
                                                <i class="icon-linkedin-logo"></i>
                                            </span>
                                </a>
                            </div>
                            <div class="item-share pinterest">
                                <a target="_blank" rel="nofollow" href="https://pinterest.com/pin/create/button/?url={{$page->fullurl}}&media=http://{{\Illuminate\Support\Facades\Request::server('HTTP_HOST')}}@if(!empty($page->img->src)){{$page->img->src}}@else{{$page->img}}@endif&description={{$page->description}}" class="table">
                                            <span class="table-cell">
                                                <i class="icon-pinterest-logo"></i>
                                            </span>
                                </a>
                            </div>
                        </div>
                        @include($template_url.'chunk/disqus')
                    </div>
                </div>
            </div>
        </section>

        {mod:Blog::postInMain}

        @include($template_url.'chunk/speak_to_us')

        @include($template_url.'chunk/instagram')
    </main>
    <!-- .content -->
</div>
<!--wrapper-->

@include($template_url.'chunk/footer')
</body>
</html>