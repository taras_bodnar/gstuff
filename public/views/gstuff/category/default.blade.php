{{--Created by Taras 2015-12-03 19:20--}}
        <!DOCTYPE html>
<html>
@include($template_url.'chunk/head')

<body>

<div class="site-wrapper">

    @include($template_url.'chunk/header')

    <main class="site-main" role="main">
        <section class="l-catalog section section--no-sidebar-right">
            <div class="container">

                <div class="section-content">
                    <div class="section-content__inner">
                        {mod:Shop::products}
                    </div>
                </div><!-- /section-content -->

                <aside class="sidebar-left">
                    {mod:Shop::filter}
                </aside><!-- /sidebar-left -->
                
            </div>
        </section><!-- /l-catalog-->

    </main><!-- .site-main -->

</div><!-- .site-wrapper -->


@include($template_url.'chunk/footer')
</body>
</html>
