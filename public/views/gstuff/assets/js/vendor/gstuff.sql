-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Чрв 26 2017 р., 23:27
-- Версія сервера: 5.7.18-ndb-7.6.2
-- Версія PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `gstuff`
--

-- --------------------------------------------------------

--
-- Структура таблиці `chunks`
--

CREATE TABLE IF NOT EXISTS `chunks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_id` int(10) unsigned NOT NULL,
  `pib` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `createdon` datetime DEFAULT NULL,
  `ip` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_content_id_foreign` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `components`
--

CREATE TABLE IF NOT EXISTS `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(4) NOT NULL,
  `isfolder` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `rang` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Дамп даних таблиці `components`
--

INSERT INTO `components` (`id`, `parent_id`, `isfolder`, `icon`, `controller`, `sort`, `published`, `rang`) VALUES
(1, 0, 0, 'fa-home nav-icon', '/admin/Admin/DashBoard', 1, 1, 200),
(2, 0, 0, 'fa-folder-open', '/admin/Admin/Pages', 1, 1, 200),
(3, 0, 1, 'fa-language', '/admin/Admin/Languages', 1, 1, 200),
(4, 3, 0, 'fa-language', '/admin/Admin/Languages', 1, 1, 200),
(5, 3, 0, 'fa-language', '/admin/Admin/Translation', 1, 1, 200),
(6, 3, 0, 'fa-language', '/admin/Admin/EmailTemplates', 1, 1, 200),
(7, 0, 1, 'fa-wrench', '/admin/Admin/Components', 1, 1, 200),
(8, 7, 0, 'fa-wrench', '/admin/Admin/Plugins', 1, 1, 200),
(9, 7, 0, 'fa-wrench', '/admin/Admin/Components', 1, 1, 200),
(10, 0, 0, 'fa-cog', '/admin/Admin/Settings', 1, 1, 200),
(11, 0, 1, 'fa-users', '/admin/Admin/UsersGroup', 1, 1, 200),
(12, 11, 0, 'fa-users', '/admin/Admin/UsersGroup', 1, 1, 200),
(13, 11, 0, 'fa-users', '/admin/Admin/Users', 1, 1, 200),
(14, 0, 1, 'fa-wrench', '', 1, 1, 200),
(15, 14, 0, 'fa-wrench', '/admin/Admin/Type', 1, 1, 200),
(16, 14, 0, 'fa-wrench', '/admin/Admin/Templates', 1, 1, 200),
(17, 14, 0, 'fa-wrench', '/admin/Admin/Features', 1, 1, 200),
(18, 14, 0, 'fa-wrench', '/admin/Admin/Themes', 1, 1, 200),
(19, 14, 0, 'fa-wrench', '/admin/Admin/Nav', 1, 1, 200),
(20, 14, 0, 'fa-wrench', '/admin/Admin/Size', 1, 1, 200);

-- --------------------------------------------------------

--
-- Структура таблиці `components_info`
--

CREATE TABLE IF NOT EXISTS `components_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `components_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `components_info_components_id_foreign` (`components_id`),
  KEY `components_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Дамп даних таблиці `components_info`
--

INSERT INTO `components_info` (`id`, `components_id`, `languages_id`, `name`, `description`) VALUES
(1, 1, 1, 'LarAdmin', ''),
(2, 2, 1, 'Pages', ''),
(3, 3, 1, 'Localisation', ''),
(4, 4, 1, 'Languages', ''),
(5, 5, 1, 'Translation', ''),
(6, 6, 1, 'Email Templates', ''),
(7, 7, 1, 'Components', ''),
(8, 8, 1, 'Plugins', ''),
(9, 9, 1, 'Components', ''),
(10, 10, 1, 'Settings', ''),
(11, 11, 1, 'Administration', ''),
(12, 12, 1, 'Administration Group', ''),
(13, 13, 1, 'Administration List', ''),
(14, 14, 1, 'Instruments', ''),
(15, 15, 1, 'Content Types', ''),
(16, 16, 1, 'Templates', ''),
(17, 17, 1, 'Features', ''),
(18, 18, 1, 'Themes', ''),
(19, 19, 1, 'Menu', ''),
(20, 19, 1, 'Images Sizes', '');

-- --------------------------------------------------------

--
-- Структура таблиці `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT '0',
  `isfolder` tinyint(4) NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL,
  `owner_id` int(10) unsigned NOT NULL,
  `templates_id` int(10) unsigned NOT NULL,
  `published` tinyint(4) DEFAULT NULL,
  `module` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` smallint(6) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `editedon` datetime DEFAULT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `content_templates_id_foreign` (`templates_id`),
  KEY `content_type_id_foreign` (`type_id`),
  KEY `content_owner_id_foreign` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Дамп даних таблиці `content`
--

INSERT INTO `content` (`id`, `parent_id`, `isfolder`, `type_id`, `owner_id`, `templates_id`, `published`, `module`, `sort`, `createdon`, `editedon`, `auto`) VALUES
(1, 0, 1, 1, 1, 7, 1, NULL, NULL, NULL, '2017-06-26 09:44:35', 0),
(2, 1, 1, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:54:26', '2017-06-26 12:55:19', 0),
(3, 2, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:55:29', '2017-06-26 12:55:48', 0),
(4, 2, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:55:57', '2017-06-26 12:56:09', 0),
(5, 2, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:56:23', '2017-06-26 12:56:40', 0),
(6, 2, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:56:49', '2017-06-26 12:57:05', 0),
(8, 1, 1, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:58:18', '2017-06-26 12:58:30', 0),
(9, 1, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:58:45', '2017-06-26 12:59:00', 0),
(10, 1, 0, 1, 1, 8, 1, NULL, NULL, '2017-06-26 12:59:12', '2017-06-26 14:34:39', 0),
(11, 1, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 12:59:44', '2017-06-26 12:59:55', 0),
(12, 8, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 13:10:42', '2017-06-26 13:11:18', 0),
(13, 8, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 13:12:45', '2017-06-26 13:13:53', 0),
(14, 8, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 13:13:59', '2017-06-26 13:14:15', 0),
(15, 1, 0, 1, 1, 1, 1, NULL, NULL, '2017-06-26 19:23:42', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `content_features`
--

CREATE TABLE IF NOT EXISTS `content_features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_features_content_id_foreign` (`content_id`),
  KEY `content_features_features_id_foreign` (`features_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `content_images`
--

CREATE TABLE IF NOT EXISTS `content_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `type` enum('image','file') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'image',
  PRIMARY KEY (`id`),
  KEY `content_images_content_id_foreign` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `content_images`
--

INSERT INTO `content_images` (`id`, `content_id`, `name`, `sort`, `type`) VALUES
(1, 12, '21733149848272812.jpg', 0, 'image'),
(2, 13, '77887149848279213.jpg', 0, 'image'),
(3, 14, '60110149848284814.jpg', 0, 'image');

-- --------------------------------------------------------

--
-- Структура таблиці `content_images_info`
--

CREATE TABLE IF NOT EXISTS `content_images_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `images_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_images_info_images_id_foreign` (`images_id`),
  KEY `content_images_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `content_info`
--

CREATE TABLE IF NOT EXISTS `content_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `content_info_content_id_foreign` (`content_id`),
  KEY `content_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=53 ;

--
-- Дамп даних таблиці `content_info`
--

INSERT INTO `content_info` (`id`, `content_id`, `languages_id`, `name`, `alias`, `title`, `keywords`, `description`, `content`) VALUES
(1, 1, 2, 'GStuff', '', 'GStuff', '', '', ''),
(2, 1, 1, 'GStuff', '', 'GStuff', '', '', ''),
(3, 1, 3, 'GStuff', '', 'GStuff', '', '', ''),
(4, 1, 4, 'GStuff', '', 'GStuff', '', '', ''),
(5, 2, 2, 'Services', 'services', 'Services', '', '', ''),
(6, 2, 1, 'Services', 'services', 'Services', '', '', ''),
(7, 2, 3, 'Services', 'services', 'Services', '', '', ''),
(8, 2, 4, 'Services', 'services', 'Services', '', '', ''),
(9, 3, 2, 'Web Develpment', 'services/web-develpment', 'Web Develpment', '', '', ''),
(10, 3, 1, 'Web Develpment', 'services/web-develpment', 'Web Develpment', '', '', ''),
(11, 3, 3, 'Web Develpment', 'services/web-develpment', 'Web Develpment', '', '', ''),
(12, 3, 4, 'Web Develpment', 'services/web-develpment', 'Web Develpment', '', '', ''),
(13, 4, 2, 'Mobile Apps', 'services/mobile-apps', 'Mobile Apps', '', '', ''),
(14, 4, 1, 'Mobile Apps', 'services/mobile-apps', 'Mobile Apps', '', '', ''),
(15, 4, 3, 'Mobile Apps', 'services/mobile-apps', 'Mobile Apps', '', '', ''),
(16, 4, 4, 'Mobile Apps', 'services/mobile-apps', 'Mobile Apps', '', '', ''),
(17, 5, 2, 'Creative Design', 'services/creative-design', 'Creative Design', '', '', ''),
(18, 5, 1, 'Creative Design', 'services/creative-design', 'Creative Design', '', '', ''),
(19, 5, 3, 'Creative Design', 'services/creative-design', 'Creative Design', '', '', ''),
(20, 5, 4, 'Creative Design', '', 'Creative Design', '', '', ''),
(21, 6, 2, 'Life time support', 'services/life-time-support', 'Life time support', '', '', ''),
(22, 6, 1, 'Life time support', 'services/life-time-support', 'Life time support', '', '', ''),
(23, 6, 3, 'Life time support', '', 'Life time support', '', '', ''),
(24, 6, 4, 'Life time support', 'services/life-time-support', 'Life time support', '', '', ''),
(25, 8, 2, 'Technology', 'technology', 'Technology', '', '', ''),
(26, 8, 1, 'Technology', 'technology', 'Technology', '', '', ''),
(27, 8, 3, 'Technology', 'technology', 'Technology', '', '', ''),
(28, 8, 4, 'Technology', 'technology', 'Technology', '', '', ''),
(29, 9, 2, 'Work', 'work', 'Work', '', '', ''),
(30, 9, 1, 'Work', 'work', 'Work', '', '', ''),
(31, 9, 3, 'Work', 'work', 'Work', '', '', ''),
(32, 9, 4, 'Work', 'work', 'Work', '', '', ''),
(33, 10, 2, 'About', 'about', 'About', '', '', ''),
(34, 10, 1, 'About', 'about', 'About', '', '                            {}\n', '<h2>We are <span class="line">good stuff</span></h2>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum, facilis.</p>\n'),
(35, 10, 3, 'About', 'about', 'About', '', '', ''),
(36, 10, 4, 'About', 'about', 'About', '', '', ''),
(37, 11, 2, 'Blog', 'blog', 'Blog', '', '', ''),
(38, 11, 1, 'Blog', 'blog', 'Blog', '', '', ''),
(39, 11, 3, 'Blog', 'blog', 'Blog', '', '', ''),
(40, 11, 4, 'Blog', 'blog', 'Blog', '', '', ''),
(41, 12, 2, 'FrontEnd', 'technology/frontend', 'FrontEnd', '', '', ''),
(42, 12, 1, 'FrontEnd', 'technology/frontend', 'FrontEnd', '', '', ''),
(43, 12, 3, 'FrontEnd', 'technology/frontend', 'FrontEnd', '', '', ''),
(44, 12, 4, 'FrontEnd', 'technology/frontend', 'FrontEnd', '', '', ''),
(45, 13, 2, 'BackEnd', 'technology/backend', 'BackEnd', '', '', ''),
(46, 13, 1, 'BackEnd', 'technology/backend', 'BackEnd', '', '', ''),
(47, 13, 3, 'BackEnd', 'technology/backend', 'BackEnd', '', '', ''),
(48, 13, 4, 'BackEnd', 'technology/backend', 'BackEnd', '', '', ''),
(49, 14, 2, 'CMS', 'technology/cms', 'CMS', '', '', ''),
(50, 14, 1, 'CMS', 'technology/cms', 'CMS', '', '', ''),
(51, 14, 3, 'CMS', 'technology/cms', 'CMS', '', '', ''),
(52, 14, 4, 'CMS', 'technology/cms', 'CMS', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблиці `content_templates`
--

CREATE TABLE IF NOT EXISTS `content_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `path` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_templates_type_id_foreign` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Дамп даних таблиці `content_templates`
--

INSERT INTO `content_templates` (`id`, `type_id`, `path`, `name`, `description`, `main`) VALUES
(1, 1, 'default', 'Default', 'Default Template', 1),
(2, 2, 'default', 'Default', 'Default Template', 1),
(3, 3, 'default', 'Default', 'Default Template', 1),
(4, 4, 'default', 'Default', 'Default Template', 1),
(5, 5, 'default', 'Default', 'Default Template', 1),
(6, 6, 'default', 'Default', 'Default Template', 1),
(7, 1, 'home', 'Home', 'Home                \n                    ', NULL),
(8, 1, 'about', 'About', 'About                \n                    ', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `content_type`
--

CREATE TABLE IF NOT EXISTS `content_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Дамп даних таблиці `content_type`
--

INSERT INTO `content_type` (`id`, `type`, `name`) VALUES
(1, 'page', 'Pages'),
(2, 'blog', 'Articles'),
(3, 'category', 'Product Category'),
(4, 'manufacturer', 'Manufacturer'),
(5, 'product', 'Product'),
(6, 'postCat', 'Articles Categories');

-- --------------------------------------------------------

--
-- Структура таблиці `content_type_features`
--

CREATE TABLE IF NOT EXISTS `content_type_features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(10) unsigned NOT NULL,
  `content_type_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_type_features_content_type_id_foreign` (`content_type_id`),
  KEY `content_type_features_features_id_foreign` (`features_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `content_type_images`
--

CREATE TABLE IF NOT EXISTS `content_type_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_type_id` int(10) unsigned NOT NULL,
  `images_sizes_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_type_images_content_type_id_foreign` (`content_type_id`),
  KEY `content_type_images_images_sizes_id_foreign` (`images_sizes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` decimal(7,3) DEFAULT NULL,
  `is_main` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `dashboard_quick_launch`
--

CREATE TABLE IF NOT EXISTS `dashboard_quick_launch` (
  `id` int(11) NOT NULL,
  `components_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  KEY `dashboard_quick_launch_components_id_foreign` (`components_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `free_from` decimal(10,2) NOT NULL DEFAULT '0.00',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `delivery_info`
--

CREATE TABLE IF NOT EXISTS `delivery_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `delivery_info_delivery_id_foreign` (`delivery_id`),
  KEY `delivery_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `delivery_payment`
--

CREATE TABLE IF NOT EXISTS `delivery_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `delivery_payment_delivery_id_foreign` (`delivery_id`),
  KEY `delivery_payment_payment_id_foreign` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `email_templates`
--

INSERT INTO `email_templates` (`id`, `code`, `name`, `email`) VALUES
(1, 'getintouch', 'Get in touch', 'bania20091@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблиці `email_templates_info`
--

CREATE TABLE IF NOT EXISTS `email_templates_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `templates_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `reply_to` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email_templates_info_templates_id_foreign` (`templates_id`),
  KEY `email_templates_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `email_templates_info`
--

INSERT INTO `email_templates_info` (`id`, `templates_id`, `languages_id`, `reply_to`, `subject`, `body`) VALUES
(1, 1, 1, 'Gstuff', 'Get in touch', 'hello world'),
(2, 1, 2, 'Gstuff', 'Get in touch', 'hello world'),
(3, 1, 3, 'Gstuff', 'Get in touch', 'hello world'),
(4, 1, 4, 'Gstuff', 'Get in touch', 'hello world');

-- --------------------------------------------------------

--
-- Структура таблиці `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('text','checkbox','radiogroup','textarea','checkboxgroup','select','sm') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `show_list` tinyint(4) NOT NULL DEFAULT '0',
  `show_compare` tinyint(4) NOT NULL DEFAULT '0',
  `show_filter` tinyint(4) NOT NULL DEFAULT '0',
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  `owner_id` int(10) unsigned NOT NULL,
  `extends` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `features_owner_id_foreign` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `features_content_values`
--

CREATE TABLE IF NOT EXISTS `features_content_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `features_values_id` int(10) unsigned NOT NULL,
  `value` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `features_content_values_content_id_foreign` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `features_info`
--

CREATE TABLE IF NOT EXISTS `features_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `features_info_features_id_foreign` (`features_id`),
  KEY `features_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `features_values`
--

CREATE TABLE IF NOT EXISTS `features_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `features_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `features_values_features_id_foreign` (`features_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `features_values_info`
--

CREATE TABLE IF NOT EXISTS `features_values_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `values_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `features_values_info_values_id_foreign` (`values_id`),
  KEY `features_values_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `guides`
--

CREATE TABLE IF NOT EXISTS `guides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `guides_info`
--

CREATE TABLE IF NOT EXISTS `guides_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guides_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `guides_info_guides_id_foreign` (`guides_id`),
  KEY `guides_info_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `images_sizes`
--

CREATE TABLE IF NOT EXISTS `images_sizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `crop` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `front` tinyint(4) DEFAULT '0',
  `back` tinyint(4) DEFAULT '0',
  `front_default` tinyint(4) DEFAULT '0',
  `back_default` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `front`, `back`, `front_default`, `back_default`) VALUES
(1, 'en', 'En', 1, 0, 1, 0),
(2, 'uk', 'Укр', 1, 0, 0, 1),
(3, 'de', 'De', 1, 0, 0, 0),
(4, 'ru', 'Рус', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_10_20_000000_create_chunks_table', 1),
('2016_10_20_000001_create_comments_table', 1),
('2016_10_20_000002_create_components_table', 1),
('2016_10_20_000003_create_components_info_table', 1),
('2016_10_20_000004_create_content_table', 1),
('2016_10_20_000005_create_content_features_table', 1),
('2016_10_20_000006_create_content_images_table', 1),
('2016_10_20_000007_create_content_images_info_table', 1),
('2016_10_20_000008_create_content_info_table', 1),
('2016_10_20_000009_create_content_templates_table', 1),
('2016_10_20_000010_create_content_type_table', 1),
('2016_10_20_000011_create_content_type_features_table', 1),
('2016_10_20_000012_create_content_type_images_table', 1),
('2016_10_20_000013_create_currency_table', 1),
('2016_10_20_000014_create_dashboard_quick_launch_table', 1),
('2016_10_20_000015_create_delivery_table', 1),
('2016_10_20_000016_create_delivery_info_table', 1),
('2016_10_20_000017_create_delivery_payment_table', 1),
('2016_10_20_000018_create_email_templates_table', 1),
('2016_10_20_000019_create_email_templates_info_table', 1),
('2016_10_20_000020_create_features_table', 1),
('2016_10_20_000021_create_features_content_values_table', 1),
('2016_10_20_000022_create_features_info_table', 1),
('2016_10_20_000023_create_features_values_table', 1),
('2016_10_20_000024_create_features_values_info_table', 1),
('2016_10_20_000025_create_guides_table', 1),
('2016_10_20_000026_create_guides_info_table', 1),
('2016_10_20_000027_create_images_sizes_table', 1),
('2016_10_20_000028_create_languages_table', 1),
('2016_10_20_000029_create_modules_table', 1),
('2016_10_20_000030_create_nav_menu_table', 1),
('2016_10_20_000031_create_nav_menu_items_table', 1),
('2016_10_20_000032_create_payment_table', 1),
('2016_10_20_000033_create_payment_info_table', 1),
('2016_10_20_000034_create_plugins_table', 1),
('2016_10_20_000035_create_plugins_structure_table', 1),
('2016_10_20_000036_create_posts_categories_table', 1),
('2016_10_20_000037_create_products_categories_table', 1),
('2016_10_20_000038_create_products_options_table', 1),
('2016_10_20_000039_create_products_related_table', 1),
('2016_10_20_000040_create_promo_codes_table', 1),
('2016_10_20_000041_create_routers_table', 1),
('2016_10_20_000042_create_settings_table', 1),
('2016_10_20_000043_create_tags_table', 1),
('2016_10_20_000044_create_tags_content_table', 1),
('2016_10_20_000045_create_translations_table', 1),
('2016_10_20_000046_create_translations_info_table', 1),
('2016_10_20_000047_create_users_table', 1),
('2016_10_20_000048_create_users_group_table', 1),
('2016_10_20_000049_create_users_group_info_table', 1),
('2016_10_28_190044_add_avatar_to_users', 1),
('2016_10_31_071350_create_pages_views_table', 1),
('2016_11_02_201931_rename_dates_colums_users', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `nav_menu`
--

CREATE TABLE IF NOT EXISTS `nav_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `auto_add_pages` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `nav_menu`
--

INSERT INTO `nav_menu` (`id`, `name`, `auto_add_pages`) VALUES
(1, 'Main', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `nav_menu_items`
--

CREATE TABLE IF NOT EXISTS `nav_menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nav_menu_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nav_menu_items_content_id_foreign` (`content_id`),
  KEY `nav_menu_items_nav_menu_id_foreign` (`nav_menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп даних таблиці `nav_menu_items`
--

INSERT INTO `nav_menu_items` (`id`, `nav_menu_id`, `content_id`, `sort`) VALUES
(1, 1, 8, 0),
(2, 1, 9, 1),
(3, 1, 10, 2),
(4, 1, 2, 3),
(5, 1, 11, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `pages_views`
--

CREATE TABLE IF NOT EXISTS `pages_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `ip` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pages_views_content_id_foreign` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Дамп даних таблиці `pages_views`
--

INSERT INTO `pages_views` (`id`, `content_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, '127.0.0.1', '2017-06-26 06:19:02', '2017-06-26 06:19:02'),
(2, 1, '176.104.184.149', '2017-06-26 19:37:03', '2017-06-26 19:37:03'),
(3, 1, '194.44.216.55', '2017-06-26 19:39:35', '2017-06-26 19:39:35'),
(4, 10, '127.0.0.1', '2017-06-26 10:42:15', '2017-06-26 10:42:15'),
(5, 8, '127.0.0.1', '2017-06-26 11:35:33', '2017-06-26 11:35:33'),
(6, 10, '176.104.184.149', '2017-06-26 22:02:43', '2017-06-26 22:02:43'),
(7, 1, '37.73.254.101', '2017-06-26 23:24:54', '2017-06-26 23:24:54'),
(8, 1, '75.149.221.170', '2017-06-26 23:48:27', '2017-06-26 23:48:27'),
(9, 1, '178.95.201.59', '2017-06-27 01:05:02', '2017-06-27 01:05:02'),
(10, 10, '178.95.201.59', '2017-06-27 01:05:57', '2017-06-27 01:05:57'),
(11, 1, '66.249.93.214', '2017-06-27 02:02:05', '2017-06-27 02:02:05'),
(12, 1, '77.123.175.7', '2017-06-27 02:02:08', '2017-06-27 02:02:08'),
(13, 10, '77.123.175.7', '2017-06-27 02:03:16', '2017-06-27 02:03:16'),
(14, 8, '77.123.175.7', '2017-06-27 02:03:19', '2017-06-27 02:03:19'),
(15, 11, '77.123.175.7', '2017-06-27 02:04:05', '2017-06-27 02:04:05'),
(16, 2, '77.123.175.7', '2017-06-27 02:04:09', '2017-06-27 02:04:09'),
(17, 3, '77.123.175.7', '2017-06-27 02:04:12', '2017-06-27 02:04:12'),
(18, 12, '77.123.175.7', '2017-06-27 02:04:16', '2017-06-27 02:04:16'),
(19, 9, '77.123.175.7', '2017-06-27 02:04:20', '2017-06-27 02:04:20');

-- --------------------------------------------------------

--
-- Структура таблиці `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency_id` int(10) unsigned NOT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `module` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sort` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `payment_currency_id_foreign` (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `payment_info`
--

CREATE TABLE IF NOT EXISTS `payment_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `payment_info_languages_id_foreign` (`languages_id`),
  KEY `payment_info_payment_id_foreign` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `position` enum('left','right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right',
  `settings` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `plugins_structure`
--

CREATE TABLE IF NOT EXISTS `plugins_structure` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `components_id` int(10) unsigned NOT NULL,
  `plugins_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plugins_structure_components_id_foreign` (`components_id`),
  KEY `plugins_structure_plugins_id_foreign` (`plugins_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `posts_categories`
--

CREATE TABLE IF NOT EXISTS `posts_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_categories_cid_foreign` (`cid`),
  KEY `posts_categories_pid_foreign` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `products_categories`
--

CREATE TABLE IF NOT EXISTS `products_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_categories_cid_foreign` (`cid`),
  KEY `products_categories_pid_foreign` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `products_options`
--

CREATE TABLE IF NOT EXISTS `products_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` int(10) unsigned NOT NULL,
  `manufacturers_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability` tinyint(4) NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `sale` tinyint(4) NOT NULL,
  `hit` tinyint(4) NOT NULL,
  `new` tinyint(4) NOT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '0',
  `is_variant` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `products_options_products_id_foreign` (`products_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `products_related`
--

CREATE TABLE IF NOT EXISTS `products_related` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` int(10) unsigned NOT NULL,
  `related_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_related_products_id_foreign` (`products_id`),
  KEY `products_related_related_id_foreign` (`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `promo_codes`
--

CREATE TABLE IF NOT EXISTS `promo_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `expire` date DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `type` enum('abs','per') COLLATE utf8_unicode_ci DEFAULT 'abs',
  `minp` decimal(10,2) DEFAULT NULL,
  `single` tinyint(4) DEFAULT '0',
  `usages` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `routers`
--

CREATE TABLE IF NOT EXISTS `routers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` enum('HTTP/1.1 301 Moved Permanently','HTTP/1.1 307 Temporary Redirect','HTTP/1.1 302 Moved Temporarily') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'HTTP/1.1 301 Moved Permanently',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autoload` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Дамп даних таблиці `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `description`, `autoload`) VALUES
(1, 'autofil_title', '1', 'Автоматично заповнювати title в стоірнках на основі назви', 1),
(2, 'autotranslit_alias', '1', 'Автоматично генерувати аліас', 1),
(3, 'editor_language', 'uk', 'Мова редактора по замовчуванню', 1),
(4, 'editor_bodyId', 'cms_content', 'ID body в шаблоні', 1),
(5, 'editor_bodyClass', 'cms_content', 'Клас контенту для редкатора', 1),
(6, 'editor_contentsCss', 'css/style.css', 'Шлях до стилів сайту, можна задавати через кому декілька', 1),
(7, 'products_list_update_price', '1', 'Оновляти ціни в списку товарів', 1),
(8, 'languages_create_info', '1', 'При створенні мови, автоматично згенерувати переклади у всіх розділах системи', 1),
(9, 'app_theme_current', 'gstuff', 'Тема по замовчуванню для сайту', 1),
(10, 'app_views_path', 'views/', 'Шлях до шаблонів', 1),
(11, 'app_chunks_path', 'chunks/', 'Шлях до чанків', 1),
(12, 'themes_path', 'public/', 'Папка з темами', 1),
(13, 'content_images_dir', '/uploads/content/', 'Шлях до зображень', 1),
(14, 'content_images_thumb_dir', 'thumb/', 'Шлях до превюшок', 1),
(15, 'content_images_source_dir', 'source/', 'Шлях до source', 1),
(16, 'admin_theme_current', 'admin', 'Активна тема для адмінки', 1),
(17, 'mod_path', '\\app\\Http\\Controllers\\modules\\', 'Абсолютний шлях до модулів', 1),
(18, 'page_404', '12', 'Ід сторінки 404', 1),
(19, 'img_source_size', '1600x1200', 'Розмір зображень source по замовчуванню', 1),
(20, 'et_header', '', 'Глобальний header', 1),
(21, 'et_footer', '', 'Глобальний footer', 1),
(22, 'google_ananytics_id', '', 'Google Analytics ID', 1),
(23, 'google_webmaster_id', '', 'G.Webmaster ID', 1),
(24, 'yandex_webmaster_id', '', 'Yandex Webmaster ID', 1),
(25, 'yandex_metrika', '', 'Yandex Metrika ID', 1),
(26, 'version', '0.1', 'Версія змін ядра.бд.системи', 1),
(27, 'version_update', '1', 'Автоматичне оновлення версій системи', 1),
(28, 'water_position', 'bottom', 'Water element', 1),
(29, 'facebook_app_id', '', 'FB app Id', 1),
(30, 'facebook_app_secret', '', 'FB app secret Id', 1),
(31, 'install_theme_path', 'install', 'Шлях теми встановлення', 1),
(32, 'install_title', 'Встановлення LarAdmin', 'Встановлення LarAdmin', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tags_languages_id_foreign` (`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `tags_content`
--

CREATE TABLE IF NOT EXISTS `tags_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) unsigned NOT NULL,
  `tags_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tags_content_content_id_foreign` (`content_id`),
  KEY `tags_content_tags_id_foreign` (`tags_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `translations`
--

CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_code_unique` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `translations`
--

INSERT INTO `translations` (`id`, `code`) VALUES
(1, 'get_in_touch');

-- --------------------------------------------------------

--
-- Структура таблиці `translations_info`
--

CREATE TABLE IF NOT EXISTS `translations_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `translations_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `translations_info_languages_id_foreign` (`languages_id`),
  KEY `translations_info_translations_id_foreign` (`translations_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `translations_info`
--

INSERT INTO `translations_info` (`id`, `translations_id`, `languages_id`, `value`) VALUES
(1, 1, 1, 'Let''s get in touch'),
(2, 1, 2, 'Давайте зв''яжемося'),
(3, 1, 3, 'Nehmen wir in Kontakt'),
(4, 1, 4, 'Давайте связываться');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_group_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `sessid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skey` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_languages_id_foreign` (`languages_id`),
  KEY `users_users_group_id_foreign` (`users_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `users_group_id`, `languages_id`, `sessid`, `address`, `name`, `phone`, `email`, `password`, `skey`, `created_at`, `updated_at`, `avatar`) VALUES
(1, 1, 2, 'a22cd4e6cd20143fd1ae3b872c6377a6a569db5c', NULL, 'Taras Bodnar', NULL, 'bania20091@gmail.com', '$2y$10$03gHcK.SZ62H7nAjNP/epeKlCB.T3rGu/3VRyDqPOw7sn4s4Tcda6', NULL, NULL, '2017-06-26 19:18:56', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `users_group`
--

CREATE TABLE IF NOT EXISTS `users_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` tinyint(4) NOT NULL,
  `rang` smallint(6) NOT NULL,
  `sort` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `users_group`
--

INSERT INTO `users_group` (`id`, `parent_id`, `rang`, `sort`) VALUES
(1, 0, 999, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `users_group_info`
--

CREATE TABLE IF NOT EXISTS `users_group_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_group_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `users_group_info_languages_id_foreign` (`languages_id`),
  KEY `users_group_info_users_group_id_foreign` (`users_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп даних таблиці `users_group_info`
--

INSERT INTO `users_group_info` (`id`, `users_group_id`, `languages_id`, `name`, `description`) VALUES
(1, 1, 2, 'Administrator', 'Administrator'),
(2, 1, 1, 'Administrator', 'Administrator');

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `components_info`
--
ALTER TABLE `components_info`
  ADD CONSTRAINT `components_info_components_id_foreign` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `components_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `content_templates_id_foreign` FOREIGN KEY (`templates_id`) REFERENCES `content_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `content_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Обмеження зовнішнього ключа таблиці `content_features`
--
ALTER TABLE `content_features`
  ADD CONSTRAINT `content_features_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_features_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_images`
--
ALTER TABLE `content_images`
  ADD CONSTRAINT `content_images_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_images_info`
--
ALTER TABLE `content_images_info`
  ADD CONSTRAINT `content_images_info_images_id_foreign` FOREIGN KEY (`images_id`) REFERENCES `content_images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_images_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_info`
--
ALTER TABLE `content_info`
  ADD CONSTRAINT `content_info_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_templates`
--
ALTER TABLE `content_templates`
  ADD CONSTRAINT `content_templates_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_type_features`
--
ALTER TABLE `content_type_features`
  ADD CONSTRAINT `content_type_features_content_type_id_foreign` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_type_features_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_type_images`
--
ALTER TABLE `content_type_images`
  ADD CONSTRAINT `content_type_images_content_type_id_foreign` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_type_images_images_sizes_id_foreign` FOREIGN KEY (`images_sizes_id`) REFERENCES `images_sizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `dashboard_quick_launch`
--
ALTER TABLE `dashboard_quick_launch`
  ADD CONSTRAINT `dashboard_quick_launch_components_id_foreign` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `delivery_info`
--
ALTER TABLE `delivery_info`
  ADD CONSTRAINT `delivery_info_delivery_id_foreign` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `delivery_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `delivery_payment`
--
ALTER TABLE `delivery_payment`
  ADD CONSTRAINT `delivery_payment_delivery_id_foreign` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `delivery_payment_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `email_templates_info`
--
ALTER TABLE `email_templates_info`
  ADD CONSTRAINT `email_templates_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `email_templates_info_templates_id_foreign` FOREIGN KEY (`templates_id`) REFERENCES `email_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `features_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_content_values`
--
ALTER TABLE `features_content_values`
  ADD CONSTRAINT `features_content_values_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_info`
--
ALTER TABLE `features_info`
  ADD CONSTRAINT `features_info_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `features_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_values`
--
ALTER TABLE `features_values`
  ADD CONSTRAINT `features_values_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_values_info`
--
ALTER TABLE `features_values_info`
  ADD CONSTRAINT `features_values_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `features_values_info_values_id_foreign` FOREIGN KEY (`values_id`) REFERENCES `features_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `guides_info`
--
ALTER TABLE `guides_info`
  ADD CONSTRAINT `guides_info_guides_id_foreign` FOREIGN KEY (`guides_id`) REFERENCES `guides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guides_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `nav_menu_items`
--
ALTER TABLE `nav_menu_items`
  ADD CONSTRAINT `nav_menu_items_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nav_menu_items_nav_menu_id_foreign` FOREIGN KEY (`nav_menu_id`) REFERENCES `nav_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `pages_views`
--
ALTER TABLE `pages_views`
  ADD CONSTRAINT `pages_views_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `payment_info`
--
ALTER TABLE `payment_info`
  ADD CONSTRAINT `payment_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_info_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `plugins_structure`
--
ALTER TABLE `plugins_structure`
  ADD CONSTRAINT `plugins_structure_components_id_foreign` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `plugins_structure_plugins_id_foreign` FOREIGN KEY (`plugins_id`) REFERENCES `plugins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD CONSTRAINT `posts_categories_cid_foreign` FOREIGN KEY (`cid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_categories_pid_foreign` FOREIGN KEY (`pid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_categories`
--
ALTER TABLE `products_categories`
  ADD CONSTRAINT `products_categories_cid_foreign` FOREIGN KEY (`cid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_categories_pid_foreign` FOREIGN KEY (`pid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_options`
--
ALTER TABLE `products_options`
  ADD CONSTRAINT `products_options_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_related`
--
ALTER TABLE `products_related`
  ADD CONSTRAINT `products_related_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_related_related_id_foreign` FOREIGN KEY (`related_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tags_content`
--
ALTER TABLE `tags_content`
  ADD CONSTRAINT `tags_content_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tags_content_tags_id_foreign` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `translations_info`
--
ALTER TABLE `translations_info`
  ADD CONSTRAINT `translations_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `translations_info_translations_id_foreign` FOREIGN KEY (`translations_id`) REFERENCES `translations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_users_group_id_foreign` FOREIGN KEY (`users_group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `users_group_info`
--
ALTER TABLE `users_group_info`
  ADD CONSTRAINT `users_group_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_group_info_users_group_id_foreign` FOREIGN KEY (`users_group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
