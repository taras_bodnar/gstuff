/**
 * Created by taras on 26.06.17.
 */
var Feedback = {
    init: function() {
        this.load();
        this.loadGetStart();
        this.removeErrors();
    },
    load : function() {
        $('.link-popup-contact-us').on('click', function(e){
            e.preventDefault();
            App.request.get('/request/Feedback/index',function(response){
                $('body').append(response);
                $('#'+$(response).attr('id')).addClass('open');
                $('html').addClass('no-scroll');
                //app.styleSelect();
                app.btnClosePopup();
                Feedback.process();
                Feedback.removeErrors();
            });
            return false;
        });
    },
    process: function () {
        var bSubmit = $('.b-submit, .b-form-save, .btn-full');
        $("#get_in_touch").ajaxForm({
            success: function (response) {
                bSubmit.removeAttr('disabled');
                $("#get_in_touch").removeClass('preloader');

                var obj = $.parseJSON(response);
                console.log(obj);
                if(!obj.s) {
                    var keys = Object.keys(obj.errors);
                    for(var i=0;i<keys.length;i++) {
                        var attrName = keys[i];
                        //
                        var text = '<label class="label-error">'+obj.errors[keys[i]]+'</label>';
                        if(!$("#get_in_touch").find('[name="data['+attrName+']"]').hasClass('error')) {
                            $("#get_in_touch").find('[name="data['+attrName+']"]').closest('div').addClass('error');
                        }
                    }
                } else {
                    Feedback.notify(obj.sm);
                    // console.log(response);
                    $('.popup-close').trigger('click');
                    $("#get_in_touch")[0].reset();
                }
            },
            beforeSend: function(){
                bSubmit.attr('disabled', true);
                $("#get_in_touch").addClass('preloader');
            }
        })
    },
    loadGetStart:function() {
        $('.lt-getstarted-popup').on('click', function(e){
            e.preventDefault();
            App.request.get('/request/Feedback/getStart',function(response){
                $('body').append(response);
                $('#'+$(response).attr('id')).addClass('open');
                $('html').addClass('no-scroll');
                app.styleSelect();
                app.sliderPrice();
                app.btnClosePopup();
                Feedback.processGetStart();
                Feedback.removeErrors();
            });
            return false;
        });
    },
    processGetStart: function () {
        var bSubmit = $('.b-submit, .b-form-save, .btn-full');
        $("#get_start").ajaxForm({
            success: function (response) {
                bSubmit.removeAttr('disabled');
                $("#get_start").removeClass('preloader');

                var obj = $.parseJSON(response);
                if(!obj.s) {
                    var keys = Object.keys(obj.errors);
                    for(var i=0;i<keys.length;i++) {
                        var attrName = keys[i];
                        //
                        var text = '<label class="label-error">'+obj.errors[keys[i]]+'</label>';
                        if(!$("#get_start").find('[name="data['+attrName+']"]').hasClass('error')) {
                            $("#get_start").find('[name="data['+attrName+']"]').closest('div').addClass('error');
                        }
                    }
                } else {
                    Feedback.notify(obj.sm);
                    $('.popup-close').trigger('click');
                    $("#get_start")[0].reset();
                }
            },
            beforeSend: function(){
                bSubmit.attr('disabled', true);
                $("#get_in_touch").addClass('preloader');
            }
        })
    },
    notify : function(text){
        new PNotify({
            title: false,
            text: text,
            delay: 5000,
            icon: true,
            // hide: false,
            buttons: {
                closer: true
            },
            after_open:function(){
                $('html').addClass('opacity');
            },
            after_close:function(){
                $('html').removeClass('opacity');
            }
        });
        return false;
    }, 
    removeErrors: function() {
        $('.input-group input, .textarea-group textarea').keypress(function(e){
            if($(this).parent().hasClass('error')) {
                $(this).parent().removeClass('error');
                $(this).siblings('.label-error').remove();
            }
        });
    }
}

$(document).ready(function(){
    Feedback.init();
});



var App = {
    request:  {
        get: function(url, success, dataType)
        {
            //var data =  {token: TOKEN};
            return $.ajax({
                url      : url,
                //data     : data,
                success  : success,
                dataType : dataType,
                type     : 'get',
                beforeSend: function(request)
                {
                    //request.setRequestHeader("app-languages-id", LANG_ID);
                }
            })
        },
        post: function(data)
        {
            if(typeof data['data'] == 'undefined') {
                data['data'] = {};
            }
            data['type']       = 'post';
            data['beforeSend'] = function(request)
            {
                //request.setRequestHeader("app-languages-id", LANG_ID);
            };
            return $.ajax(data)
        }
    }
}