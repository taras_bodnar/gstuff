var app = {
    ww: function() {
        return $(window).outerWidth()
    },

    wh: function() {
        return $(window).outerHeight()
    },

    keepFooter: function() {
        var content = $('#content'),
            header = $('#header'),
            footer = $('#footer'),

            hh = header.outerHeight(),
            fh = footer.height();

        console.log(hh);

        content.css({
            'paddingBottom': fh + 'px',
            'paddingTop': hh + 'px',
        });

        $('.lt-main-screen').css({
            'height': this.wh() - hh + 'px'
        });

        footer.css({
            'marginTop': -fh + 'px'
        });
    },

    fixedHeader: function(){
        var header = $('#header');
        if ($(window).scrollTop() > 0) {
            header.addClass('fixed');
        } else {
            header.removeClass('fixed');
        }
    },

    autoHeight: function(items){
        var maxHeight, heights;

        heights = items.map(function () {
            return $(this).innerHeight();
        }).get();

        maxHeight = Math.max.apply(null, heights);

        items.css({
            height: maxHeight
        });


    },

    alignElements: function(){
        if(this.ww() > 768){
            // this.autoHeight( $('.lt-advantages h4') );
        }
    },

    showBlocks: function(){
        var el = $('.fade-in');

        function checkBlock(elems){
            elems.each(function(){
                var current = $(this);

                if( $(window).scrollTop() >= current.offset().top - $(window).height() ){
                    current.addClass('show');
                    $(window).trigger('resize');
                } else {
                    current.removeClass('show');
                }
            });
        }

        checkBlock(el);

        $(document).scroll(function(){
            checkBlock(el);
        });
    },

    playVideo: function(){
        $('.lt-video-tools').on('click', function(){
            //console.log('focus');

            App.request.post({
                url: 'request/Pages/loadVideo',
                success: function (response) {
                    //console.log(response);
                    $('body').append(response);
                    $('#lt-video-popup').fadeIn().addClass('open');
                    $('html').addClass('no-scroll');
                    $('#lt-video-popup').find('.icon-close').on('click', function (e) {
                        console.log("here");
                        e.preventDefault();
                        $('#lt-video-popup').fadeOut();
                        $('#lt-video-popup').remove();
                        $('html').removeClass('no-scroll');
                    });


                }
            });
            // $.fancybox($('#lt-video-popup'),{
            //     autoDimision: true,
            //     padding: 0,
            //     margin: [0, 0, 0, 0],
            //     helpers: {
            //         overlay: {
            //             //closeClick: false,
            //             css: {
            //                 'background': 'rgba(50,58,69,.9)'
            //             }
            //         }
            //     },
            //     afterShow: function () {
            //         $('.fancybox-close').append('<i class="icon-close"></i>');
            //     }
            // });
        });
    },

    btnOffset: $('.screen-content .btn').length ? $('.screen-content .btn').offset().top : 0,

    fixedButton: function(){
        var el = $('.screen-content .btn');

        if(el.length){
            if( $(window).scrollTop() >= this.btnOffset ){
                el.addClass('fixed');
            } else {
                el.removeClass('fixed');
            }
        }
    },

    masonry: function(){
        var $grid = $('.grid').masonry({
          itemSelector: '.grid-item',
          percentPosition: true,
          columnWidth: '.grid-sizer'
        });
        
        $grid.imagesLoaded().progress( function() {
          $grid.masonry();
        });
    },

    setGradients: function(){
        var g = $('.grid-img .gradient');

        g.each(function(){            
            var el = $(this);
                cStart = el.data('color-start'),
                cEnd = el.data('color-end');

            el.css({
                "background": "linear-gradient(135deg, " + cStart + " 0%,"+ cEnd +" 100%)"
            });          
        });
    },

    headerNav: function (){
        var links =  $('.header-nav li > a');
        
        links.on('click', function(){
            var el = $(this);
            
            if( el.next().hasClass('header-widget') ){
                if( app.ww() < 768 ){
                    if( el.hasClass('active') ){
                        el.removeClass('active').next().slideUp();
                        return false;
                    }

                    $('.header-nav a.active').removeClass('active').next().slideUp();
                    el.toggleClass('active');
                    el.next().slideToggle();
                }
                return false;
            }
        });
    },

    btnNav: function(){
        $('#nav-btn').click(function(){
            var hh = $('#header').outerHeight(),
                hr = $('.header-right');

            $(this).toggleClass('open');
            hr.toggleClass('open');
            $('html').toggleClass('no-scroll');
        });
    },

    popupClose: function (){
        $('.lt-popup.open').removeClass('open');
        $('html').removeClass('no-scroll');
        return false;
    },

    btnClosePopup: function(){
        $('.popup-close').on('click', function(){
            app.popupClose();
            return false;
        });
    },

    popup: function(){
        // $('.link-popup').on('click', function(){
            // var href = $(this).attr('href');
            // console.log('focus');
            // // $('#' + href ).addClass('open');
            // $('#' + href ).addClass('open');
            // $('html').addClass('no-scroll');
        //     app.styleSelect();
        //     return false;
        // });
    },


    sliderPrice:function(){
        console.log($("#slider-price").length);
        $("#slider-price").ionRangeSlider({
            type: "double",
            min: 0,
            max: 20000,
            from: 1500,
            from_percent: 25,
            from_value: null,
            to: 12000,
            hide_min_max: true,
            hide_from_to: false,
            from_min: 1000,
            prefix: "$"
        });
    },

    styleSelect: function(){
        var el = $(".style-select");
        if( !el.data('select2') ){
            el.select2({
                minimumResultsForSearch: -1
            });
        }
        return false;
    },

    isotop: function(){

        var $grid = $('.grid-izotop').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                columnWidth: '.grid-sizer'
            }
        });
        // filter functions
        var filterFns = {
          // show if number is greater than 50
          numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
          },
          // show if name ends with -ium
          ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
          }
        };
        // bind filter button click
        $('.filters-button-group').on( 'click', 'a', function() {
          var filterValue = $( this ).attr('data-filter');
          // use filterFn if matches value
          filterValue = filterFns[ filterValue ] || filterValue;
          $grid.isotope({ filter: filterValue });
          return false;
        });
        // change is-checked class on buttons
        $('.filters-button-group').each( function( i, buttonGroup ) {
          var $buttonGroup = $( buttonGroup );
          $buttonGroup.on( 'click', 'a', function() {
            $buttonGroup.find('.active').removeClass('active');
            $( this ).addClass('active');
            return false;
          });
        });
    },

    instaSlider:function(){
        var slider = $('.inst-slider'),
        cMod = false;

        // if(slider.find('.photo-item').length > 2){
        //     cMod = true;
        // }

        slider.slick({
            arrows: true,
            dots: false,
            // centerMode: true,
            prevArrow:'<i class="icon-arrow-left slick-prev"></i>',
            nextArrow:'<i class="icon-arrow-right slick-next"></i>',
            slidesToShow: 7,
            slidesToScroll: 7,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    },

    gridBlog: function(){
        var $grid = $('.grid-blog').masonry({
            itemSelector: '.grid-item-blog',
            percentPosition: true,
            gutter: 10,
            columnWidth: '.grid-sizer'
        });

        $grid.imagesLoaded().progress( function() {
            $grid.masonry();
        });  
    },
 
    //------------------------------------------------------------------------///
    init: function(){
        // this.autoHeight( $('.grid img'));
        this.keepFooter();
        this.alignElements();
        this.showBlocks();
        this.masonry();
        this.btnNav();
        this.headerNav(); 
        // this.fixedButton();
        this.playVideo();
        this.popup();
        this.sliderPrice();
        this.btnClosePopup();
        this.setGradients();
        this.isotop();
        this.instaSlider();
        this.gridBlog();
        this.fixedHeader();
    }
};


$(document).ready(function(){
    app.init();
    $('html').addClass('load');
});

$(document).scroll(function(){
    // app.fixedButton();
    app.fixedHeader();
});

$(window).resize(function(){
    app.keepFooter();
    app.alignElements();
});

$(window).on("load", function (e) {
    setTimeout(function(){
        $('.loader').animate({
            opacity: 0
        }, 300, function(){
            $('.loader').remove();
            $('html').removeClass('load');
        });
    }, 1000);
    // $('html').addClass('html-visible');
});
