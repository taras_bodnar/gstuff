{{--Created by Taras 2017-06-26 13:35--}}
<!DOCTYPE html>
<html lang="en">
<!--head-->
@include($template_url.'chunk/head')
<!--.head-->
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
@include($template_url.'chunk/header')
<!--.header-->
    <!-- content -->
    <main class="content" id="content">
        <!-- wrap-screen -->

        <section>
            <div class="container cms">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="lt-cms-title">
                            <h2>{{$page->name}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {mod:Blog::getAll}

        @include($template_url.'chunk/speak_to_us')

        @include($template_url.'chunk/instagram')
    </main>
    <!-- .content -->
</div>
<!--wrapper-->

@include($template_url.'chunk/footer')
</body>
</html>