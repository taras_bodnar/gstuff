{{--Created by Taras 2017-07-03 05:31--}}
        <!DOCTYPE html>
<html lang="en">
<!--head-->
@include($template_url.'chunk/head')
        <!--.head-->
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
    @include($template_url.'chunk/header')
            <!--.header-->
    <!-- content -->
    <main class="content" id="content">
        <!-- wrap-screen -->

        <section>
            <div class="container cms">
                <div class="lt-cms-title">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="name-page">{{$t['wearegoodat']}}</div>
                            <h2>{{$page->name}}</h2>
                            <div class="page-text">
                                <p>
                                    {mod:Pages::getFeaturesValues({{$page->id}},4,textarea)}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row row-sub-pages">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </section>

        <section class="lt-cms fade-in lt-dark">
            <div class="container cms">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        {mod:Pages::getFeaturesValues({{$page->id}},5,textarea)}
                    </div>
                </div>
            </div>
        </section>

        <section>
            {mod:Pages::servicesInside}
        </section>

        <section class="lt-cms fade-in">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="lt-title">
                            <h2>{{$t['what_we_ve_done']}}</h2>
                        </div>
                    </div>
                </div>
                {mod:Projects::index}
            </div>
        </section>

        @include($template_url.'chunk/speak_to_us')

        @include($template_url.'chunk/instagram')
    </main>
    <!-- .content -->
</div>
<!--wrapper-->

@include($template_url.'chunk/footer')
</body>
</html>