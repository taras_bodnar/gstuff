{{--Created by Taras 2017-06-26 09:43--}}
<!DOCTYPE html>
<html lang="en">
<!--head-->
@include($template_url.'chunk/head')
<!--.head-->
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
    @include($template_url.'chunk/header')
    <!--.header-->
    <!-- content -->
    <main class="content" id="content">
        <!-- wrap-screen -->
        <section class= "lt-main-screen" style = "background-image: url('{{$assets_url}}assets/img/it.jpg')">
            <div class="table">
                <div class="table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="screen-content">
                                   {!! $t['baner_slogan'] !!}
                                    <!--<a href="lt-getstarted" class="btn btn-lg btn-primary lt-getstarted-popup">-->
                                    <a href="lt-getstarted" class="btn btn-lg btn-hover-wh link-popup lt-getstarted-popup">
                                        <span data-hover="{{$t['get_started']}}">{{$t['get_started']}}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {mod:Pages::services}

        <section class="lt-video">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="w-video">
                            <!-- <div class="wrap-video"> -->
                            <div id="video">
                                <img src="{{$assets_url}}assets/img/cover.jpg" alt="">
                            </div>
                            <div class="lt-video-tools">
                                <div class="table">
                                    <div class="table-cell">
                                        <i class="icon-play-button"></i>
                                        <div class="btn-play">{!! $t['watch_showrell'] !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>

        {mod:Pages::technology}

        <section class="lt-from-blog">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="lt-title">
                            {!! $t['our_mission'] !!}
                        </div>
                    </div>
                </div>
                {mod:Projects::index}
            </div>
        </section>

        @include($template_url.'chunk/speak_to_us')
        @include($template_url.'chunk/instagram')

    </main>
    <!-- .content -->
</div>
<!--wrapper-->

@include($template_url.'chunk/footer')
</body>
</html>