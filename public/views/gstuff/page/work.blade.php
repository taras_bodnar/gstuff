{{--Created by Taras 2017-07-03 05:31--}}
        <!DOCTYPE html>
<html lang="en">
<!--head-->
@include($template_url.'chunk/head')
        <!--.head-->
<body>
<!--wrapper-->
<div class="wrapper">
    <!--header-->
    @include($template_url.'chunk/header')
            <!--.header-->
    <!-- content -->
    <main class="content" id="content">
        <!-- wrap-screen -->

        {mod:Projects::portfolio}

        @include($template_url.'chunk/speak_to_us')

        @include($template_url.'chunk/instagram')
    </main>
    <!-- .content -->
</div>
<!--wrapper-->

@include($template_url.'chunk/footer')
</body>
</html>