User-agent: *
Allow: /*.css
Allow: /*.js
Allow: /*.jpg
Allow: /*.jpeg
Allow: /*.png

Host: gstuffpro.com
Sitemap: http://gstuffpro.com/sitemap
