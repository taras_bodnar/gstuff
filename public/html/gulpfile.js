'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    cssclean = require('gulp-clean-css'),
    cssmin = require('gulp-cssmin'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    sassLint = require('gulp-sass-lint'),
    sourcemaps = require('gulp-sourcemaps'),
    rimraf = require('rimraf');

var path = {
    build: {
        html: './',
        js: 'assets/js/',
        css: 'assets/css/',
    },
    src: {
        html: 'src/*.html',
        js: 'assets/js/main.js',
        css: 'assets/css/**/*.*',
        scss: 'assets/scss/style.scss',
        fonts: 'assets/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'assets/js/**/*.js',
        css: 'assets/css/**/*.*',
        scss: 'assets/scss/**/*.scss',
        fonts: 'assets/fonts/**/*.*'
    },
    clean: './assets',
    cleanHtml:'./*.html'
};

gulp.task('clean', function (cb) {
    rimraf(path.cleanHtml, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html));
});

gulp.task('js:build', function () {
    gulp.src(['assets/js/vendor/jquery-3.1.1.min.js','assets/js/vendor/!(jquery-3.1.1.min)*.js'])
        .pipe(rigger())
        //.pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('scss:build', function () {
    gulp.src(path.src.scss)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', function(e) {
            gutil.log(e);
        }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write())
        // .pipe(sassLint())
        // .pipe(sassLint.format())
        // .pipe(sassLint.failOnError())
        // .pipe(cssclean())
        .pipe(gulp.dest(path.build.css));
});

gulp.task('css:build', function () {
    gulp.src(['./assets/css/vendor/*.*'])
        .pipe(concat('vendor.css'))
        //.pipe(cssmin())
        .pipe(cssclean())
        .pipe(gulp.dest(path.build.css));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'scss:build',
    'css:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.scss], function(event, cb) {
        gulp.start('scss:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
});


gulp.task('default', [ 'build', 'watch']);