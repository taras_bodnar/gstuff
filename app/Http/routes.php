<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('/', 'Admin\Admin@index');
//
//Route::get('admin','Admin\Admin@index');
//
//Route::get('/{alias}','Admin\Admin@getPage');
Route::get('laradmin/','Admin\Auth@login');
Route::post('laradmin/auth','Admin\Auth@auth');

Route::any('plugins/{name}/{method}',['as'=>'plugins.ajax',function($name,$method){
    return \App()->make("App\\Http\\Controllers\\Admin\\plugins\\".ucfirst($name))->$method();
}]);

Route::group(array('middleware' => 'auth','namespace' => 'Admin','prefix' => 'admin'), function()
{
	Route::any('request/{controller}/{method?}/{param1?}/{param2?}','Admin@request')->where('param2','.+');
        Route::get('/{directory}/{controller?}/{action?}/{id?}', ['as'=>'method.load',function($directory='Admin',$controller='DashBoard', $action='index',$id=''){
//            $controller = str_replace('/','\\',$controller);
//            dd($controller);
            return \App()->make("App\\Http\\Controllers\\{$directory}\\{$controller}")->$action($id);
        }]);
        Route::any('ajax/{directory}/{controller?}/{action?}/{id?}/{second?}/{third?}/{fourth?}',['as'=>'ajax.load',
            function($directory='Admin',$controller='Auth', $action='login',$id='',$second='',$third='',$fourth=''){
                return \App()->make("App\\Http\\Controllers\\{$directory}\\{$controller}")->$action($id,$second,$third,$fourth);
            }]);
});

Route::any('install','Install\\Install@index');

Route::group(['middleware' => ['install','load','pagesViews']], function () {

    Route::any('request/{controller}/{method?}/{param1?}/{param2?}','Core@request')->where('param2','.+');

    Route::get('/', 'Core@index');

    Route::get('sitemap/{type?}',function($type='') {
        return \App()->make("App\\Http\\Controllers\\modules\\SiteMap\\SiteMap")->index($type);
    });

    Route::get('{lang?}/{query}', ['as' => 'main', 'uses' => 'Core@index'])
        ->where([
            'lang' => '[a-z]{2}',
            'query' => '[a-zA-Z.0-9-_/]+'
        ]);

    Route::get('{lang?}', 'Core@index')->where('lang', '[a-z]{2}');

    Route::get('{query?}', 'Core@index')->where('query', '[a-zA-Z.0-9-_/]+');


});
