<?php

namespace App\Http\Middleware;

use App\Http\Controllers\modules\Pages\PagesViews;
use Closure;
use Illuminate\Support\Facades\Request;

class SetPagesViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Request::ip()) {
            $data['ip'] = Request::ip();
            $data['content_id'] = $request->page->id;
            $t = PagesViews::whereDate('created_at','=',date('Y-m-d'))
                ->where('ip','=',$data['ip'])
                ->where('content_id','=',$data['content_id'])
                ->count();

            if($t == 0) {
                PagesViews::create($data);
            }

        }

        return $next($request);
    }
}
