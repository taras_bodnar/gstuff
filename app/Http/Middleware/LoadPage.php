<?php

namespace App\Http\Middleware;

use App\Model\app\Images;
use Closure;
use App\Model\Core;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\app\Settings;

class LoadPage
{
    public static $data = array();
    public static $translations = array();
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = Route::getCurrentRoute()->parameters();

        if(!empty($route['lang'])) {

            $languages_id = Core::getLangByCode($route['lang']);

            if(!empty($languages_id->id) && isset($languages_id->id)) {

                if($languages_id->id!=Session::get('app.languages_id')) {

                    Session::set('app.languages_id',$languages_id->id);
                }
            } else {

                $this->notFound();
            }
        } else {
            if($request->is('request/*')) {
                if(!Session::get('app.languages_id')) {
                    Session::set('app.languages_id',Core::getFrontLangDefault()->id);
                }
            } else {
                Session::set('app.languages_id',Core::getFrontLangDefault()->id);
            }
        }
//        dd(Session::all());
        $alias = !empty($route['query'])?$route['query']:"";
//        dd($alias);
        $images = new Images();
        self::$data = Core::getAlias($alias);

        if(empty(self::$data)) {
            $alias = Core::getAliasById2(52,Session::get('app.languages_id'));
            self::$data = Core::getAlias($alias->alias);
        }

        if(isset(self::$data->id)) {
            $img = $images->cover(self::$data->id,'source');
            self::$data->img = !empty($img->src)?$img->src:"/views/gstuff/assets/img/site-logo.png";
        }

        $core = new Core();
        self::$translations = $this->object_to_array($core->getTranslations());
//        dd( self::$translations);

        $request['page'] = self::$data;
        self::$data->fullurl = Request::fullUrl();
        self::$data->alias = Request::path();
        
        view()->share([
            'page'=>self::$data,
            't'=>self::$translations,
            'template_url'=>Settings::instance()->get('app_theme_current')."/",
            'assets_url'=>'/'.Settings::instance()->get('app_views_path').Settings::instance()->get('app_theme_current')."/"
        ]);


        if(self::$data == null) {
            $this->notFound();
        }

        return $next($request);
    }

    private function notFound()
    {
        $id = Settings::instance()->get('page_404');
        $alias = Core::getAliasById($id);

        if(empty($id) || empty($alias)) {
            header("HTTP/1.0 404 Not Found");
            header("Location: /"); die();
        }

        header("HTTP/1.0 404 Not Found");
        header("Location: /{$alias->alias}"); die();
    }

    private function object_to_array($object) {
        return (array) $object;
    }
}
