<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;

class InstallMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $currentRoute = Request::path();
        if($currentRoute!='install' && !Schema::hasTable('content')) {
                return redirect('install');
        }

        if(Schema::hasTable('content')) {
            $id = $content_id = DB::table('content')->select('id')->where('id',1)->pluck('id');
            if($currentRoute!='install' && empty($id)) {
                return redirect('install');
            }
        }

        if(env('install') && $currentRoute=='install') {
            return redirect('/');
        }

        return $next($request);
    }
}
