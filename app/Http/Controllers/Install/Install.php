<?php

namespace App\Http\Controllers\Install;

use App\Http\Controllers\app\Template;
use App\Http\Controllers\app\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class Install extends Controller
{
    private $installPath;
    private $template;
    private $content='';
    private $error = array();

    public function __construct()
    {
        $this->installPath = "install";
        $this->template = Template::instance($this->installPath);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if (!\Illuminate\Support\Facades\Request::isMethod('post')) {
//            if (Schema::hasTable('content')) {
//                return redirect('/');
//            }
//        }

        if (\Illuminate\Support\Facades\Request::isMethod('post')) {

            $step = Input::get('step');

            if(isset($step)) {
                if($step == 0) {
                    $this->step0();
                }elseif($step == 1) {
                    $this->step1();
                }elseif($step == 2) {
                    $this->step2Process();
                }elseif($step == 5) {
                    $this->success();
                }
            }
        }

        if($this->check()){
            $this->content = $this->template->view('terms');
        }
//        dd($this->content);
        $this->output();
    }

    private function step0()
    {
        $this->content = $this->template->view('step1');
        $this->output();
    }

    private function step1()
    {
        $conf = Input::get('data'); $db=null;
//        Artisan::call('key:generate');
        try{
            $env_update = $this->changeEnv([
                'DB_CONNECTION'   => $conf['db_connection'],
                'DB_HOST'   => $conf['host'],
                'DB_DATABASE'   => $conf['name'],
                'DB_USERNAME'   => $conf['user'],
                'DB_PASSWORD'       => $conf['pass']
            ]);

            Artisan::call('migrate');

            Artisan::call('db:seed',[
                '--class'=>'SeetingsSeeder'
            ]);

            Artisan::call('db:seed',[
                '--class'=>'LanguagesSeeder'
            ]);

            Artisan::call('db:seed',[
                '--class'=>'UsersGroupTableSeeder'
            ]);

            Artisan::call('db:seed',[
                '--class'=>'UsersGroupInfoTableSeeder'
            ]);

            Artisan::call('db:seed',[
                '--class'=>'ContentTypeSeeder'
            ]);

            Artisan::call('db:seed',[
                '--class'=>'ContentTemplatesSeeder'
            ]);

            Artisan::call('db:seed',[
                '--class'=>'ComponentsSeeder'
            ]);

        }
        catch(\PDOException $e) {
            $this->error[] = $e->getMessage();
        }
        if(empty($this->error)) {
            $this->step2();
        }

        $this->content = $this->template->view(
            'step1',
            array(
                'error' => $this->error
            )
        );

        $this->output();
    }

    private function step2()
    {
        $this->content = $this->template->view(
            'step2',
            array(
                'error' => $this->error
            )
        );

        $this->output();
    }

    private function step2Process()
    {
        $data = Input::get('data');
        $pass = bcrypt($data['pass']);

        $now = date('Y-m-d H:i:s');


        if(empty($this->error)){
            $languages = array(
                'uk' => 'Українська',
                'ru' => 'Русский',
                'en' => 'English',
                'pl' => 'Polska',
                'de' => 'Deutch',
            );
            try{
                $lang = $languages[$data['language']];
                DB::table('languages')->where('id','=',1)->update([
                    'code'=>$data['language'],
                    'name'=>$lang
                ]);
            } catch(\PDOException $e) {
                $this->error[] = 'E1:' . $e->getMessage();
            }
        }
        if(empty($this->error)){
            try{
                $owner_id = DB::table('users')->insertGetId([
                    'users_group_id' => 1,
                    'languages_id' => 1,
                    'name' => $data['user'],
                    'email' => $data['email'],
                    'password' => $pass,
                    'sessid'=>Session::getId()
                ]);

            } catch(\PDOException $e) {
                $this->error[] = 'E2:' . $e->getMessage();
            }
        }

        if(empty($this->error)){
            try{
                $content_id = DB::table('content')->insertGetId([
                    'type_id' => 1,
                    'owner_id' => $owner_id,
                    'templates_id' => 1,
                    'published' => 1,
                    'auto' => 0
                ]);
            } catch(\PDOException $e) {
                $this->error[] = $e->getMessage();
            }
        }
        if(empty($this->error)){
            try{
                DB::table('content_info')->insertGetId([
                    'content_id' => $content_id,
                    'languages_id' => 1,
                    'name' => $data['name'],
                    'title' => $data['name']
                ]);
            } catch(\PDOException $e) {
                $this->error[] = $e->getMessage();
            }
        }

        if(empty($this->error)){

            $this->changeEnv([
                'install'=>1
            ]);

            $this->success();
        } else {

            $this->content = $this->template->view(
                'step2',
                array(
                    'error' => $this->error
                )
            );

            $this->output();
        }
    }

    private function success($expire='')
    {
        $this->content = $this->template->view('success', array('expire'=>$expire));

        $this->output();
    }

    private function check()
    {

        if (version_compare(phpversion(), '5.5.9', '<') == true) {
            $this->error[] = 'PHP version must be more 5.3.0';
        }

        if(!is_writable(storage_path())) {
            $this->error[] = 'Please change permission to the directory /storage on 0775.
                             <br>You can use for it "sudo chmod 0775 -R storage"';
        }

//        dd(public_path());
        if(!is_writable(public_path().'/uploads')) {
            $this->error[] = 'Please change permission to the directory public/uploads on 0775.
                              <br>You can use for it "sudo chmod 0775 -R public/uploads"';
        }

        if(!is_writable(base_path().'/.env')) {
            $this->error[] = 'Please change permission to the file .env on 0775.
                              <br>You can use for it "sudo chmod 0775 -R .env"';
        }

        $this->content = $this->template->view('check', array('error' => $this->error));

        return empty($this->error);
    }

    private function output()
    {

        echo $this->template->view(
            'index',
            array(
                'title' => 'Install title',
                'content' => $this->content
            )
        );

        die();
    }

    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);;

            // Loop through given data
            foreach((array)$data as $key => $value){

                // Loop through .env-data
                foreach($env as $env_key => $env_value){

                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);

            return true;
        } else {
            return false;
        }
    }
}