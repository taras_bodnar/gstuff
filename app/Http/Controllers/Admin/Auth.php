<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 06.10.15
 * Time: 20:14
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth as AuthController;
use Validator;
use App\Model\Admin\Users;


class Auth extends Admin
{
    public function __construct()
    {
        if (Session::has('admin.user')) {

            return redirect(route('method.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'DashBoard'
                ]));
        }
        parent::__construct();

    }

    public function login()
    {
        if (Session::has('admin.user')) {

            return redirect(route('method.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'DashBoard'
                ]));
        } else {

            return view('admin/auth/login');
        }
    }

    public function auth()
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);
//        dd($validator);
// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('laradmin')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            // attempt to do the login
            if (AuthController::attempt($userdata)) {

                $userInfo = Users::getInfoByEmail($userdata['email']);
                Users::where('id',$userInfo->id)->update([
                    'sessid' => Session::getId()
                ]);

                Session::set('admin', [
                    'user' => $userInfo
                ]);

                return Redirect::to('admin/Admin/DashBoard');
            } else {
                // validation not successful, send back to form
                $validator->errors()->add('field', 'Одне з полів заповнено не правильно');
                return Redirect::to('laradmin')->withErrors($validator);

            }

        }
    }

    public function logout()
    {
        $id = Session::get('admin.user')->id;
        Users::where('id',$id)->update([
            'sessid' => ""
        ]);

        Session::forget('admin.user');
        return Redirect::to('laradmin');
    }


}