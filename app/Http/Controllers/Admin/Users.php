<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\content\Tree;
use App\Model\Admin\Languages;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Admin\UsersGroup;
use App\Model\Admin\Users as UserNodel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class Users extends Admin
{
    private $rang = array(100,999);

    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\UsersGroup();
        $this->u = new \App\Model\Admin\Users();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id=0)
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('fa fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'Users',
                                'action'=>'create',
                                'id'=>$parent_id
                            ]
                        )."'"
                ))
        );

        $this->dt->setId('users')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Users',
                    'action'=>'items',
                    'id'=>$parent_id
                ]
            ))
            ->setTitle('Користувачі')
            ->th('#')
            ->th('П.І.Б.')
            ->th('Телефон')
            ->th('Email')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->dt->render());

        return $this->output();
    }

    public function items($parent_id=0)
    {
        $parent_id = empty($parent_id)?0:$parent_id;

        $this->dt
//            ->debug(true)
            -> table('users u');

            if($parent_id>0) {
                $this->dt->where("g.id=$parent_id ");
            }

            $this->dt->join("join users_group g on g.id=u.users_group_id and g.rang between {$this->rang[0]} and {$this->rang[1]}")
            -> get(
                array(
                    'u.id',
                    'u.name',
                    'u.phone',
                    'u.email',
                )
            )
            -> searchCol(array('g.id','g.rang','i.name'))
            -> execute();

        $r   = $this->dt->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'Users',
                    'action'=>'edit',
                    'id'=>$row->id,
                ]),
                $row->phone,
                $row->email,
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'Users',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    )).
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick' => "Users.delete($row->id)"
                    )
                )
            );
        }


        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content =  view('admin.modules.users.form',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Users',
                        'action'=>'process',

                    ]),
                'process'=>'create',
                'languages'=>Languages::geAll(),
                'parent'=>UsersGroup::getAll()
            ]
        );
        $this->setContent($content);

        return $this->output();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($id) || empty($id)) return 'WRONG ID';
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(
            Form::link(
                'Повернутись',
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => route('method.load',
                        [
                            'directory'=>'Admin',
                            'controller'=>'Users',
                            'action'=>'index'
                        ]
                    )
                )
            )
        );

        $data = UserNodel::getInfo($id);

        $content = view('admin.modules.users.form',[
            'data'=>$data,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Users',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit',
            'languages'=>Languages::geAll(),
            'parent'=>UsersGroup::getAll($id)
        ]);

        $this->setContent($content);

        return $this->output();
    }

    public function process($id='')
    {
        $s = 0;
        $e[] = array();


        $s=0;
        $e = $this->errors['warning'];

        $data = Input::get('data');

        if(Input::hasFile('avatar') && Input::file('avatar')->isValid()){

            $avatar = Input::file('avatar');
            if ($id > 0) {
                $im = DB::table('users')->select('avatar')
                    ->where('id',$id)
                    ->pluck('avatar');

                // видалю попереднє зображення
                $im_path = public_path().'/'.(ltrim($im, "/\\"));

                if(!empty($im) && file_exists($im_path)){
                    @unlink($im_path);
                }
            }

            $image_tmp = $_FILES['avatar']['tmp_name'];

            if (function_exists('exif_read_data') && (@exif_imagetype($image_tmp) == 2 ? $exif = @exif_read_data($image_tmp) : null)) {
                $image = imagecreatefromstring(file_get_contents($image_tmp));
                if(isset($exif['Orientation']) && !empty($exif['Orientation'])) {
                    switch($exif['Orientation']) {
                        case 8:
                            $image = imagerotate($image,90,0);
                            break;
                        case 3:
                            $image = imagerotate($image,180,0);
                            break;
                        case 6:
                            $image = imagerotate($image,-90,0);
                            break;
                    }
                }
                imagejpeg($image, $image_tmp);
            }

            $upload_dir = public_path() . '/uploads/avatars/';

            File::makeDirectory($upload_dir,$mode = 0777, true, true);
            $ext = '.' .$avatar->getClientOriginalExtension();

            $image_name = md5(($id ? $id : 0) . 'x' . time()) . $ext;

            $avatar->move($upload_dir, $image_name);
            chmod($upload_dir.$image_name,0777);

            Image::make($upload_dir.$image_name)->resize(150, null,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
                )->save($upload_dir.$image_name);

            $data['avatar'] =  '/uploads/avatars/' . $image_name;
        }

        // check required fields
        if(
            empty($data['name']) ||
            empty($data['email'])
        ) {
            $this->errors[] ='Заповніть всі поля';
        }

        // add or update values
        if(empty($this->error)) {

//            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $data['password'] = bcrypt($data['password']);
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $s = UserNodel::createUser($data);

                    if($s > 0) {
//                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    unset($data['process']);
                    $data['password'] = bcrypt($data['password']);
                    $data['editetedon'] = date('Y-m-d H:i:s');
                    $s = $this->u->edit($id, $data);
                    if($s > 0) {
//                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => '', // redirect url
            'm' => $e,
        ));
    }

    public function destroy($id)
    {
        return $this->u->del($id);
    }

    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;
        $tree=array();
        $r = $this->u->tree($parent_id);
        $i=0;
        if(!empty($r)){
            foreach ( $r as $row) {
                $tree[$i]['data'] = $row->name . ' #' . $row->id;
                $tree[$i]['state'] = $row->isfolder ? 'closed' : '';

                //todo нада повіксати шлях

                $tree[$i]['attr'] = array(
                    'id'   => $row->id,
                    "rel"  => (($row->isfolder)? 'folder':'file'),
                    "href" => route('method.load',
                        [
                            'directory' => 'Admin',
                            'controller' => 'Users',
                            'action' => 'index',
                            'id' => $row->id
                        ]
                    ),
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    public function output()
    {
        $tree = new Tree(
            'content',
            route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Users',
                    'action' => 'tree'
                ]),
            '',
            array(
                Tree::contextMenu(
                    "Список груп",
                    "fa fa-list",
                    'self.location.href="/admin/Admin/UsersGroup/index/"+id;'
                ),
                Tree::contextMenu(
                    "Список Користувачів",
                    "fa fa-list",
                    'self.location.href="/admin/Admin/Users/index/"+id;'
                ),
                Tree::contextMenu(
                    "Додати групу",
                    "fa fa-plus",
                    'self.location.href="/admin/Admin/UsersGroup/create/"+id;'
                ),
                Tree::contextMenu(
                    "Редагувати групу",
                    "fa fa-edit",
                    'self.location.href="/admin/Admin/UsersGroup/edit/"+id;'
                ),
                Tree::contextMenu(
                    "Видалити групу",
                    "fa fa-remove",
                    'onclick = content.delete(id);'
                )
            ),
            '',
            true
        );
        $this->setSidebar($tree->render());
        return parent::output();
    }
}
