<?php

namespace App\Http\Controllers\Admin\plugins;

use App\Http\Controllers\Admin\Admin;
use App\Model\Admin\content\Images;
use App\Model\Admin\Languages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

/**
 * Class FileUpload
 * @name FileUpload
 * @description Завантаження файлів
 * @package App\Http\Controllers\Admin\plugins
 * @author Taras Bodnar mailto:bania20091@gmail.com
 * @version 1.0
 * @copyright &copy; 2015
 */
class FileUpload extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\plugins\FileUpload();
        $this->uploaddir = 'uploads/content/';
        $this->images = new Images();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $files = $this->m->get($id);

        $im_data = array();

        foreach ($files  as $file) {
            $im_data[] = $this->getTemplate(
                $id,
                array(
                    'file'      => $file,
//                'path'       => $dir,
//                'path_view'  => $dir_preview,
                    'content_id' => $id
                ));
        }

        return $this->template->view('plugins/fileupload/index', array(
            'id'=>$id,
            'files' => $im_data
        ));
    }

    public function get($content_id)
    {

    }

    public function getTemplate($content_id, $im_data=array())
    {

        $dir =  ltrim($this->uploaddir, DIRECTORY_SEPARATOR) . $content_id.'/files/';
        $im_data['path']      = $dir;
        $im_data['content_id'] = $content_id;
        $im_data['path_view'] = $dir;

        return $this->template->view('plugins/fileupload/template',$im_data);
    }

    public static function uploadFiles() {
        $input = Input::all();
        $id = $input['id'];

        $rules = array(
            'upload' => 'required|max:10000',
        );
        $info_name = Input::file('upload')->getClientOriginalName();
        $validation = Validator::make($input, $rules);
        if ($validation->fails()) {
//            return Response::make($validation->errors->first(), 400);
        }

        $destinationPath = 'uploads/content/'.$id;
        File::makeDirectory($destinationPath,$mode = 0777, true, true);
        $extension = Input::file('upload')->getClientOriginalExtension();
        $fileName = time().$id . '.' . $extension;
        $upload_success = Input::file('upload')->move($destinationPath."/file", $fileName);


        if ($upload_success) {
            \App\Model\Admin\content\Images::insert($id,$fileName,$info_name,'file');
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->images->imageData($id);
        $content_id = $data->content_id;

        $path = public_path() . $this->uploaddir . $content_id."/file/".$data->name;
        @unlink($path);

        return $this->images->del($id);
    }

    public function process($id)
    {
        return 1;
    }

    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }
}
