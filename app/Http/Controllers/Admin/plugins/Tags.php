<?php

namespace App\Http\Controllers\Admin\plugins;

use App\Http\Controllers\Admin\Admin;
use App\Http\Controllers\Admin\Plugins;
use App\Model\Admin\Languages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

/**
 * Class Tags
 * @name Tags
 * @description мітки
 * @package App\Http\Controllers\Admin\plugins
 * @author Taras Bodnar mailto:bania20091@gmail.com
 * @version 1.0
 * @copyright &copy; 2015
 */
class Tags extends Plugins
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model = new \App\Model\Admin\plugins\Tags();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->template->view('plugins/tags/index');
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = array();

        $l = new Languages();
        $languages =$l->geAll(1);

        foreach ($languages as $lang) {
            $t = $this->model->get($id, $lang->id);
            if(!empty($t)){
                foreach ($t as $row) {
                    $tags[$lang->id][] = $row->name;
                }
            }
        }


        return  $this->template->view('plugins/tags/index',
            array(
                'tags'      => $tags,
                'id'        => $id,
                'languages' => $languages
            )
        );
    }

    public function process($id)
    {
        $tags = Input::get('tags');
        $this->model->deleteTagByContentId($id);
        if(empty($tags)) return ;

        foreach ($tags as $languages_id => $tags) {
            $tags = explode(',', $tags);
            $this->model->set($id, $languages_id, $tags);
        }
    }

    public function remove()
    {

        $content_id = Input::get('id');
        $languages_id = Input::get('lang_id');
        $name = Input::get('name');

        return $this->model->remove($content_id, $languages_id, $name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function install()
    {
        return 1;
    }
    public function uninstall()
    {
        return 1;
    }
}
