<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 06.10.15
 * Time: 21:47
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Admin\DataTables;
use App\Model\Admin\Components as modelComponents;
use App\Model\Admin\Languages;
use Illuminate\Support\Facades\Input;

class Components extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->t = new DataTables();
        $this->languages = new Languages();
        $this->m = new \App\Model\Admin\Components();
    }

    public function index($id)
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'Components',
                                'action'=>'create',
                                'id'=>$id
                            ]
                            )."'"
                ))
        );

        $this->t->setId('components')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Components',
                    'action'=>'items',
                    'id'=>$id
                ]
            ))
            ->setTitle('Компоненти')
            ->th('#')
            ->th('Назва')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->t->render());

        return $this->output();
    }

    public function items($id)
    {
        $id = empty($id)?0:$id;
        $this->t->table('components c')
            ->join("join components_info ci on ci.components_id=c.id and ci.languages_id=1")
//            -> debug()
            -> searchCol('c.id,ci.name')
            ->where(" c.parent_id=".$id)
            -> get("c.id,ci.name")

            -> execute();

        $r   = $this->t->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'Components',
                    'action'=>'index',
                    'id'=>$row->id,
                ]),
                Form::button(
                    '',
                    Form::icon('fa-file'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'Components',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick'=>"Component.delete($row->id)"
                    )
                )
            );
        }


        return $this->t->renderJSON($res, $this->t->getTotal());
    }

    public function create($id='')
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('ffa-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );


        $content =  view('admin.modules.components.form',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Components',
                        'action'=>'process',
                        'id'=>$id
                    ]),
                'process'=>'create',
                'parent'=>modelComponents::getAll(),
                'languages'=>$this->languages->geAll(),
                'controllers'=> $this->controllersList($_SERVER['DOCUMENT_ROOT'] . '/app/Http/Controllers/Admin','/admin/Admin/')
            ]
            );
        $this->setContent($content);

        return $this->output();
    }

    public function edit($id)
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('fa-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        if(!isset($id) || empty($id)) return 'WRONG ID';

        $data  = $this->m->data('components',$id);

        $info = modelComponents::getInfo($id);

        $content = view('admin.modules.components.form',[
            'data'=>$data,
            'info'=>$info,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Components',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit',
            'parent'=>modelComponents::getAll($id),
            'languages'=>$this->languages->geAll(),
            'controllers'=> $this->controllersList($_SERVER['DOCUMENT_ROOT'] . '/app/Http/Controllers/Admin','/admin/Admin/')
        ]);

        $this->setContent($content);

        return $this->output();
    }

    public function process($id=0)
    {
        $e[] = array();
        $s=0;
        $e = $this->errors['warning'];
//        $r = './components/index/'. $_POST['data']['parent_id']; // redirect url
        $info = Input::get('info');
        $data = Input::get('data');
        $process = $data['process'];

        if(
            empty($data['controller']) ||
            empty($data['rang'])
        ) {
            $this->errors[] ='Заповніть всі поля';
        }
        // add or update values
        if(empty($this->error)) {

            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $s = modelComponents::createComponents($data, $info);

                    if($s > 0) {
                        modelComponents::toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    unset($data['process']);
                    $s = modelComponents::updateComponents($id, $data, $info);
                    if($s > 0) {
                        modelComponents::toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'process' => $process, // redirect url
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    public function delete()
    {
        return modelComponents::del(Input::get('id'));
    }


    private function readControllers($dir, $prefix = '')
    {

        $dir = rtrim($dir, '\\/');
        $result = array();

        $h = opendir($dir);
        while (($f = readdir($h)) !== false) {
            if ($f !== '.' and $f !== '..') {
                if (is_dir("$dir/$f")) {
                    $result = array_merge($result, $this->readControllers("$dir/$f", "$prefix$f/"));
                } else {
                    $f = strtr($f, array('.php'=>''));
                    $result[] = $prefix.$f;
                }
            }
        }
        closedir($h);
//        dd($result);
        return $result;
    }

    private function controllersList($dir, $prefix='', $selected='')
    {
        $res=array();
        $c = $this->readControllers($dir);

        foreach($c as $k=>$co){

//            $v = $prefix . mb_strtolower($co);
            $v = $prefix . $co;

            $s= $v==$selected ? 'selected' : '';

            $res[] = array(
                'value' => $v,
                'name'  => $co,
                'selected'=> $s
            );
        }
//        dd($res);
//        print_r($res);
        return $res;
    }


}