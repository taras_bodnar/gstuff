<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Plugins extends Admin
{
    private $cpath = 'app/Http/Controllers/Admin/plugins/';
    private $ns = 'App/Http/Controllers/Admin/plugins/';

    public function __construct()
    {
        parent::__construct();
        $this->languages = new Languages();
        $this->m = new \App\Model\Admin\Plugins();
        $this->ns = str_replace('/', '\\', $this->ns);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->dt->setId('plugins')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Plugins',
                    'action'=>'show'
                ]
            ))
            ->setTitle('Плагіни')
            ->th('#')
            ->th('Назва')
            ->th('Автор')
            ->th('Функції');

        $this->setContent($this->dt->render());

        return $this->output();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plugins = $this->listPlugins();
        return $this->dt->renderJSON($plugins,count($plugins));
    }


    private function listPlugins()
    {
        $plugins = array();
        if ($handle = opendir(base_path()."/" . $this->cpath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {

                    $plugin = str_replace('.php', '', $entry);

                    $row = $this->readPhpDoc($plugin);


                    $row['func'] = "";
                    $id=$this->m->isInstalled($this->ns.$plugin);

                    if($id){
                        $row['func'] .= "<button class='btn btn-primary' onclick=\"Plugins.edit('{$id->id}')\"><i class=\"fa fa-edit fa-white\"></i></button>";
                        $row['func'] .= "<button class='btn btn-danger uninstall-plugin' onclick=\"Plugins.destroy('{$id->id}')\"><i class=\"fa fa-power-off fa-white\"></i></button>";
                    } else {
                        $row['func'] .= "<button class='btn btn-success' onclick=\"Plugins.install('{$plugin}')\"><i class=\"fa fa-power-off fa-white\"></i></button>";
                    }


                    $row['name']    = "<b>{$row['name']}</b>";
                    $row['name']   .= "<br>" . $row['description'];
                    $row['name']   .= "<br>package: " . $row['package'];
                    $row['author'] .= "<br>" . $row['copyright'];

                    unset($row['description']);
                    unset($row['package']);
                    unset($row['copyright']);

                    $plugins[] = $row;
                }
            }
            closedir($handle);
        }

        return $plugins;
    }

    private function readPhpDoc($plugin)
    {
        $row = array();
        $rc = new \ReflectionClass($this->ns . $plugin);

        $dc = $rc->getDocComment();

        //Get the comment
        if (preg_match('#^/\*\*(.*)\*/#s', $dc, $comment) === false) {
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        $comment = trim($comment[1]);
        if (preg_match_all('#^\s*\*(.*)#m', $comment, $lines) === false) {
            $row['name'] = 'Невірно внесено DocComment';
            return $row;
        }
        foreach ($lines[1] as $line) {
            $line = trim($line);

            if (empty($line)) continue;

            if (strpos($line, '@') === 0) {
                $param = substr($line, 1, strpos($line, ' ') - 1); //Get the parameter name
                $value = substr($line, strlen($param) + 2); //Get the value
                $row[$param] = $value;
            }
        }
        return $row;
    }

    public function install()
    {
        $plugin = Input::get('plugin');
        if(!isset($plugin)) return '';
        $action = Input::get('action');

        if(isset($action) && $action == 'install'){
            return $this->processInstall();
        }

        return view(
            'admin/modules/content/plugin_form',
            array(
                'action' => 'install',
                'plugin' => $plugin,
                'id' => 0,
                'structure' => $this->m->getStructure(),
                'position'  => $this->getPosition()
            )
        );
    }

    private function getPosition($selected='')
    {
        $r = array();
        $res = $this->m->getPosition();
        foreach ($res as $row) {
            $r[] = array(
                'id' => $row,
                'name' => $row,
                'selected' => $selected == $row ? 'selected' : ''
            );
        }

        return $r;
    }


    private function processInstall()
    {
        $plugin = Input::get('plugin');

        if(!isset($plugin)) return '';
        $m=''; $c= null; $id=0;

        $data = Input::get('data');
        $structure = Input::get('structure');
        $data['controller'] = $this->ns . $plugin;

        try{
            $c = new $data['controller'];
        } catch(\Exception $e){
            $m = $e->getCode() . $e->getMessage();
        }

        if($c != null && $c->install()){
            $id = $this->m->install($data, $structure);
        }


        return json_encode(
            array(
                's' => $id > 0,
                'm' => $m
            )
        );
    }

    public function edit($id)
    {
        $action = Input::get('action');

        if(isset($action) && $action == 'edit'){
            $id = Input::get('id');
            return $this->process($id);
        }

        $data = $this->m->getData($id);

        return view(
            'admin/modules/content/plugin_form',
            array(
                'action' => 'edit',
                'id' => $id,
                'structure' => $this->m->getStructure($id),
                'position'  => $this->getPosition($data->position)
            )
        );
    }

    public function process($id)
    {
        $structure = Input::get('structure');

        if(empty($structure)) return '';

        $s = $this->m->updateData($id, Input::get('data'), $structure);
        return json_encode(
            array(
                's' => $s,
                'm' => ''
            )
        );
    }
        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = (int) Input::get('id');
        $plugin = $this->m->getData($id, 'controller');
        $c = new $plugin->controller;

        if($c->uninstall()){
            return $this->m->del($id);
        }

        return 0;
    }

    public function get($id, $action)
    {
        $plugins = $this->m->getStructurePlugins($this->route);

        $out = array();

        foreach ($plugins as $row) {
            $p = new $row->controller;
            $out[$row->position][] = $p->$action($id);
        }

        return $out;
    }
}
