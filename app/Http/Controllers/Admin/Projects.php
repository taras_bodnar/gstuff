<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\DataTables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Admin\content\Tree;

class Projects extends Content
{
    private $cat = array();
    protected $parent_id='';
    public function __construct()
    {
        parent::__construct();

        $this->setType('project');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id = '')
    {
        $this->setButtons
        (
            Form::button(
                'Створити',
                Form::icon('icon-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Projects',
                                'action' => 'create',
                                'id' => $parent_id
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('content')
            ->setConfig('sortable', true)
            ->sortableConf('content','id')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Projects',
                    'action' => 'show',
                    'id' => $parent_id
                ]
            ))
            ->setTitle('Статті')
            ->th('#')
            ->th('Назва')
            ->th('Функції');


        $this->setContent($this->dt->render());

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = 0)
    {
        parent::create($parent_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($parent_id='')
    {
        $parent_id = empty($parent_id) ? "" : $parent_id;
        $this->dt->table('content c')
            ->join("join content_info ci on ci.content_id=c.id and ci.languages_id=" . $this->languages_id)
            ->join($parent_id>0?"join posts_categories pc on pc.pid=c.id and  pc.cid={$parent_id}":"")
            ->where(" c.type_id={$this->type_id->id}")
            ->searchCol('c.id,name')
            ->get("c.id,name,published,c.sort")
            ->orderBy('c.sort asc')
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                '<i id='.$row->id.' style="cursor: move;opacity:0.5" class="fa fa-reorder"></i>',
                $row->id,

                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'Projects',
                    'action' => 'edit',
                    'id' => $row->id,
                ]), Form::button(
                    '',
                    Form::icon($row->published==1?'fa-eye':'fa-eye-slash'),
                    array(
                        'class'    =>'btn-primary',
//                        'title'   => $this->lang->core['published_tip'],
                        'onclick' => 'content.pub(\''.$row->id.'\',\''.$row->published.'\')'
                    )
                ).Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Projects',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "content.delete($row->id)"
                    )
                )
            );
        }
        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->configFormField('PostCat');
        $this->configFormField('all_pages');

        parent::edit($id);

        if ($this->data->parent_id > 0) {
            $this->prependToButtons(
                Form::link(
                    'Повернутись',
                    Form::icon('icon-external-link'),
                    array(
                        'class' => 'btn-link',
                        'href' => route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Projects',
                                'action' => 'edit',
                                'id' => $this->data->parent_id
                            ]
                        )
                    )
                )
            );
        }
        $mb = new \App\Model\Admin\Projects();
        $all_pages = $mb->getPages($id);

        $this->formAppendData('action', route('ajax.load',
            [
                'directory' => 'Admin',
                'controller' => 'Projects',
                'action' => 'process',
                'id' => $id
            ]))
            ->formAppendData('all_pages',$all_pages)
            ->formAppendData('categories', $this->categories(0, '', $id));


        return $this->renderContent();
    }

    private function categories($parent_id=0, $parent_name='', $selected)
    {
        $mb = new \App\Model\Admin\Projects();

        $cid=array();
        foreach ($mb->getCategories($selected) as $r) {
            $cid[] = $r->cid;
        }
        $cat_type = $this->ms->getTypeId('projectCategories');

        $r = $this->ms->tree($parent_id, $cat_type->id);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row->name = $parent_name .' / '. $row->name;
            }
            if(in_array($row->id, $cid)) {
                $row->selected = 'selected';
            } else {
                $row->selected = '';
            }
            $this->cat[] = $row;
            if($row->isfolder) {
                $this->categories($row->id, $row->name, $selected);
            }
        }

        return $this->cat;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process($id)
    {
        parent::process($id);
        $this->updateCategories($id);
        $this->updatePages($id);

        return json_encode($this->getFormResponse());
    }

    private function updateCategories($id)
    {
        $mb = new \App\Model\Admin\Projects();

        $cid=array();
        $c = $mb->getCategories($id);

        foreach ($c as $r) {
            $cid[$r->cid] = $r->cid;
        }

        $categories = Input::get('categories');


        foreach ($categories as $k=>$c) {
            if(in_array($c, $cid)){
                unset($cid[$c]);
                continue;
            }
            $mb->setCategories($id, $c);
        }


        foreach ($cid as $k=>$v) {
            $mb->deleteCategories($id, $v);
        }


        return 1;
    }

    private function updatePages($id)
    {
        $mb = new \App\Model\Admin\Projects();

        $cid=array();
        $c = $mb->getChildren($id);

        foreach ($c as $r) {
            $cid[$r->pid] = $r->pid;
        }

        $categories = Input::get('all_pages');


        foreach ($categories as $k=>$c) {
            if(in_array($c, $cid)){
                unset($cid[$c]);
                continue;
            }
            $mb->setCategories($c,$id);
        }


        foreach ($cid as $k=>$v) {
            $mb->deleteCategories($v,$id);
        }


        return 1;

    }

    public function output()
    {
        $tree = new Tree(
            'content',
            route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'ProjectsCategories',
                    'action' => 'tree'
                ]),
            '',
            array(
                Tree::contextMenu(
                    "Додати категорію",
                    "icon-plus",
                    'self.location.href="/admin/Admin/ProjectsCategories/create/"+id;'),
                Tree::contextMenu(
                    "Список категорій",
                    "icon-list",
                    'self.location.href="/admin/Admin/ProjectsCategories/index/"+id;'),
                Tree::contextMenu(
                    "Редагувати категорій",
                    "icon-edit",
                    'self.location.href="/admin/Admin/ProjectsCategories/edit/"+id;'),
                Tree::contextMenu(
                    "Список статей",
                    "icon-list",
                    'self.location.href="/admin/Admin/Projects/index/"+id;'),
            ),

            '',
            true
        );
        $this->setSidebar($tree->render());
        return parent::output();
    }
}