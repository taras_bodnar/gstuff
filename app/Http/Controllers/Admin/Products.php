<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\content\Tree;
use App\Model\Admin\products\Options;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class Products extends Content
{
    private $cat = array();
    protected $parent_id='';
    private $m;
    private $po;
    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\Products();
        $this->po = new Options();
        $this->setType('product');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id = '')
    {
        $this->setButtons
        (
            Form::button(
                'Створити',
                Form::icon('icon-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Products',
                                'action' => 'create',
                                'id' => $parent_id
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('content')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Products',
                    'action' => 'show',
                    'id' => $parent_id
                ]
            ))
            ->setTitle('Статті')
            ->th('#')
            ->th('Назва')
            ->th('Ціна')
            ->th('Функції');


        $this->setContent($this->dt->render());

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = 0)
    {
        parent::create($parent_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($parent_id='')
    {
        $parent_id = empty($parent_id) ? "" : $parent_id;
        $this->dt->table('content c')
            ->join("join content_info ci on ci.content_id=c.id and ci.languages_id=" . $this->languages_id)
            ->join($parent_id>0?"join products_categories pc on pc.pid=c.id and  pc.cid={$parent_id}":"")
            ->join(" join products_options po on po.products_id=c.id")
            ->where(" c.type_id={$this->type_id->id}")
            ->searchCol('c.id,ci.name,po.price')
            ->get("c.id,ci.name,po.price")
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'Products',
                    'action' => 'edit',
                    'id' => $row->id,
                ]),
                Form::input([
                    'type'=>'text',
                    'name'=>'price',
                    'value'=>$row->price,
                    'onchange'=>"content.products.updatePrice($row->id)"
                ]),
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Products',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "content.delete($row->id)"
                    )
                )
            );
        }
        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->configFormField('PostCat');
        $this->configFormField('product_options');

        parent::edit($id);

        if ($this->data->parent_id > 0) {
            $this->prependToButtons(
                Form::link(
                    'Повернутись',
                    Form::icon('fa-external-link'),
                    array(
                        'class' => 'btn-link',
                        'href' => route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Products',
                                'action' => 'edit',
                                'id' => $this->data->parent_id
                            ]
                        )
                    )
                )
            );
        }

        $po = $this->po->getData($id);
        $this->formAppendData('action', route('ajax.load',
            [
                'directory' => 'Admin',
                'controller' => 'Products',
                'action' => 'process',
                'id' => $id
            ]))
            ->formAppendData('product_options',$po)
            ->formAppendData('categories', $this->categories(0, '', $id));


        return $this->renderContent();
    }

    private function categories($parent_id=0, $parent_name='', $selected)
    {
        $cid=array();
        foreach ($this->m->getCategories($selected) as $r) {
            $cid[] = $r->cid;
        }
        $cat_type = $this->ms->getTypeId('category');

        $r = $this->ms->tree($parent_id, $cat_type->id);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row->name = $parent_name .' / '. $row->name;
            }
            if(in_array($row->id, $cid)) {
                $row->selected = 'selected';
            } else {
                $row->selected = '';
            }
            $this->cat[] = $row;
            if($row->isfolder) {
                $this->categories($row->id, $row->name, $selected);
            }
        }

        return $this->cat;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process($id)
    {
        parent::process($id);
        $this->updateCategories($id);
        $this->updateProductOptions($id);

        return json_encode($this->getFormResponse());
    }

    private function updateProductOptions($id)
    {
        $product_options = Input::get('product_options');

        $product_options['show_price'] = !isset($product_options['show_price'])?0:$product_options['show_price'];
        $product_options['availabilitya'] = !isset($product_options['availability'])?0:$product_options['availability'];
        $product_options['sale'] = !isset($product_options['sale'])?0:$product_options['sale'];
        $product_options['hit'] = !isset($product_options['hit'])?0:$product_options['hit'];
        $product_options['new'] = !isset($product_options['new'])?0:$product_options['new'];

        if(isset($product_options) && !empty($product_options))
        {
            return $this->po->edit($id, $product_options);
        }
    }

    private function updateCategories($id)
    {
        $cid=array();
        $c = $this->m->getCategories($id);
        foreach ($c as $r) {
            $cid[$r->cid] = $r->cid;
        }
        $categories = Input::get('categories');

        foreach ($categories as $k=>$c) {
            if(in_array($c, $cid)){
                unset($cid->$c);
                continue;
            }
            $this->m->setCategories($id, $c);
        }

        foreach ($cid as $k=>$v) {
            $this->m->deleteCategories($id, $v);
        }


        return 1;
    }

    public function output()
    {
        $tree = new Tree(
            'content',
            route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Categories',
                    'action' => 'tree'
                ]),
            '',
            array(
                Tree::contextMenu(
                    "Додати категорію",
                    "fa-plus",
                    'self.location.href="/admin/Admin/Categories/create/"+id;'),
                Tree::contextMenu(
                    "Список категорій",
                    "fa-list",
                    'self.location.href="/admin/Admin/Categories/index/"+id;'),
                Tree::contextMenu(
                    "Редагувати категорій",
                    "fa-edit",
                    'self.location.href="/admin/Admin/Categories/edit/"+id;'),
                Tree::contextMenu(
                    "Список Товарів",
                    "fa-list",
                    'self.location.href="/admin/Admin/Products/index/"+id;'),
            ),

            '',
            true
        );
        $this->setSidebar($tree->render());
        return parent::output();
    }
}
