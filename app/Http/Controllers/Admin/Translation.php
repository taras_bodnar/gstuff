<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Languages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class Translation extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->languages = new Languages();
        $this->m = new \App\Model\Admin\Translation();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->setButtons
        (
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'Translation',
                                'action'=>'create',
                            ]
                        )."'"
                ))
        );

        $this->dt->setId('translations')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Translation',
                    'action'=>'items'
                ]
            ))
            ->setTitle('Словник')
            ->th('#')
            ->th('Код')
            ->th('Назва')
            ->th('Функції');


        $this->setContent($this->dt->render());

        return $this->output();
    }

    public function items()
    {


        $this->dt->table('translations t')
            ->join("join translations_info ti on ti.translations_id=t.id and ti.languages_id=".$this->languages_id)

            -> searchCol('t.id,t.code,ti.value')
            -> get("t.id,t.code,ti.value")
            -> execute();

        $r   = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->code,[
                    'directory'=>'Admin',
                    'controller'=>'Translation',
                    'action'=>'edit',
                    'id'=>$row->id,
                ]),
                link_to_route('method.load',$row->value,[
                    'directory'=>'Admin',
                    'controller'=>'Translation',
                    'action'=>'edit',
                    'id'=>$row->id,
                ]),
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'Translation',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick'=>"Languages.translation.delete($row->id)"
                    )
                )
            );
        }
        return $this->dt->renderJSON($res, $this->dt->getTotal());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content =  view('admin.modules.languages.translation',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Translation',
                        'action'=>'process'
                    ]),
                'process'=>'create',
                'languages'=>$this->languages->geAll()
            ]
        );
        $this->setContent($content);

        return $this->output();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(
            Form::link(
                'Повернутись',
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => route('method.load',
                        [
                            'directory'=>'Admin',
                            'controller'=>'Translation',
                            'action'=>'index'
                        ]
                    )
                )
            )
        );

        if(!isset($id) || empty($id)) return 'WRONG ID';

        $data  = $this->m->data('translations',$id);

        $info = $this->m->getInfo($id);

        $content = view('admin.modules.languages.translation',[
            'data'=>$data,
            'info'=>$info,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Translation',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit',
            'languages'=>$this->languages->geAll()
        ]);

        $this->setContent($content);

        return $this->output();
    }

    public function process($id='')
    {
        $e[] = array();
        $s=0;
        $e = $this->errors['warning'];
        $info = Input::get('info');
        $data = Input::get('data');
        $process = $data['process'];

        if(
            empty($info) ||
            empty($data['code'])
        ) {
            $this->errors[] ='Заповніть всі поля';
        }
        // add or update values
        if(empty($this->error)) {
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $s = $this->m->insert($data,$info);

                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    unset($data['process']);
                    $s = $this->m->edit($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
                    }
                    break;
                default:
                    break;
            }
        }

        return Response::json(array(
            's' => $s > 0 ,
            'process' => $process, // redirect url
//            't' => $this->lang->core[$e], // error title
            'm' => $e // error message
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->m->del($id);
    }
}
