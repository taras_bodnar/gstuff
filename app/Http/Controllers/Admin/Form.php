<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\app\Settings;

class Form {

    // buttons colors
    const BTN_TYPE_PRIMARY 	= 'btn-primary';
    const BTN_TYPE_INFO 	= 'btn-info';
    const BTN_TYPE_SUCCESS 	= 'btn-success';
    const BTN_TYPE_WARNING 	= 'btn-warning';
    const BTN_TYPE_DANGER 	= 'btn-danger';
    const BTN_TYPE_INVERSE 	= 'btn-inverse';
    const BTN_TYPE_LINK 	= 'btn-link';
    const BTN_TYPE_DEFAULT	= '';

    // buttons sizes
    const BTN_SIZE_LARGE	= 'btn-large';
    const BTN_SIZE_SMALL	= 'btn-small';
    const BTN_SIZE_MINI		= 'btn-mini';
    const BTN_SIZE_DEFAULT	= '';

    // buttons group sizes
    const BTN_GROUP_SIZE_LG	     = 'btn-group-lg';
    const BTN_GROUP_SIZE_SM	     = 'btn-group-sm';
    const BTN_GROUP_SIZE_XS	     = 'btn-group-xs';
    const BTN_GROUP_SIZE_DEFAULT = '';

    // local storage
    private static $storage;
    private static $instance;
    // form mode horizontal | inline
    private static $mode;

    /**
     * enable editor. if true then load editor api
     * @var null
     */
    private static $enable_editor=null;
    /**
     * enable picklist
     * @var null
     */
    private static $use_picklist=null;

    /**
     * list of picklist jquery objects
     * @var array
     */
    private static $picklist_elements=array();

    /**
     * path to theme
     * @var
     */
    private static $template_url;
    /**
     * lang object vs translations
     * @var null
     */
    private static $lang=null;

    private static $form_id;

    /**
     * custom action fo form submit success
     * @var
     */
    private static $js_success_action;

    /**
     * open form
     * @param $action
     * @param array $attributes
     * @param $lang
     * @return mixed
     */
    public static  function open($action, $attributes = array(), $lang=null)
    {
        $class = __CLASS__;

        if(self::$instance == null){
            self::$instance = new $class();
        }

        if ( ! isset($attributes['method'])){
            $attributes['method'] = 'post';
        }

        if ( ! isset($attributes['accept-charset'])){
            $attributes['accept-charset'] = 'utf-8';
        }

        if ( ! isset($attributes['id'])){
            $attributes['id'] = 'form';
        }

        self::$form_id = $attributes['id'];

        $atts = self::parseAttributes($attributes);

        self::$storage[] = "<form
                            data-parsley-namespace='data-parsley-'
                            data-parsley-validate
                            action='$action'
                            $atts>";

        $themes_path = Settings::instance()->get('themes_path');
        $current = Settings::instance()->get('app_theme_current');
        $base = $themes_path . $current;

        self::$template_url =   $base.'/';

        // localisation
        self::$lang = $lang;

        return self::$instance;
    }
    /**
     * @param bool $display
     * @return string
     * @throws \Exception
     */
    public static function close($display = true)
    {
        $formID = self::$form_id;
        if(empty(self::$js_success_action)) {
            $success = "
             $('.form-submit').removeAttr('disabled').removeClass('loading');
                if(d.s > 0 && d.r != '') {
                    setTimeout(function(){self.location.href= d.r; },2000);
                }
                engine.alert(d.t, d.m, d.e)
            ";
        } else {
            $success = self::$js_success_action  .
                "$('.form-submit').removeAttr('disabled').removeClass('loading');";
        }

        self::$storage[] = "
            <script>
                $(document).ready(function(){
                    $('.form-submit').click(function(){ $('#{$formID}:visible').submit(); });
                    $('#{$formID}').submit(function(e) {

                        var form = $(this);
                        e.preventDefault();
                        if ( form.parsley('validate') ) {
                             if($(ckeditor_11').length){
                               for ( instance in CKEDITOR.instances ) {
                                    CKEDITOR.instances[instance].updateElement();
                                }
                            }
                            form.ajaxSubmit({
                            target   : '#responce',
                            type     : 'post',
                            dataType : 'json',
                            beforeSend: function()
                            {
                                $('.form-submit').attr('disabled',true).addClass('loading');
                            },
                            success: function(d)
                            {
                                {$success}
                            }
                            });
                        } else {
                            engine.alert('Warning', 'Помилка валідації', 'error')
                        }
                    });
                });
            </script>
        ";
        self::$storage[] = '</form>';

        if(self::$enable_editor) {
            self::editorInit();
        }

        if(self::$use_picklist) {
            if(is_null(self::$lang)) throw new \Exception('Pick list used translations. Add $lang to Form::open() ');

            self::html("<script src='" . self::$template_url."/scripts/vendor/picklist/jquery-picklist.min.js'></script>");
            self::html("<link rel='stylesheet' href='" . self::$template_url."/scripts/vendor/picklist/jquery-picklist.css' />");
            $pickids = implode(',', self::$picklist_elements);
            $added = self::$lang->picklist['sourceListLabel'];
            $notadded = self::$lang->picklist['targetListLabel'];
            self::html("<script >
                $(function(){ $('$pickids').pickList({
                    sourceListLabel: '$notadded',
                    targetListLabel: '$added',
                    sortItems: true
                }) });
            </script>");
        }

        $st = self::$storage;

        self::$storage=null;

        if($display){
            echo implode('', $st);
        } else{
            return implode('', $st);
        }
    }



    /**
     * set custom action of form
     * input object d
     * @param $js_action
     */
    public static function customSuccessAction($js_action)
    {
        self::$js_success_action = $js_action;
    }



    /**
     * init editor
     */
    private static function editorInit()
    {

        $themes_path = SS::instance()->get('themes_path');
        $current     = SS::instance()->get('engine_theme_current');
        $base        = $themes_path . $current;

        $path = APPURL . $base . '/ckeditor_1/ckeditor_1.js';
        self::$storage[] = "<script src='$path'></script>";
    }

    /**
     * @param $name
     * @param $title
     * @param $form_element
     * @param array $attributes
     * @param bool $horizontal
     * @return mixed
     */
    public static function formGroup($name, $title, $form_element , $attributes = array(), $horizontal = false)
    {
        $i='';
        if(!empty($title)) $i = "<i
                                    class=\"icon-info-sign uses-tooltip\"
                                    data-toggle=\"tooltip\"
                                    data-placement=\"top\"
                                    data-original-title=\"$title\"
                                    title=\"$title\"
                                    ></i>";
        if($horizontal) {
            self::$storage[] = "<div class=\"form-horizontal\">
                <div class=\"form-group\">
                    <label class=\"col-lg-4 control-label\" >$name $i
                    <div class=\"col-lg-8\">
                        $form_element
                    </div>
                </div>
            </div>";
        } else {
            $class = 'form-group ';
            if(isset( $attributes['class'] )) $class .=  $attributes['class'];

            $attributes['class'] = $class;
            $attributes = self::parseAttributes($attributes);

            self::$storage[] = "<div $attributes>
                                    <label >$name $i</label>
                                    $form_element
                                </div>";
        }
        return self::$instance;
    }

    /**
     * @param $name
     * @param $title
     * @param $languages
     * @param $lang
     * @param string $def front | back
     * @param bool $horizontal
     * @return string
     */
    public static function languagesSwitch($name, $title, $languages, $lang, $def = 'front', $horizontal = false)
    {
        if(empty($languages)) return '';

        $i='';
        if(!empty($title)) $i = "<i
                                    class=\"icon-info-sign uses-tooltip\"
                                    data-toggle=\"tooltip\"
                                    data-placement=\"top\"
                                    data-original-title=\"$title\"
                                    title=\"$title\"
                                 ></i>";
        $buttons = ''; $lang_id = 0; $lang_code='';
        foreach ($languages as $o) {
            if($o[$def . '_default'] == 1) {
                $lang_id = $o['id']; $lang_code = $o['code'];
            }

            self::$storage[] = "<input type=\"hidden\" name=\"s_languages[{$o['id']}]\" value='{$o['code']}' class='s-languages'>";

            $buttons .= "<label class=\"btn btn-primary\">
                    <input type=\"radio\" name='ls' value='{$o['id']}'> {$o['name']}
                  </label>
                  ";
        }
        $icon = strtr(Form::icon('icon-sort-by-alphabet'),array('"'=>"'"));
        $icon_spinner = strtr(Form::icon('icon-spinner'),array('"'=>"'"));

        $btn_translate = self::buttonDropdown(
            self::button(
                $lang->core['translate'],
                $icon,
                array(
                    'class'   =>'btn-translate-all btn-info',
                    'onclick' => 'engine.translator.translateAll('. $lang_id .', \''. $lang_code .'\', this); return false;',
                    'data-complete-text' => $icon .' '. $lang->core['translate'],
                    'data-loading-text' => $icon_spinner .' '. $lang->core['begin_translation']
                )
            ),
            array(
                self::a(
                    $lang->core['translate_selected'],
                    '',
                    array(
                        'onclick' => 'engine.translator.translateSelected('. $lang_id .', \''. $lang_code .'\', this); return false;',
                        'data-complete-text' => $lang->core['translate_selected'],
                        'data-loading-text' =>  $lang->core['begin_translation']
                    )
                )
            ),
            self::BTN_TYPE_INFO
        );
        $formID = self::$form_id;
        if($horizontal) {
            self::$storage[] = "<div class=\"form-horizontal\">
                <div class=\"form-group\">
                    <label class=\"col-lg-4 control-label\" >$name $i
                    <div class=\"col-lg-8\">
                        <div class=\"btn-group\" data-toggle=\"buttons\" >
                            $buttons
                        </div>
                        $btn_translate
                    </div>
                </div>
            </div>";
        } else {
            self::$storage[] = "<div class=\"form-group\">
                                    <label >$name $i</label>
                                    <div class=\"btn-group\" data-toggle=\"buttons\" id=\"languages_switch{$formID}\">
                                        $buttons
                                    </div>
                                    $btn_translate
                                </div>
                                ";
        }

            self::$storage[] = "<script>
            $(document).ready(function(){
                $('.btn-group').button();

                $('#languages_switch{$formID} > label').click(function(){
                    var btn = $(this).find('input[type=radio]');
                    var id = btn.attr('value');
                    $('#{$formID} .language-switch.language-'+id)
                    .removeClass('hide')
                    .siblings('.language-switch')
                    .addClass('hide');
                });
                $('#languages_switch{$formID} > label:first').trigger('click');
            });</script>";

        return self::$instance;
    }

    public static function input(array $attr)
    {
        if(empty($attr['type'])) { $attr['type'] = 'text'; }
        $script = '';
        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }
        if(isset($attr['value']) && !empty($attr['value'])){
            $attr['value'] = htmlspecialchars($attr['value']);
        }

        $_attr = self::parseAttributes($attr);

        if(isset($attr['counter'])){
            $script = "
                <script>
                     $(\"#{$attr['id']}\").charCount(
                        ". json_encode($attr['counter']) ."
                    );
                </script>
            ";
            unset($attr['counter']);
        }
        if($attr['type'] == 'date'){
            $script = "
                <script>
                     $(\"#{$attr['id']}\").datepicker({dateFormat: 'yy-mm-dd'});
                </script>
            ";
        }

        return "<input class='form-control' $_attr> $script";
    }

    /**
     *
     * <div class="form-group">
    <label>File Input Field</label>
    <div>
    <div class="fileinput fileinput-new" data-provides="fileinput">
    <div class="input-group">
    <div class="form-control uneditable-input span3" data-trigger="fileinput"><i class="icon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
    <span class="input-group-addon btn btn-primary btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
    <a href="#" class="input-group-addon btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
    </div>
    </div>
    </div>
    </div>

     * @param array $attr
     * @return string
     */
    public static function fileInput(array $attr)
    {
        $attr['type'] = 'file';
        $script = '';

        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }

        $_attr = self::parseAttributes($attr);
        $select = self::$lang->form['fi_select'];
        $change = self::$lang->form['fi_change'];
        $remove = self::$lang->form['fi_remove'];

        return "
        <div>
            <div class='fileinput fileinput-new' data-provides='fileinput'>
                <div class='input-group'>
                    <div class='form-control uneditable-input span3' data-trigger='fileinput'>
                        <i class='icon-file fileinput-exists'></i>
                        <span class='fileinput-filename'></span>
                    </div>
                    <span class='input-group-addon btn btn-primary btn-file'>
                        <span class='fileinput-new'>{$select}</span>
                        <span class='fileinput-exists'>{$change}</span>
                        <input $_attr>
                    </span>
                    <a href='#' class='input-group-addon btn btn-primary fileinput-exists' data-dismiss='fileinput'>{$remove}</a>
                </div>
            </div>
    </div>
        $script
    ";
    }

    public static function hidden ($name, $value='')
    {
        $id = rtrim(str_replace(array('[', '][', ']'), array('_', '_', ''), $name), '_');
        self::$storage[] = "<input id='{$id}' type='hidden' name='$name' value='$value' />";

        return self::$instance;
    }

    /**
     * @param array $attr
     * @param string $addon_left
     * @param string $addon_right
     * @return string
     */
    public static function inputGroup(array $attr, $addon_left = '', $addon_right = '')
    {
        if(empty($attr['type'])) { $attr['type'] = 'text'; }
        $attr['id'] = '';
        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }

        $_attr = self::parseAttributes($attr);

        $left_adn  = (!empty($addon_left)) ? "<span class=\"input-group-addon\">$addon_left</span>" : '';
        $right_adn = (!empty($addon_right)) ? "<span class=\"input-group-addon\">$addon_right</span>" : '';

        return "
        <div class=\"input-group\">
          $left_adn
          <input class='form-control' $_attr>
          $right_adn
        </div>
        ";
    }
    public static function textarea(array $attr)
    {
        $attr['id'] = ''; $script = '';
        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }
        $value = '';
        if(isset($attr['value'])){
            $value = $attr['value'];
            unset($attr['value']);
        }

        if(isset($attr['counter'])){
            $script = "
                <script>
                     $(\"#{$attr['id']}\").charCount(
                        ". json_encode($attr['counter']) ."
                    );
                </script>
            ";
            unset($attr['counter']);
        }

        $_attr = self::parseAttributes($attr);

        return "<textarea class='form-control' $_attr>$value</textarea>$script";
    }
    public static function editor(array $attr)
    {
        // init editor
        self::$enable_editor = 1;
        $attr['id'] = ''; $script = '';
        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }
        $value = '';
        if(isset($attr['value'])){
            $value = $attr['value'];
            unset($attr['value']);
        }

        if(isset($attr['counter'])){
            $script = "
                <script>
                     $(\"#{$attr['id']}\").charCount(
                        ". json_encode($attr['counter']) ."
                    );
                </script>
            ";
            unset($attr['counter']);
        }

        $_attr = self::parseAttributes($attr);

        return "<textarea class='form-control ckeditor_1' $_attr>$value</textarea>$script";
    }

    public static function select(array $attr, array $options, $selected=null)
    {
        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }
        $_attr = self::parseAttributes($attr);

        $_options = '';

        foreach ($options as $option) {
            if(!isset($option->selected)) $option->selected='';
            if(!isset($option->value)) $option->value = $option->id;

            if($selected != null){
                if(is_array($selected)){
                    if(in_array($option->value, $selected)){
                        $option->selected = 'selected';
                    }
                } else {
                    if( $selected == $option->value){
                        $option->selected = 'selected';
                    }
                }
            }

            // data-attributes
            $o_attr = '';
            if(isset($option->data)){
                $o_attr = str_replace('"',"'", self::parseAttributes($option->data));
            }

            $_options .= "<option {$o_attr} {$option->selected} value='{$option->value}'>{$option->name}</option>";
        }

        return "<select class=\"form-control\" $_attr>$_options</select>
                <script>$('#{$attr['id']}').select2();</script>
                ";
    }

    /**
     * @param array $attr
     * @param array $options
     * @param array $selected
     * @return string
     */
    public static function pickList(array $attr, array $options, $selected=array())
    {
        if(!empty($attr['name']) && empty($attr['id']) ) {
            $attr['id'] = rtrim(str_replace(array('[','][',']'),array('_','_',''),$attr['name']),'_');
        }

        $_attr = self::parseAttributes($attr);

        $_options = '';
        foreach ($options as $option) {
            $option->selected = isset($option->selected) ? $option->selected : '';
            if($option->selected == '' && in_array($option->id, $selected)) $option->selected = 'selected';
            $option->value = isset($option->value) ? $option->value : $option->id;
            $_options .= "<option {$option->selected} value='{$option->value}'>{$option->name}</option>";
        }
        self::$use_picklist=1;

        self::$picklist_elements[] = '#' . $attr['id'];
//        print_r(self::$picklist_elements);
        return "<select multiple class=\"form-control\" $_attr>$_options</select>
                ";
    }

    /**
     * @param $title
     * @param string $class
     */
    public static function openPanel($title, $class='')
    {
        self::$storage[] = "
<div class='panel panel-default panel-block {$class}'>
    <div class='list-group'>
        <div class='list-group-item'>
        <h4 class='section-title'>$title</h4>
        ";
    }
    public static function closePanel()
    {
        self::$storage[] = "</div></div></div>";
    }
    public static function openRow()
    {
        self::$storage[] = "<div class='row'>";
    }
    public static function closeRow()
    {
        self::$storage[] = "</div>";
    }
    public static function openCol($class)
    {
        self::$storage[] = "<div class='$class'>";
    }
    public static function closeCol()
    {
        self::$storage[] = "</div>";
    }
    public static function html($html)
    {
        self::$storage[] = $html;
    }
    public static function buttonsRadio($name, array $options)
    {
        $id = rtrim(str_replace(array('[',']'),'_',$name),'_');
        $buttons = '';
        if(!empty($options)) foreach ($options as $o) {
            $selected = isset($o['selected']) && $o['selected']  ? 'selected' : '';
            $buttons .= "<label class=\"btn btn-primary\">
                    <input type=\"radio\" name=\"$name\" value='{$o['value']}' $selected> {$o['name']}
                  </label>";
        }

        return "<div class=\"btn-group\" data-toggle=\"buttons\" id=\"$id\">
                    $buttons
                </div>
        <script>$(document).ready(function(){ $('.btn-group').button(); });</script>
";
    }
    public static function checkboxGroup($name, array $options)
    {
        $id = rtrim(str_replace(array('[',']'),'_',$name),'_');
        $inputs = '';
        if(!empty($options)) foreach ($options as $o) {
            $checked = isset($o['checked']) && $o['checked']  ? 'checked' : '';
            $inputs .= "<label class=\"checkbox\">
                    <input type=\"hidden\" name=\"$name\" value='0'>
                    <input type=\"radio\" id='{$id}' class='chb' name=\"$name\" value='1' '{$checked}'> {$o['name']}
                  </label>";
        }

        return "<div class=\"btn-group\" data-toggle=\"buttons\" id=\"$id\">
                    $inputs
                </div>
        <script>$(document).ready(function(){ $('.chb').uniform(); });</script>
";
    }
    public static function footer()
    {
        self::$storage[] = "
        <footer class=\"panel-footer text-right\">
          <button class=\"btn btn-success\" type=\"submit\">Зберегти</button>
        </footer>
        ";
    }

    public static function buttonSwitch(
        $inp_name,
        $checked   = 0,
        $data_on   = "<i class='fa-ok fa-white'></i>",
        $data_off  = "<i class='fa-remove'></i>"
    )
    {

        if(!empty($inp_name) && empty($attr['id']) ) {
            $id = rtrim(str_replace(array('[',']'),'_',$inp_name),'_');
        }
        $checked = $checked == 1 ? 'checked' : '';
        return "
                    <input type=\"hidden\" name=\"{$inp_name}\" value='0'>
                    <input class=\"form-control\"  id=\"{$id}\" type=\"checkbox\" name=\"$inp_name\"  $checked value='1'>
            <script>
            $(\"#{$id}\")[\"bootstrapSwitch\"]();
            </script>
        ";
    }
//    public static function buttonSwitch($inp_name,$name,$value)
//    {
//        $checked = $value==1?"checked":"";
//        return "<div class=\"switch\">
//                            <span class=\"sw-label\">".$inp_name."</span>
//                            <label>
//                                <input type=\"checkbox\" name=$name $checked value=\"$value\">
//                                <div class=\"sw\">
//                                    <span class=\"label\">ні</span>
//                                    <span class=\"sw-btn\"></span>
//                                    <span class=\"label\">так</span>
//                                </div>
//                            </label>
//                        </div>
//
//                        <script>
//                        $(\"document\").ready(function(){
//                        $(\".switch input\").on(\"change\", function(){
//                             if($(this).is(\":checked\")){
//                                $(this).attr(\"value\", \"1\");
//                             }else{
//                                 $(this).attr(\"value\", \"0\");
//                             }
//                         });
//                        });
//                        </script>
//                        ";
//    }
    public static function checkbox(
        $inp_name,
        $checked   = 0
    )
    {

        if(!empty($inp_name) && empty($attr['id']) ) {
            $id = rtrim(str_replace(array('[',']'),'_',$inp_name),'_');
        }
        $checked = $checked == 1 ? 'checked' : '';
        return "
           <input type=\"hidden\" name=\"{$inp_name}\" value=0>
           <input type=\"checkbox\" id=\"$id\" name=\"$inp_name\"  $checked value=1>
         ";
    }
    /**
     * create button
     * @param $name
     * @param string $icon
     * @param array $attributes
     * @return string
     */
    public static function button($name, $icon = '', $attributes=array())
    {
        if(!isset($attributes['class'])) $attributes['class'] = 'btn';
        else {
            $attributes['class'] = 'btn ' . $attributes['class'];
        }
        $attr  = self::parseAttributes($attributes);
        return "<button form='form' $attr>$icon $name</button>\r\n";
    }

    public static function link($name, $icon = '', $attributes=array())
    {
        if(!isset($attributes['class'])) $attributes['class'] = 'btn';
        else {
            $attributes['class'] = 'btn ' . $attributes['class'];
        }
        $attr  = self::parseAttributes($attributes);
        return "<a $attr>$icon $name</a>";
    }

    /**
     * make link
     * @param $name
     * @param string $icon
     * @param array $attributes
     * @return string
     */
    public static function a($name, $icon = '', $attributes=array())
    {
        if(empty($attributes['href'])) $attributes['href'] = 'javascript:void(0);';
        $attr  = self::parseAttributes($attributes);
        return "<a $attr>$icon $name</a>";
    }

    /**
     * create icon
     * @param $type
     * @param bool $white
     * @return string
     */
    public static function icon($type, $white=true)
    {
        $white = $white ? 'fa' : 'fa';
        return "<i class=\"$type $white\"></i>";
    }

    /**
     * create buttons group
     * @param array $buttons
     * @param string $size
     * @return string
     */
    public static function buttonsGroup(array $buttons, $size = self::BTN_GROUP_SIZE_DEFAULT)
    {
        if(empty($buttons)) return '';

        $out = "<div class='btn-group $size'>";
        $out .= implode('', $buttons);
        $out .= '</div>';

        return $out;
    }

    /**
     * create Buttons dropdown
     * template:
     * <div class="btn-group">
        <button type="button" class="btn btn-danger">Action</button>
        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a></li>
        </ul>
     </div>
     * @param $button
     * @param array $links
     * @param string $btn_class
     * @return string
     */
    public static function buttonDropdown($button, array $links, $btn_class='btn-primary')
    {
        if(empty($links)) return $button;

        $_links='';
        foreach ($links as $k=>$link) {
            $_links .= "<li>$link</li>";
        }

        return "
            <div class=\"btn-group\">
              $button
              <button type=\"button\" class=\"btn {$btn_class} dropdown-toggle\" data-toggle=\"dropdown\">
                <span class=\"caret\"></span>
                <span class=\"sr-only\">Toggle Dropdown</span>
              </button>
              <ul class=\"dropdown-menu dropdown-menu-arrow\" role=\"menu\">
                 $_links
              </ul>
            </div>
        ";
    }

    /**
     * parse form element attributes
     * @param array $data
     * @return string
     */
    private static function parseAttributes(array $data)
    {
        $atts = '';

        if(empty($data)) return '';

        foreach ($data as $key => $val){
            if(empty($val) or is_array($val)) continue;
            $atts .= ' '.$key.'="'.$val.'"';
        }

        return $atts;
    }

    /**
     * clear storage
     */
    public function __destruct()
    {
//        unset(self::$storage);
    }
}
