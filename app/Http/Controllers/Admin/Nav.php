<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class Nav extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->setTitle('Меню');
        $this->m = new \App\Model\Admin\Nav();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->setButtons
        (
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Nav',
                                'action' => 'create',
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('nav_menu')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Nav',
                    'action' => 'show'
                ]
            ))
            ->setTitle('Меню')
            ->th('#')
            ->th('Назва')
            ->th('Функції');


        $this->setContent($this->dt->render());

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class' => 'btn-success form-submit'
                )
            )
        );

        $content = view('admin.modules.content.nav',
            [
                'action' => route('ajax.load',
                    [
                        'directory' => 'Admin',
                        'controller' => 'Nav',
                        'action' => 'process',
                    ]),
                'process' => 'create',
                'id' => 0,
                'items' => $this->m->getItems(),
                'nav_id'=>0
            ]
        );
        $this->setContent($content);

        return $this->output();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        $this->dt->table('nav_menu n')
            ->searchCol('n.id,n.name')
            ->get("id,name")
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'Nav',
                    'action' => 'index',
                    'id' => $row->id,
                ]), Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Nav',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "nav.delete($row->id)"
                    )
                )
            );
        }
        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $buttons = array(
            Form::button(
                'Назад',
                Form::icon('icon-external-link'),
                array(
                    'class' => 'btn-link',
                    'onclick' => 'self.location.href=\'Nav/index\''
                )
            ),
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class' => 'btn-success form-submit'
                ))
        );
        $data = $this->m->data('nav_menu', $id);

        $pages = $this->m->getItems($id);
//        $this->dump($pages);

        $content = view('admin.modules.content.nav',
            [
                'action' => route('ajax.load',
                    [
                        'directory' => 'Admin',
                        'controller' => 'Nav',
                        'action' => 'process',
                        'id' => $id
                    ]),
                'process' => 'edit',
                'id' => 0,
                'items' => $pages,
                'data' => $data,
                'nav_id'=>$id
            ]
        );

        // output
        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    public function process($id = 0)
    {
        $s = 0;
        $e = $this->errors['warning'];
        $data = Input::get('data');
        $process = $data['process'];


        if (
        empty($data['name'])
        ) {
            $this->error[] = 'Заповніть всі поля';
        }
        // add or update values
        if (empty($this->error)) {
            switch ($data['process']) {
                case 'create':
                    unset($data['process']);
                    $id = $this->m->insert($data);
                    if ($id > 0) {
                        $this->addItems($id);
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->m->error();
                    }
                    break;
                case 'edit':

                    if (empty($id))  return '';
                    unset($data['process']);
                    $s = $this->m->edit($id, $data);

                    $s += $this->updateItems($id);
                    if ($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->mg->error();
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0, // status
            'process' => $process,
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->error) // error message
        ));
    }

    private function addItems($id)
    {
        $items = Input::get('items');

        if(!isset($items)) return 0;
        $sort = 0; echo '';
        foreach ($items as $k => $content_id) {
            $this->m->addItem($id, $content_id, $sort);
            $sort++;
        }
        return 1;
    }

    private function updateItems($id)
    {
        $items = Input::get('items');

        $this->m->deleteItems($id);
        if(!isset($items)) return 0;


        return $this->addItems($id);
    }

    public function sort($nav_id,$sort)
    {
        $sort = explode(',',$sort);
        $i=1;
        foreach($sort as $k=>$id){
            $this->m->sort($nav_id, $id, $i);
            $i++;
        }
        return $i;
    }

    public function delete($id)
    {
        return $this->m->del($id);
    }

}
