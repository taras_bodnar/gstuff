<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashBoard extends Admin
{

    private $widgets = array();
    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\DashBoard();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->setTitle('Вітаємо');
         $this->setContent(view('admin/modules/dashboard/index',[
             'widgets' => $this->widgets()
         ]));

        return $this->output();
    }

    private function widgets()
    {
        $this->widgetLastPages('page');
        $this->widgetLastPages('gallery');
        $this->widgetLastPages('blog');
        $this->widgetDiscSpace();
        return view('admin/modules/dashboard/widgets/index',
            [
                'content' => implode(' ', $this->widgets)
            ]
        );
    }

    private function widgetDiscSpace()
    {
        $total = 100;
        $_total = disk_total_space($_SERVER['DOCUMENT_ROOT']);
        $_free  = disk_free_space($_SERVER['DOCUMENT_ROOT']);

        $p = 100 / $_total;
        $free = ceil($_free  * $p);
        $busy = $total - $free;


        $this->widgets[] = view(
            'admin/modules/dashboard/widgets/disc_space',
            array(
                'data' => json_encode(array(
                    array(
                        'label' => 'Вільно',
                        'value' =>  $free
                    ),
                    array(
                        'label' => 'Зайнято',
                        'value' =>  $busy
                    )
                )
            )
            )
        );

        return $this;
    }

    public function widgetLastPages($type='page', $color='success', $img=false)
    {
        $title = '';
        $pages = $this->m->getLastPages($type);
        if(empty($pages)) return '';

        foreach ($pages as $i=>$page) {
            $pages[$i]->editedon = $this->timeAgo(strtotime($page->editedon));
        }

        switch($type){
            case 'page':
                $title = 'Останні сторінки';
                $type = 'pages';
                break;
            case 'category':
                $title = 'Категорії';
                $type = 'categories';
                break;
            case 'manufacturer':
                $title = 'Виробники';
                $type = 'manufacturers';
                break;
            case 'product':
                $title = 'Товари';
                $type = 'products';
                break;
            case 'gallery':
                $title = 'Останні фотоальбоми';
                $type = 'gallery';
                break;
            case 'blog':
                $title = 'Останні Статті';
                $type = 'blog';
                break;

        }
        $this->widgets[] = $this->template->view(
            'modules/dashboard/widgets/last_pages',
            array(
                'pages' => $pages,
                'type'  => ucfirst($type),
                'title' => $title,
                'color' => $color
            )
        );
        return $this;
    }

    private function timeAgo($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
            30 * 24 * 60 * 60  =>  'month',
            24 * 60 * 60  =>  'day',
            60 * 60  =>  'hour',
            60  =>  'minute',
            1  =>  'second'
        );

        $time_ago = array(
            'year'=>'років',
            'month'=>'місяців',
            'day'=>'днів',
            'hour'=>'годин',
            'minute'=>'хвилин',
            'second'=>'секунд',
            'ago'=>'тому',
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' .
                (
                $r > 1 ?
                    isset($time_ago[$str]) ? $time_ago[$str] : $str
                    : $str
                )
                . ' ' . $time_ago['ago'];
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
