<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\content\Tree;
use App\Http\Controllers\app\Template;
use App\Model\Admin\BreadCrumb;
use App\Model\Admin\Nav;
use App\Model\Admin\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Admin\Pages;
use App\Model\Admin\Languages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;


class Admin extends Controller
{
    protected $errors = array(
        'error' => 'error',
        'success' => 'success',
        'info' => 'info',
        'warning' => 'warning'
    );
    private $content;
    private $buttons;
    private $sidebar;
    private $title;
    private $component;
    private $plugins;
    protected $route;

    public function __construct(){
//        Session::forget('admin.user');
//        dd(Session::get('admin.user')->id);
        if(!Session::has('admin.user')) {
            return Redirect::to('laradmin');
        }
        $this->dt = new DataTables();
        $route = Route::getCurrentRoute()->parameters();
        $this->languages_id = Session::get('admin.languages_id')?Session::get('admin.languages_id'):1;

        //start ajax request
        $uri = Route::getCurrentRoute()->uri();
        $uri = explode('/',$uri);

        if($uri[1]!='request' && $uri[0]!='plugins') {

        $this->route = "/admin/".$route['directory']."/".$route['controller'];
        $this->request = new Request();
        $this->mode = "admin";
        $this->template = Template::instance($this->mode);

       if(!empty($route)) {

           $dir = $route['directory'];
           $controller = ucfirst($route['controller'])=="Content"?"DashBoard":ucfirst($route['controller']);

           $this->component = '/admin/'.$dir.'/'.$controller;
           $rang = Users::checkRang($this->component);

           if(!empty($rang)) {
               $this->setTitle($rang->name);
               if(Session::get('admin.user')->rang > 0 ) {
                   if($rang->rang>Session::get('admin.user')->rang ){
                       echo $this->template->view('modules/content/errors/403');
                       Session::forget('admin');
                        Redirect::to('laradmin');
                       exit();
                   }
               }
           }
       }
        }
    }

    public function request()
    {
        $route = Route::getCurrentRoute()->parameters();

        $controller = $route['controller'];

        $method = isset($route['method'])?$route['method']:"index";

        $param1 = isset($route['param1'])?$route['param1']:'';
        $param2 = isset($route['param2'])?$route['param2']:'';
        $param = explode('/',$param1);
        $params = explode('/',$param2);
        $params = array_merge($param,$params);
        return $this->module($controller,$method,$params);
    }

    private  function module($controller, $action = 'index', $params = array())
    {
        $mod_path = '\app\Http\Controllers\Admin\\';
        $controller = $mod_path . $controller;


        $className = ltrim($controller, '\\');

        $fileName = '';
        $namespace = '';
        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }

        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
        $fileName = $_SERVER['DOCUMENT_ROOT'] . '/' . $fileName;

        if (is_readable($fileName)) {

            require_once($fileName);

            $c = $namespace . '\\' . $className;

            if (class_exists($c)) {
                ob_start();

                $controller = new $c;

                $action = (is_callable(array($controller, $action))) ? $action : 'index';

                if (!empty($params)) {

                    echo call_user_func_array(array($controller, $action), $params);
                } else {
                    echo call_user_func(array($controller, $action));
                }
                return ob_get_clean();
            }
        }

        throw new Exception("Module {$controller} :: {$action} issues.");
    }

    protected function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    protected  function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    protected function setSidebar($sidebar)
    {
        $this->sidebar = $sidebar;
        return $this;
    }

    protected function output()
    {
        return view("admin/content",array(
            'title_block'=>view('admin/titleblock',
                [
                    'title'=>$this->title,
                    'breadcrumb'=>$this->breadcrumb(),
                ]
                ),
            'nav'=>$this->nav(),
            'sidebar'=>$this->sidebar,
            'content'=>$this->content
        ));
    }

    public function setLanguages()
    {
        return view('admin/chunk/lang',[
           'languages'=>Languages::geAll(1),
           'language_default'=>Languages::where('front_default',1)->where('back_default',1)->first(),
        ]);
    }

    protected function setButtons($buttons)
    {
        if(is_string($buttons)){
            $this->buttons = array($buttons);
        } else {
            $this->buttons = $buttons;
        }
        return $this;
    }

    public function nav()
    {
        $items = Nav::getNav(0);

        foreach ($items as $k=>$item ) {
            if($item->isfolder==0) continue;
            $items[$k]->children = Nav::getNav($item->components_id);
        }

        return view("admin/chunk/header",
            [
                'items'=>$items
            ]
            );
    }

    protected function getPlugins($id, $action)
    {

        $plugins = new Plugins();
        $this->plugins = $plugins->get($id, $action);

        return $this->plugins;
    }

    /**
     * додає кнопку перед кнопками
     * @param $button
     * @return $this
     */
    protected function prependToButtons($button)
    {
        array_unshift($this->buttons, $button);
        return $this;
    }

    private function breadcrumb()
    {
        $bm = new BreadCrumb();
        $route = Route::getCurrentRoute()->parameters();
        $dir = $route['directory'];
        $controller = ucfirst($route['controller']);
        $component = '/admin/'.$dir.'/'.$controller;
        $id = $bm->controllerId($component);

        return view('admin/modules/content/breadcrumb', array(
            'items' => $bm->crumbs($id->id),
            'buttons'=>$this->buttons,
            'active'=>$this->component
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
