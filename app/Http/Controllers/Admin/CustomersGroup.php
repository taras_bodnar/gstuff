<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\content\Tree;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Admin\Languages;
use App\Model\Admin\UsersGroup as UserModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class CustomersGroup extends Admin
{
    private $rang=array(1, 99);

    public function __construct()
    {
        parent::__construct();
        $this->t = new DataTables();
        $this->m = new \App\Model\Admin\UsersGroup();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id)
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('fa fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'CustomersGroup',
                                'action'=>'create',
                                'id'=>$parent_id
                            ]
                        )."'"
                ))
        );

        $this->t->setId('components')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'CustomersGroup',
                    'action'=>'items',
                    'id'=>$parent_id
                ]
            ))
            ->setTitle('Групи користувачів')
            ->th('#')
            ->th('Назва')
            ->th('Ранг')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->t->render());

        return $this->output();
    }

    public function items($parent_id)
    {
        $parent_id = empty($parent_id)?0:$parent_id;
         $this->t
//             ->debug(1)
            -> table('users_group g')
            -> where("g.parent_id=$parent_id ")
            -> where("g.rang between {$this->rang[0]} and {$this->rang[1]}")
            -> join("join users_group_info i on i.users_group_id = g.id and i.languages_id = ".$this->languages_id)
            -> get(
                array(
                    'g.id',
                    'i.name',
                    'i.description',
                    'g.rang'
                )
            )
            -> searchCol(array('g.id','g.rang','i.name'))
            -> execute();

        $r   = $this->t->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'UsersGroup',
                    'action'=>'index',
                    'id'=>$row->id,
                ]),
                $row->rang,
                Form::button(
                    '',
                    Form::icon('fa fa-edit'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'UsersGroup',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    )).
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick' => "users.group.delete($row->id)"
                    )
                )
            );
        }


        return $this->t->renderJSON($res, $this->t->getTotal());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id='')
    {

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content =  view('admin.modules.users.group.form',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'UsersGroup',
                        'action'=>'process',
                        'id'=>$id
                    ]),
                'process'=>'create',
                'languages'=>Languages::geAll(),
                'parent'=>UserModel::getAll()
            ]
        );
        $this->setContent($content);

        return $this->output();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($id) || empty($id)) return 'WRONG ID';

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('fa fa-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(
            Form::link(
                'Повернутись',
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => route('method.load',
                        [
                            'directory'=>'Admin',
                            'controller'=>'UsersGroup',
                            'action'=>'index'
                        ]
                    )
                )
            )
        );

        $data  = $this->m->data('users_group',$id);

        $info = UserModel::getInfo($id);

        $content = view('admin.modules.users.group.form',[
            'data'=>$data,
            'info'=>$info,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'UsersGroup',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit',
            'languages'=>Languages::geAll(),
            'parent'=>UserModel::getAll($id)
        ]);

        $this->setContent($content);

        return $this->output();
    }

    /**
     * @param int $id
     * @return string
     */
    public function process($id=0)
    {
        $s = 0;
        $e[] = array();


        $s=0;
        $e = $this->errors['warning'];
//        $r = './components/index/'. $_POST['data']['parent_id']; // redirect url
        $info = Input::get('info');
        $data = Input::get('data');

        // check required fields
        if(
            empty($data['controller']) ||
            empty($data['rang'])
        ) {
            $this->errors[] ='Заповніть всі поля';
        }


        // add or update values
        if(empty($this->error)) {

            $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $s = UserModel::createUsersGroup($data, $info);

                    if($s > 0) {
//                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    $r='';
                    unset($data['process']);
                    $s = UserModel::updateGroup($id, $data, $info);
                    if($s > 0) {
//                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'r' => '', // redirect url
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    public function output()
    {
        $tree = new Tree(
            'content',
            route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Users',
                    'action' => 'tree'
                ]),
            '',
            array(
                Tree::contextMenu(
                    "Список груп",
                    "fa fa-list",
                    'self.location.href="/admin/Admin/UsersGroup/index/"+id;'
                ),
                Tree::contextMenu(
                    "Список Користувачів",
                    "fa fa-list",
                    'self.location.href="/admin/Admin/Users/index/"+id;'
                ),
                Tree::contextMenu(
                    "Додати групу",
                    "fa fa-plus",
                    'self.location.href="/admin/Admin/UsersGroup/create/"+id;'
                ),
                Tree::contextMenu(
                    "Редагувати групу",
                    "fa fa-edit",
                    'self.location.href="/admin/Admin/UsersGroup/edit/"+id;'
                ),
                Tree::contextMenu(
                    "Видалити групу",
                    "fa fa-remove",
                    'onclick = content.delete(id);'
                )
            ),
            '',
            true
        );
        $this->setSidebar($tree->render());
        return parent::output();
    }
}
