<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Languages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Features extends Admin
{
    private $cat;
    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\Features();
        $this->languages = new Languages();
        $this->setTitle('Динамічні параметри');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'Features',
                                'action'=>'create'
                            ]
                        )."'"
                ))
        );

        $this->dt->setId('features')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Features',
                    'action'=>'show'
                ]
            ))
            ->setTitle('Динамічні параметри')
            ->th('#')
            ->th('Назва')
            ->th('Показувати в списку')
            ->th('Показувати в порівняннях')
            ->th('Показувати в фільтрі')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->dt->render());

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $owner_id = 1;
        $id = $this->m->createAuto($owner_id);
        return $this->edit($id);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->dt->table('features f')
            ->join("join features_info i on i.features_id = f.id and i.languages_id = 1")
//            -> debug()
            -> searchCol('f.id,i.name,f.type')
            -> get("f.id,i.name,f.type,f.published,f.show_list,f.show_compare,f.show_filter")

            -> execute();

        $r   = $this->dt->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'Features',
                    'action'=>'index',
                    'id'=>$row->id,
                ]),
                Form::link(
                    '',
                    Form::icon($row->show_list==0?'fa-eye-slash':'fa-eye'),
                    array(
                        'class'    =>'btn-primary',
                        'onclick' => 'content.features.show('.$row->id.',\'show_list\','.$row->show_list.')'
                    )
                ),
                Form::link(
                    '',
                    Form::icon($row->show_compare==0?'fa-eye-slash':'fa-eye'),
                    array(
                        'class'    =>'btn-primary',
                        'onclick' => 'content.features.show('.$row->id.',\'show_compare\','.$row->show_compare.')'
                    )
                ),
                Form::link(
                    '',
                    Form::icon($row->show_filter==0?'fa-eye-slash':'fa-eye'),
                    array(
                        'class'    =>'btn-primary',
                        'onclick' => 'content.features.show('.$row->id.',\'show_filter\','.$row->show_filter.')'
                    )
                ),
                Form::link(
                    '',
                    Form::icon($row->published==0?'fa-eye-slash':'fa-eye'),
                    array(
                        'class'    =>'btn-primary',
//                        'title'   => $this->lang->core['published_tip'],
                        'onclick' => 'content.features.pub('.$row->id.','.$row->published.')'
                    )
                ) .
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'Features',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick'=>"features.delete($row->id)"
                    )
                )
            );
        }


        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($id) || empty($id)) return 'WRONG ID';
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(Form::link(
            'Повернутись',
            Form::icon('icon-external-link'),
            array(
                'class'=>'btn-link',
                'href' => route('method.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Features',
                        'action'=>'index'
                    ]
                )
            )
        ));

        $data  = $this->m->data('features',$id);

        $info = $this->m->fulInfo('features_info', 'features_id', $id);


        $content = view('admin.modules.features.form',[
            'data'=>$data,
            'info'=>$info,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Features',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit',
            'features_id'=>$id,
            'languages'=>$this->languages->geAll(),
            'type'        => $this->m->getType($data->type),
            'categories'  => $this->categories(0,'', $this->m->getSelectedCategories($id)),
            'content_type' => $this->m->getContentType($id),
            'features_values' => $this->getValues($id)
        ]);

        $this->setContent($content);

        return $this->output();
    }

    private function categories($parent_id=0, $parent_name='', $selected=array())
    {
        $r = $this->m->categories($parent_id);
        foreach ($r as $row) {
            if($parent_name != '') {
                $row->name = $parent_name .' / '. $row->name;
            }
            if(in_array($row->id, $selected)) {
                $row->selected = 'selected';
            } else {
                $row->selected = '';
            }
            $this->cat[] = $row;
            if($row->isfolder) {
                $this->categories($row->id, $row->name, $selected);
            }
        }

        return $this->cat;
    }

    public function getValues($features_id, $type=null)
    {

        if(! $type){
            $type = $this->m->data('features', $features_id, 'type');
            $type = $type->type;
        }

        if(! in_array($type, array('radiogroup', 'checkboxgroup' ,'select', 'sm','so'))) return '';

        $values = $this->m->getValues($features_id);

        $res = '';

        foreach ($values as $values_id => $info) {
            $info = json_encode($info);
            $info = json_decode($info,true);
            $res .= view(
                "admin.modules.features.value",
                array(
                    'features_id' => $features_id,
                    'value_id'    => $values_id,
                    'values'      => $info
                )
            );
        }


        return view(
            'admin.modules.features.values',
            array(
                'features_id' => $features_id,
                'values'      => $res
            )
        );
    }

    public function process($id=0)
    {
        $s=0;
        $e = $this->errors['warning'];

        $data = Input::get('data');
        $info = Input::get('info');
        $process = $data['process'];

        // add or update values
        if(empty($this->error)) {
            switch($data['process']){
                case 'edit':
                    if(empty($id)) return '';
                    unset($data['process']);
                    $data['auto'] = 0;
                    $r='';
                    $s = $this->m->edit($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->model->error() ;
                    }
                    break;
                default:
                    break;
            }
            if($id > 0){

                // привязка до контенту
                $this->setContentFeatures($id);

//              ривязка до типів контенту
                $this->setContentTypeFeatures($id);

//              оновлюю значення параметрів
                $features_value = Input::get('features_value');
                if(isset($features_value)){
                    $this->m->updateValuesInfo($features_value);
                }
            }

        }

        return json_encode(array(
            's' => $s > 0 , // status
            'id' => $id,
            'r' => $process, // redirect url
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    private function setContentFeatures($features_id)
    {
        $content_features = Input::get('content_features');
        $this->m->deleteAllContentFeatures($features_id);
        if(isset($content_features))
            foreach ($content_features as $k=>$content_id) {
                $this->m->createContentFeatures($features_id, $content_id );
            }

        return 1;
    }

    private function setContentTypeFeatures($features_id)
    {
        $content_type_features = Input::get('content_type_features');
        // clear all content features
        $this->m->deleteAllContentTypeFeatures($features_id);

        if(isset($content_type_features) && !empty($content_type_features)){
            // не може бути і те і те
            $this->m->deleteAllContentFeatures($features_id);
            foreach ($content_type_features as $k=>$content_type_id) {
                $this->m->createContentTypeFeatures($features_id, $content_type_id );
            }
        }

        return 1;
    }

    public function delete($id)
    {
        return $this->m->del($id);
    }

    public function createValue($features_id)
    {
        $value_id = $this->m->createValue($features_id);

        $values = array();
        foreach ($this->languages->geAll() as $row) {
            $values[] = array(
                'languages_id' => $row->id,
                'placeholder'  => $row->name,
                'value'        => ''
            );
        }

        return view(
            "admin/modules/features/value",
            array(
                'features_id' => $features_id,
                'value_id'    => $value_id,
                'values'      => $values
            )
        );
    }

    public function removeValue($id)
    {
        return $this->m->removeValue($id);
    }

    public function publish($id, $col, $published)
    {
        $published = $published == 1 ? 0 : 1;
        return $this->m->show($id, $col, $published);
    }
}
