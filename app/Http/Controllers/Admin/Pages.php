<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\DataTables;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Admin\content\Tree;

class Pages extends Content
{

    public function __construct()
    {
        parent::__construct();

        $this->setType('page');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id = '')
    {
        $this->setButtons
        (
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Pages',
                                'action' => 'create',
                                'id' => $parent_id
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('content')
            ->setConfig('sortable', true)
//            ->setConfig('columns', array(
//                'orderable'=> false,
//
//            ))
            ->sortableConf('content','id')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Pages',
                    'action' => 'items',
                    'id' => $parent_id
                ]
            ))
            ->setTitle('Сторінки')
            ->th('#')
            ->th('Назва')
            ->th('Функції');


        $this->setContent($this->dt->render());

        return $this->output();

    }

    public function items($parent_id = 0)
    {
        $parent_id = empty($parent_id) ? 0 : $parent_id;
        $this->dt->table('content c')
            ->join("join content_info ci on ci.content_id=c.id and ci.languages_id=" . $this->languages_id)
            ->where("c.parent_id=$parent_id and c.type_id={$this->type_id->id}")
            ->searchCol('c.id,name')
            ->get("c.id,name,published,c.sort")
            ->orderBy('c.sort asc')
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                '<i id='.$row->id.' style="cursor: move;opacity:0.5" class="fa fa-reorder"></i>',
                $row->id,
                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'Pages',
                    'action' => 'index',
                    'id' => $row->id,
                ]), Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Pages',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .Form::button(
                    '',
                    Form::icon('fa-file'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Pages',
                                    'action' => 'create',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    )).
                    Form::button(
                        '',
                        Form::icon($row->published==1?'fa-eye':'fa-eye-slash'),
                        array(
                            'class'    =>'btn-primary',
//                        'title'   => $this->lang->core['published_tip'],
                            'onclick' => 'content.pub(\''.$row->id.'\',\''.$row->published.'\')'
                        )
                    )

                . Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "content.delete($row->id)"
                    )
                )
            );
        }
        return $this->dt->renderJSON($res, $this->dt->getTotal());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = 0)
    {
        parent::create($parent_id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->configFormField('nav_menu');

        parent::edit($id);

        if ($this->data->parent_id > 0) {
            $this->prependToButtons(
                Form::link(
                    'Повернутись',
                    Form::icon('fa-external-link'),
                    array(
                        'class' => 'btn-link',
                        'href' => route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Pages',
                                'action' => 'index',
                                'id' => $this->data->parent_id
                            ]
                        )
                    )
                )
            );
        }

        $selected_nav_menu = $this->ms->selectedNavMenu($id, $this->created);
        $this->formAppendData('action', route('ajax.load',
            [
                'directory' => 'Admin',
                'controller' => 'Pages',
                'action' => 'process',
                'id' => $id
            ]))
            ->formAppendData('nav_menu', $this->ms->getNavMenus())
            ->formAppendData('selected_nav_menu', $selected_nav_menu)
//                (isset($this->data['parent_id']) && $this->data['parent_id'] ? '/index/' . $this->data['parent_id']  : '')
//            )
        ;


        return $this->renderContent();
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function process($id)
    {
        parent::process($id);
        $this->saveNavMenus($id);

        return json_encode($this->getFormResponse());
    }

    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;

        $tree = array();
        $r = $this->ms->tree($parent_id, $this->type_id->id);
        $i = 0;
        if (!empty($r)) {
            foreach ($r as $row) {
                $tree[$i]['data'] = $row->name . ' #' . $row->id;
                $tree[$i]['state'] = $row->isfolder ? 'closed' : '';
                $tree[$i]['attr'] = array(
                    'id' => $row->id,
                    "rel" => (($row->isfolder) ? 'folder' : 'file'),
//                    "href"=>   './pages/'. (($row['isfolder'])? 'index' : 'edit') ."/" . $row['id'] ."/",
                    "href" => route('method.load',
                        [
                            'directory' => 'Admin',
                            'controller' => 'Pages',
                            'action' => 'edit',
                            'id' => $row->id
                        ]
                    ),
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    public function output()
    {
        $tree = new Tree(
            'content',
            route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Pages',
                    'action' => 'tree'
                ]),
            '',
            array(
                Tree::contextMenu(
                    "Список сторінок",
                    "fa fa-list",
                    'self.location.href="/admin/Admin/Pages/index/"+id;'
                ),
                Tree::contextMenu(
                    "Додати сторінку",
                    "fa fa-plus",
                    'self.location.href="/admin/Admin/Pages/create/"+id;'
                ),
                Tree::contextMenu(
                    "Редагувати сторінку",
                    "fa fa-edit",
                    'self.location.href="/admin/Admin/Pages/edit/"+id;'
                ),
                Tree::contextMenu(
                    "Видалити сторінку",
                    "fa fa-remove",
                     'onclick = content.delete(id);'
                )
                ),
            '',
            true
        );
        $this->setSidebar($tree->render());
        return parent::output();
    }

}
