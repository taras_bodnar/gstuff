<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\plugins\FileUpload;
use App\Http\Controllers\app\Settings;
use App\Model\Admin\content\Type;
use App\Model\Admin\Languages;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\content\images\Images;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class Content extends Admin
{

    /**
     * content type id
     * @var int
     */
    protected $type_id = 1;
    /**
     * content type
     * @var string
     */
    private   $type    = 'page';
    /**
     * autocreated label
     * @var bool
     */
    protected $created = false;
    /**
     * form vars array
     * @var array
     */
    private $form_data = array();
    /**
     * path to form view
     * @var string
     */
    private $form_path = 'admin/modules/content/content_form';

    /**
     * form response array
     * @var array
     */
    private $form_response = array();
    /**
     * default form fields
     * @var array
     */
    private $form_fields = array(
        'name',
        'title',
        'alias',
        'keywords',
        'description',
        'content',
        'templates_id',
        'content_images',
        'content_options'
    );

    /**
     * content data
     * @var array
     */
    protected $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->ms = new \App\Model\Admin\Content();
        $this->languages = new Languages();
        $this->images = new Images();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      dd($this->languages_id);
    }
    /**
     * встановлює тип контенту
     * @param $type
     * @return $this
     */
    protected function setType($type)
    {
        $this->type = $type;
        $this->setTypeId();

        return $this;
    }

    /**
     * вертає тип контенту
     * @return string
     */
    protected function getType()
    {
        return $this->type;
    }

    /**
     * встановлює ід типу контенту по вказаному типу
     * @return $this
     */
    protected function setTypeId()
    {
        $this->type_id = $this->ms->getTypeId($this->type);

        if(empty($this->type_id)){
            // відсутній тип автостворення
            $mtype = new Type();
            $this->type_id = $mtype->create(array('name'=> ucfirst($this->type), 'type' => $this->type));
            if($this->type_id>0){
                $ctype = new \App\Http\Controllers\Admin\content\Type();
                $ctype->createDir($this->type);
            }
        }

        return $this;
    }

    /**
     * вертає ід типу контенту
     * @return int
     */
    protected function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id=0)
    {

        $templates_id = $this->ms->defaultTemplatesId($this->type);

        if(empty($templates_id)) echo '<div class="alert-danger alert text-center">WRONG templates ID</div>';

        $type_id      = $this->type_id;
        $owner_id     = Session::get('admin.user')->id;

        if($this->type == 'product'){
            $parent_id=0;
        }

        $id           = $this->ms->autoCreate($parent_id, $type_id->id, $owner_id, $templates_id->id);


        $this->created = true;
        return $this->edit($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    protected function formAppendData($name, $value)
    {
        $this->form_data[$name] = $value;

        return $this;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $this->data = $this->ms->data('content', $id);
        if(empty($id) || empty($this->data)) die( '<div class="alert-danger alert text-center">WRONG ID</div>' ) ;

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('fa-save', false),
                array(
                    'class'=>'btn-primary form-submit'
                )
            )
        );

        // images
        $images = new Images();

        // default form data
        $this ->formAppendData('action',        route('ajax.load',[
            'directory'=>'Admin',
            'controller'=>'Content',
            'action'=>'process',
            'id'=>$id
        ]))
//            ->formAppendData('title',              $this->lang->pages['title'])
            ->formAppendData('languagesTab',        $this->setLanguages())
            ->formAppendData('languages',           Languages::geAll(1))
            ->formAppendData('process',             'edit')
            ->formAppendData('data',               $this->data)
            ->formAppendData('info',               $this->ms->info($id))
            ->formAppendData('id',                 $id)
            ->formAppendData('parent_id',          $this->data->parent_id)
            ->formAppendData('templates',          $this->ms->templates($this->type, $this->data->templates_id))

            ->formAppendData('autofil_title',      Settings::instance()->get('autofil_title'))
            ->formAppendData('autotranslit_alias', Settings::instance()->get('autotranslit_alias'))
            ->formAppendData('images',             $images->get($id))
            ->formAppendData('type',               $this->type)
            ->formAppendData('plugins',            $this->getPlugins($id,'edit'))
            ->formAppendData('created',            $this->created)
            ->formAppendData('features',           $this->getFeatures($id))
        ;

    }

    public function getFeatures($content_id, $products_id=0)
    {
        $features = $this->ms->getFeatures($content_id, $this->type,'', $this->type_id, $products_id);
        
        return View(
            'admin/modules/content/features/values',
            array(
                'items'=> $features,
                'use_remove_link' => !in_array($this->type, array('product')),
                'enable_values'   => !in_array($this->type, array('category')),
                'content_id'      => $content_id,
                'enable_button'   => 1
            )
        )->render();
    }

    protected function renderContent($output=true)
    {

        $this->formAppendData('form_fields', $this->form_fields);

        $content = View($this->form_path, $this->form_data)->render();
        $this->setContent($content);

        if($output){
            echo $this->output();
        }
    }

    protected function configFormField($field, $display = 1)
    {
        if($display == 0){
            $k = array_search($field, $this->form_fields);
            if($k){
                unset($this->form_fields[$k]);
            }
        } else {
            if(!in_array($field, $this->form_fields)) {
                $this->form_fields[] = $field;
            }
        }

        return $this;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process($id)
    {
        if(empty($id)) die('WRONG ID');

        $info = Input::get('info');
        $data = Input::get('data');
        $data['published'] =  !isset($data['published'])?0:$data['published'];
//        echo "<pre>";print_r($info);die;
        $s=0;
        $e = $this->errors['error'];
        $process = $data['process'];
        $created = $data['created'];

        // add or update values
        if(empty($this->error)) {
            unset($data['process']);
            unset($data['created']);
            $data['auto']    = 0;
            $data['type_id'] =  $this->convertFromObject($this->type_id)['id'];
            if(empty($id)) return '';

            $s = $this->ms->process($id, $data, $info);
            if($s > 0) {
                if(isset ($data['parent_id']) && $data['parent_id'] > 0) $this->ms->toggleFolder($data['parent_id']);
                $e = $this->errors['success'];
                $this->error[] = "Сторінку створено";
            } else {

                $e = $this->errors['error'];
                $this->error[] = "NO" ;
            }

            $s+= $this->ms->saveFeatures($id);


            $this->getPlugins($id,'process');
        }

         $this->setFormResponse('s',  $s > 0)
            ->setFormResponse('r',  isset($_POST['redirect_url']) ? $_POST['redirect_url'] : '')
            ->setFormResponse('e',  $e)
            ->setFormResponse('id', $id)
             ->setFormResponse('process',$process)
             ->setFormResponse('created',$created)
//            ->setFormResponse('t',  $this->lang->core[$e])
            ->setFormResponse('m',  implode('<br>', $this->error))
        ;
    }

    private function convertFromObject($data)
    {
        $data = json_encode($data);
        $data = json_decode($data,true);

        return $data;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    private function setFormResponse($name, $value)
    {
        $this->form_response[$name] = $value;

        return $this;
    }

    public function uploadImages()
    {
        return Images::uploadFiles();
    }

    public function uploadFiles()
    {
        return FileUpload::uploadFiles();
    }

    public function getImageTemplate($id)
    {
        return $this->images->getTemplate($id);
    }

    public function getFileTemplate($id)
    {
        $file = new FileUpload();
        return $file->getTemplate($id);
    }

    public function deleteImages($id)
    {
       return $this->images->delete($id);
    }

    public function editImagesInfo($id)
    {
        return $this->images->editInfo($id);
    }

    public function deleteFiles($id)
    {
        $file = new FileUpload();
        return $file->destroy($id);
    }

    public function fileInfo()
    {
        $id = Input::get('id');
        $process = Input::get('process');
        if(isset($process)) {
            $info = Input::get('info');
            $id = Input::get('content_id');
            return $this->images->insertInfo($id,$info);
        }

        $view = $this->template->view('plugins/fileupload/info',[
            'languages'=>Languages::geAll(1),
            'id'=>$id,
            'info'=>$this->images->getInfo($id)
        ])->render();

        return Response::json(array('view' => $view, 't' => 'Інформація про файли'));
    }


    /**
     * getter form response
     * @param $key
     * @return array
     */
    protected function getFormResponse($key=null)
    {
        if($key && isset($this->form_response[$key])) {
            return $this->form_response[$key];
        }

        return $this->form_response;
    }

    protected function saveNavMenus($id)
    {

        $nav_menu = Input::get('nav_menu');

        if(!isset($nav_menu)) return 0;

        $selected = $this->ms->selectedNavMenu($id);

        foreach ($nav_menu as $nav_menu_id => $s) {

            if($s == 1 && !in_array($nav_menu_id, $selected)) {
                $this->ms->addMenuItem($nav_menu_id,$id);
            }
            if($s == 0) $this->ms->deleteNavMenus($id, $nav_menu_id);
        }

        return 1;
    }

    public function mkAlias($parent_id, $languages_id, $languages_code, $alias=null)
    {
        if(!$alias){
            $alias = Input::get('alias');
        }

        if(empty($alias)) return '';

        // ПОТРІБНО СФОРМУВАТИ СПИСОК ВЕРХНІХ КАТЕГОРІЙ
        $parent_alias = $parent_id > 1 ? $this->ms->parentAlias($parent_id, $languages_id) : '';
        return ( empty($parent_alias->alias) ? '' : $parent_alias->alias . '/' )  . self::translit($alias);
    }


//    public static function translit($text,$code)
//    {
//        $text = mb_strtolower(trim($text),'utf8');
//        $text = preg_replace("/[^A-Za-z0-9а-яА-Яіїєёыэъñéèàùêâôîûëïüÿç\- \/]/u", "", $text);
//
////        $table = Config::instance()->get('translit.' . $code);
////        if(empty($table)) {
////            $table = Config::instance()->get('translit.def');
////        }
//
////        $text = strtr($text,$table);
//
//        return $text;
//    }

    public static function  translit ( $string )
    {
        $string = mb_strtolower(trim($string),'utf8');
        $trans = array ( "а"     => "a",
            "б"     => "b",
            "в"     => "v",
            "г"     => "h",
            "д"     => "d",
            "е"     => "e",
            "є"     => "ye",
            "ж"     => "zh",
            "з"     => "z",
            "и"     => "y",
            "і"     => "i",
            "ї"     => "yi",
            "й"     => "y",
            "к"     => "k",
            "л"     => "l",
            "м"     => "m",
            "н"     => "n",
            "о"     => "o",
            "п"     => "p",
            "р"     => "r",
            "с"     => "s",
            "т"     => "t",
            "у"     => "u",
            "ф"     => "f",
            "х"     => "kh",
            "ц"     => "ts",
            "ч"     => "ch",
            "ш"     => "sh",
            "щ"     => "sch",
            "ю"     => "yu",
            "я"     => "ya",
            "ь"     => "",

            "А"     => "A",
            "Б"     => "B",
            "В"     => "V",
            "Г"     => "H",
            "Д"     => "D",
            "Е"     => "E",
            "Є"     => "Ye",
            "Ж"     => "Zh",
            "З"     => "Z",
            "И"     => "Y",
            "І"     => "I",
            "Ї"     => "Yi",
            "Й"     => "Y",
            "К"     => "K",
            "Л"     => "L",
            "М"     => "M",
            "Н"     => "N",
            "О"     => "O",
            "П"     => "P",
            "Р"     => "R",
            "С"     => "S",
            "Т"     => "T",
            "У"     => "U",
            "Ф"     => "F",
            "Х"     => "Kh",
            "Ц"     => "Ts",
            "Ч"     => "Ch",
            "Ш"     => "Sh",
            "Щ"     => "Sch",
            "Ю"     => "Yu",
            "Я"     => "Ya",
            "Ь"     => "",
            "ы"     => "y",
            "э"     => "е",
            "Ы"     => "Y",
            "ì"     => "i",
            "ś"     => "s",
            "š"     => "s",
            "č"     => "c",
            "ą"     => "a",
            "ę"     => "e",
            "ó"     => "o",
            "ł"     => "l",
            "ï"     => "i",

            "′"     => "",
            "`"     => "",
            "’"     => "",
            "'"     => "",
            "."     => "_",
            "("     => "_",
            ")"     => "_",
            ","     => "",
            ">"     => "_less_",
            "<"     => "_over_",
            '"'     => "",
            "_"     => "",
            " "     => "-",
            "№"     => "Numb",
            "/"     => "_",
            "%"     => "proc",
            "&"     => "_and_",
            "&amp;" => "_and_",
            "+"     => "_plus_",
            "?"     => "",
            "!"     => "" );

        return strtr ( $string, $trans );

    }

    public function delete()
    {
        return $this->ms->del(Input::get('id'));
    }

    public function publish($id,$status)
    {
        $status = $status == 1 ? 0 : 1;
        return $this->ms->publish($id,$status);
    }

    public function sort($table,$pk,$col='sort',$val)
    {
        $sort = explode(',',$val);
        $i=1;
        foreach($sort as $k=>$id){
            $this->ms->sort($table, $pk, $col, $i, $id);
            $i++;
        }
        return $i;
    }
}
