<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\app\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Themes extends Admin
{
    public function __construct()
    {
        $this->setTitle('Теми');
        $this->path = Settings::instance()->get('themes_path');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current = Settings::instance()->get('app_theme_current');
        $views_path= Settings::instance()->get('app_views_path');
//        var_dump($current);
        // зчитую теми з папки
        $path = $_SERVER['DOCUMENT_ROOT'] .'/'. $this->path.'/'.$views_path;
        $path_url = $_SERVER['HTTP_HOST'] .'/'. $this->path;

        $themes = array();
        if ($handle = opendir($path)) {
            while (false !== ($theme = readdir($handle))) {
                if ($theme != "." && $theme != "..") {

                    if(!file_exists($path . '/' . $theme . '/config.php'))
                        continue;

                    $config = include($path . '/' . $theme . '/config.php');
                    if($config['type'] == 'backend') continue;

                    $config['path']    = $path . $theme . '/';
                    $config['urlpath'] = '/public/views'.'/'.$theme."/";
                    $config['theme']   = $theme;

                    // set current theme
                    $config['current'] = ($theme == $current ? 'current' : '');

                    $themes[] = $config;
                }
            }
            closedir($handle);
        }

        $content = view('admin/modules/themes',array('themes'=>$themes));

        $this->setContent($content);

        return $this->output();
    }

    public function activate($theme)
    {
        if(empty($theme)) return 0;

        return Settings::instance()->set('app_theme_current', $theme);
    }
}
