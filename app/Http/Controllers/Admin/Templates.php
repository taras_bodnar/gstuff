<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\app\Settings;
use App\Model\Admin\content\Type;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class Templates extends Admin
{
    const EXT = '.blade.php';

    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\Templates();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id = 0)
    {
        $buttons = array(
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Templates',
                                'action' => 'create',
                                'id' => $parent_id
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('templates')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Templates',
                    'action' => 'items',
                    'id' => $parent_id
                ]
            ))
            ->setTitle('Шаблони')
            ->th('#')
            ->th('Назва')
            ->th('Опис')
            ->th('Шлях')
            ->th('Основний')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->dt->render());

        return $this->output();
    }

    public function items($type_id = 0)
    {
        $yes = 'Так';
        $no = 'Ні';

        $type_id = empty($type_id) ? 0 : $type_id;
        $this->dt->table('content_templates')
            ->searchCol('id,name,description,path')
//            ->debug(1)
            ->where(empty($type_id) ? '1' : " type_id = $type_id ")
            ->get("id,name,description,path,IF(main = 1, '$yes', '$no' ) as def")
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'Templates',
                    'action' => 'edit',
                    'id' => $row->id,
                ]),
                $row->description,
                $row->path,
                $row->def,
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Templates',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "Type.deleteTemplates($row->id)"
                    )
                )
            );
        }


        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content =  view('admin.modules.type.templates',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Templates',
                        'action'=>'process',
                    ]),
                'process'=>'create',
                'type'=>Type::get()
            ]
        );
        $this->setContent($content);

        return $this->output();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($id) || empty($id)) return 'WRONG ID';

        $data  = $this->m->data('content_templates',$id);


        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );
        if($data->path != '') {
            $template = $this->readFile($data->type_id, $data->path);
        }

        $content = view('admin.modules.type.templates',[
            'data'=>$data,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Templates',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit',
            'type'=>Type::get($data->type_id),
            'template'=>$template
        ]);

        $this->setContent($content);

        return $this->output();
    }

    public function process($id)
    {
        $e[] = array();
        $s=0;
        $e = $this->errors['warning'];
        $info = Input::get('info');
        $data = Input::get('data');
        $process = $data['process'];
        if(empty($data) || empty($info)) {
            $this->errors[] = 'Заповніть всі поля';
        }

        $type = Type::getTypeById($data['type_id']);

        $current = Settings::instance()->get('app_theme_current');
        $themes_path = Settings::instance()->get('themes_path');
        $views_path = Settings::instance()->get('app_views_path');
        $path = $_SERVER['DOCUMENT_ROOT'] .'/'. $themes_path .
            $views_path . '/' . $current .'/' . $type->type.'/'.$data['path'].".blade.php";
        $dir = $_SERVER['DOCUMENT_ROOT'] .'/'. $themes_path .
            $views_path . '/' . $current .'/' . $type->type;
        if(empty($this->error)) {
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $s = $this->m->insert($data);

                    $content = Input::get('template');
                    if (!file_exists($path)) {
                        File::makeDirectory($dir, $mode = 0777, true, true);
                        File::put($path,$content);
                        File::prepend($path,$this->templateHead());
                    } else {
                        File::put($path,$content);
                    }

                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    unset($data['process']);
                    $s = $this->m->edit($id,$data);
                    $content = Input::get('template');
                    if (!file_exists($path)) {
                        File::put($path,$content);
                        File::prepend($path,$this->templateHead());
                    } else {
                        File::put($path,$content);
                    }

                    if($s > 0) {
//                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'process'=>$process,
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    private function templateHead()
    {
        return "{{--Created by Taras ".date('Y-m-d H:i')."--}}";
    }

    private function readFile($type_id, $file)
    {
        $path = $this->getPath($type_id);
        $file_path = $path . $file . self::EXT;
        if(!file_exists($file_path)) return '';

        if (!$handle = fopen($file_path, 'r')) {
            echo "I can not open the file ($file_path)";
            exit;
        }
        if(filesize($file_path) == 0) return '';
        $template = fread($handle, filesize($file_path));
        fclose($handle);

        return $template;
    }

    private function getPath($type_id)
    {
        if(empty($type_id)) return '';

        $docroot = $_SERVER['DOCUMENT_ROOT'];

        $current = Settings::instance()->get('app_theme_current');
        $themes_path = Settings::instance()->get('themes_path');
        $views_path = Settings::instance()->get('app_views_path');

        $templates_path = '/'. $themes_path . $views_path  . $current;

        $type = Type::getTypeById($type_id);

        $type_path = $templates_path .'/'. $type->type . '/';

        if(!is_dir( $docroot . $type_path )){
            try{
                mkdir( $docroot . $type_path, 0777, true );
            } catch(\Exception $e){
                $this->error[] = $e->getMessage();
                return '';
            }
        }

        return $docroot . $type_path;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->m->del($id);
    }
}
