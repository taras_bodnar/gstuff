<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Model\Admin\Languages as LangModel;

class Languages extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->t = new DataTables();
        $this->m = new \App\Model\Admin\Languages();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'Languages',
                                'action'=>'create'
                            ]
                        )."'"
                ))
        );

        $this->t->setId('languages')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Languages',
                    'action'=>'items'
                ]
            ))
            ->setTitle('Мови')
            ->th('#')
            ->th('Назва')
            ->th('Код')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->notification().$this->t->render());

        return $this->output();
    }

    private function notification()
    {
        $l = $this->getNotTranslatedLanguage();

        if(empty($l)) return '';

        $info_tables = $this->getInfoTables();

        return view(
            'admin/modules/languages/notification',
            array(
                'languages' => $l,
                'tables' => $info_tables
            )
        );
    }

    private function getNotTranslatedLanguage()
    {
        $l = $this->m->getNotTranslatedLanguage();

        foreach ($l as $k=> $row) {
            if(!empty($row->c_name)) { unset($l[$k]);}
            unset($l[$k]->c_name);
        }

        return $l;
    }

    private function getInfoTables()
    {
        $res = array();
        $r = $this->m->getInfoTables();
        foreach ($r as $row) {
            foreach ($row as $n=>$table) {
                $res[] = $table;
            }

        }

        return $res;
    }

    public function processTranslate()
    {
        $tables = Input::get('table');
        if(!isset($tables)) return 0;

        $languages_id = (int) Input::get('languages_id');
        $res= array();
        if(empty($languages_id)) return 0;
        $def_lang_id = $this->m->getFrontendDefault('id');
        foreach ($tables as $table) {
            $col = array();
            $tbl_info = $this->m->describe($table);
            // формую масив полів
            foreach ($tbl_info as $row) {
                $iv = array();
                $iv['translate'] = 0;
                if($row->Extra == 'auto_increment') continue;
                $table_fields[$table]['iv'][] = $row->Field;

                if(preg_match('/varchar|text|longtext/i',$row->Type)){
                    $table_fields[$table]['to_tranlate'][] = $row->Field;
                    $iv['translate'] = 1;
                }
                $iv['field'] = $row->Field;
                $col[] = $iv;
            }

            $total = $this->m->getTotalTableRecords($table, $def_lang_id->id);

            $res[] = array(
                'start' => 0,
                'total' => $total,
                'table' => $table,
                'col'   => $col,
                'from_lang' => $def_lang_id,
                'to_lang'   => $languages_id
            );
        }

        return json_encode(array('t' => $res));
    }

    public function translate()
    {
//        $this->dump($_POST);
        $t = new Translator();
        $table = Input::get('table');
        $start = (int) Input::get('start');
        $total = (int) Input::get('total');
        $col = $_POST['col'];
        $from_lang = (int) Input::get('from_lang');
        $to_lang   = (int) Input::get('to_lang');

        $from = $this->m->rowData($from_lang, 'code');
        $to   = $this->m->rowData($to_lang, 'code');

        $iv = array();
        $i = time();

        $rowInfo = $this->m->getTableRow($table, $from_lang, $start);
        foreach ($col as $field) {
            $value = $rowInfo->$field['field'];

            if($field['field'] == 'languages_id') {
                $value = $to_lang;
            }

            if($field['field'] == 'alias') {
                $value .=  $i;
            }
            if($field['translate'] == 1) {
                $value = $t->translate($value, $from->code, $to->code);
            }

            $iv[$field['field']] = $value;
            $i++;
        }

        $this->m->insertTranslatedData($table, $iv);

        $start++;
        return json_encode(
            array(
                'table' => $table,
                'start' => $start,
                'total' => $total,
                'col'   => $col,
                'from_lang' => $from_lang,
                'to_lang'   => $to_lang
            )
        );
    }

    public function generateAlias()
    {
        $lang=1;
        $total = $this->m->getTotalTableRecords('content_info', $lang);

        return view(
            'admin/modules/languages/generate_alias',
            array(
                'lng'  => $lang,
                'total' => $total->t
            )
        );
    }

    public function processGenerateAlias()
    {
        $total = (int) Input::get('total');
        $start = (int) Input::get('start');
        $languages_id = (int) Input::get('languages_id');
        $code = $this->m->rowData($languages_id, 'code');
        $c = new Content();
        $row = $this->m->getAliasInfo($languages_id,$start);

        $alias = '';
        if($row->content_id != 1){
            $alias = $c->mkAlias($row->parent_id, $languages_id, $code, $row->name);
        }

        $this->m->updateAlias($row['id'], $alias);

        $start ++ ;

        return json_encode(array(
            'total' => $total,
            'start' => $start
        ));
    }

    public function items()
    {
        $this->t->table('languages')

//            -> debug()
            -> searchCol('id,name')

            -> get("id,name,code,front")

            -> execute();

        $r   = $this->t->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'Languages',
                    'action'=>'edit',
                    'id'=>$row->id,
                ]),
                $row->code,
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Languages',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    )).
                Form::button(
                    '',
                    Form::icon($row->front==1?'fa-eye':'fa-eye-slash'),
                    array(
                        'class'    =>'btn-primary',
                        'title'   => 'Доступна для сайту',
                        'onclick' => 'Languages.pub(\''.$row->id.'\',\''.$row->front.'\')'
                    )
                ).
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "Languages.delete($row->id)"
                    )
                )
            );
        }


        return $this->t->renderJSON($res, $this->t->getTotal());
    }

    public function publish($id,$status)
    {
        $status = $status == 1 ? 0 : 1;
        return $this->m->publish($id,$status);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('fa-save-as', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(
            Form::link(
                'Повернутись',
                Form::icon('fa-external-link'),
                array(
                    'class' => 'btn-link',
                    'href' => route('method.load',
                        [
                            'directory' => 'Admin',
                            'controller' => 'Languages',
                            'action' => 'index'
                        ]
                    )
                )
            )
        );

        $returnHTML = view('admin.modules.languages.form',
            [
                'process'=>'create',
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Languages',
                        'action'=>'process'
                    ]),
            ]
            )->render();
        $this->setContent($returnHTML);
//        return response()->json(array('success' => true, 'view'=>$returnHTML));
        return $this->output();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($id) || empty($id)) return 'WRONG ID';

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('fa-save-as', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(
            Form::link(
                'Повернутись',
                Form::icon('fa-external-link'),
                array(
                    'class' => 'btn-link',
                    'href' => route('method.load',
                        [
                            'directory' => 'Admin',
                            'controller' => 'Languages',
                            'action' => 'index'
                        ]
                    )
                )
            )
        );

        $items = LangModel::getInfo($id);

        $content = view('admin.modules.languages.form',[
            'data'=>$items,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Languages',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit'
        ])->render();

        $this->setContent($content);
        return $this->output();
//        return response()->json(array('success' => true, 'view'=>$content));
    }


    public function process($id='')
    {
        $e[] = array();
        $s=0;
        $e = $this->errors['warning'];
        $data = Input::get('data');
        $process = $data['process'];

        if(
            empty($data)
        ) {
            $this->errors[] ='Заповніть всі поля';
        }
        // add or update values
        if(empty($this->error)) {
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $s = $this->m->insert($data);

                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    unset($data['process']);
                    $s = $this->m->edit($id,$data);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'process' => $process, // redirect url
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->m->destroy($id);
    }
}
