<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\app\Settings;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class Type extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\content\Type();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'Type',
                                'action'=>'create',
                            ]
                        )."'"
                ))
        );


        $this->dt->setId('type')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Type',
                    'action'=>'items'
                ]
            ))
            ->setTitle('Компоненти')
            ->th('#')
            ->th('Тип')
            ->th('Назва')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->dt->render());

        return $this->output();
    }

    public function create()
    {

        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content =  view('admin.modules.type.type_form',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'Type',
                        'action'=>'process',
                    ]),
                'process'=>'create',
            ]
        );
        $this->setContent($content);

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function items()
    {
        $this->dt->table('content_type c')
//            -> debug()
            -> searchCol('c.id,c.name,c.type')
            -> get("c.id,c.name,c.type")
            -> execute();

        $r   = $this->dt->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'Templates',
                    'action'=>'index',
                    'id'=>$row->id,
                ]),
                $row->type,
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'Type',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick' => "Type.delete($row->id)"
                    )
                )
            );
        }


        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!isset($id) || empty($id)) return 'WRONG ID';

        $data  = $this->m->data('content_type',$id);


        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content = view('admin.modules.type.type_form',[
            'data'=>$data,
            'action'=>route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'Type',
                    'action'=>'process',
                    'id'=>$id
                ]),
            'process'=>'edit'
        ]);

        $this->setContent($content);

        return $this->output();
    }

    public function process($id)
    {
        $e[] = array();
        $s=0;
        $e = $this->errors['warning'];
        $info = Input::get('info');
        $data = Input::get('data');
        $process = $data['process'];
        if(empty($data) || empty($info)) {
            $this->errors[] = 'Заповніть всі поля';
        }

        if(empty($this->error)) {
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $s = $this->m->createType($data);
                    if($s > 0) {
                       $this->createDir($data['type']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->errors[] = $this->ms->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    unset($data['process']);
                    $s = $this->m->edit($id,$data);
                    if($s > 0) {
//                        $this->ms->toggleFolder($data['parent_id']);
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'process'=>$process,
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    public function createDir($type)
    {
        $current = Settings::instance()->get('app_theme_current');
        $themes_path = Settings::instance()->get('themes_path');
        $views_path = Settings::instance()->get('app_views_path');
        $dir = $_SERVER['DOCUMENT_ROOT'] .'/'. $themes_path . $views_path . '/' . $current .'/' . $type;
        if(! is_dir($dir))
            return mkdir($dir);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->m->del($id);
    }
}
