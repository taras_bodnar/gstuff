<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Settings extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\Settings();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                'Зберегти',
                Form::icon('fa-save'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY
                ))
        );
        $this->setButtons($buttons);

        $data = $this->m->get();

        $this->setContent($this->template->view('modules/settings',[
            'data'=>$data
        ]));

        return $this->output();
    }

    public function process()
    {
        $data = Input::get('data');
        $s = 0;
        $m = [];

        if(empty($data)) {
            $m[] = 'Заповніть всі пол\'я';
        } else {
            if(Input::file('watermark')) {
                Input::file('watermark')->move("uploads/watermark","watermark.png");
            }
            $s = 1;
            $this->m->edit($data);
            $m[] = 'Налаштування збережено';
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'm' => implode('<br>', $m) // error message
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
