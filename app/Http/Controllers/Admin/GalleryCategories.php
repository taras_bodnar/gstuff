<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\content\Tree;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class GalleryCategories extends Content
{

    public function __construct()
    {
        parent::__construct();

        $this->setType('albumCategory');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($parent_id = '')
    {
        $this->setButtons
        (
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'GalleryCategories',
                                'action' => 'create',
                                'id' => $parent_id
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('content')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'GalleryCategories',
                    'action' => 'show',
                    'id' => $parent_id
                ]
            ))
            ->setTitle('Категорії статей')
            ->th('#')
            ->th('Назва')
            ->th('Функції');


        $this->setContent($this->dt->render());

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = 0)
    {
        $parent_id = empty($parent_id) ? 0 : $parent_id;
        parent::create($parent_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($parent_id='')
    {
        $parent_id = empty($parent_id) ? 0 : $parent_id;
        $this->dt->table('content c')
            ->join("join content_info ci on ci.content_id=c.id and ci.languages_id=" . $this->languages_id)
            ->where("c.parent_id=$parent_id and c.type_id={$this->type_id->id}")
            ->searchCol('c.id,name')
            ->get("c.id,name")
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'GalleryCategories',
                    'action' => 'index',
                    'id' => $row->id,
                ]), Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'GalleryCategories',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "content.delete($row->id)"
                    )
                )
            );
        }
        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        parent::edit($id);

        if ($this->data->parent_id > 0) {
            $this->prependToButtons(
                Form::link(
                    'Повернутись',
                    Form::icon('icon-external-link'),
                    array(
                        'class' => 'btn-link',
                        'href' => route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'GalleryCategories',
                                'action' => 'index',
                                'id' => $this->data->parent_id
                            ]
                        )
                    )
                )
            );
        }

//        $selected_nav_menu = $this->ms->selectedNavMenu($id, $this->created);
        $this->formAppendData('action', route('ajax.load',
            [
                'directory' => 'Admin',
                'controller' => 'GalleryCategories',
                'action' => 'process',
                'id' => $id
            ]))
            ->formAppendData('nav_menu', $this->ms->getNavMenus())
//            ->formAppendData('selected_nav_menu', $selected_nav_menu)
//                (isset($this->data['parent_id']) && $this->data['parent_id'] ? '/index/' . $this->data['parent_id']  : '')
//            )
        ;


        return $this->renderContent();
    }

    public function process($id)
    {
        parent::process($id);

        return json_encode($this->getFormResponse());
    }

    public function tree()
    {
        $parent_id = isset($_POST['id']) ? (int)$_POST['id'] : 0;

        $tree = array();
        $r = $this->ms->tree($parent_id, $this->type_id->id);

        $i = 0;
        if (!empty($r)) {
            foreach ($r as $row) {
                $tree[$i]['data'] = $row->name . ' #' . $row->id;
                $tree[$i]['state'] = $row->isfolder ? 'closed' : '';
                $tree[$i]['attr'] = array(
                    'id' => $row->id,
                    "rel" => (($row->isfolder) ? 'folder' : 'file'),
//                    "href"=>   './pages/'. (($row['isfolder'])? 'index' : 'edit') ."/" . $row['id'] ."/",
                    "href" => route('method.load',
                        [
                            'directory' => 'Admin',
                            'controller' => 'GalleryCategories',
                            'action' => 'edit',
                            'id' => $row->id
                        ]
                    ),
                );
                $i++;
            }
        }

        return json_encode($tree);
    }

    public function output()
    {
        $tree = new Tree(
            'content',
            route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'GalleryCategories',
                    'action' => 'tree'
                ]),
            '',
            array(
                Tree::contextMenu(
                    "Додати категорію",
                    "fa fa-plus",
                    'self.location.href="/admin/Admin/GalleryCategories/create/"+id;'),
                Tree::contextMenu(
                    "Список категорій",
                    "fa fa-list",
                    'self.location.href="/admin/Admin/GalleryCategories/index/"+id;'),
                Tree::contextMenu(
                    "Редагувати категорій",
                    "fa fa-edit",
                    'self.location.href="/admin/Admin/GalleryCategories/edit/"+id;'),
                ),

            '',
            true
        );
        $this->setSidebar($tree->render());
        return parent::output();
    }
}
