<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\Languages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class EmailTemplates extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->setTitle('Шаблони e-mail');
        $this->languages = new Languages();
        $this->m = new \App\Model\Admin\EmailTemplates();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buttons= array(
            Form::button(
                'Створити',
                Form::icon('icon-file'),
                array(
                    'class'   => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='".route('method.load',
                            [
                                'directory'=>'Admin',
                                'controller'=>'EmailTemplates',
                                'action'=>'create'
                            ]
                        )."'"
                ))
        );

        $this->dt->setId('components')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory'=>'Admin',
                    'controller'=>'EmailTemplates',
                    'action'=>'items'
                ]
            ))
            ->setTitle('Шаблони e-mail')
            ->th('#')
            ->th('Назва')
            ->th('Код')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->dt->render());

        return $this->output();
    }

    public function items()
    {
        $this->dt->table('email_templates e')
            ->join("join email_templates_info ei on ei.templates_id=e.id and ei.languages_id=1")
//            -> debug()
            -> searchCol('e.id,e.code,e.name')

            -> get("e.id,e.code,e.name")

            -> execute();

        $r   = $this->dt->getResults(false);

        $res = array();
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load',$row->name,[
                    'directory'=>'Admin',
                    'controller'=>'EmailTemplates',
                    'action'=>'edit',
                    'id'=>$row->id,
                ]),
                $row->code,
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class'   => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='".route('method.load',
                                [
                                    'directory'=>'Admin',
                                    'controller'=>'EmailTemplates',
                                    'action'=>'edit',
                                    'id'=>$row->id
                                ]
                            )."'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class'=>'btn-danger',
                        'onclick'=>"emailTemplates.delete($row->id)"
                    )
                )
            );
        }


        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $content =  view('admin.modules.email.form',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'EmailTemplates',
                        'action'=>'process',
                    ]),
                'process'=>'create',
                'languages'=>$this->languages->geAll()
            ]
        );
        $this->setContent($content);

        return $this->output();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->setButtons(
            Form::button(
                'Зберегти',
                Form::icon('icon-save', false),
                array(
                    'class'=>'btn-success form-submit'
                )
            )
        );

        $this->prependToButtons(
            Form::link(
                'Повернутись',
                Form::icon('icon-external-link'),
                array(
                    'class'=>'btn-link',
                    'href' => route('method.load',
                        [
                            'directory'=>'Admin',
                            'controller'=>'EmailTemplates',
                            'action'=>'index'
                        ]
                    )
                )
            )
        );

        $data  = $this->m->data('email_templates',$id);
        $info  = $this->m->getInfo($id);

        $content =  view('admin.modules.email.form',
            [
                'action'=>route('ajax.load',
                    [
                        'directory'=>'Admin',
                        'controller'=>'EmailTemplates',
                        'action'=>'process',
                        'id'=>$id,
                    ]),
                'process'=>'edit',
                'data'=>$data,
                'info'=>$info,
                'languages'=>$this->languages->geAll()
            ]
        );
        $this->setContent($content);

        return $this->output();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process($id)
    {
        $s=0;
        $e = $this->errors['warning'];
        $info = Input::get('info');
        $data = Input::get('data');
        $process = $data['process'];

        if(
        empty($data['name'])
        ) {
            $this->error[] = 'Заповніть всі поля';
        }
        // add or update values
        if(empty($this->error)) {
            switch($data['process']){
                case 'create':
                    unset($data['process']);
                    $id = $this->m->insert($data, $info);
                    if($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->mg->error();
                    }
                    break;
                case 'edit':
                    if(empty($id)) return '';
                    unset($data['process']);
                    $s = $this->m->edit($id, $data, $info);
                    if($s > 0) {
                        $e = $this->errors['success'];
                        $this->error[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->mg->error() ;
                    }
                    break;
                default:
                    break;
            }
        }

        return json_encode(array(
            's' => $s > 0 , // status
            'process' => $process, // redirect url
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
