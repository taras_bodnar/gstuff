<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Admin;
use App\Http\Controllers\Admin\Form;
use App\Http\Controllers\app\Settings;
use App\Model\Admin\content\Images;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;

class Size extends Admin
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new \App\Model\Admin\Size();
        $this->images = new Images();
    }

    public function index($id)
    {
        $buttons = array(
            Form::button(
                'Створити',
                Form::icon('fa-file'),
                array(
                    'class' => Form::BTN_TYPE_PRIMARY,
                    'onclick' => "self.location.href='" . route('method.load',
                            [
                                'directory' => 'Admin',
                                'controller' => 'Size',
                                'action' => 'create',
                                'id' => $id
                            ]
                        ) . "'"
                ))
        );

        $this->dt->setId('images_size')
            ->ajaxConfig(route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Size',
                    'action' => 'show'
                ]
            ))
            ->setTitle('Розміри зображень')
            ->th('#')
            ->th('Назва')
            ->th('Ширина')
            ->th('Висота')
            ->th('Функції');

        $this->setButtons($buttons);
        $this->setContent($this->dt->render());

        return $this->output();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $buttons = array(
            Form::button(
                'Зберегти',
                Form::icon('fa-save', false),
                array(
                    'class' => 'btn-success form-submit'
                ))
        );

//        $this->prependToButtons(
//            array(
//                Form::link(
//                    'Повернутись',
//                    Form::icon('fa-external-link'),
//                    array(
//                        'class' => 'btn-link',
//                        'href' => route('method.load',
//                            [
//                                'directory' => 'Admin',
//                                'controller' => 'Size',
//                                'action' => 'index'
//                            ]
//                        )
//                    )
//                )
//            )
//        );

        $content = view('admin/modules/content/images/size', [

            'action' => route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Size',
                    'action' => 'process',
                ]),
            'process' => 'create',
            'id' => 0,
            'content_type' => $this->getType()
        ]);

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->dt->table('images_sizes')
            ->searchCol('id,name')
            ->get("id,name,width,height")
            ->execute();

        $r = $this->dt->getResults(false);

        $res = array();
//        dd($r);
        foreach ($r as $row) {
            $res[] = array(
                $row->id,
                link_to_route('method.load', $row->name, [
                    'directory' => 'Admin',
                    'controller' => 'Size',
                    'action' => 'edit',
                    'id' => $row->id,
                ]),
                $row->width,
                $row->height,
                Form::link(
                    '',
                    Form::icon('fa-crop'),
                    array(
                        'class'  =>'btn-info',
                        'title' => 'Перенарізати зображення',
                        'onclick'  => "content.imagesSize.crop($row->id)"
                    )
                ).
                Form::button(
                    '',
                    Form::icon('fa-edit'),
                    array(
                        'class' => Form::BTN_TYPE_PRIMARY,
                        'onclick' => "self.location.href='" . route('method.load',
                                [
                                    'directory' => 'Admin',
                                    'controller' => 'Size',
                                    'action' => 'edit',
                                    'id' => $row->id
                                ]
                            ) . "'"
                    ))
                .
                Form::button(
                    '',
                    Form::icon('fa-remove'),
                    array(
                        'class' => 'btn-danger',
                        'onclick' => "content.imagesSize.delete($row->id)"
                    )
                )
            );
        }


        return $this->dt->renderJSON($res, $this->dt->getTotal());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buttons = array(
            Form::button(
                'Зберегти',
                Form::icon('fa-save', false),
                array(
                    'class' => 'btn-success form-submit'
                ))
        );

//        $this->prependToButtons(
//            Form::link(
//                'Повернутись',
//                Form::icon('fa-external-link'),
//                array(
//                    'class' => 'btn-link',
//                    'href' => route('method.load',
//                        [
//                            'directory' => 'Admin',
//                            'controller' => 'Languages',
//                            'action' => 'index'
//                        ]
//                    )
//                )
//            )
//        );

        $data = $this->m->data('images_sizes', $id);
//        dd($this->getType($id));
        $content = view('admin/modules/content/images/size', [

            'action' => route('ajax.load',
                [
                    'directory' => 'Admin',
                    'controller' => 'Size',
                    'action' => 'process',
                    'id' => $id
                ]),
            'process' => 'edit',
            'id' => $id,
            'data' => $data,
            'content_type' => $this->getType($id)
        ]);

        $this->setButtons($buttons);
        $this->setContent($content);

        return $this->output();
    }

    private function getType($size_id = 0)
    {
        $selected = array();
        if ($size_id > 0) {
            $s = $this->m->getSelectedType($size_id);
            foreach ($s as $row) {
                $selected[] = $row->id;
            }
        }

        $types = $this->m->getTypes();

        $res = array();
        $i = 0;
        foreach ($types as $type) {
            $res[$i] = $type;
            $res[$i]->selected = in_array($type->id, $selected) ? 'selected' : '';
            $i++;
        }

        return $res;
    }

    public function process($id = 0)
    {
        $e[] = array();
        $s = 0;
        $e = $this->errors['warning'];
        $data = Input::get('data');
        $process = $data['process'];

        if (empty($data['name'])) {
            $this->errors[] = 'Заповніть всі поля';
        }
        // add or update values
        if (empty($this->error)) {
            switch ($data['process']) {
                case 'create':
                    unset($data['process']);
                    $id = $this->m->insert($data);
                    if ($id > 0) {
                        $s = $id;
                        $e = $this->errors['success'];
                        $this->error[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
                    }
                    break;
                case 'edit':
                    if (empty($id)) return '';
                    unset($data['process']);
                    $s = $this->m->edit($id, $data);
                    if ($s > 0) {
                        $e = $this->errors['success'];
                        $this->errors[] = 'Успішно збережено';
                    } else {
                        $e = $this->errors['error'];
//                        $this->error[] = $this->ms->error() ;
                    }
                    break;
                default:
                    break;
            }
            if ($id > 0) {
                $content_type = Input::get('content_type');
                $ct = isset($content_type) ? $content_type : array();
                $this->saveTypes($id, $ct);
            }
        }

        return json_encode(array(
            's' => $s > 0, // status
            'process' => $process, // redirect url
            'e' => $e, // error class
//            't' => $this->lang->core[$e], // error title
            'm' => implode('<br>', $this->errors) // error message
        ));
    }

    private function saveTypes($id, $data)
    {
        $this->m->clearSelectedType($id);
        if (empty($data)) return 0;
        foreach ($data as $k => $content_type_id) {
            $this->m->addSelectedType($id, $content_type_id);
        }
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->m->del($id);
    }

    public function crop($id)
    {
        return view(
            'admin/modules/content/images/sizes_crop',
            [
                'sizes_id' => $id,
                'total'    => $this->images->getTotalPages($id)
            ]
        );
    }

    public function cropProcess()
    {

        $sizes_id = (int) Input::get('sizes_id');
        $start    = (int) Input::get('start');

        if(empty($sizes_id)) return 0;

        $size = $this->images->getSizeData($sizes_id);
        $images = $this->images->getPageImages($sizes_id, $start);

        $content_id = $images->id;

        if(! empty($images)){
            foreach ($images->images as $row) {
                $src = 'uploads/content/'. $content_id . '/source/' . $row->src;
                $destinationPath = 'uploads/content/' . $content_id . '/' . $size->name .'/';

                if(!is_dir($destinationPath)){
                    if(! File::makeDirectory($destinationPath,$mode = 0777, true, true)){
                        return 'Неможу створити директорію ' . $destinationPath;
                    }
                }

                if(file_exists($destinationPath . $row->src)){
                    unlink($destinationPath . $row->src);
                }

//                echo $src , ' : ' , $dest_dir, '<br>';

                if(!file_exists($src)) continue;
                try {

                    $upload = Image::make($src)
                        ->resize($size->width==0?null:$size->width, $size->height, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                         if(file_exists("uploads/watermark/watermark.png")) {
                             $upload->insert('uploads/watermark/watermark.png',Settings::instance()->get('water_position'));
                         }

                    $upload->save($destinationPath."/".$row->src);
                } catch (Exceptions $e){}
            }

        }

        return 1;
    }


    public function urlUpload()
    {
        $url = Input::get("url");
        $id = Input::get("content_id");
        $size = \App\Model\Admin\content\Images::getSize($id);

        $file = file_get_contents($url);

        $destinationPath = 'uploads/content/'.$id;

        File::makeDirectory($destinationPath.'/source/',$mode = 0777, true, true);
        file_put_contents($destinationPath.'/source/'.'new_image.jpg',$file);
        $file = $destinationPath."/source/new_image.jpg";

        $extension = pathinfo($file,PATHINFO_EXTENSION);
        $fileName = rand(11111,99999).time().$id . '.' . $extension;

        $upload_success = Image::make($file)
            ->save($destinationPath."/source/".$fileName);

        if (!file_exists($destinationPath."/thumb")) {
            File::makeDirectory($destinationPath."/thumb", $mode = 0777, true, true);
        }

        $upload_success = Image::make($destinationPath."/source/".$fileName)
            ->resize(null, 300)
            ->save($destinationPath."/thumb/".$fileName);

        foreach ($size as $item) {
            if (!file_exists($destinationPath."/".$item->name)) {
                File::makeDirectory($destinationPath."/".$item->name, $mode = 0777, true, true);
            }
            $upload_success = Image::make($destinationPath."/source/".$fileName)
                ->resize($item->width==0?null:$item->width, $item->height, function ($constraint) {
                    $constraint->aspectRatio();
                });

            if(file_exists("uploads/watermark/watermark.png")) {
                $upload_success->insert('uploads/watermark/watermark.png',Settings::instance()->get('water_position'));
            }

            $upload_success->save($destinationPath."/".$item->name."/".$fileName);

        }
//
        list($width, $height) = getimagesize($destinationPath."/source/".$fileName);
        $upload_success = Image::make($destinationPath."/source/".$fileName)
            ->resize($width, $height)
            ->save($destinationPath."/source/".$fileName);


        if ($upload_success) {
            unlink($file);
            $id = \App\Model\Admin\content\Images::insert($id,$fileName);
            return json_encode(array(
                'id'   => $id,
                'name' => $fileName
            ));
        } else {
            return Response::json('error', 400);
        }
    }
}
