<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\translator\Bing;
use App\Http\Controllers\Admin\translator\Google;
use App\Http\Controllers\Admin\translator\Yandex;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class Translator extends Controller
{

    public function translate($text, $from, $to){

        $translator = 'yandex';

        switch($translator){
            case 'bing':
                $t = new Bing();
                break;
            case 'google':
                $t = new Google();
                break;
            case 'yandex':
                $yandex_api_key = 'trnsl.1.1.20161109T131519Z.84e9e7b4435c4813.837f936459a342623930aca4a97f8f8ef6c4676b';
                $t = new Yandex($yandex_api_key);
                break;
            default :
                return 'Could to instance Translator ' . $translator;
                break;
        }
        return  $t->translate($text, $from, $to);
    }

    public function translateAll($id, $code)
    {

        $sys_l = Input::get('s_languages');

        $def = array(); $out = array();
// todo якась помилка з циклом , вертає пусті значення
        foreach(Input::get('info') as $_id => $field ){
            foreach ($field as $n => $v) {
//                echo $_id, ',' , $n ,',' , $v;
                if( $_id == $id ){
                    $def[$n] = $v;
                } else {

                    if(empty($def[$n])) continue;

                    $out[] = array(
                        'n' => 'info_' . $_id .'_'. $n,
                        'v' => $this->translate($def[$n], $code, $sys_l[$_id] )
                    );
                }
//                echo $def[$n], '|' , $code , '|' ,  $sys_l[$_id] , '<br>';
            }
        }
//        print_r($out);
        return json_encode($out);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
