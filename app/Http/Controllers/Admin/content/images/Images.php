<?php

namespace App\Http\Controllers\Admin\content\images;

use App\Http\Controllers\Admin\Admin;
use App\Http\Controllers\app\Settings;
use App\Model\Admin\Languages;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class Images extends Admin
{

    public function __construct()
    {
        parent::__construct();
        $this->images = new \App\Model\Admin\content\Images();
        $this->uploaddir = Settings::instance()->get('content_images_dir');
        $this->thumnbs_dir = Settings::instance()->get('content_images_thumb_dir');
        $this->source_dir = Settings::instance()->get('content_images_source_dir');
    }

    /**
     * @return mixed
     */
    public static function uploadFiles()
    {
        ini_set('memory_limit', '-1');
        $input = Input::all();
        $id = $input['id'];
        $size = \App\Model\Admin\content\Images::getSize($input['id']);

        $rules = array(
            'file' => 'required',
        );

        $validation = Validator::make($input, $rules);
        if ($validation->fails()) {
            return Response::make($validation->errors()->all(), 400);
        }

        $destinationPath = 'uploads/content/' . $id;
        File::makeDirectory($destinationPath, $mode = 0777, true, true);
        $extension = Input::file('file')[0]->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . time() . $id . '.' . $extension;

        $upload_success = Input::file('file')[0]->move($destinationPath . "/source", $fileName);
//        Image::make(Input::file('file')[0])->resize(300, 200)->save($fileName);

        if (!file_exists($destinationPath . "/thumb")) {
            File::makeDirectory($destinationPath . "/thumb", $mode = 0777, true, true);
        }

        $upload_success = Image::make($destinationPath . "/source/" . $fileName)
            ->resize(null, 300)
            ->save($destinationPath . "/thumb/" . $fileName);

        foreach ($size as $item) {
            if (!file_exists($destinationPath . "/" . $item->name)) {
                File::makeDirectory($destinationPath . "/" . $item->name, $mode = 0777, true, true);
            }
            $upload_success = Image::make($destinationPath . "/source/" . $fileName)
                ->resize($item->width == 0 ? null : $item->width, $item->height, function ($constraint) {
                    $constraint->aspectRatio();
                });

            if (file_exists("uploads/watermark/watermark.png")) {
                $upload_success->insert('uploads/watermark/watermark.png', Settings::instance()->get('water_position'));
            }

            $upload_success->save($destinationPath . "/" . $item->name . "/" . $fileName);

        }

        list($width, $height) = getimagesize($destinationPath . "/source/" . $fileName);
        $upload_success = Image::make($destinationPath . "/source/" . $fileName)
            ->resize($width, $height)
            ->save($destinationPath . "/source/" . $fileName);


        if ($upload_success) {
            $id = \App\Model\Admin\content\Images::insert($id, $fileName);
            return json_encode(array(
                'id' => $id,
                'name' => $fileName
            ));
        } else {
            return Response::json('error', 400);
        }
    }

    public function getTemplate($content_id, $im_data = array())
    {
        // get thumbs path
        $thumb_path = $this->thumnbs_dir; // $im->thumbPath();
        $preview_path = $this->source_dir;// $im->previewSize($content_id);

        $dir = ltrim($this->uploaddir, DIRECTORY_SEPARATOR) . $content_id . '/' . $thumb_path . '/';
        $dir_preview = ltrim($this->uploaddir, DIRECTORY_SEPARATOR) . $content_id . '/' . $preview_path . '/';
        $im_data['path'] = $dir;
        $im_data['path_view'] = $dir_preview;
        $im_data['content_id'] = $content_id;

        return view('admin/modules/content/images/template', $im_data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get($content_id)
    {
        $images = \App\Model\Admin\content\Images::get($content_id);
        $im_data = array();

        foreach ($images as $image) {
            $im_data[] = $this->getTemplate(
                $content_id,
                array(
                    'image' => $image,
//                'path'       => $dir,
//                'path_view'  => $dir_preview,
                    'content_id' => $content_id
                ));
        }

        return view('admin/modules/content/images/list', array(
            'id' => $content_id,
            'images' => $im_data
        ));
    }

    public function delete($id)
    {
        $data = $this->images->imageData($id);
        $content_id = $data->content_id;
        $types = $this->images->getSize($content_id);
        $sizes = [];
        foreach ($types as $k => $type) {
            $sizes[$k]['name'] = $type->name . "/";
        }
        $sizes[]['name'] = $this->thumnbs_dir;
        $sizes[]['name'] = $this->source_dir;


        // формую шлях
        foreach ($sizes as $size) {
            $path = public_path() . $this->uploaddir . $content_id . "/" . $size['name'] . $data->name;
            if (!File::exists($path)) continue;
            File::delete($path);
        }
//        dd();
        return $this->images->del($id);
    }

    public function insertInfo($id, $info)
    {
        $s = $this->images->info($id, $info);
        return json_encode(['s' => $s]);
    }

    public function getInfo($id)
    {
        $items = $this->images->getInfo($id);

        $res = array();

        foreach ($items as $row) {
            $res[$row->languages_id] = $row->alt;
        }

        return $res;
    }

    public function editInfo($id)
    {
        if(\Illuminate\Support\Facades\Request::isMethod('post')) {
            return $this->images->updateInfo($id,Input::get('info'));
        } else {
            $languages = new Languages();

            $info = $this->images->getAlt($id);
            return $this->template->view(
                'modules/content/images/editInfo',
                [
                    'info' => $info,
                    'id' => $id,
                    'languages' => $languages->geAll(true)
                ]);
        }
    }
}
