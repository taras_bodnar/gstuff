<?php

namespace App\Http\Controllers;

use App\Http\Controllers\app\Settings;
use App\Http\Controllers\app\Template;
use App\Http\Controllers\modules\Nav\NavModel;
use App\Model\Admin\Nav;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Core as ModelCore;
use App\Http\Middleware\LoadPage;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\View\View;
use Mockery\CountValidator\Exception;

class Core extends Controller
{
    protected $template_url;
    private $adapters = array();
    public function __construct()
    {
        $this->template = Template::instance(Settings::instance()->get('app_theme_current'));
        $this->languages_id = Session::get('app.languages_id');
        $this->mc = new \App\Model\Core();
        $this->nav = new NavModel();
        $this->page = LoadPage::$data;
        $this->translation = LoadPage::$translations;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $template = ModelCore::getTemplate(LoadPage::$data->templates_id,LoadPage::$data->type_id);

        $template = $this->template->view($template->type.'/'.$template->path);

        $template = $this->parse($template);

        $template = $this->makeFriendlyUrl($template);

        return $template;
    }


    public function parse($ds)
    {

        $self = $this;

        $themes_path = Settings::instance()->get('themes_path');

        $current = Settings::instance()->get('app_theme_current');
        $views = Settings::instance()->get('app_views_path');
        $chunks_path = Settings::instance()->get('app_chunks_path');
        $chunks_path = $themes_path . $current . '/' . $chunks_path;

        $template_url = $themes_path .$views. $current . '/';
        $base_url = base_path();

        $ds = preg_replace_callback(
            '@\{([page|app|mod|chunk]+):([a-zA-Z_0-9\-]+)?(::[a-zA-Z_0-9]+)??(\([a-zA-z0-9,-: ]+\))?\}@s',
            function ($data) use ($self, $template_url, $base_url, $chunks_path) {

                $out = '';

                switch ($data[1]) {
                    case 'mod': // модуль
                        $module = '';
                        $action = '';
                        $params = array();
                        if (isset($data[2]) && !empty($data[2])) {
                            $module = $data[2];
                        }
                        if (isset($data[3]) && !empty($data[3])) {
                            $action = ltrim($data[3], '::');
                        }
                        if (isset($data[4]) && !empty($data[4])) {

                            $params = explode(',', trim($data[4], '()'));
                        }

                        $out = $this->module($module, $action, $params);
                        $out = $this->parse($out);
                        break;
//                    case 'chunk': // чанк
//                        $out = $self->template->fetch($chunks_path . $data[2]);
//                        $out = $app->parse($out);
//                        break;
//                    case 'app': // виклик функції метатегів
//                        if (isset($data[2])) {
//                            if ($data[2] == 'metatags') {
//                                $out = $self->metatags();
//                            } else {
//                                $out = '<b>Error.</b> invalid parse: ' . $data[2];
//                            }
//                        }
//                        break;
                    default:
                        return 'invalid parse, use pattenrn:
                          @\[{([mod|chunk]+):([a-zA-Z_0-9]+)?(::[a-zA-Z_0-9]+)?}\]@s';
                        break;
                }
                return $out;
            },
            $ds
        );
        return $ds;
    }

    /**
     * @return string
     * post, put, get, request
     */
    public function request()
    {
        $route = Route::getCurrentRoute()->parameters();
        $controller = $route['controller'];
        $method = $route['method'];
        $param1 = isset($route['param1'])?$route['param1']:'';
        $param2 = isset($route['param2'])?$route['param2']:'';
        $param = explode('/',$param1);
        $params = explode('/',$param2);
        $params = array_merge($param,$params);
        return $this->module($controller,$method,$params);
    }


    public function module($controller, $action = 'index', $params = array())
    {
        $mod_path = Settings::instance()->get('mod_path');
        $controller = $mod_path . $controller . '\\' . $controller;

        $className = ltrim($controller, '\\');

        $fileName = '';
        $namespace = '';
        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
        $fileName = $_SERVER['DOCUMENT_ROOT'] . '/' . $fileName;

        if (is_readable($fileName)) {

            require_once($fileName);

            $c = $namespace . '\\' . $className;

            if (class_exists($c)) {
                ob_start();

                $controller = new $c;

                $action = (is_callable(array($controller, $action))) ? $action : 'index';

                if (!empty($params)) {

                    echo call_user_func_array(array($controller, $action), $params);
                } else {
                    echo call_user_func(array($controller, $action));
                }
                return ob_get_clean();
            }
        }

        throw new Exception("Module {$controller} :: {$action} issues.");
    }

    private function makeFriendlyUrl($ds)
    {
        $mc = new \App\Model\Core();

        $current_languages_id =  $this->languages_id;
//        мову по замовчуванню
        $_languages_id = $this->mc->getFrontLangDefault();

        // список мов
        $_languages = $this->nav->getLanguages();
        $languages = array();
        foreach ($_languages as $row) {
            $languages[$row->id] = $row->code;
        }


        $ds = preg_replace_callback(
            '/(href|action)=\"([^\"]*)\"/siU',
            function($data) use ($mc, $_languages_id, $languages, $current_languages_id){

                $id=0; $languages_id=0; $p=0; $tag = ''; $out = ''; $qs = array();
                $action = $data[1];
                // пошук по системних параметрах
                if(strpos($data[2], ';') !== false){
                    // пошук по системних параметрах
                    $a = explode(';', $data[2]);
//                    dd($a);
//                    $qs = end($a);
//                    $qs = '?'. ltrim($qs, '&');
                    if(isset($a[1])){

                        foreach ($a as $i=>$row) {
                            if($i == 0) {
                                $id = (int) $row;
                            } else {
                                if(strpos($row, 'l=') !== false){
                                    $languages_id = ltrim($row,'l=');
                                    $languages_id = rtrim($languages_id, ';');
                                    continue;
                                }elseif(strpos($row, 'p=') !== false){
                                    $p = ltrim($row,'p=');
                                    $p = rtrim($p,';');

                                    continue;
                                }elseif(strpos($row, 'tag=') !== false){
                                    $tag = ltrim($row,'tag=');
                                    $tag = rtrim($tag,';');
                                    continue;
                                } elseif(!empty($row)){

                                    $qs[] = $row;
                                }
                            }
                        }

                    }
                } else if(preg_match('@^([0-9]+)$@', $data[2], $mathes)){
                    $id = (int)$mathes[1];
                } else {
                    return $action . '="'.$data[2].'"';
                }
                // якщо вказана мова

                if($languages_id > 0){
                    // якщо не мова по замовчванню то присвою префік

                    if($languages_id != $_languages_id->id) {
                        $out .= "/".$languages[$languages_id] . '/';
                    }


                } else {

                    if($_languages_id->id != $current_languages_id ) {

                        $out .= "/".$languages[$current_languages_id] . '/';
                    }

                    $languages_id = $current_languages_id;

                }

                if($id > 0){
                    if($id == 1){
                        $alias = '/';

                    } else {
                        $alias = $mc->getAliasById2($id, $languages_id);
                    }

                    $out .=!empty($alias->alias)?"/".$alias->alias."":$alias;
                }
//                if($p > 0){
//                    $out .= '/' . $p;
//                }
//
//                if($tag != '') {
//                    $out .= '/tag/' . $tag;
//                }
//
                if(!empty($qs)){
                    $out .= '?'. implode('&', $qs);
                }

                $out = str_replace('//','/', $out);
                $result = $action . '="'.$out.'"';
                return $result;
            },
            $ds
        );

        return $ds;
    }

    public function oAuth()
    {
        require_once base_path('app/Http/Controllers/vendor/SocialAuther/autoload.php');
        $s = Settings::instance();


        if(!isset($_SESSION['referer'])) {
            if(isset($_SERVER['HTTP_REFERER'])) {
                $_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
            } else {
                $message = 'NOT FOUND HTTP_REFERER';
            }
        }
//        $succes_url = $mc->getAliasById( 7, $this->languages_id );
        $adapterConfigs = array(
            'facebook' => array(
                'client_id'     => $s->get('facebook_app_id'),
                'client_secret' => $s->get('facebook_app_secret'),
                'redirect_uri'  => URL::previous() . '?provider=facebook'
            )
        );

        foreach ($adapterConfigs as $adapter => $settings) {
            $class = 'App\Http\Controllers\vendor\SocialAuther\Adapter\\' . ucfirst($adapter);
//            echo $class."<br>";

            $this->adapters[$adapter] = new $class($settings);
            \Illuminate\Support\Facades\View::share('oauth_' . $adapter . '_url', $this->adapters[$adapter]->getAuthUrl());
        }

        if (isset($_GET['provider']) && array_key_exists($_GET['provider'], $this->adapters)) {
            $auther = new \App\Http\Controllers\vendor\SocialAuther\SocialAuther($this->adapters[$_GET['provider']]);
//            $auther = new SocialAuther($this->adapters[$_GET['provider']]);
//            echo $redirect_id;die;
            if ($auther->authenticate()) {
                $data = array();
                $data['provider'] = $auther->getProvider();
                $data['social_id'] =$auther->getSocialId();
                $data['name'] = $auther->getName();
                $data['email'] = $auther->getEmail();
                $data['avatar'] = $auther->getAvatar();


                $data['users_group_id'] = 2;
                $data['languages_id'] = $this->languages_id;
                $data['password'] = '';

                if(empty($data['provider']) || empty($data['social_id'])) die();
                // розділяю імя на імя і прізвище
            }
        }
    }

}
