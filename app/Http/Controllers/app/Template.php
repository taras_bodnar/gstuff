<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Template extends Controller
{
    private static $_instance;
    public $template_url;

    public function __construct($url)
    {
        $this->template_url = $url;
    }

    public static function instance($url)
    {
        if(self::$_instance == null) {
            self::$_instance = new Template($url);
        }

        return self::$_instance;
    }

    public function view($path,$arguments=array())
    {
        return view($this->template_url."/".$path,$arguments)->render();
    }
}
