<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\app\Settings as SettingsM;

class Settings extends Controller
{

    private static $_instance;
    private static $_data;
    private static $ms;

    public function __construct()
    {
        self::$ms = new SettingsM();
    }

    public function index(){}

    public static function instance()
    {
        if(self::$_instance == null) {
            self::$_instance = new Settings();
            self::load();
        }

        return self::$_instance;
    }

    private static function load()
    {
        $data = self::$ms->loadS();
        foreach($data as $row){
            self::$_data[$row->name] = $row->value;
        }
    }

    public function set($n, $v)
    {
        return self::$ms->set($n, $v);
    }

    public function del($key)
    {
        return self::$ms->deleteS($key);
    }

    public function get($key=null)
    {
        return $key ? self::$_data[$key] : self::$_data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
