<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 15.10.15
 * Time: 11:18
 */

spl_autoload_register('autoload');
function autoload($class)
{
    require_once str_replace('SocialAuther/', 'SocialAuther/', str_replace('\\', '/', $class) . '.php');
}