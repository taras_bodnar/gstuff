<?php

namespace App\Http\Controllers\modules\Users;

use Illuminate\Support\Facades\Auth as AuthController;
use App\Http\Controllers\Core;
use App\Http\Controllers\modules\Feedback\FeedbackModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Users extends Core
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new UsersM();
        $this->t = new FeedbackModel();
    }

    public function registration()
    {
        if (\Illuminate\Support\Facades\Request::isMethod('post')) {
            $error = [];
            $s = 0;
            $data = Input::get('data');
            $i = [];

            if(empty($data['name'])) {
                $error[] = 'Вкажіть ім\'я';
                $i[] = '#r_name';
            } if( empty($data['email'])) {
                $error[] = 'Вкажіть e-mail';
                $i[] = '#r_email';
            } if( empty($data['phone'])) {
                $error[] = 'Вкажіть телефон';
                $i[] = '#r_phone';
            } if(empty($data['company'])) {
                $error[] = 'Вкажіть Компанію';
                $i[] = '#r_company';
            } if(empty($data['address'])) {
                $error[] = 'Вкажіть адресу';
                $i[] = '#r_address';
            } if(empty($data['password'])) {
                $error[] = 'Вкажіть пароль';
                $i[] = '#r_password';
            }

            if($data['password']!=$data['repeat_pswd']) {
                $error[] = 'Паролі не співпадають';
                $i[] = '#r_repeat_password';
            }

            $info = $this->m->getInfoByEmail($data['email']);
            if(!empty($info)) {
                if($info->id>0){
                    $error[] = 'Користувач з таким email вже зареєстрований';
                    $i[] = '#r_email';
                }
            }

            $data['password'] = bcrypt($data['password']);
            $data['createdon'] = date('Y-m-d H:i:s');
            $data['users_group_id'] = 2;
            $data['languages_id'] = $this->languages_id;

            if(empty($error)) {
                unset($data['repeat_pswd']);
                $id = $this->m->createUser($data);

                $template = $this->t->getTemplate('registration');

                $data['href'] = 'http://'.\Illuminate\Support\Facades\Request::server('HTTP_HOST')."/request/Users/confirm/$id";
                $re = "/([\\{][]a-zA-Z0-9-_\\}]+)/";

                $body = preg_replace_callback(
                    $re,
                    function ($matches) use ($data) {
                        $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                        return isset($data[$matches[0]]) ? $data[$matches[0]] :"";
                    },
                    $template->body
                );

                Mail::send([], [], function ($message) use ($data,$template,$body) {
                    $message->to($template->email, $template->name)
                        ->from($data['email'], $data['name'])
                        ->subject($template->subject)
                        ->setBody($body,'text/html');
                });


                if( count(Mail::failures()) > 0 ) {
                    $error[] = Mail::failures();
                } else {
                    $s = true;
                    $error[] = 'Заявку на реєстрацію вашого акаунту надіслано';
                }

            }
            return json_encode(
                array(
                    's' => $s,
                    'm' => implode('<br>', $error),
                    'i'=>$i
                )
            );
        } else {
            return $this->template->view('modules/users/registration');
        }
    }

    public function confirm($id)
    {
        $s = $this->m->confirm($id);

        if($s) {
            $template = $this->t->getTemplate('confirm');
            $info = $this->m->getInfoById($id);

            $re = "/([\\{][]a-zA-Z0-9-_\\}]+)/";

            $body = preg_replace_callback(
                $re,
                function ($matches) use ($info) {
                    $matches[0] = str_replace(array('{','}'), array(), $matches[0]);
                    return isset($data[$matches[0]]) ? $data[$matches[0]] :"";
                },
                $template->body
            );

            Mail::send([], [], function ($message) use ($info,$template,$body) {
                $message->to($info->email, $info->name)
                    ->from($template->email, $template->name)
                    ->subject($template->subject)
                    ->setBody($body,'text/html');
            });


            return redirect('/');
        }
    }

    public function auth()
    {
        if (\Illuminate\Support\Facades\Request::isMethod('post')) {
            $rules = array(
                'email' => 'required|email', // make sure the email is an actual email
                'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
            );

// run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
//        dd($validator);
// if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return json_encode([
                    's' => 0,
                    'm' => 'Заповніть будь ласка всі поля'
                ]);
//                    ->withErrors($validator)
//                    ->withInput(Input::except('password'));
            } else {

                // create our user data for the authentication
                $userdata = array(
                    'email' => Input::get('email'),
                    'password' => Input::get('password')
                );

                // attempt to do the login
                if (AuthController::attempt($userdata)) {

                    $this->m->setWhere('status',1);
                    $userInfo = $this->m->getInfoByEmail($userdata['email']);

                    if(!empty($userInfo)) {
                        Session::set('app', [
                            'client' => $userInfo
                        ]);

                        return json_encode([
                            's' => 1
                        ]);
                    } else {
                        return json_encode([
                            's' => 0,
                            'm' => 'Адміністратор ще не надав вам доступу'
                        ]);
                    }
                } else {
                    // validation not successful, send back to form
                    return json_encode([
                        's' => 0,
                        'm' => 'Одне з полів заповнено не правильно'
                    ]);

                }

            }
        }else {
            return $this->template->view('modules/users/auth');
        }

    }
}
