<?php

namespace App\Http\Controllers\modules\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersM extends Model
{
    public  function createUser($data)
    {
        return DB::table('users')->insertGetId(
            $data
        );
    }

    public function getInfoByEmail($email)
    {
        $query = DB::table('users')->select('id')
            ->where('email','=',"$email");

        if ($this->where) {
            $this->getWhere($query);
        }

        return $query->first();
    }

    public function confirm($id)
    {
        return DB::table('users')->where('id','=',$id)
            ->update([
               'status'=>1
            ]);
    }

    public function getInfoById($id)
    {
        $query = DB::table('users')->select('email','name')
            ->where('id','=',$id);

        if ($this->where) {
            $this->getWhere($query);
        }

        return $query->first();
    }
}
