<?php

namespace App\Http\Controllers\modules\BreadCrumbs;

use App\Http\Controllers\Core;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BreadCrumbs extends Core
{

    public function __construct()
    {
        parent::__construct();
        $this->m = new BreadCrumbsM();
    }

    public function index()
    {
       $id = $this->page->id;
        $items = array();

        if($id > 1) {
            $items[] = $this->m->home();
        }

        $current = $this->m->current($id);

        if($current->parent_id > 0) {

            $items[] = $this->m->parents($current->parent_id);
        }

        $items[] = $current;

        return $this->template->view('modules/breadcrumb/index',['items'=>$items]);
    }

}
