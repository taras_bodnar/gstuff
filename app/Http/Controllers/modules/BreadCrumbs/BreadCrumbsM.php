<?php

namespace App\Http\Controllers\modules\BreadCrumbs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BreadCrumbsM extends Model
{
    private $items = [];

    public function current($id)
    {
        return DB::table('content as c')->select('c.id','c.parent_id','ci.name','ci.title')
            ->join('content_info as ci',function($join) {
                $join->on('c.id','=','ci.content_id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.id','=',$id)
            ->first();
    }

    public function parents($id)
    {
        if($id==1) return;
//        DB::setFetchMode(\PDO::FETCH_ASSOC);
        $r = DB::table('content as c')->select('c.id','c.parent_id','ci.name','ci.title')
            ->join('content_info as ci',function($join) {
                $join->on('c.id','=','ci.content_id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.id','=',$id)
            ->first();

        $this->items = (object) array_merge((array) $this->items, (array) $r);

        if($r->parent_id > 0) {

            $this->parents($r->parent_id);
        }
//        DB::setFetchMode(\PDO::FETCH_CLASS);
        return $this->items;
    }


    public function home()
    {
        return DB::table('content as c')->select('c.id','c.parent_id','ci.name','ci.title')
            ->join('content_info as ci',function($join) {
                $join->on('c.id','=','ci.content_id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.id','=',1)
            ->first();
    }

}
