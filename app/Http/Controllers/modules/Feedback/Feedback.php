<?php

namespace App\Http\Controllers\modules\Feedback;

use App\Http\Controllers\Core;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class Feedback extends Core
{

    public function __construct()
    {
        parent::__construct();
        $this->m = new FeedbackModel();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->template->view('modules/feedback/index');
    }

    public function getStart()
    {
        if (!\Illuminate\Support\Facades\Request::isMethod('post')) {
            return $this->template->view('modules/feedback/getStart');
        } else {
            $s = false;
            $sm = '';
            $errors = array();

            $data = Input::get('data');

            $template = $this->m->getTemplate('get_start');


            $validator = Validator::make($data, [
                'price' => 'required',
                'name' => 'required',
                'email' => 'required|email'
            ],[
                'required' => 'Fields are required',
                'email' => 'Correct email',
            ]);

            if(isset($data['type']) && !empty($data['type'])) {
                $data['type'] = implode(',',$data['type']);
            }


            if ($validator->fails()) {
                $errors =  $validator->errors()->getMessages();
            }


            if(empty($errors)) {
                $re = "/([\\{][]a-zA-Z0-9-_\\}]+)/";

                $body = preg_replace_callback(
                    $re,
                    function ($matches) use ($data) {
                        $matches[0] = str_replace(array('{', '}'), array(), $matches[0]);
                        return isset($data[$matches[0]]) ? $data[$matches[0]] : "";
                    },
                    $template->body
                );

                Mail::send([], [], function ($message) use ($data, $template, $body) {
                    $message->to($template->email, $template->name)
                        ->from($data['email'], $data['name'])
                        ->subject($template->subject);
                            if(Input::hasFile('file')) {
                                $message->attach(Input::file('file')->getRealPath(), array(
                                        'as' => 'resume.' . Input::file('file')->getClientOriginalExtension(),
                                        'mime' => Input::file('file')->getMimeType())
                                );
                            }

                        $message->setBody($body, 'text/html');
                });


                if (count(Mail::failures()) > 0) {
                    $errors = Mail::failures();
                } else {
                    $s = true;
                    $sm = $this->translation['msg_sent'];
                }
            }


            return json_encode(
                [
                    's' => $s,
                    'sm' => $sm,
                    'errors' => $errors
                ]
            );
        }
    }

    public function run()
    {
        $s = false;
        $sm = '';
        $errors = array();

        $data = Input::get('data');
        $template = $this->m->getTemplate('getintouch');


        $validator = Validator::make($data, [
            'name' => 'required',
            'message' => 'required',
            'email' => 'required|email'
        ],[
           'required' => 'Fields are required',
           'email' => 'Correct email',
        ]);



        if ($validator->fails()) {
            $errors =  $validator->errors()->getMessages();
        }

        if(empty($errors)) {
            $re = "/([\\{][]a-zA-Z0-9-_\\}]+)/";

            $body = preg_replace_callback(
                $re,
                function ($matches) use ($data) {
                    $matches[0] = str_replace(array('{', '}'), array(), $matches[0]);
                    return isset($data[$matches[0]]) ? $data[$matches[0]] : "";
                },
                $template->body
            );

            Mail::send([], [], function ($message) use ($data, $template, $body) {
                $message->to($template->email, $template->name)
                    ->from($data['email'], $data['name'])
                    ->subject($template->subject)
                    ->setBody($body, 'text/html');
            });


            if (count(Mail::failures()) > 0) {
                $errors = Mail::failures();
            } else {
                $s = true;
                $sm = $this->translation['msg_sent'];
            }
        }


        return json_encode(
            [
                's' => $s,
                'sm' => $sm,
                'errors' => $errors
            ]
        );
    }
}
