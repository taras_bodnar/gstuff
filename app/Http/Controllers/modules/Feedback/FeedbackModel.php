<?php

namespace App\Http\Controllers\modules\Feedback;

use App\Http\Controllers\app\Settings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FeedbackModel extends Model
{
    public function getTemplate($name)
    {
        $template = DB::table('email_templates as t')->select('*')
            ->join('email_templates_info as ti',function($join) {
               $join->on('t.id','=','ti.templates_id')
                   ->where('ti.languages_id','=',$this->languages_id);
            })
            ->where('code','=',"$name")
            ->first();

        $header = Settings::instance()->get('et_header');
        $footer = Settings::instance()->get('et_footer');

        $template->body = $header . $template->body . $footer;

        return $template;
    }
}
