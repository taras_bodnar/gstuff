<?php

namespace App\Http\Controllers\modules\Pages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PagesM extends Model
{

    public function getFiles()
    {
        $query = DB::table('content_images as ci')->select('ci.id', 'ci.content_id', 'ci.name', 'cii.alt')
            ->join('content_images_info as cii', function ($join) {
                $join->on('ci.id', '=', 'cii.images_id')
                    ->where('cii.languages_id', '=', $this->languages_id);
            });

        if (!empty($this->join)) {
            $this->getJoin($query);
        }
        if ($this->where) {
            $this->getWhere($query);
        }


        $query->orderBy('ci.id', 'desc');
        if (!empty($this->limit)) {
            $this->getLimit($query);
        }

        return !empty($this->limit) ? $query->get() : $query->paginate(30);
    }


    public function getByType($type_id)
    {
        return DB::table('content as c')->select('c.id', 'ci.name', 'ci.title')
            ->join('content_info as ci', function ($join) {
                $join->on('c.id', '=', 'ci.content_id')
                    ->where('ci.languages_id', '=', $this->languages_id);
            })
            ->where('c.auto', '=', 0)
            ->where('c.type_id', '=', $type_id)
            ->where('c.published', '=', 1)
            ->orderBy('c.sort', 'asc')
            ->get();
    }

    public function getRand($type_id)
    {
        return DB::table('content as c')->select('c.id', 'ci.name', 'ci.title')
            ->join('content_info as ci', function ($join) {
                $join->on('c.id', '=', 'ci.content_id')
                    ->where('ci.languages_id', '=', $this->languages_id);
            })
            ->where('c.auto', '=', 0)
            ->where('c.type_id', '=', $type_id)
            ->where('c.published', '=', 1)
            ->orderBy(DB::raw('RAND()'))
            ->first();
    }

    public function getItems($parent_id)
    {
        return DB::table('content as c')->select('c.*','ci.name','ci.title','ci.description','ci.content')
            ->join('content_info as ci',function($join) {
                $join->on('ci.content_id','=','c.id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('published','=',1)
            ->where('auto','=',0)
            ->where('parent_id','=',$parent_id)
            ->get();
    }

    public function getInfo($id)
    {
        return DB::table('content_info')
            ->where('content_id','=',$id)
            ->where('languages_id','=',$this->languages_id)
            ->first();
    }

    public function getAlbums($id)
    {
        return DB::table('content as c')->select('c.id','ci.name','ci.description','ci.title')
            ->join('content_info as ci',function($join){
                $join->on('c.id','=','ci.content_id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->join('posts_categories as pc',function($join){
                $join->on('pc.pid','=','c.id');
            })
            ->where('c.type_id','=','10')
            ->where('c.published','=',1)
            ->where('c.auto','=',0)
            ->where('pc.cid','=',$id)
            ->orderBy('c.id','desc')
            ->paginate(10);
    }

}
