<?php

namespace App\Http\Controllers\modules\Pages;

use Illuminate\Database\Eloquent\Model;

class PagesViews extends Model
{
    protected $fillable = ['ip', 'content_id'];
}
