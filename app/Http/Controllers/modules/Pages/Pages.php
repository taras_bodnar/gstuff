<?php

namespace App\Http\Controllers\modules\Pages;

use App\Http\Controllers\Core;
use App\Model\app\Features;
use App\Model\app\Images;
use App\Model\app\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Charts;
use Illuminate\Support\Facades\Redirect;

class Pages extends Core
{
    private $m;
    private $images;
    private $features;
    public function __construct()
    {
        parent::__construct();
        $this->m = new PagesM();
        $this->images = new Images();
        $this->features = new Features();
    }

    private function processURL($url)
    {
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function instagramLoad()
    {
//        $client_id = "d969888ceaf346589e690fa4df8a26af";
//        $redirect_url = "http://gstuff.loc";
//        $url = 'https://www.instagram.com/oauth/authorize/?client_id='.$client_id.'&redirect_uri='.$redirect_url.'&response_type=token&scope=public_content';
//        return Redirect::to($url);

        $url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=5745177036.d969888.7af09d9010734260a9efc78a39dd2812';

        $all_result  = $this->processURL($url);
        $decoded_results = json_decode($all_result, true);


        //Now parse through the $results array to display your results...
        $arr  = [];

        foreach($decoded_results['data'] as $k=>$item){
            $arr[$k]['thumb'] = $item['images']['thumbnail']['url'];
            $arr[$k]['standard'] = $item['images']['standard_resolution']['url'];
            $arr[$k]['link'] = $item['link'];
            $arr[$k]['location'] = $item['location']['name'];
        }

        $arr = json_encode($arr);
        DB::table('settings')->where('name','instagram')->update([
            'value' => $arr,
            'description' => 'Photo from instagram'
        ]);
        return true;

    }

    public function services()
    {
        $id = 2;

        $items = $this->m->getItems($id);
        foreach ($items as $k=>$item) {
            $value = $this->features->getContentValues($item->id,1);
            $items{$k}->options = explode("\n",$value->value);
        }

        return $this->template->view('modules/pages/services',[
            'items'=>$items
        ]);
    }

   public function technology()
   {
       $id = 8;

       $items = $this->m->getItems($id);
       foreach ($items as $k=>$item) {
           $items{$k}->cover = $this->images->cover($item->id,'source');
           $items{$k}->images = $this->images->get($item->id,'technology');
       }

       return $this->template->view('modules/pages/technology',[
           'items'=>$items
       ]);
   }

   public static function instagram()
   {
       $items = Settings::get('instagram');

       return json_decode($items->value,true);
   }

   public function getFeaturesValues($id,$features_id,$type)
   {
        $items = (array)$this->features->getContentValues($id,$features_id,$type);

       return isset($items) && !empty($items)?$items['value']:"";
   }

   public function getTechnologiesImages()
   {
       $id = $this->page->id;

       $images = $this->images->get($id,'source');

       return $this->template->view('modules/pages/technologyImages',[
           'images'=>$images
       ]);
   }

   public function servicesInside()
   {
       if(!$this->page->isfolder) return "";

       $children = $this->m->getItems($this->page->id);

       $info = $this->m->getInfo($children[0]->id);

       $images = $this->images->get($children[0]->id,'source');

       return $this->template->view('modules/pages/serviceInside',[
           'info' => $info,
           'images' => $images
         ]);
   }

    public function loadVideo()
    {
        return $this->template->view('modules/pages/video');
    }
}
