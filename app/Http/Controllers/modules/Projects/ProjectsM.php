<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 28.06.17
 * Time: 16:19
 */

namespace App\Http\Controllers\modules\Projects;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectsM extends Model
{
    private static $type_id = 7;
    private static $cat_type_id = 8;

    public function get($s = false)
    {
        $query =  DB::table('content as c')->select('c.id','i.name','i.description')->
            join('content_info as i',function ($join) {
                $join->on('i.content_id','=','c.id')
                    ->where('i.languages_id','=',$this->languages_id);
            });

        if($s){
            $query->join('posts_categories as pc',function ($join) {
                $join->on('pc.cid','=','c.id');
            });
        }


            if (!empty($this->join)) {
                $this->getJoin($query);
            }

        if ($this->where) {
            $this->getWhere($query);
        }

        return $query->where('published',1)
            ->where('type_id',self::$type_id)
            ->where('auto',0)
            ->orderBy('c.sort','asc')
            ->get();
    }

    public function getCategories($id)
    {
        return DB::table('content as c')->select('c.id','i.name')
            ->join('content_info as i',function($join){
                $join->on('i.content_id','=','c.id')
                    ->where('i.languages_id','=',$this->languages_id);
            })
            ->join('posts_categories as pc',function($join){
                $join->on('pc.cid','=','c.id');
            })
            ->where('pid','=',$id)
            ->get();
    }

    public function categories()
    {
        return DB::table('content as c')->select('c.id','i.name','i.description',
            DB::raw("(Select count(pid) from posts_categories where cid=c.id) as t"))->
        join('content_info as i',function ($join) {
            $join->on('i.content_id','=','c.id')
                ->where('i.languages_id','=',$this->languages_id);
        })
            ->where('published',1)
            ->where('type_id',self::$cat_type_id)
            ->where('auto',0)
            ->get();
    }

    public function getTags($content_id)
    {
        return DB::table('tags as t')->select('t.id','t.name')
            ->join('tags_content as tc',function($join) use ($content_id){
                $join->on('tc.tags_id','=','t.id')
                    ->where('tc.content_id','=',$content_id);
            })
            ->where('t.languages_id','=',$this->languages_id)
            ->get();
    }
}