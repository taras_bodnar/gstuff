<?php
namespace App\Http\Controllers\modules\Projects;
use App\Http\Controllers\Core;
use App\Model\app\Features;
use App\Model\app\Images;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: taras
 * Date: 28.06.17
 * Time: 16:18
 */
class Projects extends Core
{
    private $model;
    private $images;
    private $features;
    public function __construct()
    {
        parent::__construct();
        $this->model = new ProjectsM();
        $this->images = new Images();
        $this->features = new Features();
    }

    public function index()
    {
        if(!empty($this->page->id)) {
//            $this->model->setKeys("pc.cid","42");
            $this->model->setWhere("pc.pid","=","{$this->page->id}");
//            $this->model->setJoin('posts_categories as pc');
        }
        $items = $this->model->get(true);

        foreach ($items as $k=>$item) {
            $items{$k}->cover = $this->images->cover($item->id,'source');
//            if(isset($items{$k}->cover->src) && file_exists($items{$k}->cover->src)) {
//                $items{$k}->cover->size = getimagesize(public_path().$items{$k}->cover->src)[0];
//            }
            $items{$k}->cover->size = $this->features->getContentValues($item->id,6,'select');
            $items{$k}->behance = $this->features->getContentValues($item->id,7);
            $items{$k}->categories = $this->model->getCategories($item->id);
            $items{$k}->tags = $this->model->getTags($item->id);
            $items{$k}->colourF = $this->features->getContentValues($item->id,2,'colour');
            $items{$k}->colourT = $this->features->getContentValues($item->id,3,'colour');
        }

        return $this->template->view('modules/projects/index',[
           'items' => $items
        ]);
    }

    public function portfolio()
    {
        $categoriesAll = $this->model->categories();


        $items = $this->model->get();

        foreach ($items as $k=>$item) {
            $items{$k}->cover = $this->images->cover($item->id,'source');
//            if(isset($items{$k}->cover->src) && file_exists($items{$k}->cover->src)) {
//                $items{$k}->cover->size = getimagesize(public_path().$items{$k}->cover->src)[0];
//            }

            $items{$k}->cover->size = $this->features->getContentValues($item->id,6,'select');
            $items{$k}->behance = $this->features->getContentValues($item->id,7);
            $categories = $this->model->getCategories($item->id);

            $item->cat = $categories;

            if(!empty($categories)) {
                $categoriesNew = [];
                foreach($categories as $category) {
                    $categoriesNew[] = $category->id;
                    $items{$k}->categories = implode(',',$categoriesNew);
                    $items{$k}->class = implode(' ',$categoriesNew);
                }
            }

            $items{$k}->tags = $this->model->getTags($item->id);

            $items{$k}->colourF = $this->features->getContentValues($item->id,2,'colour');
            $items{$k}->colourT = $this->features->getContentValues($item->id,3,'colour');
        }


//        foreach($categories as $k=>$item) {
//            $this->model->clearJoin();
//            $this->model->setKeys("pc.cid","{$item->id}");
//            $this->model->setJoin('posts_categories as pc');
//            $categories{$k}->items = $this->model->get();
//            foreach($categories{$k}->items as $i=>$project) {
//                $categories{$k}->items{$i}->cover = $this->images->cover($project->id,'source');
//                if(isset($categories{$k}->items{$i}->cover->src) && file_exists($categories{$k}->items{$i}->cover->src)) {
//                    $categories{$k}->items{$i}->cover->size = getimagesize(public_path(). $categories{$k}->items{$i}->cover->src)[0];
//                }
//                $categories{$k}->items{$i}->colourF = $this->features->getContentValues($project->id,2,'colour');
//                $categories{$k}->items{$i}->colourT = $this->features->getContentValues($project->id,3,'colour');
//            }
//        }


        return $this->template->view('modules/projects/portfolio',[
            'categories' => $categoriesAll,
            'items' => $items
        ]);
    }
}