<?php

namespace App\Http\Controllers\modules\Nav;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NavModel extends Model
{
    public function menuItems($menu_id)
    {
        return DB::table('content as c')->select('c.id', 'c.isfolder', 'ci.name' ,'ci.title','ci.alias')
            ->join('content_info as ci',function($join) {
                $join->on('ci.content_id','=','c.id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
        ->join('nav_menu_items as n',function($join) use ($menu_id) {
                $join->on('n.content_id','=','c.id')
                    ->where('n.nav_menu_id','=',$menu_id);
            })
            ->where('c.published','=',1)
            ->where('c.auto','=',0)
            ->orderBy('n.sort','asc')
            ->get();
    }

    public function getChildren($parent_id)
    {
        return DB::table('content as c')->select('c.id','c.isfolder','ci.name','ci.title','ci.alias')
            ->join('content_info as ci',function($join) {
                $join->on('ci.content_id','=','c.id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.parent_id','=',$parent_id)
            ->where('c.published','=',1)
            ->where('c.auto','=',0)
            ->orderBy('c.sort','asc')
            ->get();
    }

    public function getLanguages()
    {
        return DB::table('languages')->select('*')
            ->where('front','=',1)
            ->get();
    }
}
