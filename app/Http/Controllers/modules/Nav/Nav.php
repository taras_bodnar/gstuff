<?php

namespace App\Http\Controllers\modules\Nav;

use App\Http\Controllers\Core;
use App\Model\app\Images;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Nav extends Core
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new NavModel();
        $this->image = new Images();
    }

    public static function test($param,$param2,$param3)
    {
      return json_encode(array('var'=>$param));
    }

    public function top()
    {
        $items = $this->menu(1,2);

        foreach($items as $k=>$item) {
            if($item->isfolder) {
              if(isset($item->children) && !empty($item->children)) {
                  foreach ($item->children as $c=>$child) {
                      $cover = $this->image->cover($child->id,'source');
                      if(!empty($cover)) {
                          $items{$k}->children{$c}->image = $cover;
                      }
                  }
              }
            }
        }

        return  $this->template->view('modules/nav/top',
            array(
                'items'=>$items
            )
        );
    }


    public  function bottom()
    {
        return $this->template->view('modules/nav/bottom',
            array(
                'items'=>$this->menu(2,2)
            )
        );
    }

    public function map()
    {
        return $this->template->view('modules/nav/map',
            array(
                'items'=>$this->menu(2,2)
            )
        );
    }

    public function languages()
    {
        return $this->template->view('modules/nav/languages',
            array(
                'items'=>$this->m->getLanguages(),
                'current_languages'=>$this->languages_id
            )
        );
    }

    private function menu($menu_id, $level=0)
    {
        $res = array();

        $r = $this->m->menuItems($menu_id);

        foreach ($r as $row) {
            if($level > 0 ){
                $c = $this->getChildren($row->id, $level);
                if(!empty($c)){
                    $row->children = $c;
                }
            }

            $res[] = $row;
        }

        return $res;
    }

    private function getChildren($parent_id, $level)
    {
        if($level <= 0) return array();
        $items = array();
        $r = $this->m->getChildren($parent_id);
        foreach ($r as $row) {
            if($row->isfolder){
                $c = $this->getChildren($row->id, --$level);
                if(!empty($c)){
                    $row->children = $c;
                }
            }
            $items[] = $row;
        }
        return $items;
    }
}
