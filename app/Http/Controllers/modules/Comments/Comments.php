<?php

namespace App\Http\Controllers\modules\Comments;

use App\Http\Controllers\Core;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Comments extends Core
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new CommentsM();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = $this->page->id;
        $items = $this->m->get($id);

        return $this->template->view('modules/comments/index');
    }


}
