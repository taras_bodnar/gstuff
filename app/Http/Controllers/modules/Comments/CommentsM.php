<?php

namespace App\Http\Controllers\modules\Comments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommentsM extends Model
{
    public function get($id)
    {
        return DB::table('comments')
            ->where('content_id','=',$id)
            ->where('published','=',1)
            ->get();
    }
}
