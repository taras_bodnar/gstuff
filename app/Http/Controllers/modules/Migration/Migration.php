<?php

namespace App\Http\Controllers\modules\Migration;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class Migration extends Controller
{

    public function import()
    {
        $news = DB::table('news')->get();
        dd($news);
        foreach($news as $item) {
            $date = date_create();
            date_timestamp_set($date, $item->data_c);
            $createdon = date_format($date, 'Y-m-d H:i:s');
            $id = DB::table('content')->insertGetId([
                'type_id'=>8,
                'owner_id'=>1,
                'published'=>1,
                'templates_id'=>9,
                'createdon'=>$createdon,
                'auto'=>0,
            ]);

            DB::table('content_info')->insert([
               'content_id'=>$id,
                'languages_id'=>1,
                'name'=>$item->title,
                'alias'=>$this->translit($item->title).$id,
                'title'=>$item->title,
                'keywords'=>$item->title,
                'description'=>$item->opus,
                'content'=>$item->text_f,
            ]);

            DB::table('posts_categories')->insert([
                'cid'=>15,
                'pid'=>$id
            ]);
        }
    }

    private function  translit ( $string )
    {
        $string = mb_strtolower(trim($string),'utf8');
        $trans = array ( "а"     => "a",
            "б"     => "b",
            "в"     => "v",
            "г"     => "h",
            "д"     => "d",
            "е"     => "e",
            "є"     => "ye",
            "ж"     => "zh",
            "з"     => "z",
            "и"     => "y",
            "і"     => "i",
            "ї"     => "yi",
            "й"     => "y",
            "к"     => "k",
            "л"     => "l",
            "м"     => "m",
            "н"     => "n",
            "о"     => "o",
            "п"     => "p",
            "р"     => "r",
            "с"     => "s",
            "т"     => "t",
            "у"     => "u",
            "ф"     => "f",
            "х"     => "kh",
            "ц"     => "ts",
            "ч"     => "ch",
            "ш"     => "sh",
            "щ"     => "sch",
            "ю"     => "yu",
            "я"     => "ya",
            "ь"     => "",
            "«"     => "",
            "»"     => "",

            "А"     => "A",
            "Б"     => "B",
            "В"     => "V",
            "Г"     => "H",
            "Д"     => "D",
            "Е"     => "E",
            "Є"     => "Ye",
            "Ж"     => "Zh",
            "З"     => "Z",
            "И"     => "Y",
            "І"     => "I",
            "Ї"     => "Yi",
            "Й"     => "Y",
            "К"     => "K",
            "Л"     => "L",
            "М"     => "M",
            "Н"     => "N",
            "О"     => "O",
            "П"     => "P",
            "Р"     => "R",
            "С"     => "S",
            "Т"     => "T",
            "У"     => "U",
            "Ф"     => "F",
            "Х"     => "Kh",
            "Ц"     => "Ts",
            "Ч"     => "Ch",
            "Ш"     => "Sh",
            "Щ"     => "Sch",
            "Ю"     => "Yu",
            "Я"     => "Ya",
            "Ь"     => "",
            "ы"     => "y",
            "э"     => "е",
            "Ы"     => "Y",
            "ì"     => "i",
            "ś"     => "s",
            "š"     => "s",
            "č"     => "c",
            "ą"     => "a",
            "ę"     => "e",
            "ó"     => "o",
            "ł"     => "l",
            "ï"     => "i",

            "′"     => "",
            "`"     => "",
            "’"     => "",
            "'"     => "",
            "."     => "_",
            "("     => "_",
            ")"     => "_",
            ","     => "",
            ">"     => "_less_",
            "<"     => "_over_",
            '"'     => "",
            "_"     => "",
            " "     => "-",
            "№"     => "Numb",
            "/"     => "_",
            "%"     => "proc",
            "&"     => "_and_",
            "&amp;" => "_and_",
            "+"     => "_plus_",
            "?"     => "",
            "!"     => "" );

        return strtr ( $string, $trans );

    }

    public function importAlbum()
    {
        $albums = DB::table('category_ec')
            ->get();

        foreach($albums as $k=>$item){
            $albums{$k}->images = DB::table('gallery_ec')
                ->where('category','=',$item->id)
                ->get();
        }

        foreach($albums as $album){
            $id = DB::table('content')->insertGetId([
                'type_id'=>10,
                'owner_id'=>1,
                'published'=>1,
                'templates_id'=>13,
                'createdon'=>date("Y-m-d H:i:s"),
                'auto'=>0,
            ]);

            DB::table('content_info')->insert([
                'content_id'=>$id,
                'languages_id'=>1,
                'name'=>$album->title,
                'alias'=>$this->translit($album->title).$id,
                'title'=>$album->title,
                'keywords'=>$album->title
            ]);

            DB::table('posts_categories')->insert([
                'cid'=>351,
                'pid'=>$id
            ]);

            foreach($album->images as $image) {
                if(!file_exists('gallery/'.$image->file_path)) continue;
                    $destinationPath = 'uploads/content/'.$id;
                    File::makeDirectory($destinationPath,$mode = 0777, true, true);
                    chmod($destinationPath, 0777);
                    File::makeDirectory($destinationPath."/source",$mode = 0777, true, true);
                    $extension = pathinfo('gallery/'.$image->file_path,PATHINFO_EXTENSION);
                    $fileName = rand(11111,99999).time().$id . '.' . $extension;

//                    $upload_success = Image::make('gallery/'.$image->file_path)
//                        ->save($destinationPath."/source/".$fileName);
                if (!copy('gallery/'.$image->file_path, $destinationPath."/source/".$fileName)) continue;
                \App\Model\Admin\content\Images::insert($id,$fileName);
//                    if (!file_exists($destinationPath."/thumb")) {
//                        File::makeDirectory($destinationPath."/thumb", $mode = 0777, true, true);
//                    }

//                    $upload_success = Image::make($destinationPath."/source/".$fileName)
//                        ->resize(null, 300)
//                        ->save($destinationPath."/thumb/".$fileName);
//                    if($upload_success) {
//                       \App\Model\Admin\content\Images::insert($id,$fileName);
//                    }
            }

        }
    }
}
