<?php

namespace App\Http\Controllers\modules\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BlogM extends Model
{
    private $type_id = 2;
    private $c=10;
    public function getPosts()
    {
        return DB::table('content as c')->select('c.*','ci.name','ci.title','ci.description','ci.content')
            ->join('content_info as ci',function($join) {
                $join->on('c.id','=','ci.content_id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.published','=',1)
            ->where('c.auto','=',0)
            ->where('c.type_id','=',$this->type_id)
            ->orderBy('c.createdon','desc')
            ->paginate($this->c);
    }

    public function getTags($content_id)
    {
        return DB::table('tags as t')->select('t.id','t.name')
            ->join('tags_content as tc',function($join) use ($content_id){
                $join->on('tc.tags_id','=','t.id')
                    ->where('t.content_id','=',$content_id);
            })
            ->where('t.languages_id','=',$this->languages_id);
    }

    public function setPerPages($c)
    {
        $this->c = $c;
    }
}
