<?php

namespace App\Http\Controllers\modules\Blog;

use App\Http\Controllers\Core;
use App\Model\app\Images;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Blog extends Core
{

    public function __construct()
    {
        parent::__construct();
        $this->m = new BlogM();
        $this->images = new Images();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = $this->page->id;
        $image = $this->images->cover($id,'blog_preview');

        return $this->template->view('modules/blog/index',
                        ['image'=>$image]
        );
    }

    public function getAll()
    {
        $this->m->setPerPages(10);
        $items = $this->m->getPosts();

        foreach($items as $k=>$item) {
            $items{$k}->img = $this->images->cover($item->id,'blog_preview');
        }

        return $this->template->view('modules/blog/list',[
            'items'=>$items
        ]);
    }

    public function postInMain()
    {
        $this->m->setPerPages(6);
        $items = $this->m->getPosts();

        foreach($items as $k=>$item) {
           $items{$k}->img = $this->images->cover($item->id,'blog_preview');
        }

        return $this->template->view('modules/blog/onmain',[
            'items'=>$items
        ]);
    }

}
