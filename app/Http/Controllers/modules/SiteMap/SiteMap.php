<?php

namespace App\Http\Controllers\modules\SiteMap;

use App\Http\Controllers\Core;
use App\Model\Admin\Languages;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SiteMap extends Core
{

    public function __construct()
    {
        parent::__construct();
        $this->m = new SiteMapM();
        $this->lang = new Languages();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ct='')
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? "https://" : "http://";
        if(!$ct){
            $items = array();
//            $items[] = array(
//                'loc'     =>  $protocol.\Illuminate\Support\Facades\Request::server('HTTP_HOST')."/" ."sitemap",
//                'lastmod' => date('c')
//            );
            foreach($this->lang->geAll(1) as $lang){
                $l = $lang->front_default == 1 ? '' : "?l={$lang->code}";
                foreach ($this->m->getType() as $ct) {
                    $items[] = array(
                        'loc'     =>  $protocol.\Illuminate\Support\Facades\Request::server('HTTP_HOST') ."/sitemap/{$ct->type}{$l}",
                        'lastmod' => date('c')
                    );
                }
            }
            $x=new \XMLWriter();
            $x->openMemory();
            $x->startDocument('1.0', 'UTF-8');
//            $x->writePi('xml-stylesheet', "type=\"text/xsl\" href=\"{$appurl}themes/default/views/modules/sitemap/sitemap-index.xsl\"");

            $x->startElement('sitemapindex');
            $x->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            $x->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
            $x->writeAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd');
            foreach ($items as $item) {
                $x->startElement('sitemap');
                $x->writeElement('loc',     $item['loc']);
                $x->writeElement('lastmod', $item['lastmod']);
                $x->endElement(); //sitemap
            }

            $x->endElement(); //sitemapindex
            $out = $x->outputMemory();
        } else {
            $items = $this->m->getItemsByType($ct);

            $x=new \XMLWriter();
            $x->openMemory();
            $x->setIndent(1);
            $x->startDocument('1.0', 'UTF-8');
//            $x->writePi('xml-stylesheet', "type=\"text/xsl\" href=\"{$appurl}themes/default/views/modules/sitemap/sitemap.xsl\"");

            $x->startElement('urlset');
            $x->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
            $x->writeAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');

            foreach ($items as $item) {
                $x->startElement('url');
                $x->writeElement('loc',    $protocol.\Illuminate\Support\Facades\Request::server('HTTP_HOST')."/" . $item->loc);
                $x->writeElement('lastmod', $item->lastmod);
                $x->writeElement('changefreq', $item->changefreq);
                $x->writeElement('priority', str_replace(',','.',$item->priority));
                $x->endElement(); //url
            }

            $x->endElement(); //urlset
            $out = $x->outputMemory();
        }

        header('Content-Type: application/xml; charset=utf-8');
        echo $out; die();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
