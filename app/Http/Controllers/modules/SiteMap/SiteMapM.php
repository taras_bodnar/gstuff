<?php

namespace App\Http\Controllers\modules\SiteMap;

use App\Model\app\Images;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SiteMapM extends Model
{
    protected $primaryKey = 'ct.id';
    public function getType()
    {
        return DB::table('content_type')->select('*')
            ->whereNotIn('id', [3,4,5])
            ->get();
    }

    public function getItemsByType($type, $priority = 0.9, $changefreq = 'Weekly')
    {
        $front_default = DB::table('languages')->select('id')
            ->where('front_default','=',1)
            ->first();

        $lid = DB::table('languages')->select('id')
            ->where('code','=',Input::get('l'))
            ->first();
        $lid = empty($lid)?$front_default->id:$lid->id;

        $result = DB::table('content_type as ct')->select('c.id',
            DB::Raw("IF(l.id=$front_default->id, ci.alias, CONCAT(l.code, '/', ci.alias) ) as loc"),
            DB::Raw('0 as images'),
            DB::Raw("IF(c.editedon = '0000-00-00 00:00', if(c.createdon='0000-00-00 00:00','".date('c')."',DATE_FORMAT(c.createdon, '%Y-%m-%dT%TZ')), DATE_FORMAT(c.editedon, '%Y-%m-%dT%TZ')) as lastmod"),
            DB::Raw("{$priority} as priority"),
            DB::Raw("'$changefreq' as changefreq")
            )
            ->join('content as c',function($join){
                $join->on('c.type_id','=','ct.id')
                    ->where('c.published','=',1)
                    ->where('c.auto','=',0);
            })
//            ->join('languages as ld','ld.front_default','=',$this->primaryKey)
//            ->join('languages as l ','ld.id','=',$this->languages_id)
            ->join('content_info as ci ',function($join) use ($lid){
                $join->on('ci.content_id','=','c.id')
                ->where('ci.languages_id','=',$lid);
            })
            ->join('languages as l ','l.id','=','ci.languages_id')
            ->where('ct.type','=',$type)
            ->orderBy('lastmod','desc')
            ->get();
        $images = new Images();
        foreach ($result as $i => $row) {
            $im = $images->get($row->id, 'source');
            $result{$i}->image = $im;
        }

        return $result;
    }
}
