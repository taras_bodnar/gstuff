<?php

namespace App\Http\Controllers\modules\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopM extends Model
{
    public function getProducts()
    {
        $query = DB::table('content as c')->select('c.id','pi.price','pi.old_price','pi.show_price','pi.sale','ci.name','ci.title')
            ->join('content_info as ci',function($join) {
              $join->on('c.id','=','ci.content_id')
                  ->where('ci.languages_id','=',$this->languages_id);
            })
            ->join('products_options as pi',function($join) {
              $join->on('pi.products_id','=','c.id');
            })
            ->join('products_categories as pc',function($join) {
              $join->on('pc.pid','=','c.id');
            })
        ->where('c.published','=',1)
        ->where('c.auto','=',0);

        if (!empty($this->join)) {
            $this->getJoin($query);
        }


        if ($this->where) {
            $this->getWhere($query);
        }

        if($this->orderBy) {
            $this->getOrderBy($query);
        } else {
            $query->orderBy('ci.id', 'desc');
        }



        return $query->paginate(15);
    }

    public function filterGetFeatures($content_id)
    {
        $features = DB::table('features as f')->select('f.id','f.type','fi.name')
        ->join('content_features as cf',function($join) use($content_id) {
            $join->on('cf.features_id','=','f.id')
                ->where('cf.content_id','=',$content_id);
        })
        ->join('features_info as fi',function($join) {
            $join->on('fi.features_id','=','f.id')
                ->where('fi.languages_id','=',$this->languages_id);
        })
        ->where('f.show_filter','=',1)
        ->where('f.auto','=',0)
        ->where('f.published','=',1)
        ->groupBy('f.id')
        ->get();

        foreach ( $features as $k => $item ) {
//            $join = '';
//            if ( !empty($pids) && $pids!=0) {
//                $join = "join features_content_values fcv on
//                fcv.features_id='{$item['id']}'
//                and fcv.features_values_id=v.id
//                and fcv.content_id in (" . implode(',', $pids) . ")
//                ";
//            }
            switch ($item->type) {
                case 'select':
                case 'checkboxgroup':
                case 'radiogroup':
                case 'sm':
                    $features{$k}->values = DB::table('features_values as v')->select('v.id','i.value')
                        ->join('features_values_info as i',function($join) {
                            $join->on('i.values_id','=','v.id')
                                ->where('i.languages_id','=',$this->languages_id);
                        })
                        ->where('v.features_id','=',$item->id)
                        ->orderBy('v.sort','asc')
                        ->get();
                    break;
                case 'number':
                case 'text':
                    $features{$k}->values = DB::table('features_content_values')->select('value')
                        ->where('features_id','=',$item->id)
                        ->get();
                    break;
            }
        }

        return $features;
    }

    public function getPrice($id)
    {
        return DB::table('products_options as po')->select(DB::raw('min(price) as min'),DB::raw('max(price) as max'))
            ->join('products_categories as pc',function($join) {
                $join->on('pc.pid','=','po.products_id');
            })
            ->where('pc.cid','=',$id)
            ->first();
    }

    public function getSpecialProducts($type,$cid)
    {
        $query = DB::table('content as c')->select('c.id','ci.name','ci.title','po.price','po.old_price','po.show_price')
            ->join('content_info as ci',function($join){
                $join->on('c.id','=','ci.content_id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })->join('products_options as po',function($join){
                $join->on('c.id','=','po.products_id');
            });

        if($cid>0) {
            $query->join('products_categories as pc',function($join) use($cid) {
                $join->on('pc.pid','=','c.id')
                    ->where('pc.cid','=',$cid);
            });
        }

        $query->where('sale','=',1)
            ;

        return $query->get();
    }
}
