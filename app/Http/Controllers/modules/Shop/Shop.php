<?php

namespace App\Http\Controllers\modules\Shop;

use App\Http\Controllers\Core;
use App\Http\Controllers\modules\Pages\PagesM;
use App\Model\app\Features;
use App\Model\app\Images;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Shop extends Core
{
    public function __construct()
    {
        parent::__construct();
        $this->m = new ShopM();
        $this->pages = new PagesM();
        $this->images = new Images();
        $filter = Input::get('filter');
        $minPrice = Input::get('minPrice');
        $maxPrice = Input::get('maxPrice');
        $this->features = new Features();


        if(isset($filter)) {
            if(isset($filter['minp']) && isset($filter['maxp']) && $filter['maxp'] > 0){
                $minp = (int)$filter['minp'];
                $maxp = (int)$filter['maxp'];

                $this->m->setWhere('pi.price',array($minp,$maxp),'=','whereBetween');

                $this->sel_parts[] = "{$minp} - {$maxp} грн.";
            }
            if(isset($filter['f'])) {
                foreach ($filter['f'] as $features_id => $a) {
                    $features = $this->features->getItem($features_id, true);
//                    $this->dump($features);

                    foreach ($features->values as $k=>$v) {
                        if(is_array($a) && in_array($v->id, $a)){
                            $this->sel_parts[] = $v->value;
//                            unset($features['values'][$k]);
                        } elseif($a == $v->id){
                            $this->sel_parts[] = $v->value;
//                            $this->sel_parts[] = $features['name'] . ': ' . $v['value'];
                        }
                    }
//                    $selected_features[] = $features;

                    $type = $features->type;

//                    echo $type, '|';
                    if($type == 'sm' || $type == 'select' || $type='checkboxgroup'){

                        if(is_array($a)){
//                        dd($a);
                            $this->m->setKeys("fcv{$features_id}.features_values_id",$a,'=','whereIn');
//                        $in = "fcv{$features_id}.features_values_id in (". implode(',', $a) .")";
                        } else {
                            $a = (int)$a;
                            $this->m->setKeys("fcv{$features_id}.features_values_id","$a",'=');
                        }
//                        $this->dump($a); continue;
                        if(empty($a)) continue;
                        $this->m->setKeys("fcv{$features_id}.features_id","{$features_id}");
                        $this->m->setKeys("fcv{$features_id}.content_id","pi.products_id",'=','on');
                        $this->m->setJoin("features_content_values as fcv{$features_id}");

                    } elseif($type == 'text'){

                        foreach ($a as $k=>$v) {
                            $a[$k] = "'$v'";
                        }
                        $in = implode(',',$a);

                        $this->model->setJoin("
                        join features_content_values fcv{$features_id} on
                        fcv{$features_id}.value in({$in})
                        and fcv{$features_id}.features_id='{$features_id}'
                        and fcv{$features_id}.content_id=po.products_id
                        and fcv{$features_id}.languages_id = {$this->languages_id}
                    ");
                    } elseif($type == 'number'){
                        foreach ($a as $k=>$v) {
                            $a[$k] = "'$v'";
                        }
                        $in = implode(',',$a);
                        $this->model->setJoin("
                        join features_content_values fcv{$features_id} on
                        fcv{$features_id}.value in({$in})
                        and fcv{$features_id}.features_id='{$features_id}'
                        and fcv{$features_id}.content_id=po.products_id
                        and fcv{$features_id}.languages_id = 0
                    ");
                    }
                }
            }
        }

        $order = Input::get('order');

        if(isset($order)) {
            switch($order) {
                case 1 :
                    $this->m->setOrderBy('pi.price','asc');
                    break;
                case 2 :
                    $this->m->setOrderBy('pi.price','desc');
                    break;
                default :
                    $this->m->setOrderBy('c.sort','asc');
                    break;
            }
        }
    }


    public function getCategories($photo=false)
    {
        $items = $this->pages->getByType(3);
        foreach($items as $k=>$item) {
            $items{$k}->img = $this->images->cover($item->id,'category');
        }
        if($photo) {
            $view = $this->template->view('modules/shop/widget/catPhoto',
                [
                  'items'=>$items
                ]);
        } else {
            $view = $this->template->view('modules/shop/widget/catList',
                [
                    'items'=>$items
                ]);
        }

        return $view;
    }

    public function products()
    {
        $id = $this->page->id;

        $this->m->setWhere('pc.cid','=',$id);

        $items = $this->m->getProducts();

        foreach ($items as $k=>$item) {         
            $items{$k}->img = $this->images->cover($item->id,'product_preview');
            $items{$k}->bigimg = $this->images->cover($item->id,'product');
        }
//        dd($items);

        return $this->template->view('modules/shop/list',[
           'items'=>$items
        ]);
    }

    public function filter()
    {
        $id = $this->page->id;

        $filter = $this->m->filterGetFeatures($id);
//        dd($filter);

        $minMaxPrice = $this->m->getPrice($id);
//        dd($minMaxPrice);

        $categories = $this->pages->getByType(3);
        return $this->template->view('modules/shop/filter',[
            'filter'=>$filter,
            'minMaxPrice'=>$minMaxPrice,
            'categories'=>$categories,
        ]);
    }

    public function getSpecialProducts($type,$cid=0)
    {

       $items = $this->m->getSpecialProducts($type,$cid);

            foreach ($items as $k=>$item) {
                if(empty($item->id)) continue;
                $items{$k}->img = $this->images->cover($item->id,'product_preview');
            }
      
        return $this->template->view('modules/shop/'.$type,[
            'items'=>$items,
            'catName'=>$this->pages->getInfo($cid)
             
        ]);
    }
}
