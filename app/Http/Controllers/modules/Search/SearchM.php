<?php

namespace App\Http\Controllers\modules\Search;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SearchM extends Model
{
    public function get()
    {
       $query = DB::table('content as c')->select('c.id','ci.title','ci.name','ci.description')
           ->join('content_info as ci',function($join){
               $join->on('ci.content_id','=','c.id')
                   ->where('ci.languages_id','=',$this->languages_id);
           })
           ->where('c.published','=',1)
           ->where('c.auto','=',0);
        if(!empty($this->where)) {
            $this->getWhere($query);
        }
        return $query->paginate(10);
    }

    public function getAll()
    {
        $query = DB::table('content as c')->select('c.id','ci.title','ci.alias','ci.name','ci.description')
            ->join('content_info as ci',function($join){
                $join->on('ci.content_id','=','c.id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.published','=',1)
            ->where('c.auto','=',0);
        if(!empty($this->where)) {
            $this->getWhere($query);
        }
        return $query->get();
    }

    public function total()
    {
        $query = DB::table('content as c')->select(DB::raw('count(c.id) as t'))
            ->join('content_info as ci',function($join){
                $join->on('ci.content_id','=','c.id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->where('c.published','=',1)
            ->where('c.auto','=',0);
        if(!empty($this->where)) {
            $this->getWhere($query);
        }
        return $query->first();
    }
}
