<?php

namespace App\Http\Controllers\modules\Search;

use App\Http\Controllers\Core;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Search extends Core
{
    public function __construct()
    {
      parent::__construct();
        $this->m = new SearchM();
        $this->content = new \App\Model\Core();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\Illuminate\Support\Facades\Request::isMethod('post')) {
            if (!isset($_GET['s'])) return '';

            $q = trim($_GET['s']);
            $t = $this->translation;
            $e = array();
            $m = array();
            $results = [];
            if (strlen($q) < 3) {
                $e[] = $t->search_min_word_len;
            }
            if (empty($e)) {
                $uq = explode(' ', $q);

                for ($i = 0; $i < count($uq); $i++) {
                    if (empty($uq[$i])) continue;
                    $this->m->setWhere('ci.title', "%" . $uq[$i] . "%", 'like');
                    $this->m->setWhere('ci.name', "%" . $uq[$i] . "%", 'like', 'orWhere');
                    $this->m->setWhere('ci.content', "%" . $uq[$i] . "%", 'like', 'orWhere');
                }

                $total = $this->m->total();

                if ($total->t == 0) {
                    $e[] = $t->search_not_found;
                } else {

                    $m = str_replace('{t}', $total->t, $t->search_found);

                    $results = $this->m->get();

                }
            }
            if (!empty($results)) {
                $results->appends(['&s' => $q]);
            }
            return $this->template->view('modules/search/index',
                [
                    'results' => $results,
                    'm' => $m,
                    'e' => $e,
                    'q' => $q,
                ]
            );
        } else {

            $q = trim(Input::get('sq'));
            $t = $this->translation;
            $e = array();
            $m = array();
            $results = [];
            if (strlen($q) < 3) {
                $e[] = 'мінімальна к-сть символів рівна 3';
            }
            if (empty($e)) {
                $uq = explode(' ', $q);

                for ($i = 0; $i < count($uq); $i++) {
                    if (empty($uq[$i])) continue;
                    $this->m->setWhere('ci.title', "%" . $uq[$i] . "%", 'like');
                    $this->m->setWhere('ci.name', "%" . $uq[$i] . "%", 'like', 'orWhere');
                    $this->m->setWhere('ci.content', "%" . $uq[$i] . "%", 'like', 'orWhere');
                }

                $total = $this->m->total();

                if ($total->t == 0) {
                    $e[] = 'Пошук не дав результатів';//$t->search_not_found;
                } else {

//                    $m = str_replace('{t}', $total->t, $t->search_found);
                    $m = str_replace('{t}', $total->t, 'За вашим запитом знайдено {t} результатів');

                    $results = $this->m->getAll();

                    foreach ($results as $k=>$item) {
                        $results{$k}->url = $this->content->getLangAliasById($item->id,$this->languages_id);
                    }
                   
                }
            }
            return json_encode(
                [
                    'results' => $results,
                    'm' => $m,
                    'e' => $e,
                    'q' => $q,
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
