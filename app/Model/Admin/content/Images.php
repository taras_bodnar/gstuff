<?php

namespace App\Model\Admin\content;

use App\Http\Controllers\Admin\Translator;
use App\Model\Admin\Languages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Images extends Model
{
    public static function getSize($id)
    {
        return DB::table('content as c')->select('is.*')
            ->join('content_type_images as i',function($join){
                $join->on('c.type_id','=','i.content_type_id');
            })
            ->join('images_sizes as is',function($join){
                $join->on('i.images_sizes_id','=','is.id');
            })->where('c.id','=',$id)
            ->get();
    }

    public static function insert($id,$name,$info_name='',$type='image')
    {
        $imgId = DB::table('content_images')->insertGetId(
            [
                'content_id'=>$id,
                'name'=>$name,
                'type'=>$type
            ]
        );

        $languages = Languages::all();
        $translator = new Translator();
        if($info_name && !empty($info_name)) {
            foreach ($languages as $lang) {
                DB::table('content_images_info')->insert(
                    [
                        'images_id'=>$imgId,
                        'languages_id'=>$lang->id,
                        'alt'=>$lang->front_default!=1?$translator->translate($info_name,'uk',$lang->code):$info_name
                    ]
                );
            }
        }

        return $imgId;
    }

    public static function get($id,$type='image')
    {
        return DB::table('content_images')->select("*")
            ->where('content_id','=',$id)
            ->where('type','=',$type)
            ->orderBy('sort','asc')
            ->get();
    }

    public function imageData($id, $key='*')
    {
        return $this->data('content_images', $id, $key);
    }

    public function getSizes($content_id)
    {
        return DB::table('images_sizes as s')->select('s.*','s.id as value')
            ->join('content as c',function($join) use($content_id){
                $join->where('c.id','=',$content_id);
            })
            ->join('content_type_images as t',function($join){
                $join->on('t.images_sizes_id','=','s.id')
                ->where('c.type_id','=', 't.content_type_id');
            })->get();
    }

    public function del($id)
    {
        return DB::table('content_images')->where('id','=',$id)
            ->delete();
    }

    public function info($id,$info)
    {
        $s = '';
        foreach ($info as $l=>$name) {
//            echo $k."-"."-".$id;
            $s = DB::table('content_images_info')
                ->where('images_id','=',$id)
                ->where('languages_id','=',$l)
                ->delete();
//            echo $s;
//            if($s) {
                $s = DB::table('content_images_info')
                    ->insertGetId(
                        [
                            'images_id'=>$id,
                            'languages_id'=>$l,
                            'alt'=>$name['name']
                        ]
                    );
//            }
       }

        return $s;

    }

    public function getInfo($id)
    {
        return DB::table('content_images_info')->select('*')
            ->where('images_id','=',$id)
            ->get();
    }

    public function getTotalPages($id)
    {
        return DB::table('content as c')->select(DB::raw('count(c.id) as t'))
            ->join('content_type_images as ci',function($join) use ($id){
               $join->on('ci.content_type_id','=','c.type_id')
                   ->where('ci.images_sizes_id','=',$id);
            })->pluck('t');
    }

    public function getSizeData($id)
    {
        return DB::table('images_sizes')->select('name','width','height')
            ->where('id','=',$id)->first();
    }

    public function getPageImages($sizes_id, $start=0)
    {
        $res = DB::table('content as c')->select('c.id')
            ->join('content_type_images as ci',function($join) use ($sizes_id){
                $join->on('ci.content_type_id','=','c.type_id')
                    ->where('ci.images_sizes_id','=',$sizes_id);
            })
            ->orderBy('c.id','asc')
            ->skip($start)
            ->take(1)
            ->first();

        if(empty($res)) return $res;

        $res->images = DB::table('content_images')->select('name as src')
            ->where('content_id','=',$res->id)
            ->get();

        return $res;
    }

    public function getAlt($id)
    {
        return $this->fulInfo('content_images_info', 'images_id', $id);
    }

    public function updateInfo($id,$data)
    {
        $r=0;
        foreach ($data as $languages_id =>$field) {
            $aid = DB::table('content_images_info')->select('id')
                ->where('images_id',$id)
                ->where('languages_id',$languages_id)
                ->first();
            if(empty($aid->id)){
                $r += DB::table('content_images_info')
                    ->insertGetId(
                        [
                            'images_id'=>$id,
                            'languages_id'=>$languages_id,
                            'alt'=>$data[$languages_id]['alt']
                        ]
                    );
            } else {
                $r +=  DB::table('content_images_info')->where('id','=',$aid->id)
                    ->update([
                        'alt'=>$data[$languages_id]['alt']
                    ]);
            }
        }

        return $r;
    }
}
