<?php

namespace App\Model\Admin\content;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Type extends Model
{
    public  function createType($data)
    {
      return DB::table('content_type')
        ->insertGetId($data);
    }

    public function edit($id,$data)
    {
        return DB::table('content_type')->where('id','=',$id)
            ->update($data);

    }

    public static function get($type_id='')
    {
        return DB::table('content_type as ct')->select('ct.*',DB::raw('if(ctm.id>0,"selected","") AS selected'))
            ->leftJoin('content_templates as ctm',function($join) use ($type_id){
                $join->on('ct.id','=','ctm.type_id')
                    ->where('ctm.type_id','=',$type_id);
            })
            ->get();
    }

    public static function getTypeById($type_id)
    {
        return DB::table('content_type')->select('type')
            ->where('id','=',$type_id)->first();
    }

    public function del($id)
    {
        return DB::table('content_type')->where('id','=',$id)
            ->delete();
    }
}
