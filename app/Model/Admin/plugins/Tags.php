<?php

namespace App\Model\Admin\plugins;

use App\Http\Controllers\Admin\Content;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tags extends Model
{
    public function get($content_id,$languages_id)
    {
        return DB::table('tags_content as tc')->select('t.name')
            ->join('tags as t',function ($join) use ($languages_id){
                $join->on('t.id','=','tc.tags_id')
                    ->where('t.languages_id','=',$languages_id);
            })
            ->where('tc.content_id',$content_id)->get();
    }

    public function set($content_id, $languages_id, array $tags)
    {

        foreach ($tags as $k=>$name) {
            $name= trim($name);
            if(empty($name)) continue;

            $tags_id = $this->getId($languages_id, $name);

            if(!isset($tags_id) && empty($tags_id)){
                $alias = Content::translit($name);

                $tags_id = $this->createTag(array('name'=> $name, 'languages_id'=>$languages_id, 'alias' => $alias));
            } else {
                $tags_id = $tags_id->id;
            }

            if(isset($tags_id) && !empty($tags_id )) {
                $cid = DB::table('tags_content')->select('id')
                    ->where('content_id','=',$content_id)
                    ->where('tags_id','=',$tags_id)
                    ->first();

                if(empty($cid->id)){
                    DB::table('tags_content')->insertGetId(array('tags_id'=>$tags_id,'content_id'=>$content_id));
                }
            }
        }
    }

    public function createTag($data, $info=array())
    {
        return DB::table('tags')->insertGetId($data);
    }

    private function getId($languages_id, $name)
    {
        return DB::table('tags')->select('id')
            ->where('alias','=',Content::translit($name))
            ->where('languages_id',$languages_id)->first();
    }

    public function remove($content_id, $languages_id, $name)
    {
        $name = Content::translit($name);
        $tags_id = $this->getId($languages_id, $name);
        if(empty($tags_id)) return 0;

        $t =$this->total($tags_id->id);

        if($t->t>1){
            DB::table('tags_content')->where('tags_id','=',$tags_id->id)
                ->where('content_id','=',$content_id)
                ->delete();
        } else{
            $this->deleteTag($tags_id->id);
        }

        return 1;
    }

    private function total($tags_id)
    {
        return DB::table('tags_content')->select(DB::raw('count(id) as t'))
            ->where('tags_id','=',$tags_id)->first();
    }

    public function deleteTag($id)
    {
        return DB::table('tags')->where('id','=',$id)->delete();
    }

    public function deleteTagByContentId($id)
    {
        return DB::table('tags_content')->where('content_id','=',$id)->delete();
    }
}
