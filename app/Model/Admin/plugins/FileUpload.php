<?php

namespace App\Model\Admin\plugins;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FileUpload extends Model
{
    public static function get($id)
    {
        return DB::table('content_images as c')->select('c.*','ci.alt')
            ->join('content_images_info as ci',function($join){
              $join->on('c.id','=','ci.images_id')
                  ->where('ci.languages_id','=',self::$language);
            })
            ->where('content_id','=',$id)
            ->where('type','=','file')
            ->get();
    }
}
