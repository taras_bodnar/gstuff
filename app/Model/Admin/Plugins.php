<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plugins extends Model
{
    public function isInstalled($controller)
    {
//        $controller = addslashes($controller);
//        die($controller);
        return DB::table('plugins')->select('id')
            ->where('controller','=',"$controller")
            ->first();
    }

    public function getPosition()
    {
        return $this->getPossibleRoles('plugins','position');
    }

    public function getStructure($plugins_id=0)
    {
        return DB::table('components as c')->select('c.id','ci.name',
            DB::raw('if(ps.id>0,"selected","") as selected')
        )
            ->join('components_info as ci',function($join) {
                    $join->on('c.id','=','ci.components_id')
                        ->where('ci.languages_id','=',$this->language_id);
            })
            ->leftJoin('plugins_structure as ps',function($join) use ($plugins_id) {
                $join->on('ps.components_id','=','c.id')
                    ->where('ps.plugins_id','=',$plugins_id);
            })
            ->where('c.id','<>',1)
            ->where('c.isfolder','<>',1)
            ->where('c.published','=',1)
            ->get();

    }

    public function install($data, $structure)
    {
        DB::beginTransaction(); $ps = 0;
        $id = DB::table('plugins')->insertGetId($data);
        if($id > 0) {
            foreach ($structure as $k=>$components_id) {
                $ps += DB::table('plugins_structure')
                ->insert([
                    'plugins_id' => $id,
                    'components_id' => $components_id
                ]);
            }
        }
        if($id > 0 && $ps > 0){
            DB::commit();
        } else {
            DB::rollBack();
            $id=0;
        }

        return $id ;
    }

    public function getData($id, $key='*')
    {
        return DB::table('plugins')->select($key)
            ->where('id','=',$id)
            ->first();
    }

    public function updateData($id, $data, $structure)
    {
        $s = DB::table('plugins')->where('id','=',$id)
        ->update($data);
        $s2=0;
        if($id > 0) {
            DB::table('plugins_structure')->where('plugins_id','=',$id)
            ->delete();
            foreach ($structure as $k=>$components_id) {
                $s2 = DB::table('plugins_structure')
                ->insert([
                    'plugins_id' => $id,
                    'components_id' => $components_id
                ]);
            }
        }

        return $s > 0 || $s2 > 0 ;
    }

    public function del($id)
    {
        return DB::table('plugins')->where('id','=',$id)
            ->delete();
    }

    public function getStructurePlugins($controller)
    {
        return DB::table('components as c')->select('p.controller','p.position')
            ->join('plugins_structure as ps',function($join) {
                $join->on('ps.components_id','=','c.id');
            })
            ->join('plugins as p',function($join) {
                $join->on('p.id','=','ps.plugins_id');
            })
            ->where('c.controller','=',"$controller")
            ->get();
    }
}
