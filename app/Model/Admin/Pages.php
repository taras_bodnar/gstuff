<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pages extends Model
{
    public function getPages()
    {
        return DB::table('pages')->get();
    }

    public function getPageInfo($alias)
    {
        return Db::table('pages')->where('alias',$alias)->first();
    }
}
