<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Size extends Model
{
    public function getSelectedType($size_id)
    {
        return DB::table('content_type_images')->select('content_type_id as id')
            ->where('images_sizes_id','=',$size_id)
            ->get();
    }

    public function getTypes()
    {
        return DB::table('content_type')->select('id',DB::raw('CONCAT(name , " (" ,type, ")") as name'))
            ->orderBy('name','asc')
            ->get();
    }

    public function insert($data)
    {
        return DB::table('images_sizes')
            ->insertGetId($data);
    }

    public function clearSelectedType($images_sizes_id)
    {
        return DB::table('content_type_images')->where('images_sizes_id','=',$images_sizes_id)
            ->delete();
    }

    public function addSelectedType($id,$type_id)
    {
        return DB::table('content_type_images')
            ->insert([
                'content_type_id' => $type_id,
                'images_sizes_id' =>$id
            ]);
    }

    public function edit($id, $data, $info=array()){
        return DB::table('images_sizes')->where('id','=',$id)
            ->update($data);
    }

    public function del($id)
    {
        return DB::table('images_sizes')->where('id','=',$id)
            ->delete();
    }
}
