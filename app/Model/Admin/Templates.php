<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Templates extends Model
{
    public function insert($data)
    {
        return DB::table('content_templates')->insertGetId($data);
    }

    public function edit($id,$data)
    {
        return DB::table('content_templates')->where('id','=',$id)
                        ->update($data);
    }

    public function del($id)
    {
        return DB::table('content_templates')->where('id','=',$id)
            ->delete();
    }
}
