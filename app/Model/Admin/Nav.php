<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Nav extends Model
{
    public function getPages()
    {
        return DB::table('pages')->get();
    }

    public function getPageInfo($alias)
    {
        return Db::table('pages')->where('alias',$alias)->first();
    }

    public static function getNav($parent_id)
    {
        return DB::table('components as c')->select('*')
            ->join('components_info as ci',function($join){
               $join->on('ci.components_id','=','c.id')
                   ->where('ci.languages_id','=',self::$language);
            })
            ->where('parent_id','=',$parent_id)
            ->where('published','=',1)
            ->where('c.rang','<=',Session::get('admin.user')->rang)
            ->orderBy('sort', 'asc')
            ->get();
    }

    public function getItems($nav_menu_id = 0)
    {
       return  DB::table('content as c')->select('c.id',
           DB::raw('CONCAT("#", c.id ," ", i.name) as name'),
           DB::raw('IF(m.id > 0 , "selected" , "") as selected')
           )
            ->leftjoin("nav_menu_items as m",function($join) use($nav_menu_id) {
               $join->on('m.content_id','=','c.id')
                   ->where('m.nav_menu_id','=',$nav_menu_id);
            })
            ->join("content_type as t",function($join){
                $join->on('c.type_id','=','t.id');
            })
            ->join("content_info as i",function($join){
                $join->on('i.content_id','=','c.id')
                ->where('i.languages_id','=',1);
            })->where('c.published','=',1)
            ->where('c.auto','=',0)
            ->whereIn('t.type',array('page','category'))
           ->orderBy('m.sort', 'asc')
            ->get();
    }

    public function insert($data)
    {
        return DB::table('nav_menu')->insertGetId($data);
    }

    public function addItem($id, $content_id, $sort)
    {
        return DB::table('nav_menu_items')->insert(
            array(
                'nav_menu_id'=>$id,
                'content_id'=>$content_id,
                'sort'=>$sort
            )
        );
    }

    public function edit($id,$data)
    {
        return DB::table('nav_menu')->where('id','=',$id)
            ->update($data);
    }

    public function deleteItems($id)
    {
        return DB::table('nav_menu_items')->where('nav_menu_id','=',$id)
            ->delete();
    }

    public function del($id)
    {
        return DB::table('nav_menu')->where('id','=',$id)
            ->delete();
    }

    public function sort($nav_id,$content_id, $i)
    {
        return DB::table('nav_menu_items')
            ->where('nav_menu_id','=',$nav_id)
            ->where('content_id','=',$content_id)
            ->update([
                'sort'=>$i
            ]);
    }
}
