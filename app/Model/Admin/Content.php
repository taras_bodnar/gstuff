<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class Content extends Model
{
    public function getTypeId($type)
    {
        return DB::table('content_type')->select('id')
            ->where('type', '=', $type)->first();
    }

    public function defaultTemplatesId($type = 'page')
    {
        return DB::table('content_templates as c')->select('c.id')
            ->join('content_type as ct', function ($join) use ($type) {
                $join->on('ct.id', '=', 'c.type_id')
                    ->where('ct.type', '=', $type);
            })
            ->where('c.main', '=', 1)->first();
    }

    public function autoCreate($parent_id, $type_id, $owner_id, $templates_id)
    {
        DB::table('content')->where('auto', '=', 1)->where('owner_id', '=', $owner_id)->delete();
        return DB::table('content')
            ->insertGetId([
                'parent_id' => (int)$parent_id,
                'type_id' => $type_id,
                'owner_id' => $owner_id,
                'templates_id' => $templates_id,
                'published' => 1,
                'createdon' => date('Y-m-d H:i:s')
            ]);
    }

    public static function info($id)
    {
//        $columns = DB::select("SHOW COLUMNS FROM content_info");
        $columns = Schema::getColumnListing('content_info');

        $r = DB::table('content_info')->select('*')
            ->where('content_id', '=', $id)->get();
        $res = array();
        foreach ($r as $row) {
            foreach ($columns as $col) {
                $res[$row->languages_id][$col] = $row->{$col};
            }
        }

        return $res;
    }

    public function templates($type = 'page', $selected = 0)
    {

        return DB::table('content_templates as c')->select('c.id', 'c.name', 'c.id as value',
//            DB::raw('if(c.id=' . $selected . ',\'selected\',\'\') AS selected')
             DB::raw('Case when c.id='.$selected.' then \'selected\' else \'\' end as selected')
         )
             ->join('content_type as ct', function ($join) use ($type) {
                 $join->on('ct.id', '=', 'c.type_id')
                     ->where('ct.type', '=', "$type");
             })->get();
    }

    public function process($id, $data, $info = array())
    {
        DB::beginTransaction();
        $data['editedon'] = date('Y-m-d H:i:s');

        $r = DB::table('content')->where('id', $id)
            ->update($data);

        foreach ($info as $languages_id => $a) {
            $aid = DB::table('content_info')->select('id')
                ->where('content_id', '=', $id)
                ->where('languages_id', '=', $languages_id)
                ->first();

            if (empty($aid)) {
                $r += DB::table('content_info')->insert(
                    [
                        'content_id' => $id,
                        'languages_id' => $languages_id,
                        'name' => $info[$languages_id]['name'],
                        'alias' => $info[$languages_id]['alias'],

                        'title' => isset($info[$languages_id]['title']) ? $info[$languages_id]['title'] : $info[$languages_id]['name'],
                        'keywords' => isset($info[$languages_id]['keywords']) ? $info[$languages_id]['keywords'] : '',
                        'meta_h1' => isset($info[$languages_id]['meta_h1']) ? $info[$languages_id]['meta_h1'] : '',
                        'description' => isset($info[$languages_id]['description']) ? $info[$languages_id]['description'] : '',
                        'meta_description' => isset($info[$languages_id]['meta_description']) ? $info[$languages_id]['meta_description'] : '',
                        'content' => isset($info[$languages_id]['content']) ? $info[$languages_id]['content'] : '',
                    ]
                );
            } else {
                $r += DB::table('content_info')->where('id', '=', $aid->id)
                    ->update(
                        [
                            'name' => $info[$languages_id]['name'],
                            'alias' => $info[$languages_id]['alias'],
                            'title' => isset($info[$languages_id]['title']) ? $info[$languages_id]['title'] : $info[$languages_id]['name'],
                            'keywords' => isset($info[$languages_id]['keywords']) ? $info[$languages_id]['keywords'] : '',
                            'meta_h1' => isset($info[$languages_id]['meta_h1']) ? $info[$languages_id]['meta_h1'] : '',
                            'description' => isset($info[$languages_id]['description']) ? $info[$languages_id]['description'] : '',
                            'meta_description' => isset($info[$languages_id]['meta_description']) ? $info[$languages_id]['meta_description'] : '',
                            'content' => isset($info[$languages_id]['content']) ? $info[$languages_id]['content'] : '',
                        ]
                    );
            }
        }

        if ($r > 0) {
            DB::commit();
        } else {
            DB::rollBack();
        }

        return $r;
    }

    public function toggleFolder($id)
    {
        $isFolder = ($this->hasChildren($id)) ? 1 : 0;
        return DB::table('content')->where('id', '=', $id)
            ->update([
                'isfolder' => $isFolder
            ]);
    }

    public function hasChildren($parent_id)
    {
        return DB::table('content')->select(DB::raw('count(id) as t'))
            ->where('parent_id', '=', $parent_id)->first();
    }

    public function tree($parent_id, $type_id)
    {
        return DB::table('content as c')->select('c.id', 'c.isfolder', 'ci.name')
            ->join('content_info as ci', function ($join) {
                $join->on('c.id', '=', 'ci.content_id')
                    ->where('ci.languages_id', '=', $this->language_id);
            })
            ->where('c.parent_id', '=', $parent_id)
            ->where('c.type_id', '=', $type_id)->get();
    }

    public function parentAlias($id, $languages_id)
    {
        return DB::table('content_info as i')->select('i.alias')
            ->where('i.content_id', '=', $id)
            ->where('i.languages_id', '=', $languages_id)->first();
    }

    public function del($id)
    {
        return DB::table('content')->where('id', '=', $id)
            ->delete();
    }

    public function getNavMenus()
    {
        return DB::table('nav_menu')->select('id', 'name')
            ->get();
    }

    public function selectedNavMenu($content_id, $auto = 0)
    {
        $res = array();
        if ($auto) {
            $r = DB::table('nav_menu')->select('id')->where('auto_add_pages', '=', 1)->get();
            foreach ($r as $row) {
                $res[] = $row->id;
            }
        }
        $r = DB::table('nav_menu_items')->select('nav_menu_id as id')->where('content_id', '=', $content_id)->get();
        foreach ($r as $row) {
            $res[] = $row->id;
        }

        return $res;
    }

    public function addMenuItem($nav_menu_id, $content_id)
    {
        $sort = DB::table("nav_menu_items")->select( DB::raw('MAX(sort) AS s'))
            ->where('content_id', '=', $content_id)->first();

        return DB::table('nav_menu_items')->insert(
            array(
                'nav_menu_id' => $nav_menu_id,
                'content_id' => $content_id,
                'sort' => ++$sort->s
            )
        );
    }

    public function deleteNavMenus($content_id, $menu_id)
    {
        return DB::table('nav_menu_items')->where('content_id', '=', $content_id)
            ->where('nav_menu_id', '=', $menu_id)
            ->delete();
    }

    public function getFeatures($content_id, $type, $in = '', $type_id = 0, $products_id = 0)
    {
        $cid = $content_id;

        if ($type == 'product') {
            // витягну параметри які привязані до категорії
            $r = DB::table('products_categories')->select('cid')->where('pid', '=', $content_id)->first();
            if (!empty($r)) {
                $cid = $r->cid;
            }
        }

        if ($type == 'product' || $products_id > 0) {
//            $type_id = 'product';
            $type_id = DB::table('content_type')->select('id')->where('type', '=', 'product')->first();
        }

        $union = DB::table('features as f')->select('f.id','i.name','f.type','cft.sort as sort',
            DB::raw('Case when f.required>0 then \'required\' else \'\' end as required')
        )
            ->join('content_type_features as cft',function($join) use ($type_id){
                $join->on('cft.features_id','=','f.id')
                    ->where('cft.content_type_id','=',$type_id->id);
            })->join('features_info as i',function($join){
                $join->on('i.features_id','=','f.id')
                    ->where('i.languages_id','=',$this->language_id);
            })
            ->where('f.auto',0);
        if (!empty($in)) {
            $union->whereIn('f.id',$in);
        }
        $union->where('f.published',1)
            ->where('f.extends',1);

        $query = DB::table('features as f')->select('f.id','i.name','f.type','cf.sort as sort',
            DB::raw('Case when f.required>0 then \'required\' else \'\' end as required')
        )
            ->join('content_features as cf',function($join) use ($cid){
                $join->on('cf.features_id','=','f.id')
                    ->where('cf.content_id','=',$cid);
            })
            ->join('features_info as i',function($join){
                $join->on('i.features_id','=','f.id')
                    ->where('i.languages_id','=',$this->language_id);
            })
            ->where('f.auto',0);
        if (!empty($in)) {
            $query->whereIn('f.id',$in);
        }
        $query->where('f.published',1)
            ->where('f.extends',0)
            ->union($union);

        $query = $query->get();

//        dd($query);

//        $r = DB::select("
//            select distinct f.id, IF(f.required, 'required','') required, i.name, f.type, cf.sort as sort
//            from features f
//            join content_features cf on cf.content_id={$cid} and cf.features_id=f.id
//            join features_info i on i.features_id=f.id and i.languages_id = {$this->languages_id}
//            where f.auto = 0 {$in} and f.published=1 and f.extends=0
//            union
//            select distinct f.id, IF(f.required, 'required','') required, i.name, f.type,ctf.sort as sort
//            from features f
//            join content_type_features ctf on ctf.content_type_id={$type_id->id} and ctf.features_id=f.id
//            join features_info i on i.features_id=f.id and i.languages_id = {$this->languages_id}
//            where f.auto = 0 {$in} and f.published=1 and f.extends=1
//            order by abs(sort) asc
//        ");



        if (in_array($type, array('category'))) return $r;

        $res = array();
        foreach ($query as $row) {
            switch ($row->type) {
                case 'text' :
                case 'textarea' :
                case 'code' :
                case 'editor' :
                case 'number' :

                    $values_id = $row->id;
                    $row->values = DB::table('languages as l')->select('l.id as languages_id', 'l.name as placeholder', 'i.value')
                        ->leftJoin('features_content_values as i', function ($join) use ($content_id, $values_id) {
                            $join->on('i.languages_id', '=', 'l.id')
                                ->where('i.features_id', '=', $values_id)
                                ->where('i.content_id', '=', $content_id);
                        })
                        ->where('l.front', '=', 1)
                        ->orderBy('l.front_default', 'desc')
                        ->get();

                    continue;
                    break;
                case 'checkbox':
                    $row->checked =
                        DB::table('features_content_values')->select('value')
                            ->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $row->id)
                            ->where('languages_id', '=', 0)->first() == 1 ? 'checked' : '';
                    break;
                case 'colour' :
                    $row->value = DB::table('features_content_values')->select('value')
                        ->where('content_id', '=', $content_id)
                        ->where('features_id', '=', $row->id)
                        ->where('languages_id', '=', 0)->first();
                    break;
                case 'radiogroup':
                case 'checkboxgroup':
                case 'select':
                case 'sm':
                    $values_id = $row->id;
                    $row->values = DB::table('features_values as v')->select('v.id', 'i.value',
                        DB::raw('IF(fcv.id > 0, "checked", "") as checked'))
                        ->join('features_values_info as i', function ($join) {
                            $join->on('i.values_id', '=', 'v.id')
                                ->where('i.languages_id', '=', $this->languages_id);
                        })
                        ->leftJoin('features_content_values as fcv', function ($join) use ($content_id, $values_id) {
                            $join->on('fcv.features_id', '=', 'v.id')
                                ->where('fcv.content_id', '=', $content_id)
                                ->where('fcv.features_id', '=', $values_id)
                                ->where('fcv.languages_id', '=', 0);
                        })
                        ->where('v.features_id', '=', $row->id)
                        ->orderBy('v.sort', 'asc')->get();
                    break;
            }
            $res[] = $row;
        }
        return $res;
    }

    public function saveFeatures($content_id)
    {
        $features = Input::get('features');

        if (!isset($features)) return 0;
//        echo "<pre>";
//        print_r($features);
//        echo "</pre>";
//        die;
//        echo '<pre>';print_r($_POST['features']);
        foreach ($features as $type => $a) {
            switch ($type) {
                case 'colours':
                    foreach ($a as $features_id => $aa) {
                        $id = DB::table('features_content_values')->select('id')
                            ->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $features_id)
                            ->where('languages_id', '=', 0)
                            ->first();

                        if(empty($id)) {
                            DB::table('features_content_values')->insert(
                                array(
                                    'content_id' => $content_id,
                                    'features_id' => $features_id,
                                    'languages_id' => 0,
                                    'value' => $aa
                                )
                            );
                        } else {
                            DB::table('features_content_values')->where('id', '=', $id->id)
                                ->update(array('value' => $aa));
                        }
                    }
                    break;
                case 'text':
                    foreach ($a as $features_id => $aa) {
                        foreach ($aa as $languages_id => $value) {
                            $id = DB::table('features_content_values')->select('id')
                                ->where('content_id', '=', $content_id)
                                ->where('features_id', '=', $features_id)
                                ->where('languages_id', '=', $languages_id)
                                ->first();
                            if (empty($id)) {
                                DB::table('features_content_values')->insert(
                                    array(
                                        'content_id' => $content_id,
                                        'features_id' => $features_id,
                                        'languages_id' => $languages_id,
                                        'value' => $value
                                    )
                                );
                            } else {
                                DB::table('features_content_values')->where('id', '=', $id->id)
                                    ->update(array('value' => $value));
                            }
                        }

                    }

                    break;
                case 'select':
                    foreach ($a as $features_id => $features_values_id) {
                        DB::table('features_content_values')->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $features_id)
                            ->delete();
                        DB::table('features_content_values')
                            ->insert(array(
                                    'content_id' => $content_id,
                                    'features_id' => $features_id,
                                    'features_values_id' => $features_values_id
                                )
                            );
                    }
                    break;
                case 'checkbox':
                    foreach ($a as $features_id => $checked) {
                        $id = DB::table('features_content_values')->select('id')
                            ->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $features_id)
                            ->first();
                        if (empty($id)) {
                            if ($checked == 1) {
                                DB::table('features_content_values')
                                    ->insert(array(
                                            'content_id' => $content_id,
                                            'features_id' => $features_id,
                                            'value' => $checked
                                        )
                                    );
                            }
                        } else if ($id > 0 && $checked == 0) {
                            DB::table('features_content_values')->where('id', '=', $id->id)
                                ->delete();
                        }
                    }
                    break;
                case 'sm':
                    foreach ($a as $features_id => $values) {
                        DB::table('features_content_values')->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $features_id)
                            ->delete();
                        foreach ($values as $k => $features_values_id) {
                            DB::table('features_content_values')
                                ->insert(array(
                                        'content_id' => $content_id,
                                        'features_id' => $features_id,
                                        'features_values_id' => $features_values_id
                                    )
                                );
                        }

                    }
                    break;
                case 'checkboxgroup':
                    foreach ($a as $features_id => $values) {
                        DB::table('features_content_values')->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $features_id)
                            ->delete();
                        foreach ($values as $features_values_id => $checked) {
                            if ($checked == 1) {
                                DB::table('features_content_values')
                                    ->insert(array(
                                            'content_id' => $content_id,
                                            'features_id' => $features_id,
                                            'features_values_id' => $features_values_id
                                        )
                                    );
                            }
                        }

                    }
                    break;
                case 'radiogroup':
                    foreach ($a as $features_id => $features_values_id) {
                        $id = DB::table('features_content_values')->select('id')
                            ->where('content_id', '=', $content_id)
                            ->where('features_id', '=', $features_id)
                            ->where('features_values_id', '=', $features_values_id)
                            ->first();
                        if (empty($id)) {
                            DB::table('features_content_values')
                                ->insert(array(
                                        'content_id' => $content_id,
                                        'features_id' => $features_id,
                                        'features_values_id' => $features_values_id
                                    )
                                );
                        } else {
                            DB::table('features_content_values')->where('id', '=', $id->id)
                                ->update(
                                    array(
                                        'features_values_id' => $features_values_id
                                    )
                                );
                        }
                    }

                    break;
            }
        }

    }

    public function publish($id,$status)
    {
        return DB::table('content')->where('id','=',$id)
            ->update([
                'published'=>$status
            ]);
    }

    public function sort($table, $pk, $col, $i, $id)
    {
        return DB::table($table)->where($pk,'=',$id)
            ->update([
                $col=>$i
            ]);
    }
}
