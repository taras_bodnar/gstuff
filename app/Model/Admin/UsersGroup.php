<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UsersGroup extends Model
{
    public static function getAll($id=0)
    {

        return DB::table('users_group as ug')->select('ug.id','ui.name',
            DB::raw('if(ugi.id>0,"selected","") AS selected')
            )
            ->join('users_group_info as ui',function($join){
                $join->on('ug.id','=','ui.users_group_id');
            }
            )
            ->leftjoin('users_group as ugi',function($join) use ($id){
                $join->on('ugi.parent_id','=','ug.id')
                ->where('ugi.id','=',$id);
            })
            ->where('ui.languages_id','=',self::$language)
            ->get();
    }

    public static function createUsersGroup($data,$info)
    {
        $id = DB::table('users_group')->insertGetId($data);

        $r = 0;

        if($id>0) {

//            dd($info);
//            print_r($info);die;
            foreach ($info as $languages_id =>$a) {
                if(DB::table('users_group_info')->insert(
                    [
                        'users_group_id' => $id,
                        'languages_id' => $languages_id,
                        'name'         => $info[$languages_id]['name'],
                        'description'  => $info[$languages_id]['description']
                    ]
                )){
                    $r++;
                } else{
                    echo "error";
                }
            }
        }

        return $r;
    }

    public static function getInfo($id)
    {
        $r = DB::table('users_group_info')->select('*')
            ->where('users_group_id','=',$id)
            ->get();
//       dd($r);

        $res=array();

        foreach ($r as $row) {
            $res[$row->languages_id]['name'] = $row->name;
            $res[$row->languages_id]['description'] = $row->description;
        }
        return $res;
    }

    public static function updateGroup($id,$data,$info)
    {
        $s = DB::table('users_group')
            ->where('id', $id)
            ->update($data);

        foreach ($info as $languages_id =>$a) {
            $aid = DB::table('users_group_info')->select('id')
                ->where('users_group_id','=',$id)
                ->where('languages_id','=',$languages_id)
                ->first();

            if(empty($aid)){

                $s += DB::table('users_group_info')->insert(
                    [
                        'users_group_id'=>$id,
                        'languages_id'=>$languages_id,
                        'name'=>$info[$languages_id]['name'],
                        'description'=>$info[$languages_id]['description'],
                    ]
                );
            } else {
                $s +=  DB::table("users_group_info")
                    ->where('id',$aid->id)
                    ->update(
                        [
                            'name'        => $info[$languages_id]['name'],
                            'description' => $info[$languages_id]['description']
                        ]
                    );


            }
        }
        return $s;
    }


}
