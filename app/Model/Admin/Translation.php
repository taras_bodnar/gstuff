<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Translation extends Model
{
    public function insert($data, $info)
    {
        $id = DB::table('translations')->insertGetId($data);
        $r = 0;

        if ($id > 0) {
            foreach ($info as $languages_id => $a) {
                if (DB::table('translations_info')->insert(
                    [
                        'translations_id' => $id,
                        'languages_id' => $languages_id,
                        'value' => $info[$languages_id]['value']
                    ]
                )
                ) {
                    $r++;
                } else {
                    echo "error";
                }
            }
        }

        return $r;
    }

    public function edit($id, $data, $info)
    {

        $s = DB::table('translations')
            ->where('id', $id)
            ->update($data);

        foreach ($info as $languages_id => $a) {

            $aid = DB::table('translations_info')->select('id')
                ->where('translations_id',$id)
                ->where('languages_id',$languages_id)
                ->pluck('id');

            if (empty($aid)) {
                $s = DB::table('translations_info')->insertGetId(
                    [
                        'translations_id' => $id,
                        'languages_id' => $languages_id,
                        'value' => $info[$languages_id]['value']
                    ]
                );
            } else {
                DB::table("translations_info")
                    ->where('id', $aid)
                    ->update(
                        [
                            'value' => $info[$languages_id]['value']
                        ]
                    );
                $s = 1;
            }
        }

        return $s;
    }

    public function getInfo($id)
    {
        $r = DB::table('translations_info')->select('*')
            ->where('translations_id','=',$id)->get();

        $res=array();

        foreach ($r as $row) {
            $res[$row->languages_id]['value'] = $row->value;
        }
        return $res;
    }

    public function del($id)
    {
        return DB::table('translations')
            ->where('id', $id)
            ->delete();
    }
}
