<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Languages extends Model
{
    public static function getInfo($id)
    {
        return DB::table('languages')->select('*')
            ->where('id','=',$id)->first();
    }

    public static function geAll($front= null, $back = null)
    {
        $query = DB::table('languages')->select('*','id as value')->orderBy('id','asc');

        if($back) {
            $query->where('back',$back);
            $query->orderBy('back_default','desc');
        }

        if($front) {
            $query->where('front',$front);
            $query->orderBy('front_default','desc');
        }

        $query = $query->get();

        return $query;
    }

    public function del($id)
    {
        return DB::table('languages')->where('id','=',$id)
            ->delete();
    }

    public function insert($data)
    {
        return DB::table('languages')->insert($data);
    }

    public function edit($id,$data)
    {
        return DB::table('languages')->where('id','=',$id)
            ->update($data);
    }

    public function getNotTranslatedLanguage()
    {
        return DB::table('languages as l')->select('l.name','l.id','i.name as c_name','l.back')
            ->leftJoin('content_info as i',function($join){
                $join->on('i.languages_id','=','l.id');
            })
            ->groupBy('l.id')
            ->orderBy('l.id','desc')
            ->get();
    }

    public function getInfoTables()
    {
        return DB::select("SHOW TABLES LIKE '%_info'");
    }

    public function getFrontendDefault($key='*')
    {
        return DB::table('languages')->select($key)
            ->where('front_default','=',1)
            ->first();
    }

    public function describe($table)
    {
        return DB::select("DESCRIBE {$table}");
    }

    public function getTotalTableRecords($table, $languages_id)
    {
        return DB::table($table)->select(DB::raw('count(*) as t'))
            ->where('languages_id','=',$languages_id)
            ->first();
    }

    public function rowData($id, $key='*')
    {
        return DB::table('languages')->select($key)
            ->where('id','=',$id)
            ->first();
    }

    public function getAliasInfo($languages_id,$start)
    {
        return DB::table('content as c')->select('i.id','c.parent_id','i.content_id','i.name','i.alias')
            ->join('content_info as i',function($join) use ($languages_id) {
                $join->on('i.content_id','=','c.id')
                    ->where('i.languages_id','=',$languages_id);
            })
            ->orderBy('c.id','asc')
            ->skip($start)->take(1)
            ->first();
    }

    public function updateAlias($id, $alias)
    {
        return DB::table('content_info')->where('id','=',$id)
            ->update(array('alias'=> $alias));
    }

    public function getTableRow($table, $languages_id, $start)
    {
        return DB::table($table)->select('*')
            ->where('languages_id','=',$languages_id)
            ->orderBy('id','asc')
            ->skip($start)->take(1)
            ->first();
    }

    public function insertTranslatedData($table, $iv, $debug=0)
    {
        return DB::table($table)->insert($iv);
    }

    public function publish($id,$status)
    {
        return DB::table('languages')->where('id','=',$id)
            ->update([
                'front'=>$status
            ]);
    }
}
