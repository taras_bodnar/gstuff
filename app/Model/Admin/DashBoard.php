<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DashBoard extends Model
{
    public function getLastPages($type='page')
    {
        return DB::table('content as c')->select('c.id','c.editedon','ci.name')
            ->join('content_info as ci',function($join) {
                $join->on('ci.content_id','=','c.id')
                    ->where('ci.languages_id','=',$this->languages_id);
            })
            ->join('content_type as t',function($join) use ($type) {
                $join->on('t.id','=','c.type_id')
                    ->where('t.type','=',$type);
            })
            ->orderBy('c.editedon','desc')
            ->take(4)
            ->get();
    }
}
