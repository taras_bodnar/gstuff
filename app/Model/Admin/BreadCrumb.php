<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BreadCrumb extends Model
{
    private $items = array();
    public function controllerId($nc)
    {
        return DB::table('components')->select('id')
            ->where('controller','like',$nc)
            ->first();
    }

    public function crumbs($id)
    {
        $r = DB::table('components as c')->select('c.id','c.controller','c.icon','c.parent_id','c.isfolder','ci.name')
            ->join('components_info as ci',function($join) {
                    $join->on('c.id','=','ci.components_id')
                        ->where('ci.languages_id','=',1);
            })
            ->where('c.id','=',$id)
            ->first();


        if($r->id == 1)  return $this->items;

        $this->items[] = array(
            'controller'=>$r->controller,
            'name'=>$r->name,
            'icon'=>$r->icon,
            'children' => $r->isfolder ? $this->children($r->id) : null
        );

        if($r->parent_id != 0) $this->crumbs($r->parent_id);

        return $this->items;
    }

    private function children($parent_id)
    {
        return DB::table('components as c')->select('c.id','c.controller','c.icon','c.parent_id','c.isfolder','ci.name')
            ->join('components_info as ci',function($join) {
                $join->on('c.id','=','ci.components_id')
                    ->where('ci.languages_id','=',1);
            })
            ->where('c.parent_id','=',$parent_id)
            ->orderBy('c.sort','asc')
            ->get();
    }
}
