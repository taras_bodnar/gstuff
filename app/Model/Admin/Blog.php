<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Blog extends Model
{
    public function getCategories($post_id)
    {
        return DB::table('posts_categories')->select('id','cid')
            ->where('pid','=',$post_id)
            ->get();
    }

    public function deleteCategories($post_id, $cid)
    {
        return DB::table('posts_categories')->where('id','=',$post_id)
            ->where('cid','=',$cid)
            ->delete();
    }
    public function setCategories($post_id, $cid)
    {
        return DB::table('posts_categories')
            ->insert(array('pid'=>$post_id, 'cid' => $cid));
    }
}
