<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Projects extends Model
{
    public function getCategories($post_id)
    {
        return DB::table('posts_categories')->select('id','cid')
            ->where('pid','=',$post_id)
            ->get();
    }

    public function getChildren($post_id)
    {
        return DB::table('posts_categories')->select('id','pid')
            ->where('cid','=',$post_id)
            ->get();
    }

    public function deleteCategories($post_id, $cid)
    {
        return DB::table('posts_categories')->where('pid','=',$post_id)
            ->where('cid','=',$cid)
            ->delete();
    }
    public function setCategories($post_id, $cid)
    {
        return DB::table('posts_categories')
            ->insert(array('pid'=>$post_id, 'cid' => $cid));
    }

    public function getPages($id)
    {
        return DB::table("content as c")->select('c.id','i.name',
            DB::raw('if(pc.id>0,"selected","") AS selected'))
            ->join('content_info as i',function ($join){
                $join->on('c.id','=','i.content_id')
                    ->where('i.languages_id','=',$this->language_id);
            })
            ->leftjoin('posts_categories as pc',function($join) use ($id){
                $join->on('pc.pid','=','c.id')
                    ->where('pc.cid','=',$id);
            })
            ->where('auto','=',0)
            ->where('type_id','=',1)
            ->get();
    }
}
