<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Settings extends Model
{
    public function get()
    {
        $data = DB::table('settings')->get();

        $arr = [];
        foreach($data as $k=>$value) {
            $arr{$value->name}=$value->value;
//            $arr{$value->name}['description']=$value->description;
        }

        return $arr;
    }

    public function edit($data)
    {

        foreach($data as $k=>$v) {
            $s = DB::table('settings')->where('name','=',"$k")
                ->update([
                    'value'=>"$v"
                ]);
        }

        return $s;
    }
}
