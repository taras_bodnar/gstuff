<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Features extends Model
{

    public function createAuto($owner_id)
    {
        DB::table('features')->where('auto','=',1)
            ->where('owner_id','=',1)
            ->delete();
        return DB::table('features')->insertGetID(array('owner_id' => $owner_id, 'published'=>1));
    }

    public function edit($id, $data, $info){
        DB::beginTransaction();

        $r = DB::table('features')->where('id','=',$id)
            ->update($data);

        foreach ($info as $languages_id =>$a) {
            $aid = DB::table('features_info')->select('id')
                ->where('features_id','=',$id)
                ->where('languages_id','=',$languages_id)
                ->first();
            if(empty($aid)){
                $r += DB::table('features_info')->insert(
                    array(
                        'features_id'  => $id,
                        'languages_id'=> $languages_id,
                        'name'        => $info[$languages_id]['name']
                    )
                );
            } else {
                $r += DB::table('features_info')->where('id','=',$aid->id)
                    ->update( array(
                        'name'        => $info[$languages_id]['name']
                    )
                    );
            }
        }

        if($r > 0) {
            DB::commit();
        } else {
            DB::rollBack();
        }

        return $r;
    }

    public function deleteAllContentFeatures($features_id)
    {
        DB::table('features')->where('id','=',$features_id)
            ->update(array('extends' => 0));
        return DB::table('content_features')->where('features_id','=',$features_id)
            ->delete();
    }

    public function createContentFeatures($features_id, $content_id)
    {
        return DB::table('content_features')
            ->insert(array(
            "features_id"=>$features_id,
            "content_id" => $content_id
            )
        );
    }

    public function deleteAllContentTypeFeatures($features_id)
    {
        return DB::table('content_type_features')->where('features_id','=',$features_id)
            ->delete();
    }

    public function createContentTypeFeatures($features_id, $content_type_id)
    {
        $s = DB::table('content_type_features')->insertGetId(
            array(
                'features_id'     => $features_id,
                "content_type_id" => $content_type_id
            )
        );
        if($s > 0) {
            DB::table('features')->where('id','=',$features_id)
                ->update(array('extends' => 1));
        }
        return $s;
    }

    public function updateValuesInfo($values)
    {
        foreach ($values as $values_id=>$a) {
            foreach ($a as $languages_id=>$value) {
                $id = DB::table('features_values_info')->select('id')
                    ->where('values_id','=',$values_id)
                    ->where('languages_id','=',$languages_id)
                    ->first();
                if(isset($id) && $id->id > 0){
                    DB::table('features_values_info')
                        ->where('id','=',$id->id)
                        ->update(array('value' => $value)
                        );
                } else {
                    DB::table('features_values_info')->insert(
                        array(
                            'values_id'    => $values_id,
                            'languages_id' => $languages_id,
                            'value'        => $value
                        )
                    );
                }
            }
        }

        return 1;
    }


    public function getType($selected='')
    {
        $r = $this->getPossibleRoles('features','type');
        $res=array();
        foreach($r as $k=>$v){
            $res[] = array(
                'id' => $v,
                'name' => $v,
                'selected' => $v == $selected ? 'selected' : '',
            );
        }

        return $res;
    }

    public function categories($parent_id)
    {
        return  DB::table('content as c')->select('c.id','i.name','c.isfolder')
            ->join("content_type as t",function($join){
                $join->on('c.type_id','=','t.id');
            })
            ->join("content_info as i",function($join){
                $join->on('i.content_id','=','c.id')
                    ->where('i.languages_id','=',1);
            })
            ->where('c.parent_id','=',$parent_id)
            ->whereIn('t.type',array('page','category'))
            ->get();
    }

    public function getSelectedCategories($features_id)
    {
        $r = DB::table('content_features')->select('content_id as id')
            ->where('features_id','=',$features_id)
            ->get();
        $res=array();
        foreach ($r as $row) {
            $res[] = $row->id;
        }

        return $res;
    }

    public function getContentType($features_id)
    {
        return DB::table('content_type as ct')->select('ct.id','ct.type',
            DB::raw('CONCAT(ct.name," (",ct.type, ")") as name'),
            DB::raw('IF(cto.id > 0, "selected" , "") as selected')
            )
            ->leftJoin('content_type_features as cto',function($join) use ($features_id) {
                $join->on('cto.content_type_id','=','ct.id')
                    ->where('cto.features_id','=',$features_id);
            })->get();
    }

    public function getValues($features_id)
    {
        $r = DB::table('features_values')
            ->where('features_id','=',$features_id)
            ->get();
        $res = array();
        foreach ($r as $row) {
            $values_id = $row->id;
            $res[$row->id] = DB::table('languages as l')->select('l.id as languages_id','l.name as placeholder','i.value')
                ->leftJoin('features_values_info as i',function($join) use ($values_id){
                        $join->on('i.languages_id','=','l.id')
                            ->where('i.values_id','=',$values_id);
                })
                ->where('l.front','=',1)
                ->get();
        }

        return $res;
    }

    public function del($id)
    {
        return DB::table('features')->where('id','=',$id)
        ->delete();
    }

    public function createValue($features_id)
    {
        return DB::table('features_values')
            ->insertGetId(array('features_id'=> $features_id));
    }

    public function removeValue($id)
    {
        return DB::table('features_values')->where('id','=',$id)
            ->delete();
    }

    public function show($id, $col, $published)
    {
        return DB::table('features')->where('id','=',$id)
            ->update([
                $col=>$published
            ]);
    }

}
