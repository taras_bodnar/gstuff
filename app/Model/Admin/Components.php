<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Components extends Model
{
    public static function getInfo($id)
    {
        $r = DB::select("
                                SELECT *
                                FROM components_info where components_id = {$id}
                                ");

        $res=array();

        foreach ($r as $row) {
            $res[$row->languages_id]['name'] = $row->name;
            $res[$row->languages_id]['description'] = $row->description;
        }
        return $res;
    }

    public static function del($id)
    {
        return DB::delete("Delete from components where id={$id}");
    }

    public static function createComponents($data,$info)
    {
         $id = DB::table('components')->insertGetId($data);
            $r = 0;

        if($id>0) {
            foreach ($info as $languages_id =>$a) {
                if(DB::table('components_info')->insert(
                    [
                        'components_id' => $id,
                        'languages_id' => $languages_id,
                        'name'         => $info[$languages_id]['name'],
                        'description'  => $info[$languages_id]['description']
                    ]
                )){
                    $r++;
                } else{
                    echo "error";
                }
            }
        }

        return $r;
    }

    public static function updateComponents($id,$data,$info)
    {
      $s = DB::table('components')
            ->where('id', $id)
            ->update($data);

        foreach ($info as $languages_id =>$a) {
            $aid = DB::select(DB::raw("select id
                                    from components_info
                                    where components_id=$id and languages_id=$languages_id
                                    limit 1
                                    "))[0];


            if(empty($aid)){

                $s += DB::table('components_info')->insert(
                    [
                        'components_id'=>$id,
                        'languages_id'=>$languages_id,
                        'name'=>$info[$languages_id]['name'],
                        'description'=>$info[$languages_id]['description'],
                    ]
                );
            } else {
                $s +=  DB::table("components_info")
                    ->where('id',$aid->id)
                    ->update(
                        [
                            'name'        => $info[$languages_id]['name'],
                            'description' => $info[$languages_id]['description']
                        ]
                    );


            }
        }
        return $s;
    }

    public static function getAll($id='')
    {

        return DB::table('components as c')->select('c.id','ci.name',
            DB::raw('if(c1.id>0,"selected","") AS selected')
        )
            ->join('components_info as ci',function($join){
                $join->on('c.id','=','ci.components_id');
            }
            )
            ->leftjoin('components as c1',function($join) use ($id){
                $join->on('c1.parent_id','=','c.id')
                    ->where('c1.id','=',$id);
            })
            ->where('ci.languages_id','=',self::$language)
            ->get();
    }

    public static function toggleFolder($parent_id)
    {
        if($parent_id==0) return'';
        return DB::table('components')->where('id','=',$parent_id)
        ->update(['isfolder'=>1]);
    }
}
