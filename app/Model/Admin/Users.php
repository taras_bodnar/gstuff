<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    public $rang = array(100,999);

    public static function getInfo($id)
    {
        return DB::table('users')->select('*')
            ->where('id','=',$id)->first();
    }

    public static function createUser($data)
    {
        return DB::table('users')->insert(
            $data
        );
    }

    public function edit($id,$data)
    {
        return DB::table('users')->where('id','=',$id)
            ->update($data);
    }

    public static function getInfoByEmail($email)
    {
        return DB::table('users as u')->select('u.id','u.name','u.email','g.rang','u.avatar')
            ->join('users_group as g',function($join) {
                $join->on('g.id','=','u.users_group_id');
            })
            ->where('email','=',$email)->first();
    }

    public static function checkRang($component)
    {
        return DB::table('components as c')->select('c.rang','ci.name')
            ->join('components_info as ci',function($join) {
                $join->on('ci.components_id','=','c.id')
                    ->where('ci.languages_id','=',1);
            })
            ->where('c.controller','=',"$component")
            ->first();
    }

    public function tree($parent_id)
    {
        return DB::table('users_group as s')->select('s.id','i.name',
            DB::raw('(Select count(t.id) from users_group t where t.parent_id =s.id) as isfolder')
            )
            ->join('users_group_info as i',function($join) {
                $join->on('i.users_group_id','=','s.id')
                    ->where('i.languages_id','=',$this->language_id);
            })
            ->where('s.parent_id','=',$parent_id)
            ->whereBetween('s.rang', $this->rang)
            ->orderBy('s.sort','asc')
            ->get();
    }

    public function del($id)
    {
        return DB::table('users')->where('id','=',$id)
            ->delete();
    }
}
