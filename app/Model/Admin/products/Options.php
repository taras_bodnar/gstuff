<?php

namespace App\Model\Admin\products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Options extends Model
{
    public function getData($id)
    {
        return DB::table('products_options')->select('*')
            ->where('products_id','=',$id)
            ->first();
    }

    public function edit($id,$data)
    {
        $aid = DB::table('products_options')
            ->where('products_id','=',$id)
            ->where('is_variant','=',0)
            ->pluck('id');

        if($aid > 0) {
            return DB::table('products_options')->where('id','=',$aid)
                ->update($data);
        } else {
            $data['products_id'] = $id;
            return DB::table('products_options')->insert($data);
        }
    }
}
