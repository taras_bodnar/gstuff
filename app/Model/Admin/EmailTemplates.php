<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class EmailTemplates extends Model
{
    public function getInfo($id)
    {
        $r = DB::table('email_templates_info')->select('*')
            ->where('templates_id','=',$id)
            ->get();

        $res=array();

        foreach ($r as $row) {
            $res[$row->languages_id]['reply_to'] = $row->reply_to;
            $res[$row->languages_id]['subject'] = $row->subject;
            $res[$row->languages_id]['body'] = $row->body;
        }
        return $res;
    }

    public function insert($data, $info)
    {
        $id = DB::table('email_templates')->insertGetId($data);
        $r = 0;

        if($id>0) {
            foreach ($info as $languages_id =>$a) {
                if(DB::table('email_templates_info')->insert(
                    [
                        'templates_id' => $id,
                        'languages_id' => $languages_id,
                        'reply_to'        => $info[$languages_id]['reply_to'],
                        'subject'        => $info[$languages_id]['subject'],
                        'body'        => $info[$languages_id]['body']
                    ]
                )){
                    $r++;
                } else{
                    echo "error";
                }
            }
        }

        return $r;
    }

    public function edit($id, $data, $info=array())
    {
        DB::beginTransaction();

        $r = DB::table('email_templates')->where('id','=',$id)
            ->update($data);

        foreach ($info as $languages_id =>$a) {
            $aid = DB::table('email_templates_info')->select('id')
                ->where('templates_id','=',$id)
                ->where('languages_id','=',$languages_id)
                ->first();
            if(empty($aid)){
                $r += DB::table('email_templates_info')->insert(
                    [
                        'templates_id'=> $id,
                        'languages_id'=> $languages_id,
                        'reply_to'    => $info[$languages_id]['reply_to'],
                        'subject'     => $info[$languages_id]['subject'],
                        'body'        => $info[$languages_id]['body']
                    ]
                );
            } else {
                $r += DB::table('email_templates_info')->where('id','=',$aid->id)
                    ->update(
                        [
                            'reply_to' => $info[$languages_id]['reply_to'],
                            'subject'  => $info[$languages_id]['subject'],
                            'body'     => $info[$languages_id]['body']
                        ]
                    );
            }
        }

        if($r > 0) {
            DB::commit();
        } else {
            DB::rollBack();
        }

        return $r;
    }
}
