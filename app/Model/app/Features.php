<?php

namespace App\Model\app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Features extends Model
{
    public function getItem($id, $values = false)
    {
        $r = DB::table('features as f')->select('f.id','f.type','fi.name')
            ->join('features_info as fi',function($join) {
                $join->on('fi.features_id','=','f.id')
                    ->where('fi.languages_id','=',$this->languages_id);
            })
        ->where('f.id','=',$id)
        ->where('f.auto','=',0)
        ->where('f.published','=',1)
        ->first();

        if(empty($r)) return array();
        if($values){
            $r->values = $this->getValues($r->id, $r->type);
        }

        return $r;
    }

    private function getValues($features_id, $type)
    {
        if($type == 'number'){
            $values = DB::table('features_content_values')->select(DB::raw('max(value) as value'))
                ->where('features_id','=',$features_id)
                ->where('languages_id','=',0)->first();
        } else {
            $values = DB::table('features_values as fv')->select('fv.id','i.value')
                ->join('features_values_info as i',function($join) {
                    $join->on('i.values_id','=','fv.id')
                        ->where('i.languages_id','=',$this->languages_id);
                })
                ->where('fv.features_id','=',$features_id)
                ->orderBy('i.value','asc')
                ->get();
        }

        return $values;
    }

    public function getContentValues($content_id, $features_id,$type='text')
    {
        $res=[];

        if(in_array($type, array('sm','checkboxgroup'))){
            $v = DB::table('features_content_values as fcv')->select('fvi.value',
                'fcv.features_values_id as id')
                ->join('features_values_info as fvi',function ($join){
                    $join->on('fvi.values_id','=','fcv.features_values_id')
                        ->where('fvi.languages_id','=',$this->languages_id);
                })
                ->where('fcv.features_id','=',$features_id)
                ->where('fcv.content_id','=',$content_id)
                ->where('languages_id','=',0)->get();

            foreach ($v as $rrr) {
                $res['values'][$rrr['id']] = $rrr['value'];
            }

        } elseif(in_array($type, array('select','radiogroup'))){

            $res['value'] =  DB::table('features_content_values as fcv')->select('fvi.value')
                ->join('features_values_info as fvi',function ($join){
                    $join->on('fvi.values_id','=','fcv.features_values_id')
                        ->where('fvi.languages_id','=',$this->languages_id);
                })
                ->where('fcv.features_id','=',$features_id)
                ->where('fcv.content_id','=',$content_id)
                ->where('fcv.languages_id','=',0)
                ->get();

        } elseif($type == 'number') {

            $res['value'] = DB::table('features_content_values')->select(DB::raw('max(value) as value'))
                ->where('features_id','=',$features_id)
                ->where('languages_id','=',0)->first();
        } elseif($type == 'colour') {

            $res = DB::table('features_content_values')->select('value')
                ->where('content_id','=',$content_id)
                ->where('features_id','=',$features_id)
                ->where('languages_id','=',0)->first();
        } else {

            $res = DB::table('features_content_values')->select('value')
                ->where('features_id','=',$features_id)
                ->where('content_id','=',$content_id)
                ->where('languages_id','=',$this->languages_id)->first();
        }

        return $res;
    }
}
