<?php

namespace App\Model\app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Images extends Model
{
    public function cover($content_id, $size = 'source')
    {
        $r = DB::table('content_images as i')->select('i.name as src','ii.alt')
            ->leftJoin('content_images_info as ii',function($join){
                $join->on('ii.images_id','=','i.id')
                    ->where('ii.languages_id','=',$this->languages_id);
            })
            ->where('i.content_id','=',$content_id)
            ->orderBy('i.sort','asc')
            ->first();

        if($size && !empty($r)){
            $r->src = "/uploads/content/{$content_id}/{$size}/{$r->src}";
        }
        return $r;
    }

    public function get($content_id, $size = null, $start=0, $num = 1000)
    {
        $r = DB::table('content_images as i')->select('i.name as src','ii.alt')
            ->leftjoin('content_images_info as ii',function($join){
                $join->on('i.id','=','ii.images_id')
                    ->where('ii.languages_id','=',$this->languages_id);
            })
            ->where('i.content_id','=',$content_id)
            ->orderBy('i.sort','asc')
            ->skip($start)
            ->take($num)
            ->get();

        $res = array();
        foreach ($r as $row) {
            if($size){
                $row->src = "/uploads/content/{$content_id}/{$size}/{$row->src}";
            }
            $res[] = $row;
        }

        return $res;
    }
}
