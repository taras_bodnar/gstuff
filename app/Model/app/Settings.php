<?php

namespace App\Model\app;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Settings extends Model
{
    public function loadS()
    {
        return DB::table('settings')->select('name','value')
            ->where('autoload','=',1)->get();

    }

    public static function get($key)
    {
        return DB::table('settings')->select('value')
            ->where('name','=',$key)->first();
    }

    public function set($name, $value)
    {
        return DB::table('settings')->where('name','=',$name)
            ->update(
            [
                'value' => $value
            ]
        );
    }

    public function deleteS($name)
    {
        return DB::table('settings')->where('name','=',$name)->delete();
    }
}
