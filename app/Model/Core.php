<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Core extends Model
{
    public static function getAlias($alias)
    {
        return DB::table('content as c')->select('c.*','ci.name','ci.title','ci.keywords','ci.meta_h1','ci.meta_description','ci.description','ci.content')
//            ->join('content_info ci','c.id', '=', 'ci.content_id')
//            ->on('languages_id','=',1)
            ->join('content_info as ci', function ($join) {
                $join->on('c.id', '=', 'ci.content_id');
            })
            ->where('alias', '=', "$alias")
            ->where('ci.languages_id', '=', Session::get('app.languages_id'))
            ->where('published', '=', 1)
            ->where('auto', '=', 0)
            ->first();
    }

    public static function getAliasById($id)
    {
        return DB::table('content_info')->select('alias')
            ->where('content_id', '=', $id)
            ->where('languages_id', '=', Session::get('languages_id'))
            ->first();
    }

    public static function getAliasById2($id,$languages_id)
    {
        return DB::table('content_info')->select('alias')
            ->where('content_id', '=', $id)
            ->where('languages_id', '=', $languages_id)
            ->first();
    }

    public static function getLangByCode($code)
    {
        return DB::table('languages')->select('id')
            ->where('code', '=', $code)
            ->where('front','=',1)
            ->first();
    }

    public static function getTemplate($id, $type_id)
    {
        return DB::table('content_templates as t')
            ->join('content_type as ct', function ($join) {
                $join->on('ct.id', '=', 't.type_id');
            })
            ->select('t.path', 'ct.type')
            ->where('t.id', '=', $id)
            ->where('ct.id', '=', $type_id)
            ->first();
    }

    public static function getFrontLangDefault()
    {
        return DB::table('languages')->select('id')
            ->where('front_default','=',1)
            ->first();
    }

    public function getLangAliasById($id, $languages_id)
    {
        return DB::table('content_info as ci')->select('ci.alias','l.code')
            ->join('languages as l', function ($join) {
                $join->on('ci.languages_id', '=', 'l.id');
            })
            ->where('ci.languages_id', '=', $languages_id)
            ->where('ci.content_id', '=', $id)
            ->first();
    }

    public  function getTranslations()
    {
        $r = DB::table('translations as t')->select('t.code','ti.value')
            ->join('translations_info as ti', function($join){
                $join->on('t.id','=','ti.translations_id')
                    ->where('ti.languages_id','=',$this->languages_id);
            })->get();

        $res=array();

        foreach ($r as $row) {
            $res{$row->code} = $row->value;
        }
        $res = json_encode($res);
        $res = json_decode($res,false);

        return $res;
    }


}
