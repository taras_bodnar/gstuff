<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages_id = DB::table('languages')->where('back_default', '=', 1)->pluck('id');

        $group_id = DB::table('users_group')->pluck('id');

        $password = 'password';

        DB::table('users')->insert([
            'users_group_id' => $group_id,
            'languages_id' => $languages_id,
            'name' => 'Admin',
            'email' => 'admin@larAdmin.com',
            'password' => bcrypt($password)
        ]);
    }
}
