<?php

use Illuminate\Database\Seeder;

class ComponentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('components')->delete();

        $data = [
            [
                'id' => 1,
                'parent_id' => 0,
                'isfolder' => 0,
                'icon' => 'fa-home nav-icon',
                'controller' => '/admin/Admin/DashBoard',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 2,
                'parent_id' => 0,
                'isfolder' => 0,
                'icon' => 'fa-folder-open',
                'controller' => '/admin/Admin/Pages',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 3,
                'parent_id' => 0,
                'isfolder' => 1,
                'icon' => 'fa-language',
                'controller' => '/admin/Admin/Languages',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 4,
                'parent_id' => 3,
                'isfolder' => 0,
                'icon' => 'fa-language',
                'controller' => '/admin/Admin/Languages',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 5,
                'parent_id' => 3,
                'isfolder' => 0,
                'icon' => 'fa-language',
                'controller' => '/admin/Admin/Translation',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 6,
                'parent_id' => 3,
                'isfolder' => 0,
                'icon' => 'fa-language',
                'controller' => '/admin/Admin/EmailTemplates',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 7,
                'parent_id' => 0,
                'isfolder' => 1,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Components',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 8,
                'parent_id' => 7,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Plugins',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 9,
                'parent_id' => 7,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Components',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 10,
                'parent_id' => 0,
                'isfolder' => 0,
                'icon' => 'fa-cog',
                'controller' => '/admin/Admin/Settings',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 11,
                'parent_id' => 0,
                'isfolder' => 1,
                'icon' => 'fa-users',
                'controller' => '/admin/Admin/UsersGroup',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 12,
                'parent_id' => 11,
                'isfolder' => 0,
                'icon' => 'fa-users',
                'controller' => '/admin/Admin/UsersGroup',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 13,
                'parent_id' => 11,
                'isfolder' => 0,
                'icon' => 'fa-users',
                'controller' => '/admin/Admin/Users',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 14,
                'parent_id' => 0,
                'isfolder' => 1,
                'icon' => 'fa-wrench',
                'controller' => '',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 15,
                'parent_id' => 14,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Type',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 16,
                'parent_id' => 14,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Templates',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 17,
                'parent_id' => 14,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Features',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 18,
                'parent_id' => 14,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Themes',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 19,
                'parent_id' => 14,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Nav',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
            [
                'id' => 20,
                'parent_id' => 14,
                'isfolder' => 0,
                'icon' => 'fa-wrench',
                'controller' => '/admin/Admin/Size',
                'sort' => 1,
                'published' => 1,
                'rang' => 200,
            ],
        ];

        DB::table('components')->insert($data);

        $languages_id = DB::table('languages')->where('back_default', '=', 1)->pluck('id');

        $info = [
            [
                'components_id' => 1,
                'languages_id' => $languages_id,
                'name' => 'LarAdmin',
                'description' => '',
            ],
            [
                'components_id' => 2,
                'languages_id' => $languages_id,
                'name' => 'Pages',
                'description' => '',
            ],
            [
                'components_id' => 3,
                'languages_id' => $languages_id,
                'name' => 'Localisation',
                'description' => '',
            ],
            [
                'components_id' => 4,
                'languages_id' => $languages_id,
                'name' => 'Languages',
                'description' => '',
            ],
            [
                'components_id' => 5,
                'languages_id' => $languages_id,
                'name' => 'Translation',
                'description' => '',
            ],
            [
                'components_id' => 6,
                'languages_id' => $languages_id,
                'name' => 'Email Templates',
                'description' => '',
            ],
            [
                'components_id' => 7,
                'languages_id' => $languages_id,
                'name' => 'Components',
                'description' => '',
            ],
            [
                'components_id' => 8,
                'languages_id' => $languages_id,
                'name' => 'Plugins',
                'description' => '',
            ],
            [
                'components_id' => 9,
                'languages_id' => $languages_id,
                'name' => 'Components',
                'description' => '',
            ],
            [
                'components_id' => 10,
                'languages_id' => $languages_id,
                'name' => 'Settings',
                'description' => '',
            ],
            [
                'components_id' => 11,
                'languages_id' => $languages_id,
                'name' => 'Administration',
                'description' => '',
            ],
            [
                'components_id' => 12,
                'languages_id' => $languages_id,
                'name' => 'Administration Group',
                'description' => '',
            ],
            [
                'components_id' => 13,
                'languages_id' => $languages_id,
                'name' => 'Administration List',
                'description' => '',
            ],
            [
                'components_id' => 14,
                'languages_id' => $languages_id,
                'name' => 'Instruments',
                'description' => '',
            ],
            [
                'components_id' => 15,
                'languages_id' => $languages_id,
                'name' => 'Content Types',
                'description' => '',
            ],
            [
                'components_id' => 16,
                'languages_id' => $languages_id,
                'name' => 'Templates',
                'description' => '',
            ],
            [
                'components_id' => 17,
                'languages_id' => $languages_id,
                'name' => 'Features',
                'description' => '',
            ],
            [
                'components_id' => 18,
                'languages_id' => $languages_id,
                'name' => 'Themes',
                'description' => '',
            ],
            [
                'components_id' => 19,
                'languages_id' => $languages_id,
                'name' => 'Menu',
                'description' => '',
            ],
            [
                'components_id' => 19,
                'languages_id' => $languages_id,
                'name' => 'Images Sizes',
                'description' => '',
            ],
        ];

        DB::table('components_info')->insert($info);
    }

}
