<?php

use Illuminate\Database\Seeder;

class ContentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'type' => 'page',
                'name' => 'Pages',
            ],
            [
                'type' => 'blog',
                'name' => 'Articles',
            ],
            [
                'type' => 'category',
                'name' => 'Product Category',
            ],
            [
                'type' => 'manufacturer',
                'name' => 'Manufacturer',
            ],
            [
                'type' => 'product',
                'name' => 'Product',
            ],
            [
                'type' => 'postCat',
                'name' => 'Articles Categories',
            ]
        ];
        DB::table('content_type')->insert($data);
    }
}
