<?php

use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'code' => 'uk',
            'name' => 'Ukraine',
            'front' => 1,
            'front_default' => 1,
            'back_default' => 1
        ]);
    }
}
