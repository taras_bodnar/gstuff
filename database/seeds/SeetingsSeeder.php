<?php

use Illuminate\Database\Seeder;

class SeetingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        $data = [
            [
                'name'=>'autofil_title',
                'value'=>1,
                'description'=>'Автоматично заповнювати title в стоірнках на основі назви',
                'autoload'=>1,
            ],
            [
                'name'=>'autotranslit_alias',
                'value'=>1,
                'description'=>'Автоматично генерувати аліас',
                'autoload'=>1,
            ],
            [
                'name'=>'editor_language',
                'value'=>'uk',
                'description'=>'Мова редактора по замовчуванню',
                'autoload'=>1,
            ],
            [
                'name'=>'editor_bodyId',
                'value'=>'cms_content',
                'description'=>'ID body в шаблоні',
                'autoload'=>1,
            ],
            [
                'name'=>'editor_bodyClass',
                'value'=>'cms_content',
                'description'=>'Клас контенту для редкатора',
                'autoload'=>1,
            ],
            [
                'name'=>'editor_contentsCss',
                'value'=>'css/style.css',
                'description'=>'Шлях до стилів сайту, можна задавати через кому декілька',
                'autoload'=>1,
            ],
            [
                'name'=>'products_list_update_price',
                'value'=> 1,
                'description'=>'Оновляти ціни в списку товарів',
                'autoload'=>1,
            ],
            [
                'name'=>'languages_create_info',
                'value'=> 1,
                'description'=>'При створенні мови, автоматично згенерувати переклади у всіх розділах системи',
                'autoload'=>1,
            ],
            [
                'name'=>'app_theme_current',
                'value'=> 'default',
                'description'=>'Тема по замовчуванню для сайту',
                'autoload'=>1,
            ],
            [
                'name'=>'app_views_path',
                'value'=> 'views/',
                'description'=>'Шлях до шаблонів',
                'autoload'=>1,
            ],
            [
                'name'=>'app_chunks_path',
                'value'=> 'chunks/',
                'description'=>'Шлях до чанків',
                'autoload'=>1,
            ],
            [
                'name'=>'themes_path',
                'value'=> 'public/',
                'description'=>'Папка з темами',
                'autoload'=>1,
            ],
            [
                'name'=>'content_images_dir',
                'value'=> '/uploads/content/',
                'description'=>'Шлях до зображень',
                'autoload'=>1,
            ],
            [
                'name'=>'content_images_thumb_dir',
                'value'=> 'thumb/',
                'description'=>'Шлях до превюшок',
                'autoload'=>1,
            ],
            [
                'name'=>'content_images_source_dir',
                'value'=> 'source/',
                'description'=>'Шлях до source',
                'autoload'=>1,
            ],
            [
                'name'=>'admin_theme_current',
                'value'=> 'admin',
                'description'=>'Активна тема для адмінки',
                'autoload'=>1,
            ],
            [
                'name'=>'mod_path',
                'value'=> '\\app\\Http\\Controllers\\modules\\',
                'description'=>'Абсолютний шлях до модулів',
                'autoload'=>1,
            ],
            [
                'name'=>'page_404',
                'value'=> '12',
                'description'=>'Ід сторінки 404',
                'autoload'=>1,
            ],
            [
                'name'=>'img_source_size',
                'value'=> '1600x1200',
                'description'=>'Розмір зображень source по замовчуванню',
                'autoload'=>1,
            ],
            [
                'name'=>'et_header',
                'value'=> '',
                'description'=>'Глобальний header',
                'autoload'=>1,
            ],
            [
                'name'=>'et_footer',
                'value'=> '',
                'description'=>'Глобальний footer',
                'autoload'=>1,
            ],
            [
                'name'=>'google_ananytics_id',
                'value'=> '',
                'description'=>'Google Analytics ID',
                'autoload'=>1,
            ],
            [
                'name'=>'google_webmaster_id',
                'value'=> '',
                'description'=>'G.Webmaster ID',
                'autoload'=>1,
            ],
            [
                'name'=>'yandex_webmaster_id',
                'value'=> '',
                'description'=>'Yandex Webmaster ID',
                'autoload'=>1,
            ],
            [
                'name'=>'yandex_metrika',
                'value'=> '',
                'description'=>'Yandex Metrika ID',
                'autoload'=>1,
            ],
            [
                'name'=>'version',
                'value'=> '0.1',
                'description'=>'Версія змін ядра.бд.системи',
                'autoload'=>1,
            ],
            [
                'name'=>'version_update',
                'value'=> '1',
                'description'=>'Автоматичне оновлення версій системи',
                'autoload'=>1,
            ],
            [
                'name'=>'water_position',
                'value'=> 'bottom',
                'description'=>'Water element',
                'autoload'=>1,
            ],
            [
                'name'=>'facebook_app_id',
                'value'=> '',
                'description'=>'FB app Id',
                'autoload'=>1,
            ],
            [
                'name'=>'facebook_app_secret',
                'value'=> '',
                'description'=>'FB app secret Id',
                'autoload'=>1,
            ],
            [
                'name'=>'install_theme_path',
                'value'=> 'install',
                'description'=>'Шлях теми встановлення',
                'autoload'=>1,
            ],
            [
                'name'=>'install_title',
                'value'=> 'Встановлення LarAdmin',
                'description'=>'Встановлення LarAdmin',
                'autoload'=>1,
            ]
        ];

        DB::table('settings')->insert($data);
    }
}
