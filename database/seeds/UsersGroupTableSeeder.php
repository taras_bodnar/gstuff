<?php

use Illuminate\Database\Seeder;

class UsersGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_group')->insert([
            'parent_id' => 0,
            'rang' => 999,
            'sort' => 0,
        ]);
    }
}
