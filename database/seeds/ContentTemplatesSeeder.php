<?php

use Illuminate\Database\Seeder;

class ContentTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = \Illuminate\Support\Facades\DB::table('content_type')->select('id')->get();

        foreach($items as $item) {
            \Illuminate\Support\Facades\DB::table('content_templates')->insert(
                [
                    'type_id'=>$item->id,
                    'path'=>'default',
                    'name'=>'Default',
                    'description'=>'Default Template',
                    'main'=>1,
                ]
            );
        }
    }
}
