<?php

use Illuminate\Database\Seeder;

class CommponetsSeedr extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages_id = DB::table('languages')->where('back_default', '=', 1)->pluck('id');

        DB::table('components')->insert([
            'code' => 'uk',
            'name' => 'Ukraine',
            'front' => 1,
            'front_default' => 1,
            'back_default' => 1
        ]);
    }
}
