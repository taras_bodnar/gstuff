-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Жов 20 2016 р., 17:25
-- Версія сервера: 5.6.30-0ubuntu0.15.10.1
-- Версія PHP: 5.6.11-1ubuntu3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `dream`
--

-- --------------------------------------------------------

--
-- Структура таблиці `chunks`
--

CREATE TABLE IF NOT EXISTS `chunks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_id` int(10) unsigned NOT NULL,
  `pib` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `createdon` datetime DEFAULT NULL,
  `ip` char(15) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `components`
--

CREATE TABLE IF NOT EXISTS `components` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` tinyint(4) NOT NULL,
  `isfolder` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `rang` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `components`
--

INSERT INTO `components` (`id`, `parent_id`, `isfolder`, `icon`, `controller`, `sort`, `published`, `rang`) VALUES
(1, 0, 0, 'fa-home nav-icon', '/admin/Admin/DashBoard', 0, 1, 101),
(46, 0, 1, 'fa-puzzle-piece', '/admin/Admin/Components', 5, 1, 200),
(47, 0, 0, 'fa-folder-open', '/admin/Admin/Pages', 1, 1, 101),
(48, 0, 1, 'fa-language', '/admin/Admin/Languages', 2, 1, 200),
(49, 48, 1, '1', '/admin/Admin/Languages', 1, 1, 200),
(50, 48, 0, '1', '/admin/Admin/Translation', 2, 1, 200),
(51, 48, 0, '1', '/admin/Admin/EmailTemplates', 3, 1, 200),
(52, 0, 1, 'fa-shopping-cart', '/admin/Admin/Categories', 3, 1, 200),
(53, 52, 0, '0', '/admin/Admin/Categories', 1, 1, 200),
(54, 52, 0, '0', '/admin/Admin/Products', 2, 1, 200),
(55, 0, 1, 'fa-book', '/admin/Admin/UsersGroup', 3, 1, 150),
(56, 55, 0, '0', '/admin/Admin/PostCategories', 1, 1, 150),
(57, 55, 0, '0', '/admin/Admin/Posts', 2, 1, 150),
(58, 0, 1, 'fa-wrench ', '/admin/Admin/UsersGroup', 5, 1, 200),
(59, 58, 0, '0', '/admin/Admin/Type', 1, 1, 200),
(60, 58, 0, '0', '/admin/Admin/Templates', 2, 1, 200),
(61, 58, 0, '0', '/admin/Admin/Features', 3, 1, 200),
(62, 58, 0, '0', '/admin/Admin/Themes', 4, 1, 200),
(63, 0, 1, 'fa-users', '/admin/Admin/UsersGroup', 6, 1, 200),
(64, 63, 0, '0', '/admin/Admin/UsersGroup', 1, 1, 200),
(65, 63, 0, '0', '/admin/Admin/Users', 2, 1, 200),
(66, 58, 0, '1', '/admin/Admin/Nav', 5, 1, 200),
(67, 58, 0, '1', '/admin/Admin/Size', 6, 1, 200),
(68, 46, 0, '1', '/admin/Admin/Plugins', 1, 1, 200),
(69, 46, 0, '2', '/admin/Admin/Components', 2, 1, 200),
(70, 0, 1, 'fa-picture-o', '/admin/Admin/Gallery', 4, 1, 200),
(71, 0, 0, 'fa-cog', '/admin/Admin/Settings', 8, 1, 200),
(72, 70, 0, '1', '/admin/Admin/GalleryCategories', 1, 1, 200),
(73, 70, 0, '1', '/admin/Admin/Gallery', 2, 1, 200);

-- --------------------------------------------------------

--
-- Структура таблиці `components_info`
--

CREATE TABLE IF NOT EXISTS `components_info` (
  `id` int(10) unsigned NOT NULL,
  `components_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=446 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `components_info`
--

INSERT INTO `components_info` (`id`, `components_id`, `languages_id`, `name`, `description`) VALUES
(1, 1, 1, 'WhoIAm', 'WhoIAm :: Система керування контентом'),
(132, 46, 1, 'Компоненти', 'Компоненти'),
(133, 47, 1, 'Сторінки', 'Сторінки'),
(134, 48, 1, 'Локалізація', 'Локалізація'),
(135, 49, 1, 'Мови', 'Мови'),
(136, 50, 1, 'Переклади', 'Переклади                                \n                            '),
(137, 51, 1, 'Шаблони e-mail', 'Шаблони e-mail'),
(138, 52, 1, 'Магазин', 'Магазин'),
(139, 53, 1, 'Категорії', 'Категорії товарів                                \n                            '),
(140, 54, 1, 'Товари', 'Товари                                \n                            '),
(141, 55, 1, 'Блог', 'Блог'),
(142, 56, 1, 'Категорії', 'Категорії'),
(143, 57, 1, 'Статті', 'Список статтей'),
(144, 58, 1, 'Інструменти', 'Інструменти'),
(145, 59, 1, 'Типи і шаблони', '                                Типи і шаблони\n                            '),
(146, 60, 1, 'Шаблони', 'Шаблони'),
(147, 61, 1, 'Динамічні параметри', 'Динамічні параметри'),
(148, 62, 1, 'Теми', 'Теми'),
(149, 63, 1, 'Адміністратори', 'Адміністратори'),
(150, 64, 1, 'Групи', 'Групи'),
(151, 65, 1, 'Список', 'Список адміністаторів'),
(152, 66, 1, 'Менеджер меню', 'Менеджер меню                                \n                            '),
(197, 67, 1, 'Розміри зображень', '                                Розміри зображень\n                            '),
(436, 68, 1, 'Плагіни', 'Плагіни'),
(439, 69, 1, 'Компоненти', 'Компоненти'),
(442, 70, 1, 'Галерея', '                                Галерея\n                            '),
(443, 71, 1, 'Налаштування', 'Налаштування                                \n                            '),
(444, 72, 1, 'Категорії', 'Категорії'),
(445, 73, 1, 'Альбоми', '                                                                Альбоми\n                            \n                            ');

-- --------------------------------------------------------

--
-- Структура таблиці `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `isfolder` tinyint(4) NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `owner_id` int(10) unsigned NOT NULL,
  `templates_id` int(10) unsigned NOT NULL,
  `published` tinyint(4) NOT NULL,
  `module` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(6) NOT NULL,
  `createdon` datetime NOT NULL,
  `editedon` datetime NOT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_features`
--

CREATE TABLE IF NOT EXISTS `content_features` (
  `id` int(10) unsigned NOT NULL,
  `features_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_images`
--

CREATE TABLE IF NOT EXISTS `content_images` (
  `id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `type` enum('image','file') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'image'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_images_info`
--

CREATE TABLE IF NOT EXISTS `content_images_info` (
  `id` int(10) unsigned NOT NULL,
  `images_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_info`
--

CREATE TABLE IF NOT EXISTS `content_info` (
  `id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_templates`
--

CREATE TABLE IF NOT EXISTS `content_templates` (
  `id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `path` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_type`
--

CREATE TABLE IF NOT EXISTS `content_type` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_type_features`
--

CREATE TABLE IF NOT EXISTS `content_type_features` (
  `id` int(10) unsigned NOT NULL,
  `features_id` int(10) unsigned NOT NULL,
  `content_type_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `content_type_images`
--

CREATE TABLE IF NOT EXISTS `content_type_images` (
  `id` int(10) unsigned NOT NULL,
  `content_type_id` int(10) unsigned NOT NULL,
  `images_sizes_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` decimal(7,3) DEFAULT NULL,
  `is_main` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `dashboard_quick_launch`
--

CREATE TABLE IF NOT EXISTS `dashboard_quick_launch` (
  `id` int(11) NOT NULL,
  `components_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(10) unsigned NOT NULL,
  `free_from` decimal(10,2) NOT NULL DEFAULT '0.00',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `delivery_info`
--

CREATE TABLE IF NOT EXISTS `delivery_info` (
  `id` int(10) unsigned NOT NULL,
  `delivery_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `delivery_payment`
--

CREATE TABLE IF NOT EXISTS `delivery_payment` (
  `id` int(10) unsigned NOT NULL,
  `delivery_id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `email_templates_info`
--

CREATE TABLE IF NOT EXISTS `email_templates_info` (
  `id` int(10) unsigned NOT NULL,
  `templates_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `reply_to` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(10) unsigned NOT NULL,
  `type` enum('text','checkbox','radiogroup','textarea','checkboxgroup','select','sm') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `required` tinyint(4) NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `show_list` tinyint(4) NOT NULL DEFAULT '0',
  `show_compare` tinyint(4) NOT NULL DEFAULT '0',
  `show_filter` tinyint(4) NOT NULL DEFAULT '0',
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  `owner_id` int(10) unsigned NOT NULL,
  `extends` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `features_content_values`
--

CREATE TABLE IF NOT EXISTS `features_content_values` (
  `id` int(10) unsigned NOT NULL,
  `features_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `features_values_id` int(10) unsigned NOT NULL,
  `value` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `features_info`
--

CREATE TABLE IF NOT EXISTS `features_info` (
  `id` int(10) unsigned NOT NULL,
  `features_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `features_values`
--

CREATE TABLE IF NOT EXISTS `features_values` (
  `id` int(10) unsigned NOT NULL,
  `features_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `features_values_info`
--

CREATE TABLE IF NOT EXISTS `features_values_info` (
  `id` int(10) unsigned NOT NULL,
  `values_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `guides`
--

CREATE TABLE IF NOT EXISTS `guides` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `guides_info`
--

CREATE TABLE IF NOT EXISTS `guides_info` (
  `id` int(10) unsigned NOT NULL,
  `guides_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `images_sizes`
--

CREATE TABLE IF NOT EXISTS `images_sizes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `crop` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `front` tinyint(4) DEFAULT '0',
  `back` tinyint(4) DEFAULT '0',
  `front_default` tinyint(4) DEFAULT '0',
  `back_default` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `front`, `back`, `front_default`, `back_default`) VALUES
(1, 'uk', 'Ukraine', 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_10_20_000000_create_chunks_table', 1),
('2016_10_20_000001_create_comments_table', 1),
('2016_10_20_000002_create_components_table', 1),
('2016_10_20_000003_create_components_info_table', 1),
('2016_10_20_000004_create_content_table', 1),
('2016_10_20_000005_create_content_features_table', 1),
('2016_10_20_000006_create_content_images_table', 1),
('2016_10_20_000007_create_content_images_info_table', 1),
('2016_10_20_000008_create_content_info_table', 1),
('2016_10_20_000009_create_content_templates_table', 1),
('2016_10_20_000010_create_content_type_table', 1),
('2016_10_20_000011_create_content_type_features_table', 1),
('2016_10_20_000012_create_content_type_images_table', 1),
('2016_10_20_000013_create_currency_table', 1),
('2016_10_20_000014_create_dashboard_quick_launch_table', 1),
('2016_10_20_000015_create_delivery_table', 1),
('2016_10_20_000016_create_delivery_info_table', 1),
('2016_10_20_000017_create_delivery_payment_table', 1),
('2016_10_20_000018_create_email_templates_table', 1),
('2016_10_20_000019_create_email_templates_info_table', 1),
('2016_10_20_000020_create_features_table', 1),
('2016_10_20_000021_create_features_content_values_table', 1),
('2016_10_20_000022_create_features_info_table', 1),
('2016_10_20_000023_create_features_values_table', 1),
('2016_10_20_000024_create_features_values_info_table', 1),
('2016_10_20_000025_create_guides_table', 1),
('2016_10_20_000026_create_guides_info_table', 1),
('2016_10_20_000027_create_images_sizes_table', 1),
('2016_10_20_000028_create_languages_table', 1),
('2016_10_20_000029_create_modules_table', 1),
('2016_10_20_000030_create_nav_menu_table', 1),
('2016_10_20_000031_create_nav_menu_items_table', 1),
('2016_10_20_000032_create_payment_table', 1),
('2016_10_20_000033_create_payment_info_table', 1),
('2016_10_20_000034_create_plugins_table', 1),
('2016_10_20_000035_create_plugins_structure_table', 1),
('2016_10_20_000036_create_posts_categories_table', 1),
('2016_10_20_000037_create_products_categories_table', 1),
('2016_10_20_000038_create_products_options_table', 1),
('2016_10_20_000039_create_products_related_table', 1),
('2016_10_20_000040_create_promo_codes_table', 1),
('2016_10_20_000041_create_routers_table', 1),
('2016_10_20_000042_create_settings_table', 1),
('2016_10_20_000043_create_tags_table', 1),
('2016_10_20_000044_create_tags_content_table', 1),
('2016_10_20_000045_create_translations_table', 1),
('2016_10_20_000046_create_translations_info_table', 1),
('2016_10_20_000047_create_users_table', 1),
('2016_10_20_000048_create_users_group_table', 1),
('2016_10_20_000049_create_users_group_info_table', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL,
  `module` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `nav_menu`
--

CREATE TABLE IF NOT EXISTS `nav_menu` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `auto_add_pages` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `nav_menu_items`
--

CREATE TABLE IF NOT EXISTS `nav_menu_items` (
  `id` int(10) unsigned NOT NULL,
  `nav_menu_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL,
  `currency_id` int(10) unsigned NOT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `module` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sort` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `payment_info`
--

CREATE TABLE IF NOT EXISTS `payment_info` (
  `id` int(10) unsigned NOT NULL,
  `payment_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` int(10) unsigned NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `position` enum('left','right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right',
  `settings` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `plugins_structure`
--

CREATE TABLE IF NOT EXISTS `plugins_structure` (
  `id` int(10) unsigned NOT NULL,
  `components_id` int(10) unsigned NOT NULL,
  `plugins_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `posts_categories`
--

CREATE TABLE IF NOT EXISTS `posts_categories` (
  `id` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `products_categories`
--

CREATE TABLE IF NOT EXISTS `products_categories` (
  `id` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `products_options`
--

CREATE TABLE IF NOT EXISTS `products_options` (
  `id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `manufacturers_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability` tinyint(4) NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `old_price` decimal(10,2) NOT NULL,
  `sale` tinyint(4) NOT NULL,
  `hit` tinyint(4) NOT NULL,
  `new` tinyint(4) NOT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '0',
  `is_variant` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `products_related`
--

CREATE TABLE IF NOT EXISTS `products_related` (
  `id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `related_id` int(10) unsigned NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `promo_codes`
--

CREATE TABLE IF NOT EXISTS `promo_codes` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `expire` date DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `type` enum('abs','per') COLLATE utf8_unicode_ci DEFAULT 'abs',
  `minp` decimal(10,2) DEFAULT NULL,
  `single` tinyint(4) DEFAULT '0',
  `usages` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `routers`
--

CREATE TABLE IF NOT EXISTS `routers` (
  `id` int(10) unsigned NOT NULL,
  `url_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header` enum('HTTP/1.1 301 Moved Permanently','HTTP/1.1 307 Temporary Redirect','HTTP/1.1 302 Moved Temporarily') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'HTTP/1.1 301 Moved Permanently'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autoload` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `description`, `autoload`) VALUES
(1, 'autofil_title', '1', 'Автоматично заповнювати title в стоірнках на основі назви', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `tags_content`
--

CREATE TABLE IF NOT EXISTS `tags_content` (
  `id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `tags_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `translations`
--

CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `translations_info`
--

CREATE TABLE IF NOT EXISTS `translations_info` (
  `id` int(10) unsigned NOT NULL,
  `translations_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `users_group_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `sessid` char(35) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skey` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `createdon` datetime NOT NULL,
  `editetedon` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `users_group_id`, `languages_id`, `sessid`, `address`, `name`, `phone`, `email`, `password`, `skey`, `createdon`, `editetedon`) VALUES
(1, 1, 1, '', '', 'Admin', '', 'admin@larAdmin.com', '$2y$10$StH2hHK1QlGHWsdrJIvLi.I2Rqxx1BQxhceL1zVBhS0vduZ7P0rLS', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблиці `users_group`
--

CREATE TABLE IF NOT EXISTS `users_group` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` tinyint(4) NOT NULL,
  `rang` smallint(6) NOT NULL,
  `sort` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `users_group`
--

INSERT INTO `users_group` (`id`, `parent_id`, `rang`, `sort`) VALUES
(1, 0, 999, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `users_group_info`
--

CREATE TABLE IF NOT EXISTS `users_group_info` (
  `id` int(10) unsigned NOT NULL,
  `users_group_id` int(10) unsigned NOT NULL,
  `languages_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `users_group_info`
--

INSERT INTO `users_group_info` (`id`, `users_group_id`, `languages_id`, `name`, `description`) VALUES
(1, 1, 1, 'Administrator', 'Administrator');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `chunks`
--
ALTER TABLE `chunks`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_content_id_foreign` (`content_id`);

--
-- Індекси таблиці `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `components_info`
--
ALTER TABLE `components_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `components_info_components_id_foreign` (`components_id`),
  ADD KEY `components_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_templates_id_foreign` (`templates_id`),
  ADD KEY `content_type_id_foreign` (`type_id`),
  ADD KEY `content_owner_id_foreign` (`owner_id`);

--
-- Індекси таблиці `content_features`
--
ALTER TABLE `content_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_features_content_id_foreign` (`content_id`),
  ADD KEY `content_features_features_id_foreign` (`features_id`);

--
-- Індекси таблиці `content_images`
--
ALTER TABLE `content_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_images_content_id_foreign` (`content_id`);

--
-- Індекси таблиці `content_images_info`
--
ALTER TABLE `content_images_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_images_info_images_id_foreign` (`images_id`),
  ADD KEY `content_images_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `content_info`
--
ALTER TABLE `content_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_info_content_id_foreign` (`content_id`),
  ADD KEY `content_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `content_templates`
--
ALTER TABLE `content_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_templates_type_id_foreign` (`type_id`);

--
-- Індекси таблиці `content_type`
--
ALTER TABLE `content_type`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `content_type_features`
--
ALTER TABLE `content_type_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_type_features_content_type_id_foreign` (`content_type_id`),
  ADD KEY `content_type_features_features_id_foreign` (`features_id`);

--
-- Індекси таблиці `content_type_images`
--
ALTER TABLE `content_type_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_type_images_content_type_id_foreign` (`content_type_id`),
  ADD KEY `content_type_images_images_sizes_id_foreign` (`images_sizes_id`);

--
-- Індекси таблиці `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `dashboard_quick_launch`
--
ALTER TABLE `dashboard_quick_launch`
  ADD KEY `dashboard_quick_launch_components_id_foreign` (`components_id`);

--
-- Індекси таблиці `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `delivery_info`
--
ALTER TABLE `delivery_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_info_delivery_id_foreign` (`delivery_id`),
  ADD KEY `delivery_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `delivery_payment`
--
ALTER TABLE `delivery_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_payment_delivery_id_foreign` (`delivery_id`),
  ADD KEY `delivery_payment_payment_id_foreign` (`payment_id`);

--
-- Індекси таблиці `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `email_templates_info`
--
ALTER TABLE `email_templates_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_templates_info_templates_id_foreign` (`templates_id`),
  ADD KEY `email_templates_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_owner_id_foreign` (`owner_id`);

--
-- Індекси таблиці `features_content_values`
--
ALTER TABLE `features_content_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_content_values_content_id_foreign` (`content_id`);

--
-- Індекси таблиці `features_info`
--
ALTER TABLE `features_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_info_features_id_foreign` (`features_id`),
  ADD KEY `features_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `features_values`
--
ALTER TABLE `features_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_values_features_id_foreign` (`features_id`);

--
-- Індекси таблиці `features_values_info`
--
ALTER TABLE `features_values_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `features_values_info_values_id_foreign` (`values_id`),
  ADD KEY `features_values_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `guides`
--
ALTER TABLE `guides`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `guides_info`
--
ALTER TABLE `guides_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guides_info_guides_id_foreign` (`guides_id`),
  ADD KEY `guides_info_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `images_sizes`
--
ALTER TABLE `images_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `nav_menu`
--
ALTER TABLE `nav_menu`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `nav_menu_items`
--
ALTER TABLE `nav_menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nav_menu_items_content_id_foreign` (`content_id`),
  ADD KEY `nav_menu_items_nav_menu_id_foreign` (`nav_menu_id`);

--
-- Індекси таблиці `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_currency_id_foreign` (`currency_id`);

--
-- Індекси таблиці `payment_info`
--
ALTER TABLE `payment_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_info_languages_id_foreign` (`languages_id`),
  ADD KEY `payment_info_payment_id_foreign` (`payment_id`);

--
-- Індекси таблиці `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `plugins_structure`
--
ALTER TABLE `plugins_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plugins_structure_components_id_foreign` (`components_id`),
  ADD KEY `plugins_structure_plugins_id_foreign` (`plugins_id`);

--
-- Індекси таблиці `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_categories_cid_foreign` (`cid`),
  ADD KEY `posts_categories_pid_foreign` (`pid`);

--
-- Індекси таблиці `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categories_cid_foreign` (`cid`),
  ADD KEY `products_categories_pid_foreign` (`pid`);

--
-- Індекси таблиці `products_options`
--
ALTER TABLE `products_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_options_products_id_foreign` (`products_id`);

--
-- Індекси таблиці `products_related`
--
ALTER TABLE `products_related`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_related_products_id_foreign` (`products_id`),
  ADD KEY `products_related_related_id_foreign` (`related_id`);

--
-- Індекси таблиці `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `routers`
--
ALTER TABLE `routers`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_languages_id_foreign` (`languages_id`);

--
-- Індекси таблиці `tags_content`
--
ALTER TABLE `tags_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_content_content_id_foreign` (`content_id`),
  ADD KEY `tags_content_tags_id_foreign` (`tags_id`);

--
-- Індекси таблиці `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `translations_info`
--
ALTER TABLE `translations_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translations_info_languages_id_foreign` (`languages_id`),
  ADD KEY `translations_info_translations_id_foreign` (`translations_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_languages_id_foreign` (`languages_id`),
  ADD KEY `users_users_group_id_foreign` (`users_group_id`);

--
-- Індекси таблиці `users_group`
--
ALTER TABLE `users_group`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `users_group_info`
--
ALTER TABLE `users_group_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_group_info_languages_id_foreign` (`languages_id`),
  ADD KEY `users_group_info_users_group_id_foreign` (`users_group_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `chunks`
--
ALTER TABLE `chunks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT для таблиці `components_info`
--
ALTER TABLE `components_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=446;
--
-- AUTO_INCREMENT для таблиці `content`
--
ALTER TABLE `content`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_features`
--
ALTER TABLE `content_features`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_images`
--
ALTER TABLE `content_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_images_info`
--
ALTER TABLE `content_images_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_info`
--
ALTER TABLE `content_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_templates`
--
ALTER TABLE `content_templates`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_type`
--
ALTER TABLE `content_type`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_type_features`
--
ALTER TABLE `content_type_features`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `content_type_images`
--
ALTER TABLE `content_type_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `delivery_info`
--
ALTER TABLE `delivery_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `delivery_payment`
--
ALTER TABLE `delivery_payment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `email_templates_info`
--
ALTER TABLE `email_templates_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `features_content_values`
--
ALTER TABLE `features_content_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `features_info`
--
ALTER TABLE `features_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `features_values`
--
ALTER TABLE `features_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `features_values_info`
--
ALTER TABLE `features_values_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `guides`
--
ALTER TABLE `guides`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `guides_info`
--
ALTER TABLE `guides_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `images_sizes`
--
ALTER TABLE `images_sizes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `nav_menu`
--
ALTER TABLE `nav_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `nav_menu_items`
--
ALTER TABLE `nav_menu_items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `payment_info`
--
ALTER TABLE `payment_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `plugins_structure`
--
ALTER TABLE `plugins_structure`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `posts_categories`
--
ALTER TABLE `posts_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `products_options`
--
ALTER TABLE `products_options`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `products_related`
--
ALTER TABLE `products_related`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `routers`
--
ALTER TABLE `routers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `tags_content`
--
ALTER TABLE `tags_content`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `translations_info`
--
ALTER TABLE `translations_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `users_group`
--
ALTER TABLE `users_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `users_group_info`
--
ALTER TABLE `users_group_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `components_info`
--
ALTER TABLE `components_info`
  ADD CONSTRAINT `components_info_components_id_foreign` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `components_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `content_templates_id_foreign` FOREIGN KEY (`templates_id`) REFERENCES `content_templates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `content_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Обмеження зовнішнього ключа таблиці `content_features`
--
ALTER TABLE `content_features`
  ADD CONSTRAINT `content_features_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_features_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_images`
--
ALTER TABLE `content_images`
  ADD CONSTRAINT `content_images_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_images_info`
--
ALTER TABLE `content_images_info`
  ADD CONSTRAINT `content_images_info_images_id_foreign` FOREIGN KEY (`images_id`) REFERENCES `content_images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_images_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_info`
--
ALTER TABLE `content_info`
  ADD CONSTRAINT `content_info_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_templates`
--
ALTER TABLE `content_templates`
  ADD CONSTRAINT `content_templates_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_type_features`
--
ALTER TABLE `content_type_features`
  ADD CONSTRAINT `content_type_features_content_type_id_foreign` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_type_features_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `content_type_images`
--
ALTER TABLE `content_type_images`
  ADD CONSTRAINT `content_type_images_content_type_id_foreign` FOREIGN KEY (`content_type_id`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `content_type_images_images_sizes_id_foreign` FOREIGN KEY (`images_sizes_id`) REFERENCES `images_sizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `dashboard_quick_launch`
--
ALTER TABLE `dashboard_quick_launch`
  ADD CONSTRAINT `dashboard_quick_launch_components_id_foreign` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `delivery_info`
--
ALTER TABLE `delivery_info`
  ADD CONSTRAINT `delivery_info_delivery_id_foreign` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `delivery_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `delivery_payment`
--
ALTER TABLE `delivery_payment`
  ADD CONSTRAINT `delivery_payment_delivery_id_foreign` FOREIGN KEY (`delivery_id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `delivery_payment_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `email_templates_info`
--
ALTER TABLE `email_templates_info`
  ADD CONSTRAINT `email_templates_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `email_templates_info_templates_id_foreign` FOREIGN KEY (`templates_id`) REFERENCES `email_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `features_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_content_values`
--
ALTER TABLE `features_content_values`
  ADD CONSTRAINT `features_content_values_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_info`
--
ALTER TABLE `features_info`
  ADD CONSTRAINT `features_info_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `features_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_values`
--
ALTER TABLE `features_values`
  ADD CONSTRAINT `features_values_features_id_foreign` FOREIGN KEY (`features_id`) REFERENCES `features` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `features_values_info`
--
ALTER TABLE `features_values_info`
  ADD CONSTRAINT `features_values_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `features_values_info_values_id_foreign` FOREIGN KEY (`values_id`) REFERENCES `features_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `guides_info`
--
ALTER TABLE `guides_info`
  ADD CONSTRAINT `guides_info_guides_id_foreign` FOREIGN KEY (`guides_id`) REFERENCES `guides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guides_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `nav_menu_items`
--
ALTER TABLE `nav_menu_items`
  ADD CONSTRAINT `nav_menu_items_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nav_menu_items_nav_menu_id_foreign` FOREIGN KEY (`nav_menu_id`) REFERENCES `nav_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `payment_info`
--
ALTER TABLE `payment_info`
  ADD CONSTRAINT `payment_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_info_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `plugins_structure`
--
ALTER TABLE `plugins_structure`
  ADD CONSTRAINT `plugins_structure_components_id_foreign` FOREIGN KEY (`components_id`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `plugins_structure_plugins_id_foreign` FOREIGN KEY (`plugins_id`) REFERENCES `plugins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD CONSTRAINT `posts_categories_cid_foreign` FOREIGN KEY (`cid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posts_categories_pid_foreign` FOREIGN KEY (`pid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_categories`
--
ALTER TABLE `products_categories`
  ADD CONSTRAINT `products_categories_cid_foreign` FOREIGN KEY (`cid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_categories_pid_foreign` FOREIGN KEY (`pid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_options`
--
ALTER TABLE `products_options`
  ADD CONSTRAINT `products_options_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `products_related`
--
ALTER TABLE `products_related`
  ADD CONSTRAINT `products_related_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_related_related_id_foreign` FOREIGN KEY (`related_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tags_content`
--
ALTER TABLE `tags_content`
  ADD CONSTRAINT `tags_content_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tags_content_tags_id_foreign` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `translations_info`
--
ALTER TABLE `translations_info`
  ADD CONSTRAINT `translations_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `translations_info_translations_id_foreign` FOREIGN KEY (`translations_id`) REFERENCES `translations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_users_group_id_foreign` FOREIGN KEY (`users_group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `users_group_info`
--
ALTER TABLE `users_group_info`
  ADD CONSTRAINT `users_group_info_languages_id_foreign` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_group_info_users_group_id_foreign` FOREIGN KEY (`users_group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
