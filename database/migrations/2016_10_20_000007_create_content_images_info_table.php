<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentImagesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_images_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('images_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('alt', 255)->nullable()->default(NULL);

            $table->foreign('images_id')->references('id')->on('content_images')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_images_info', function (Blueprint $table) {
            $table->dropForeign(['images_id']);
        });

        Schema::drop('content_images_info');
    }
}