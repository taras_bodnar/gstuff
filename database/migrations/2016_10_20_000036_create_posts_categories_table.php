<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cid')->unsigned();
            $table->integer('pid')->unsigned();

            $table->foreign('cid')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pid')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts_categories', function (Blueprint $table) {
            $table->dropForeign(['cid']);
            $table->dropForeign(['pid']);
        });

        Schema::drop('posts_categories');
    }
}