<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('templates_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('reply_to', 60);
            $table->string('subject', 255);
            $table->text('body');

            $table->foreign('templates_id')->references('id')->on('email_templates')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_templates_info', function (Blueprint $table) {
            $table->dropForeign(['templates_id']);
        });

        Schema::drop('email_templates_info');
    }
}