<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_group', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('parent_id');
            $table->smallInteger('rang');
            $table->tinyInteger('sort');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('users_group_id')->references('id')->on('users_group')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['users_group_id']);
        });

        Schema::drop('users_group');
    }
}