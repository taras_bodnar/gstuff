<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersGroupInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_group_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_group_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('name', 100);
            $table->text('description')->nullable()->default(NULL);

            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('users_group_id')->references('id')->on('users_group')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_group_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
            $table->dropForeign(['users_group_id']);
        });

        Schema::drop('users_group_info');
    }
}