<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesContentValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_content_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('features_id')->unsigned();
            $table->integer('content_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->integer('features_values_id')->unsigned();
            $table->string('value', 500)->nullable()->default(NULL);

            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('features_content_values', function (Blueprint $table) {
            $table->dropForeign(['content_id']);
        });

        Schema::drop('features_content_values');
    }
}