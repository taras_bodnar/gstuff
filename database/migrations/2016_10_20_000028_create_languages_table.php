<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code', 2);
            $table->string('name', 30);
            $table->tinyInteger('front')->nullable()->default('0');
            $table->tinyInteger('back')->nullable()->default('0');
            $table->tinyInteger('front_default')->nullable()->default('0');
            $table->tinyInteger('back_default')->nullable()->default('0');
        });

        Schema::table('components_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('content_images_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('content_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('delivery_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('email_templates_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('features_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('features_values_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('guides_info', function (Blueprint $table) {
            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('components_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('content_images_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('content_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('delivery_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('email_templates_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('features_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('features_values_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::table('guides_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });

        Schema::drop('languages');
    }
}