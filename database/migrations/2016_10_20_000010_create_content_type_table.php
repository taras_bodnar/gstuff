<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 45);
            $table->string('name', 45)->nullable()->default(NULL);
        });

        Schema::table('content', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('content_type')->onDelete('no action')->onUpdate('no action');
        });

        Schema::table('content_templates', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('content_type')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('content', function (Blueprint $table) {
            $table->dropForeign(['type_id']);
        });

        Schema::table('content_templates', function (Blueprint $table) {
            $table->dropForeign(['type_id']);
        });

        Schema::drop('content_type');
    }
}