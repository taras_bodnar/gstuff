<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->string('path', 30)->nullable()->default(NULL);
            $table->string('name', 60);
            $table->string('description', 255)->nullable()->default(NULL);
            $table->tinyInteger('main')->nullable()->default(NULL);
        });

        Schema::table('content', function (Blueprint $table) {
            $table->foreign('templates_id')->references('id')->on('content_templates')->onDelete('no action')->onUpdate('no action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('content', function (Blueprint $table) {
            $table->dropForeign(['templates_id']);
        });

        Schema::drop('content_templates');
    }
}