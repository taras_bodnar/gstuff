<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('parent_id');
            $table->tinyInteger('isfolder')->default('0');
            $table->string('icon', 30)->nullable()->default(NULL);
            $table->string('controller', 150)->nullable()->default(NULL);
            $table->tinyInteger('sort')->nullable()->default('0');
            $table->tinyInteger('published')->default('0');
            $table->integer('rang')->unsigned()->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('components');
    }
}