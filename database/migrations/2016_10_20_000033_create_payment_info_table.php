<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('name', 60)->nullable()->default(NULL);
            $table->text('description')->nullable()->default(NULL);

            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('payment_id')->references('id')->on('payment')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
            $table->dropForeign(['payment_id']);
        });

        Schema::drop('payment_info');
    }
}