<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 16);
            $table->integer('width')->unsigned();
            $table->integer('height')->unsigned();
            $table->tinyInteger('crop')->nullable()->default(NULL);
        });

        Schema::table('content_type_images', function (Blueprint $table) {
            $table->foreign('images_sizes_id')->references('id')->on('images_sizes')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('content_type_images', function (Blueprint $table) {
            $table->dropForeign(['images_sizes_id']);
        });

        Schema::drop('images_sizes');
    }
}