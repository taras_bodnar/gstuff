<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashboardQuickLaunchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_quick_launch', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('components_id')->unsigned();
            $table->tinyInteger('sort');

            $table->foreign('components_id')->references('id')->on('components')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dashboard_quick_launch', function (Blueprint $table) {
            $table->dropForeign(['components_id']);
        });

        Schema::drop('dashboard_quick_launch');
    }
}