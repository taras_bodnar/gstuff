<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45)->nullable()->default(NULL);
            $table->char('code', 3)->nullable()->default(NULL);
            $table->string('symbol', 10)->nullable()->default(NULL);
            $table->decimal('rate', 7, 3)->nullable()->default(NULL);
            $table->tinyInteger('is_main')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currency');
    }
}