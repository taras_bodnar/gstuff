<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesValuesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_values_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('values_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('value', 255)->nullable()->default(NULL);

            $table->foreign('values_id')->references('id')->on('features_values')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('features_values_info', function (Blueprint $table) {
            $table->dropForeign(['values_id']);
        });

        Schema::drop('features_values_info');
    }
}