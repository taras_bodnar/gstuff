<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_type_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_type_id')->unsigned();
            $table->integer('images_sizes_id')->unsigned();

            $table->foreign('content_type_id')->references('id')->on('content_type')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_type_images', function (Blueprint $table) {
            $table->dropForeign(['content_type_id']);
        });

        Schema::drop('content_type_images');
    }
}