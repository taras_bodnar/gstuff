<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('currency_id')->unsigned();
            $table->tinyInteger('published')->default('0');
            $table->string('module', 60);
            $table->text('settings')->nullable()->default(NULL);
            $table->tinyInteger('sort')->default('0');

            $table->foreign('currency_id')->references('id')->on('currency')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('delivery_payment', function (Blueprint $table) {
            $table->foreign('payment_id')->references('id')->on('payment')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment', function (Blueprint $table) {
            $table->dropForeign(['currency_id']);
        });


        Schema::table('delivery_payment', function (Blueprint $table) {
            $table->dropForeign(['payment_id']);
        });

        Schema::drop('payment');
    }
}