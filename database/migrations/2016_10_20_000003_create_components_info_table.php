<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components_info', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->integer('components_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('name', 60);
            $table->text('description')->nullable()->default(NULL);

            $table->foreign('components_id')->references('id')->on('components')->onDelete('cascade')->onUpdate('cascade');
        });

//        Schema::table('components_info', function($table) {
//            $table->foreign('component_id')->references('id')->onDelete('cascade')->onUpdate('cascade')->on('components');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('components_info', function (Blueprint $table) {
            $table->dropForeign(['components_id']);
        });

        Schema::drop('components_info');
    }
}