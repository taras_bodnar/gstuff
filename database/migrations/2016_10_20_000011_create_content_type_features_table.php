<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypeFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_type_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('features_id')->unsigned();
            $table->integer('content_type_id')->unsigned();
            $table->tinyInteger('sort');

            $table->foreign('content_type_id')->references('id')->on('content_type')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_type_features', function (Blueprint $table) {
            $table->dropForeign(['content_type_id']);
        });

        Schema::drop('content_type_features');
    }
}