<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePluginsStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plugins_structure', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('components_id')->unsigned();
            $table->integer('plugins_id')->unsigned();

            $table->foreign('components_id')->references('id')->on('components')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('plugins_id')->references('id')->on('plugins')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plugins_structure', function (Blueprint $table) {
            $table->dropForeign(['components_id']);
            $table->dropForeign(['plugins_id']);
        });

        Schema::drop('plugins_structure');
    }
}