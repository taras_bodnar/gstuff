<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 45);
            $table->date('expire')->nullable()->default(NULL);
            $table->decimal('discount', 10, 2)->nullable()->default(NULL);
            $table->enum('type', ['abs','per'])->nullable()->default('abs');
            $table->decimal('minp', 10, 2)->nullable()->default(NULL);
            $table->tinyInteger('single')->nullable()->default('0');
            $table->integer('usages')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promo_codes');
    }
}