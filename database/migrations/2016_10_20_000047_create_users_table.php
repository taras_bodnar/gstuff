<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_group_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('sessid', '255')->nullable()->default(NULL);
            $table->string('address', 100)->nullable()->default(NULL);
            $table->string('name', 60);
            $table->string('phone', 20)->nullable()->default(NULL);
            $table->string('email', 60);
            $table->string('password', 255)->nullable()->default(NULL);
            $table->string('skey', 35)->nullable()->default(NULL);
            $table->dateTime('createdon')->nullable()->default(NULL);
            $table->dateTime('editetedon')->nullable()->default(NULL);

            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('content', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
        });

        Schema::table('features', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
        });


        Schema::table('content', function (Blueprint $table) {
            $table->dropForeign(['owner_id']);
        });

        Schema::table('features', function (Blueprint $table) {
            $table->dropForeign(['owner_id']);
        });

        Schema::drop('users');
    }
}