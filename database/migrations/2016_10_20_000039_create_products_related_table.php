<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsRelatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_related', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id')->unsigned();
            $table->integer('related_id')->unsigned();
            $table->tinyInteger('sort');

            $table->foreign('products_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('related_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_related', function (Blueprint $table) {
            $table->dropForeign(['products_id']);
            $table->dropForeign(['related_id']);
        });

        Schema::drop('products_related');
    }
}