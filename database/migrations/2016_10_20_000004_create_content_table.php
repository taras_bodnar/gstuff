
<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('content')) {
            Schema::create('content', function (Blueprint $table) {
                $table->engine = "InnoDB";
                $table->increments('id');
                $table->integer('parent_id')->nullable()->default(0)->unsigned();
                $table->tinyInteger('isfolder')->default(0);
                $table->integer('type_id')->unsigned();
                $table->integer('owner_id')->unsigned();
                $table->integer('templates_id')->unsigned();
                $table->tinyInteger('published')->nullable()->default(NULL);
                $table->string('module', 60)->nullable()->default(NULL);
                $table->smallInteger('sort')->nullable()->default(NULL);
                $table->dateTime('createdon')->nullable()->default(NULL);
                $table->dateTime('editedon')->nullable()->default(NULL);
                $table->tinyInteger('auto')->default('1');
            });

        }

        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign(['content_id']);
        });

        Schema::drop('content');
    }
}