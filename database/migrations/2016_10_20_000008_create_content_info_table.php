<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('name', 255);
            $table->string('alias', 255)->nullable();
            $table->string('title', 255)->nullable()->default(NULL);
            $table->string('keywords', 255)->nullable()->default(NULL);
            $table->string('description', 255)->nullable()->default(NULL);
            $table->longText('content')->nullable()->default(NULL);

            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_info', function (Blueprint $table) {
            $table->dropForeign(['content_id']);
        });

        Schema::drop('content_info');
    }
}