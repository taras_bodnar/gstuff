<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('translations_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->text('value');

            $table->foreign('languages_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('translations_id')->references('id')->on('translations')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translations_info', function (Blueprint $table) {
            $table->dropForeign(['languages_id']);
            $table->dropForeign(['translations_id']);
        });

        Schema::drop('translations_info');
    }
}