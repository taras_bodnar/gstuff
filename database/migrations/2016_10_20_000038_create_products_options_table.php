<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id')->unsigned();
            $table->integer('manufacturers_id')->unsigned();
            $table->string('name', 255)->nullable()->default(NULL);
            $table->string('code', 45)->nullable()->default(NULL);
            $table->tinyInteger('availability');
            $table->tinyInteger('quantity');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2);
            $table->tinyInteger('sale');
            $table->tinyInteger('hit');
            $table->tinyInteger('new');
            $table->tinyInteger('sort')->default('0');
            $table->tinyInteger('is_variant')->default('0');

            $table->foreign('products_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_options', function (Blueprint $table) {
            $table->dropForeign(['products_id']);
        });

        Schema::drop('products_options');
    }
}