<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->default('0');
            $table->integer('content_id')->unsigned();
            $table->string('pib', 60);
            $table->string('email', 60);
            $table->text('message')->nullable()->default(NULL);
            $table->tinyInteger('published')->default('0');
            $table->dateTime('createdon')->nullable()->default(NULL);
            $table->char('ip', 15)->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}