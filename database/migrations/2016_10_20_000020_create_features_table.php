<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['text','checkbox','radiogroup','textarea','checkboxgroup','select','sm'])->default('text');
            $table->tinyInteger('required')->default('0');
            $table->tinyInteger('published')->default('0');
            $table->tinyInteger('show_list')->default('0');
            $table->tinyInteger('show_compare')->default('0');
            $table->tinyInteger('show_filter')->default('0');
            $table->tinyInteger('auto')->default('1');
            $table->integer('owner_id')->unsigned();
            $table->tinyInteger('extends')->default('0');
        });

        Schema::table('content_features', function (Blueprint $table) {
            $table->foreign('features_id')->references('id')->on('features')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('content_type_features', function (Blueprint $table) {
            $table->foreign('features_id')->references('id')->on('features')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('content_features', function (Blueprint $table) {
            $table->dropForeign(['features_id']);
        });

        Schema::table('content_type_features', function (Blueprint $table) {
            $table->dropForeign(['features_id']);
        });

        Schema::drop('features');
    }
}