<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('features_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('name', 255);

            $table->foreign('features_id')->references('id')->on('features')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('features_info', function (Blueprint $table) {
            $table->dropForeign(['features_id']);
        });

        Schema::drop('features_info');
    }
}