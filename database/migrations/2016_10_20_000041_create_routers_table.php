<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url_from', 255);
            $table->string('url_to', 255);
            $table->enum('header', ['HTTP/1.1 301 Moved Permanently','HTTP/1.1 307 Temporary Redirect','HTTP/1.1 302 Moved Temporarily'])->default('HTTP/1.1 301 Moved Permanently');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routers');
    }
}