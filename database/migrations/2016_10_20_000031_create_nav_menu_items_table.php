<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nav_menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nav_menu_id')->unsigned();
            $table->integer('content_id')->unsigned();
            $table->tinyInteger('sort');

            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('nav_menu_id')->references('id')->on('nav_menu')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nav_menu_items', function (Blueprint $table) {
            $table->dropForeign(['content_id']);
            $table->dropForeign(['nav_menu_id']);
        });

        Schema::drop('nav_menu_items');
    }
}