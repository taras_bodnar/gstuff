<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuidesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guides_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guides_id')->unsigned();
            $table->integer('languages_id')->unsigned();
            $table->string('name', 255);

            $table->foreign('guides_id')->references('id')->on('guides')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guides_info', function (Blueprint $table) {
            $table->dropForeign(['guides_id']);
        });

        Schema::drop('guides_info');
    }
}